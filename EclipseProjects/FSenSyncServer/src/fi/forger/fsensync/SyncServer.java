/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import com.illposed.osc.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class SyncServer extends SafeAbstractTableModel implements Runnable {

	private static final long serialVersionUID = 8361613631010060338L;
	public static final int serverVersion = 129;
	
	public ArrayList<AppInformation> clients;
	private String myIp;
	private int myId;
	public ServerGui gui;
	private String messages;
	public static String newline = System.getProperty("line.separator");
	private final String[] columnNames = {"selection", "ip", "Sync +- ms", "Mean ping", "Dropped %", "Battery level %", "App ID", 
			"App state", "Health", "Last contact",
			"Device ID", "Free space (MB)", "Type", "Tag 1", "Tag 2"};
	
	final String[] appStates = {"?", "?", "?", "?", "?", "Initial sync", "Waiting for sensors", "Ready", "Recording", 
			"Preparing play", "Playing", "File transfer", "Post sync"};
	
	final String[] sensorHealthTexts = {"<font color='black'>I</font>", "<font color='red'>B</font>", "<font color='green'>G</font>"};
	
	private FileManager fileManager;
	private TagManager tagManager;
	private NoteManager noteManager;
	public UploadsData uploadsData;
	public StreamingData streamingData;
	public ScriptManager scriptManager;

	private PrintWriter logFileWriter;
	
	private String experimentPath;
	private int nextShortId = 1;
	private static String broadcastAddress = "192.168.0.255"; // Is determined programmatically.
	
	public int serverState;
	public final String[] serverStates = {"Starting", "Ready", "Starting recordings", "Stopping recordings",
			"Downloading", "Network error", "Refreshing", "Calculating sync", "Starting stimuli", "Cancelling stimuli", "Uploading",
			"Starting streams", "Stopping streams"};
	public static final int STATE_STARTING = 0;
	public static final int STATE_READY = 1;
	public static final int STATE_STARTING_RECORDINGS = 2;
	public static final int STATE_STOPPING_RECORDINGS = 3;
	public static final int STATE_DOWNLOADING = 4;
	public static final int STATE_NETWORK_ERROR = 5;
	public static final int STATE_REFRESHING = 6;
	public static final int STATE_CALCULATING_SYNC = 7;
	public static final int STATE_STARTING_STIMULI = 8;
	public static final int STATE_CANCELLING_STIMULI = 9;
	public static final int STATE_UPLOADING = 10;
	public static final int STATE_STARTING_STREAMING = 11;
	public static final int STATE_STOPPING_STREAMING = 12;
	
	public static final int DEFAULT_CLIENT_UNICAST_PORT = 11101;
	public static final int DEFAULT_CLIENT_TCP_PORT = 11201;
	
	private int timeReplyErrorCount = 0;
	private int malformedErrorCount = 0;
	private int reportErrorCount = 0;
	private int clockOffsetErrors = 0;
	
	private int totalNetworkErrorCount = 0;
	private int totalNetworkSuccessCount = 0;
	
	private static long startUpTimeMillis;
	private static long startUpTimeNanos;
	private static long offsetBetweenReportedTimes;
	
	private static long streamingZeroTime = 0;
	
	private Boolean firstTimeIsSet;
	
	private String lastTaskResult = "";
	
	public final int minClientVersionNumber = 12;
	public final int maxClientVersionNumber = 12;
	
	public int getServerState() {
		return serverState;
	}
	
	public TagManager getTagManager() {
		return tagManager;
	}
	
	public FileManager getFileManager() {
		return fileManager;
	}
	
	public NoteManager getNoteManager() {
		return noteManager;
	}
	
	public ScriptManager getScriptManager() {
		return scriptManager;
	}

	public int getCurrentRecordingNumber() {
		return fileManager.getCurrentRecordingNumber();
	}

	public String getExperimentPath() {
		return experimentPath;
	}
	
	public String getExperimentName()
	{
		File f = new File(experimentPath);
		return f.getName();
	}
	
	public static long getCurrentTimeMillis()
	{
		return ((System.nanoTime() - startUpTimeNanos) / 1000000L) + startUpTimeMillis;
	}
	
	public static long getCurrentTimeMicros()
	{
		return ((System.nanoTime() - startUpTimeNanos) / 1000L) + (startUpTimeMillis * 1000L);
	}
	
	private void resetServerTime()
	{
		myId = new Random(System.currentTimeMillis()).nextInt();
		startUpTimeNanos = System.nanoTime();
		startUpTimeMillis = System.currentTimeMillis();
		
		offsetBetweenReportedTimes = 0;
		
		if (firstTimeIsSet)
		{
			sendClockReset();
			
		}
		else
		{
			streamingZeroTime = getCurrentTimeMillis();
		}
		firstTimeIsSet = true;
	}
	
	public SyncServer(String path)
	{
		firstTimeIsSet = false;
		messages = "";
		experimentPath = path;
		
		resetServerTime();
		
		File logFile = new File(getExperimentPath() + File.separator + "debugLog.txt");
		try {
			logFileWriter = new PrintWriter(new FileOutputStream(logFile, true));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		log("Started server, version: " + serverVersion);
		
		loadAppInformation();
		fileManager = new FileManager(this);
		tagManager = new TagManager(this);
		noteManager = new NoteManager(this);
		uploadsData = new UploadsData(this);
		streamingData = new StreamingData(this);
		scriptManager = new ScriptManager(this);
		setState(STATE_STARTING);

		updateMyIp();
		
		if (myIp == null)
		{
			updateGuiTaskResult(", network problem.");
			log("Error: Cannot determine own ip address.", true);
			myIp = "127.0.0.1";
		}

		// Broadcast receivers (ip address request from clients)
		try 
		{
			OSCPortIn receiver = new OSCPortIn(11200, true); // Broadcast receive
			OSCListener listener = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 9)
					{
						if (a.size() == 1)
						{
							// This is the installer app
							String addr = (String)a.get(0);
							InetAddress address;
							try {
								address = InetAddress.getByName(addr);
								OSCPortOut sender = new OSCPortOut(address, 11198);
								ArrayList<Object> outObjects = new ArrayList<Object>(1);
								outObjects.add(myIp);
								OSCMessage msg = new OSCMessage("/serveraddress", outObjects);
								sender.send(msg);
								sender.close();
							} catch (Exception ex) {
								log(ex, true);
							}
							return;
						}
						if (malformedErrorCount < 6)
						{
							malformedErrorCount = malformedErrorCount + 1;
							log("Received an old or a malformed handshake message from an app.", true);
							try
							{
								if (a.size() > 0 && String.class.isInstance(a.get(0)))
								{
									String addr = (String)a.get(0);
									sendVersionErrorMessage(addr, "");
								}
							}
							catch (Exception e)
							{
								logSilentNetworkError();
							}
						}
						else
						{
							logSilentNetworkError();
						}
						return;
					}
					String addr = (String)a.get(0);
					String clientUniqueId = (String)a.get(1);
					String clientType = (String)a.get(2);
					int clientVersionNumber = (int)a.get(3);
					String deviceModel = (String)a.get(4);
					int clientPortOffset = (int)a.get(5);

					if (clientVersionNumber >= minClientVersionNumber && clientVersionNumber <= maxClientVersionNumber)
					{
						String deviceId = (String)a.get(6);
						String longDeviceId = (String)a.get(7);
						Boolean needsToGiveAppTags = (Boolean)a.get(8);
						
						AppInformation dev = findAppByUniqueId(clientUniqueId);

						closeOtherAppsAtSameIpAndPort(addr, clientPortOffset, dev);
						
						if (dev == null)
						{
							dev = new AppInformation(addr, nextShortId, clientUniqueId, clientType,
									deviceModel, clientPortOffset, deviceId, longDeviceId, needsToGiveAppTags);
							clients.add(dev);
							nextShortId += 1;
							saveAppInformation();
							fireTableRowsInserted(clients.size()-1, clients.size()-1);
							tagManager.assignTags();
							log("Added client: " + clientType + " " + dev.shortId + " at " + addr);
						}
						else
						{
							dev.ip = addr;
							dev.setPortOffset(clientPortOffset);
							dev.deviceId = deviceId;
							sendTags(dev);
							log("Existing client: " + clientType + " " + dev.shortId + " at " + addr);
						}

						dev.noteUpdate();

						// Reply with the server ip address
						InetAddress address;
						try {
							address = InetAddress.getByName(addr);
							OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
							ArrayList<Object> outObjects = new ArrayList<Object>(1);
							outObjects.add(myIp);
							OSCMessage msg = new OSCMessage("/serveraddress", outObjects);
							sender.send(msg);
						} catch (Exception ex) {
							log(ex, true);
						}
					}
					else
					{
						sendVersionErrorMessage(addr, clientType, clientPortOffset);
					}
				}
			};
			receiver.addListener("/giveserverip", listener);
			receiver.startListening();
		}
		catch (Exception ex)
		{
			log("Listening to port failed");
		}
		
		// Broadcast receiver (ip addresses from other servers)
        try
        {
            OSCPortIn receiver = new OSCPortIn(11300, true); // Broadcast receive

            OSCListener listener = new OSCListener()
            {
                public void acceptMessage(java.util.Date time, OSCMessage message)
                {
                	List<Object> a = message.getArguments();
                	if (a.size() != 1)
					{
						log("Received malformed message. (002)");
						return;
					}
                	int id = (int)a.get(0);
                    if (id != myId)
                    {
                    	updateGuiTaskResult(", Warning: Detected another server in the network.");
                    	log("Warning: Detected another server in the network.");
                    }
                }
            };
            receiver.addListener("/updateip", listener);
            receiver.startListening();
        }
        catch (Exception ex)
        {
            System.out.println("Listening to broadcast port failed.");
        }

		// Unicast receivers
		try 
		{
			OSCPortIn receiver = new OSCPortIn(11100); // Regular receive
			OSCListener listenerCurrentTime = new OSCListener() {
				public void acceptMessage(java.util.Date time, OSCMessage message)
				{
					List<Object> a = message.getArguments();
					if (a.size() != 4)
					{
						log("Received malformed message. (003)");
						return;
					}
					long tabletTime = (long)a.get(0);
					String tabletIp = (String)a.get(1);
					long serverTime = SyncServer.getCurrentTimeMicros();
					String uniqueId = (String)a.get(2);
					int port = (int)a.get(3) + DEFAULT_CLIENT_UNICAST_PORT;
					
					// The reply must be sent as fast as possible
					try {
						InetAddress address = InetAddress.getByName(tabletIp);
						OSCPortOut sender = new OSCPortOut(address, port);
						ArrayList<Object> replyObject = new ArrayList<Object>();
						replyObject.add(tabletTime);
						replyObject.add(serverTime);
						replyObject.add(myId);
						replyObject.add(uniqueId);
						OSCMessage msg = new OSCMessage("/replycurrenttime", replyObject);
						sender.send(msg);
						sender.close();
					} 
					catch (Exception ex) 
					{
						if (timeReplyErrorCount < 2)
						{
							timeReplyErrorCount = timeReplyErrorCount + 1;
							if (ex.getMessage() != null)
							{
								log("Error in time reply to: " + tabletIp + ", " + ex.getMessage());
							}
							else
							{
								log("Error in time reply to: " + tabletIp);
							}
						}
						else
						{
							logSilentNetworkError();
						}
					}
					
					AppInformation dev = findAppByUniqueId(uniqueId);
					if (dev != null)
					{
						dev.noteUpdate();
						if (dev.ip.length() == 0)
						{
							dev.ip = tabletIp;
						}
						if (!dev.samePortOffset((int)a.get(3)))
						{
							dev.setPortOffset((int)a.get(3));
						}
					}
				}
			};
			OSCListener listenerReports = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 11 && a.size() != 12)
					{
						log("Received malformed message. (004)");
						return;
					}
					String ip = (String)a.get(0);
					double battery = (double)a.get(1);
					double syncAccuracy = (double)a.get(2);
					int appState = (int)a.get(3);
					long freeSpace = (long)a.get(4);
					int recordingNumber = (int)a.get(5);
					Boolean isOnBackground = (Boolean)a.get(6);
					Boolean recordingForOtherServer = (Boolean)a.get(7);
					String uniqueId = (String)a.get(8);
					Double meanPing = (Double)a.get(9);
					int droppedPackagePercentage = (int)a.get(10);
					AppInformation dev = findAppByUniqueId(uniqueId);
					boolean isStreaming = false;
					if (a.size() == 12)
					{
						isStreaming = (boolean)a.get(11);
					}
					if (dev != null)
					{
						dev.ip = ip;
						dev.lastSyncAccuracy = syncAccuracy;
						dev.batteryLevel = (int)battery;
						dev.appState = appState;
						dev.freeSpace = freeSpace;
						dev.recordingNumber = recordingNumber;
						dev.isOnBackground = isOnBackground;
						dev.recordingForOtherServer = recordingForOtherServer;
						dev.isStreaming = isStreaming;
						if (meanPing > 0.0)
						{
							dev.meanPing = meanPing;
						}
						else
						{
							dev.meanPing = Double.NaN;
						}
						dev.droppedPackagePercentage = droppedPackagePercentage;
						dev.noteUpdate();
					}
				}
			};
			OSCListener listenerSensorHealth = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() < 2)
					{
						log("Received malformed message. (014)");
						return;
					}
					String uniqueId = (String)a.get(0);
					int sensorCount = (int)a.get(1);
					int[] health = new int[sensorCount];
					
					for (int i = 0; i < sensorCount; i++)
					{
						if (1+ i < a.size())
						{
							health[i] = (int)a.get(2+i);
						}
					}

					AppInformation dev = findAppByUniqueId(uniqueId);
					if (dev != null)
					{
						dev.sensorHealth = health;
						dev.noteHealthUpdate();
						dev.noteUpdate();
					}
				}
			};
			OSCListener listenerGiveTags = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 1)
					{
						log("Received malformed message. (011)");
						return;
					}
					String uniqueId = (String)a.get(0);

					AppInformation app = findAppByUniqueId(uniqueId);
					
					if (app != null)
					{
						taskSendTags(app);
					}
				}
			};
			OSCListener listenerSetAppTags = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 4)
					{
						log("Received malformed message. (010)");
						return;
					}
					String uniqueId = (String)a.get(0);
					String appTag1 = (String)a.get(1);
					String appTag2 = (String)a.get(2);
					String appTag3 = (String)a.get(3);
					
					AppInformation app = findAppByUniqueId(uniqueId);
					
					if (app != null)
					{
						tagManager.updateTagFromClient(app, appTag1, appTag2, appTag3);
					}
				}
			};
			OSCListener listenerClientTimeReport = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 6)
					{
						log("Received malformed message. (013)");
						return;
					}
					String uniqueId = (String)a.get(0);
					int serverIdReported = (int)a.get(1);
					int serverIdOriginalTime = (int)a.get(2);
					long serverTime1Nanos = (long)a.get(3);
					long appTimeNanos = (long)a.get(4);
					long appUncertaintyNanos = (long)a.get(5);
					long serverTime2Nanos = System.nanoTime();
					
					AppInformation app = findAppByUniqueId(uniqueId);
					
					if (app != null)
					{
						// return ((System.nanoTime() - startUpTimeNanos) / 1000000L) + startUpTimeMillis;
						long suggestedTime = ((System.nanoTime() - ((serverTime2Nanos + serverTime1Nanos)/2)) / 1000000L) + (appTimeNanos / 1000000L);
						long myTime = getCurrentTimeMillis();
						System.out.println(serverIdReported + " " + serverIdOriginalTime + 
								" " + (myTime-suggestedTime) + 
								" +-" + ((serverTime2Nanos - serverTime1Nanos) + appUncertaintyNanos)/ 1000000.0);
					}
				}
			};
			OSCListener listenerClientError = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 3)
					{
						log("Received malformed message. (006)");
						return;
					}
					String ip = (String)a.get(0);
					//String uniqueId = (String)a.get(1);
					String errorMessage = (String)a.get(2);
					log("Client error from " + ip + ": " + errorMessage);
				}
			};
			OSCListener listenerClientClosing = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 2)
					{
						log("Received malformed message. (007)");
						return;
					}
					String ip = (String)a.get(0);
					String uniqueId = (String)a.get(1);
					AppInformation dev = findAppByUniqueId(uniqueId);
					if (dev != null && dev.appState != AppInformation.APP_CLOSED)
					{
						dev.markAppClosed();
						log("Client at ip " + ip + " closed. ");
					}
				}
			};
			OSCListener listenerSettingReport = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 3)
					{
						log("Received malformed message. (008)");
						return;
					}
					String uniqueId = (String)a.get(0);
					AppInformation dev = findAppByUniqueId(uniqueId);
					if (dev != null)
					{
						dev.addSetting((String)a.get(1), (String)a.get(2));
						dev.noteUpdate();
					}
				}
			};
			OSCListener listenerFileReport = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 2)
					{
						log("Received malformed message. (009)");
						return;
					}
					String uniqueId = (String)a.get(0);
					String fileName = (String)a.get(1);
					AppInformation dev = findAppByUniqueId(uniqueId);
					if (dev != null)
					{
						fileManager.addToMasterFileList(fileName, uniqueId);
						dev.noteUpdate();
					}
					
				}
			};
			OSCListener listenerInstallerConnection = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					List<Object> a = message.getArguments();
					if (a.size() != 1)
					{
						log("Received malformed message. (012)");
						return;
					}
					String ip = (String)a.get(0);
					fileManager.installerConnection(ip);
				}
			};
			OSCListener listenerGeneric = new OSCListener()
			{
				public void acceptMessage(java.util.Date time, OSCMessage message) {
					logSilentNetworkSuccess();
				}
			};
			receiver.addListener("/clienttimereply", listenerClientTimeReport);
			receiver.addListener("/givecurrenttime", listenerCurrentTime);
			receiver.addListener("/setapptags", listenerSetAppTags);
			receiver.addListener("/givetags", listenerGiveTags);
			receiver.addListener("/clientreport", listenerReports);
			receiver.addListener("/sensorhealth", listenerSensorHealth);
			receiver.addListener("/errormessage", listenerClientError);
			receiver.addListener("/closingapp", listenerClientClosing);
			receiver.addListener("/appsettingreport", listenerSettingReport);
			receiver.addListener("/filereport", listenerFileReport);
			receiver.addListener("/installerconnection", listenerInstallerConnection);
			receiver.addListener("/*", listenerGeneric);
			receiver.startListening();
		}
		catch (Exception ex)
		{
			setState(STATE_NETWORK_ERROR);
			log("Listening to port failed. Is another server running at the same time?", true);
		}
		
		askIpUpdates();
	}
	
	private boolean updateMyIp() {
		boolean foundNewIp = false;
		String foundIp = null;
		String bAdress = null;
		try {
			Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
			while(en.hasMoreElements()){
				NetworkInterface ni=(NetworkInterface) en.nextElement();
				Enumeration<InetAddress> ee = ni.getInetAddresses();
				while(ee.hasMoreElements()) {
					InetAddress ia= (InetAddress) ee.nextElement();
					//log(ia.getHostAddress());
					if (ia.isSiteLocalAddress() && ia instanceof Inet4Address)
					{
						foundIp = ia.getHostAddress();
					}
				}
				if(!ni.isLoopback() && ni.isUp()) 
				{
					Iterator<InterfaceAddress> eBr = ni.getInterfaceAddresses().iterator();
					while(eBr.hasNext()) {
						InterfaceAddress ia = (InterfaceAddress) eBr.next();
						if (ia != null && ia.getBroadcast() != null)
						{
							bAdress = ia.getBroadcast().toString();
						}
					}
				}
			}
		} 
		catch (SocketException e) 
		{
			log(e, true);
		}

		if (foundIp != null)
		{
			if (myIp == null || !myIp.equals(foundIp))
			{
				foundNewIp = true;
				myIp = foundIp;
				broadcastAddress = bAdress.replaceAll("/", "");
				if (myIp == null)
				{
					log("Broadcast: " + broadcastAddress);
				}
				log("Updated own ip: " + foundIp);
				
				if (serverState == STATE_READY)
				{
					updateGuiTaskResult(", ip updated.");
				}
			}
		}
		
		return foundNewIp;
	}

	public void updateGuiTaskResult(String taskResult)
	{
		lastTaskResult = taskResult;
		if (gui != null)
		{
			gui.setState(serverStates[serverState] + taskResult);
		}
	}
	
	public void setState(int state)
	{
		if (gui != null)
		{
			gui.setState(serverStates[state] + lastTaskResult);
		}
		serverState = state;
		
		if (gui != null && state != STATE_READY)
		{
			gui.disableButtons();
		}
		if (gui != null && state == STATE_READY)
		{
			gui.enableButtons();
		}
	}
	
	private void sendVersionErrorMessage(String ip, String type, int portOffset)
	{
		InetAddress address;
		try {
			address = InetAddress.getByName(ip);
			OSCPortOut sender = new OSCPortOut(address, portOffset + DEFAULT_CLIENT_UNICAST_PORT);
			OSCMessage msg = new OSCMessage("/versionerror");
			sender.send(msg);
			sender.close();
			log("Client " + type + " from " + ip + " tried to connect with wrong version number.");
		} catch (Exception ex) {
			log(ex);
		}
	}
	
	private void sendVersionErrorMessage(String ip, String type) 
	{
		InetAddress address;
		try {
			address = InetAddress.getByName(ip);
			OSCPortOut sender = new OSCPortOut(address, 11100);
			OSCMessage msg = new OSCMessage("/versionerror");
			sender.send(msg);
			sender.close();
			log("Client " + type + " from " + ip + " tried to connect with wrong version number.");
		} catch (Exception ex) {
			log(ex);
		}
	}
	
	public AppInformation findAppByUniqueId(String uniqueId)
	{
		int index = -1;
		for (int i = 0; i < clients.size(); i++)
		{
			if (clients.get(i).uniqueId.equals(uniqueId))
			{
				index = i;
			}
		}
		if (index > -1)
		{
			return clients.get(index);
		}
		else
		{
			return null;
		}
	}
	
	public ArrayList<AppInformation> findAppsByLongDeviceId(String longDeviceId)
	{
		ArrayList<AppInformation> apps = new ArrayList<AppInformation>();
		for (int i = 0; i < clients.size(); i++)
		{
			if (clients.get(i).longDeviceId.equals(longDeviceId))
			{
				apps.add(clients.get(i));
			}
		}
		return apps;
	}
	
	private void closeOtherAppsAtSameIpAndPort(String ip, int portOffset, AppInformation currentDev)
	{
		for (int i = 0; i < clients.size(); i++)
		{
			AppInformation dev = clients.get(i);
			if (dev != currentDev && dev.ip.equals(ip) && dev.samePortOffset(portOffset))
			{
				dev.markAppAssumedClosed();
			}
		}
	}
	
	private void askIpUpdates()
	{
		try {
			InetAddress address = InetAddress.getByName(broadcastAddress);
			for (int i = 0; i < 10; i++)
			{
				OSCPortOut sender = new OSCPortOut(address, 11300 + i);
				ArrayList<Object> replyObject = new ArrayList<Object>(1);
				replyObject.add(myId);
				OSCMessage msg = new OSCMessage("/updateip", replyObject);
				sender.send(msg);
				sender.close();
			}
		} 
		catch (Exception ex) 
		{
			log("Error IP update request", true);
			log(ex);
		}
	}
	
	private int highestReportedRecordingNumber()
	{
		int number = -1;
		for (int i = 0; i < clients.size(); i++)
		{
			AppInformation dev = clients.get(i);
			if (dev.appState == AppInformation.APP_RECORDING
					|| dev.appState == AppInformation.APP_PREPARING_PLAY
					|| dev.appState == AppInformation.APP_PLAYING)
			{
				number = Math.max(number, clients.get(i).recordingNumber);
			}
			else
			{
				number = Math.max(number, clients.get(i).recordingNumber + 1);
			}
		}
		return number;
	}
	
	public void taskStartRecording()
	{
		log("Task: Start recording");
		Runnable runner = new Runnable()
		{
			public void run()
			{
				int startedCount = 0;
				int appCount = 0;
				fileManager.setRecordingNumberSentToApps(highestReportedRecordingNumber());
				noteManager.addNote("Started recording " + fileManager.getCurrentRecordingNumber());
				saveNotes();
				tagManager.createTagBackup(getCurrentRecordingNumber());
				noteManager.createNoteBackup(getCurrentRecordingNumber());
				
				for (int tries = 0; tries < 5; tries++)
				{
					for (int i = 0; i < clients.size(); i++)
					{
						try {
							AppInformation dev = clients.get(i);
							if (dev.isContactedAndSelected() && (tries == 1 || dev.appState != AppInformation.APP_RECORDING))
							{
								InetAddress address = InetAddress.getByName(dev.ip);
								OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
								ArrayList<Object> messages = new ArrayList<Object>(5);
								messages.add(getExperimentName());
								messages.add(fileManager.getCurrentRecordingNumber());
								messages.add(dev.shortId);
								messages.add(myId);
								messages.add(dev.uniqueId);
								OSCMessage msg = new OSCMessage("/startrecording", messages);
								sender.send(msg);
								sender.close();
							}
						} 
						catch (Exception ex) 
						{
							log("Error sending 'Start recording'", true);
							log(ex);
						}
					}
					try 
					{
						Thread.sleep(200);
					} catch (InterruptedException e) {
						log(e);
					}
					startedCount = 0;
					appCount = 0;
					for (int i = 0; i < clients.size(); i++)
					{
						if (clients.get(i).isContactedAndSelected())
						{
							appCount += 1;
							if (clients.get(i).appState == AppInformation.APP_RECORDING)
							{
								startedCount += 1;
							}
						}
					}
					updateGuiTaskResult(" " + startedCount + " / " + appCount);
				}
				updateGuiTaskResult(", started " + startedCount + " / " + appCount);
				setState(STATE_READY);
			}
		};
		
		if (serverState == STATE_READY)
		{
			setState(STATE_STARTING_RECORDINGS);
			updateGuiTaskResult("");
			Thread thread = new Thread(runner);
			thread.start();
		}
	}
	
	public void taskStopRecording()
	{
		log("Task: Stop recording");
		Runnable runner = new Runnable()
		{
			public void run()
			{
				int stoppedCount = 0;
				int appCount = 0;
				for (int tries = 0; tries < 5; tries++)
				{
					for (int i = 0; i < clients.size(); i++)
					{
						try {
							if (clients.get(i).isContactedAndSelected() 
									&& (tries == 1 || 
									(clients.get(i).appState != AppInformation.APP_POST_SYNC ||clients.get(i).appState != AppInformation.APP_READY)))
							{
								InetAddress address = InetAddress.getByName(clients.get(i).ip);
								OSCPortOut sender = new OSCPortOut(address, clients.get(i).getUniPort());
								ArrayList<Object> replyObject = new ArrayList<Object>(1);
								replyObject.add(clients.get(i).uniqueId);
								OSCMessage msg = new OSCMessage("/stoprecording", replyObject);
								sender.send(msg);
								sender.close();
							}
						} 
						catch (Exception ex) 
						{
							log("Error sending 'Stop recording'", true);
							log(ex);
						}
					}
					try 
					{
						Thread.sleep(200);
					} catch (InterruptedException e) {
						log(e);
					}
					stoppedCount = 0;
					appCount = 0;
					for (int i = 0; i < clients.size(); i++)
					{
						if (clients.get(i).isContactedAndSelected())
						{
							appCount += 1;
							if (clients.get(i).appState == AppInformation.APP_READY
									|| clients.get(i).appState == AppInformation.APP_POST_SYNC)
							{
								stoppedCount += 1;
							}
						}
					}
					updateGuiTaskResult(" " + stoppedCount + " / " + appCount);
				}
				
				updateGuiTaskResult(", stopped " + stoppedCount + " / " + appCount);
				
				setState(STATE_READY);
			}
		};
		if (serverState == STATE_READY)
		{
			setState(STATE_STOPPING_RECORDINGS);
			updateGuiTaskResult("");
			Thread thread = new Thread(runner);
			thread.start();
			noteManager.addNote("Stopped recording " + fileManager.getCurrentRecordingNumber());
			saveNotes();
			fileManager.nextRecordingNumber();
		}
	}
	
	public void taskStartStreaming(final String multicastIp, final int multicastPort, final int maxFps)
	{
		log("Task: Start streams");
		Runnable runner = new Runnable()
		{
			public void run()
			{
				int startedCount = 0;
				int appCount = 0;
				
				for (int tries = 0; tries < 5; tries++)
				{
					for (int i = 0; i < clients.size(); i++)
					{
						try {
							AppInformation dev = clients.get(i);
							if (dev.isContactedAndSelected() && StreamingData.isStreamingClient(dev) && (tries == 1 || !dev.isStreaming))
							{
								InetAddress address = InetAddress.getByName(dev.ip);
								OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
								ArrayList<Object> messages = new ArrayList<Object>(7);
								messages.add(dev.uniqueId);
								messages.add(myId);
								messages.add(multicastIp);
								messages.add(multicastPort);
								messages.add(maxFps);
								messages.add(streamingZeroTime);
								int knownLag = fileManager.knowLagForDeviceModel(dev.deviceModel, dev.clientType);
								messages.add(knownLag);
								
								OSCMessage msg = new OSCMessage("/startstreaming", messages);
								sender.send(msg);
								sender.close();
							}
						} 
						catch (Exception ex) 
						{
							log("Error sending 'Start streaming'", true);
							log(ex);
						}
					}
					try 
					{
						Thread.sleep(200);
					} catch (InterruptedException e) {
						log(e);
					}
					startedCount = 0;
					appCount = 0;

					for (int i = 0; i < clients.size(); i++)
					{
						if (clients.get(i).isContactedAndSelected() && StreamingData.isStreamingClient(clients.get(i)))
						{
							appCount += 1;
							if (clients.get(i).isStreaming)
							{
								startedCount += 1;
							}
						}
					}
					updateGuiTaskResult(" " + startedCount + " / " + appCount);
				}
				updateGuiTaskResult(", started streams " + startedCount + " / " + appCount);
				setState(STATE_READY);
			}
		};
		
		if (serverState == STATE_READY)
		{
			setState(STATE_STARTING_RECORDINGS);
			updateGuiTaskResult("");
			Thread thread = new Thread(runner);
			thread.start();
		}
	}
	
	public void taskStopStreaming()
	{
		log("Task: Stop streams");
		Runnable runner = new Runnable()
		{
			public void run()
			{
				int stoppedCount = 0;
				int appCount = 0;
				
				for (int tries = 0; tries < 5; tries++)
				{
					for (int i = 0; i < clients.size(); i++)
					{
						try {
							AppInformation dev = clients.get(i);
							if (dev.isContactedAndSelected() && StreamingData.isStreamingClient(dev) && (tries == 1 || dev.isStreaming))
							{
								InetAddress address = InetAddress.getByName(dev.ip);
								OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
								ArrayList<Object> messages = new ArrayList<Object>(1);
								messages.add(dev.uniqueId);
								OSCMessage msg = new OSCMessage("/stopstreaming", messages);
								sender.send(msg);
								sender.close();
							}
						} 
						catch (Exception ex) 
						{
							log("Error sending 'Start streaming'", true);
							log(ex);
						}
					}
					try 
					{
						Thread.sleep(200);
					} catch (InterruptedException e) {
						log(e);
					}
					stoppedCount = 0;
					appCount = 0;
					
					for (int i = 0; i < clients.size(); i++)
					{
						if (clients.get(i).isContactedAndSelected() && StreamingData.isStreamingClient(clients.get(i)))
						{
							appCount += 1;
							if (!clients.get(i).isStreaming)
							{
								stoppedCount += 1;
							}
						}
					}
					updateGuiTaskResult(" " + stoppedCount + " / " + appCount);
				}
				updateGuiTaskResult(", stopped streams " + stoppedCount + " / " + appCount);
				setState(STATE_READY);
			}
		};
		
		if (serverState == STATE_READY)
		{
			setState(STATE_STOPPING_STREAMING);
			updateGuiTaskResult("");
			Thread thread = new Thread(runner);
			thread.start();
		}
	}
	
	public void taskSendTags(final AppInformation dev)
	{
		Runnable runner = new Runnable()
		{
			public void run()
			{
				for (int i = 0; i < 3; i++)
				{
					sendTags(dev);
					
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						log(e);
					}
				}
			}
		};
		
		Thread thread = new Thread(runner);
		thread.start();
	}
	
	public void sendTags(AppInformation dev)
	{
		if (dev.isContacted())
		{
			try {
				InetAddress address = InetAddress.getByName(dev.ip);
				OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
				ArrayList<Object> replyObject = new ArrayList<Object>(5);
				if (dev.tag != null)
				{
					replyObject.add(dev.tag.tags[0]);
					replyObject.add(dev.tag.tags[1]);
					replyObject.add(dev.tag.tags[2]);
				}
				else
				{
					replyObject.add("");
					replyObject.add("");
					replyObject.add("");
				}
				replyObject.add(dev.uniqueId);
				OSCMessage msg = new OSCMessage("/settags", replyObject);
				sender.send(msg);
				sender.close();
			}
			catch (Exception ex) 
			{
				log("Error in sending tags", true);
				log(ex);
			}
		}
	}
	
	public void taskGetFileLists()
	{
		log("Task: Fetching file information");
		fileManager.taskGetFileLists();
	}
	
	public boolean appsWithSelectedFilesAreRecording()
	{
		boolean areRecording = false;
		for (int i = 0; i < fileManager.downloadList.size(); i++)
		{
			if (fileManager.downloadList.get(i).isSelected && fileManager.downloadList.get(i).app.appState > AppInformation.APP_READY)
			{
				areRecording = true;
			}
		}
		return areRecording;
	}
	
	public void taskDownloadFiles(final Boolean onlySmallFiles, final Boolean onlySelected,  final Boolean alsoStop)
	{
		log("Task: Download files");
		
		Runnable runner = new Runnable()
		{
			public void run()
			{
				if (alsoStop)
				{
					if (onlySelected)
					{
						unselectAllApps();
						
						// Selecting only apps that have currently selected files in them
						for (int i = 0; i < fileManager.downloadList.size(); i++)
						{
							if (fileManager.downloadList.get(i).isSelected)
							{
								fileManager.downloadList.get(i).app.isSelected = true;
							}
						}
						taskStopRecording();
					}
					else
					{
						selectAllApps();
						taskStopRecording();
					}
				}
				int count = 0;
				while (serverState != STATE_READY && count < 200)
				{
					count += 1;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				fileManager.taskDownloadFiles(onlySmallFiles, onlySelected);
			}
		};
		
		Thread thread = new Thread(runner);
		thread.start();
	}
	
	public void setSelectionByCell(int row, int column)
	{
		Object cellContent = getValueAt(row, column);
		if (cellContent != null && !(cellContent instanceof Boolean))
		{
			for (int i = 0; i < getRowCount(); i++)
			{
				if (cellContent.equals(getValueAt(i, column)))
				{
					clients.get(i).isSelected = true;
				}
				else
				{
					clients.get(i).isSelected = false;
				}
			}
		}
		this.fireTableDataChanged();
	}
	
	public void selectAllApps()
	{
		for (int i = 0; i < clients.size(); i++)
		{
			clients.get(i).isSelected = true;
		}
		this.fireTableDataChanged();
	}
	
	public void unselectNonUploadsClients()
	{
		for (int i = 0; i < clients.size(); i++)
		{
			if (UploadsData.isUploadClient(clients.get(i)) == false)
			{
				clients.get(i).isSelected = false;
			}
		}
		this.fireTableDataChanged();
	}
	
	public void unselectNonStreamingClients()
	{
		for (int i = 0; i < clients.size(); i++)
		{
			if (StreamingData.isStreamingClient(clients.get(i)) == false)
			{
				clients.get(i).isSelected = false;
			}
		}
		this.fireTableDataChanged();
	}
	
	public void unselectAllApps()
	{
		for (int i = 0; i < clients.size(); i++)
		{
			clients.get(i).isSelected = false;
		}
		this.fireTableDataChanged();
	}
	
	public Boolean appsAreBusy()
	{
		Boolean appsBusy = false;
		for (int i = 0; i < clients.size(); i++)
		{
			if (clients.get(i).appState == AppInformation.APP_RECORDING
					|| clients.get(i).appState == AppInformation.APP_PLAYING
					|| clients.get(i).appState == AppInformation.APP_POST_SYNC)
			{
				appsBusy = true;
			}
		}
		return appsBusy;
	}
	
	public Boolean appsAreDoingPostSync()
	{
		Boolean appsBusy = false;
		for (int i = 0; i < clients.size(); i++)
		{
			AppInformation cli = clients.get(i);
			if (cli.appState == AppInformation.APP_POST_SYNC
					|| (cli.appState == AppInformation.APP_RECORDING && cli.clientType.equals("MOVE")))
			{
				appsBusy = true;
			}
		}
		return appsBusy;
	}
	
	public Boolean selectedUploadAppsAreBusy()
	{
		Boolean appsBusy = false;
		for (int i = 0; i < clients.size(); i++)
		{
			if (UploadsData.isUploadClient(clients.get(i)) && clients.get(i).isSelected && 
					(clients.get(i).appState == AppInformation.APP_RECORDING
					|| clients.get(i).appState == AppInformation.APP_PLAYING
					|| clients.get(i).appState == AppInformation.APP_POST_SYNC))
			{
				appsBusy = true;
			}
		}
		return appsBusy;
	}

	public void run() 
	{
		int second = 0;
		int maxSeconds = 10;
		int errorCountBuffer[] = new int[maxSeconds];
		int successCountBuffer[] = new int[maxSeconds];
		int ipChangedCounter = 0;
		while (true)
		{
			try {
				Thread.sleep(1000);
			} 
			catch(InterruptedException ex) 
			{
				Thread.currentThread().interrupt();
				log("Interupted.");
			}
			
			second = (second + 1) % maxSeconds;

			{
				long timeServer = getCurrentTimeMillis();
				long wallClockTime = System.currentTimeMillis();
				long timeOffset = timeServer - wallClockTime;
				
				if (Math.abs(timeOffset - offsetBetweenReportedTimes) <= 4)
				{
					// This is done to prevent clock resets on systems where 
					// the wall clock time and nanoTime drift apart smoothly.
					offsetBetweenReportedTimes = timeOffset;
				}
				
				//log("Offset: " + offsetBetweenReportedTimes + " " + timeOffset + " " + timeServer + " " + wallClockTime);
				
				if (Math.abs(timeOffset - offsetBetweenReportedTimes)  > 4L)
				{
					// This is done to prevent  resets due to pauses caused by scheduling.
					timeServer = getCurrentTimeMillis();
					wallClockTime = System.currentTimeMillis();
					timeOffset = Math.abs(timeServer - wallClockTime);
					if (Math.abs(timeOffset - offsetBetweenReportedTimes)  > 4L)
					{
						timeServer = getCurrentTimeMillis();
						wallClockTime = System.currentTimeMillis();
						timeOffset = Math.abs(timeServer - wallClockTime);
					}
				}
				if (Math.abs(timeOffset - offsetBetweenReportedTimes)  > 4L)
				{
					resetServerTime();
					if (appsAreBusy())
					{
						updateGuiTaskResult(", Warning: Server clock has drifted. Stopped syncing recording apps.");
						log("Warning: Server clock has drifted " + timeOffset + "ms from the wall clock time. Stopped syncing recording apps. Resetting server clock. Stopped syncing recording apps.");
					}
					else
					{
						updateGuiTaskResult(", Warning: Server clock has drifted and has been reset.");
						log("Notice: Server clock has drifted " + timeOffset + "ms from the wall clock time. Resetting server clock.");
					}

					clockOffsetErrors += 1;
				}
			}

			if (serverState == STATE_STARTING)
			{
				askIpUpdates();
				setState(STATE_READY);
			}

			if (second == 0)
			{
				timeReplyErrorCount -= 1;
				if (timeReplyErrorCount < 0)
					timeReplyErrorCount = 0;
				malformedErrorCount -= 1;
				if (malformedErrorCount < 0)
					malformedErrorCount = 0;
				reportErrorCount -= 1;
				if (reportErrorCount < 0)
					reportErrorCount = 0;
				
//				// TODO: use this to regain an old timeline after clock jump when apps are recording
//				for (int i = 0; i < clients.size(); i++)
//				{
//					try {
//						if (clients.get(i).isContacted())
//						{
//							InetAddress address = InetAddress.getByName(clients.get(i).ip);
//							OSCPortOut sender = new OSCPortOut(address, clients.get(i).getUniPort());
//							ArrayList<Object> replyObject = new ArrayList<Object>(3);
//							replyObject.add(clients.get(i).uniqueId);
//							replyObject.add(myId);
//							replyObject.add(System.nanoTime());
//							OSCMessage msg = new OSCMessage("/givetimeanduncertainty", replyObject);
//							sender.send(msg);
//						}
//					} 
//					catch (Exception ex) 
//					{
//					
//					}
//				}
				
				boolean ipChanged = updateMyIp();
				if (ipChanged)
				{
					ipChangedCounter = 3;
				}
				if (ipChangedCounter > 0)
				{
					ipChangedCounter -= 1;
					askIpUpdates();
				}
			}

			{
				errorCountBuffer[second] = totalNetworkErrorCount;
				totalNetworkErrorCount -= errorCountBuffer[second];
				successCountBuffer[second] = totalNetworkSuccessCount;
				totalNetworkSuccessCount -= successCountBuffer[second];
				int errors = 0;
				int successes = 0;
				for (int i = 0; i < maxSeconds; i++)
				{
					errors += errorCountBuffer[i];
					successes += successCountBuffer[i];
				}
				if (gui != null)
				{
					gui.setNetworkStateText(String.format("%1$5s", errors) 
							+ " /" + String.format("%1$5s", successes));
				}
			}

			fireTableDataChanged();
			this.tagManager.fireTableDataChanged();
			this.uploadsData.fireTableDataChanged();
			this.streamingData.fireTableDataChanged();

			if (serverState != STATE_DOWNLOADING)
			{
				fileManager.fireTableDataChanged();
			}

			Boolean appsBusy = appsAreBusy();

			if (second == 0 || !appsBusy)
			{
				for (int i = 0; i < clients.size(); i++)
				{
					try {
						if (clients.get(i).isContacted())
						{
							InetAddress address = InetAddress.getByName(clients.get(i).ip);
							OSCPortOut sender = new OSCPortOut(address, clients.get(i).getUniPort());
							ArrayList<Object> replyObject = new ArrayList<Object>(2);
							replyObject.add(clients.get(i).shortId);
							replyObject.add(getExperimentName());
							replyObject.add(clients.get(i).uniqueId);
							OSCMessage msg = new OSCMessage("/report", replyObject);
							sender.send(msg);
							sender.close();
						}
					} 
					catch (Exception ex) 
					{
						if (reportErrorCount < 1)
						{
							reportErrorCount += 1;;
							if (ex.getMessage() != null)
							{
								log("Error in report request: " + ex.getMessage(), true);
							}
							else
							{
								log("Error in report request.", true);
							}
						}
						else
						{
							logSilentNetworkError();
						}
					}
				}
			}
		}
	}
	
	

	public void logSilentNetworkSuccess()
	{
		totalNetworkSuccessCount += 1;
	}
	
	public void logSilentNetworkError()
	{
		totalNetworkErrorCount += 1;
	}
	
	public void log(String text, Boolean isNetworkError)
	{
		if (isNetworkError)
		{
			totalNetworkErrorCount += 1;
		}
		else
		{
			totalNetworkSuccessCount += 1;
		}
		log(text);
	}
	
	public void log(String text)
	{
		if (gui != null)
		{
			gui.addToLog(text + newline);
		}
		else
		{
			messages = messages + text + newline;
		}
		
		
		if (logFileWriter != null)
		{
			logFileWriter.println(text);
			logFileWriter.flush();
		}
		
		System.out.println(text);
	}
	
	public void setGui(ServerGui gui)
	{
		this.gui = gui;
		gui.addToLog(messages);
	}
	
	public void log(Throwable ex, Boolean isNetworkError)
	{
		if (isNetworkError)
		{
			totalNetworkErrorCount += 1;
		}
		else
		{
			totalNetworkSuccessCount += 1;
		}
		log(ex);
	}
	
	public void log(Throwable ex)
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		log(sw.toString());
	}
	
	public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
    	return clients.size();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }
    
	public void setValueAt(Object value, int row, int col)
	{
		if (clients.size() > row && col == 0)
		{
			clients.get(row).isSelected = (Boolean)value;
		}
	}

    public Object getValueAt(int row, int col) {
    	if (clients.size() > row)
		{
    		AppInformation dev = clients.get(row);
    		switch (col)
    		{
    		case 0: return dev.isSelected;
    		case 1: return dev.ip;
    		case 2: return dev.lastSyncAccuracy;
    		case 3: return dev.meanPing;
    		case 4: return dev.droppedPackagePercentage;
    		case 5: return new BatteryLevel(dev.batteryLevel);
    		case 6: return dev.shortId;
    		case 7: 
    			if (dev.appState == AppInformation.APP_RECORDING)
    			{
    				if (dev.recordingForOtherServer == true)
    				{
    					return appStates[dev.appState] + " for other server";
    				}
    				else
    				{
    					return appStates[dev.appState] + ": " + dev.recordingNumber;
    				}
    			}
    			else
    			{
    				return appStates[dev.appState];
    			}
    		case 8: 
    			int[] health = dev.sensorHealth;
    			if (health != null && health.length > 0 && dev.healthInfoIsCurrent())
    			{
    				String t = "<html><span style=\"font-family: monospace;font-weight: bold\"";
    				for (int i = 0; i < health.length; i++)
    				{
    					if (health[i] < sensorHealthTexts.length)
    					{
    						t = t + sensorHealthTexts[health[i]];
    					}
    					else
    					{
    						t = t + "?";
    					}
    				}
    				return new SensorHealthText(t + "</span></html>");
    			}
    			else
    			{
    				return new SensorHealthText("-");
    			}
    		case 9: 
    			return dev.timeFromLastUpdate();
    		case 10:
    			return dev.deviceId;
    		case 11: 
    			if (dev.freeSpace == -1L)
    				return new StorageLevel(-1, dev.clientType);
    			else
    				return new StorageLevel(dev.freeSpace / 1024 / 1024, dev.clientType);
    		case 12:
    			return dev.clientType;
    		case 13:
    			if (dev.tag != null && dev.tag.tags.length > 2)
    			{
    				return dev.tag.tags[0];
    			}
    			else
    			{
    				return "";
    			}
    		case 14:
    			if (dev.tag != null && dev.tag.tags.length > 2)
    			{
    				return dev.tag.tags[1];
    			}
    			else
    			{
    				return "";
    			}
    		default: return null;
    		}
		}
		else
		{
			return null;
		}
    }

	public Class<?> getColumnClass(int c)
	{
		if (clients.size() == 0)
		{
			return Object.class;
		}
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {
    	if (col == 0)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    private void loadAppInformation()
    {
    	clients = new ArrayList<AppInformation>();
        File file = new File(experimentPath + File.separator + ".deviceInformation.txt");
        if (file.exists()) {
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            	String line;
            	while ((line = br.readLine()) != null)
            	{
            		log("Loaded saved app: " + line);
            		String[] info = line.split(",");
            		if (info.length == 6)
            		{
            			String ip = "";
            			int shortId = Integer.valueOf(info[0]);
            			String uniqueId = info[1];
            			String clientType = info[2];
            			String deviceModel = info[3];
            			String deviceId = info[4];
            			String longDeviceId = info[5];
            			AppInformation dev = new AppInformation(ip, shortId, uniqueId, clientType,
            					deviceModel, -1, deviceId, longDeviceId, false);
						clients.add(dev);
            		}
            		else
            		{
            			log("Badly formatted app info file.");
            		}
            	}
            	br.close();
            } catch (FileNotFoundException e) 
            {
				log(e);
			} catch (IOException e) 
            {
				log(e);
			}
        }
        else
        {
        	saveAppInformation();
        }
        
        int maxShortId = 0;
        for (int i = 0; i < clients.size(); i++)
		{
        	maxShortId = Math.max(maxShortId, clients.get(i).shortId);
		}
        nextShortId = maxShortId + 1;
    }
    
    private void saveAppInformation()
    {
    	File file = new File(experimentPath + File.separator + ".deviceInformation.txt");
    	if (file.exists())
        {
            file.delete();
        }
    	PrintWriter output;
		try {
			output = new PrintWriter(file, "UTF-8");
			for (int i = 0; i < clients.size(); i++)
			{
				output.println(clients.get(i).shortId + "," + clients.get(i).uniqueId
						+ "," + clients.get(i).clientType + "," + clients.get(i).deviceModel + ","
						+ clients.get(i).deviceId + "," + clients.get(i).longDeviceId);
			}
            output.close();
		} catch (FileNotFoundException e) {
			log(e);
		} catch (UnsupportedEncodingException e) {
			log(e);
		}
    }
    
    public void sendAllTags()
	{
		log("Task: Resend tags");
		for (int i = 0; i < clients.size(); i++)
		{
			if (!clients.get(i).needsToGiveAppTags)
			{
				sendTags(clients.get(i));
			}
		}
	}
    
	public void taskRefreshApps(final Boolean verbose)
	{
		log("Task: Refresh apps");
		Runnable runner = new Runnable()
		{
			public void run()
			{
				askIpUpdates();
				for (int i = 0; i < clients.size(); i++)
				{
					sendTags(clients.get(i));
				}
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					log(e);
				}
				if (verbose)
				{
					updateGuiTaskResult("");
				}
				setState(STATE_READY);
			}
		};
		
		if (serverState == STATE_READY)
		{
			setState(STATE_REFRESHING);
			if (verbose)
			{
				updateGuiTaskResult("");
			}
			Thread thread = new Thread(runner);
			thread.start();
		}
	}

	public void loadTags(File file, Boolean userSupplied)
	{
		tagManager.loadTags(file, userSupplied);
	}

	public void sendDimScreens(AppInformation dev)
	{
		if (dev.isContactedAndSelected())
		{
			try {
				InetAddress address = InetAddress.getByName(dev.ip);
				OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
				ArrayList<Object> replyObject = new ArrayList<Object>(1);
				replyObject.add(dev.uniqueId);
				OSCMessage msg = new OSCMessage("/dimscreen", replyObject);
				sender.send(msg);
				sender.close();
			}
			catch (Exception ex) 
			{
				log("Error in sending dim screen.", true);
				log(ex);
			}
		}
	}
	
	public void sendBrightScreens(AppInformation dev)
	{
		if (dev.isContactedAndSelected())
		{
			try {
				InetAddress address = InetAddress.getByName(dev.ip);
				OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
				ArrayList<Object> replyObject = new ArrayList<Object>(1);
				replyObject.add(dev.uniqueId);
				OSCMessage msg = new OSCMessage("/brightscreen", replyObject);
				sender.send(msg);
				sender.close();
			}
			catch (Exception ex) 
			{
				log("Error in sending dim screen.", true);
				log(ex);
			}
		}
	}

	public void taskCalculateSync()
	{
		log("Task: Sync downloaded files");
		fileManager.taskSyncDownloadedFiles();
	}

	public void addNewNote()
	{
		noteManager.createNewEmptyNote();
	}
	
	public void deleteNotes(int[] rows)
	{
		noteManager.deleteNotes(rows);
	}

	public void saveNotes()
	{
		if (noteManager.saveNotes())
		{
			gui.setSavedText("Changes saved.");
		}
		else
		{
			gui.setSavedText("Error in saving notes.");
		}
	}
	
	public void loadNotes()
	{
		if (noteManager.loadNotes())
		{
			gui.setSavedText("Loaded notes from file.");
		}
		
	}

	public void taskStartStimuli()
	{
		log("Task: Start stimuli");
		Runnable runner = new Runnable()
		{
			public void run()
			{
				String stimulusName = gui.getStimulusFileName();
				if (stimulusName == null)
				{
					updateGuiTaskResult(", tried starting with a bad stimulus file name.");
					setState(STATE_READY);
				}
				
				int startedCount = 0;
				int appCount = 0;
				
				fileManager.setRecordingNumberSentToApps(highestReportedRecordingNumber());
				tagManager.createTagBackup(getCurrentRecordingNumber());
				noteManager.createNoteBackup(getCurrentRecordingNumber());
				
				noteManager.addNote("Started stimuli: '" + stimulusName + "' during recording " + fileManager.getCurrentRecordingNumber());
				saveNotes();
				
				long desiredStartTime = SyncServer.getCurrentTimeMillis() + 3000l;
				
				for (int tries = 0; tries < 5; tries++)
				{
					for (int i = 0; i < clients.size(); i++)
					{
						try {
							AppInformation dev = clients.get(i);
							if (dev.isContactedAndSelected()
									&& UploadsData.isUploadClient(dev)
									&& (tries == 1 || dev.appState != AppInformation.APP_PREPARING_PLAY))
							{
								InetAddress address = InetAddress.getByName(dev.ip);
								OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
								ArrayList<Object> messages = new ArrayList<Object>();
								messages.add(getExperimentName());
								messages.add(dev.shortId);
								messages.add(myId);
								messages.add(desiredStartTime);
								messages.add(stimulusName);
								messages.add(dev.uniqueId);
								messages.add(fileManager.getCurrentRecordingNumber());
								OSCMessage msg = new OSCMessage("/startstimuli", messages);
								sender.send(msg);
								sender.close();
							}
						}
						catch (Exception ex) 
						{
							log("Error sending 'Start stimuli'", true);
							log(ex);
						}
					}
					try 
					{
						Thread.sleep(150);
					} catch (InterruptedException e) {
						log(e);
					}
					startedCount = 0;
					appCount = 0;
					for (int i = 0; i < clients.size(); i++)
					{
						if (clients.get(i).isContactedAndSelected() && UploadsData.isUploadClient(clients.get(i)))
						{
							appCount += 1;
							if (clients.get(i).appState == AppInformation.APP_PREPARING_PLAY)
							{
								startedCount += 1;
							}
						}
					}
					updateGuiTaskResult(", preparing " + startedCount + " / " + appCount);
				}
				
				startedCount = 0;
				appCount = 0;
				try 
				{
					Thread.sleep(3000l);
				} catch (InterruptedException e) {
					log(e);
				}
				for (int i = 0; i < clients.size(); i++)
				{
					if (clients.get(i).isContactedAndSelected() && UploadsData.isUploadClient(clients.get(i)))
					{
						appCount += 1;
						if (clients.get(i).appState == AppInformation.APP_PLAYING)
						{
							startedCount += 1;
						}
					}
				}
				updateGuiTaskResult(", started playing on " + startedCount + " / " + appCount);
				setState(STATE_READY);
			}
		};
		
		if (serverState == STATE_READY)
		{
			setState(STATE_STARTING_STIMULI);
			updateGuiTaskResult("");
			Thread thread = new Thread(runner);
			thread.start();
		}
	}

	public void taskCancelStimuli()
	{
		log("Task: Cancel stimuli");
		Runnable runner = new Runnable()
		{
			public void run()
			{
				int cancelledCount = 0;
				int appCount = 0;
				boolean cancelledNonStimulusApps = false;
				for (int tries = 0; tries < 5; tries++)
				{
					for (int i = 0; i < clients.size(); i++)
					{
						try {
							if (clients.get(i).isContactedAndSelected()
									&& UploadsData.isUploadClient(clients.get(i))
									&& (tries == 1 || clients.get(i).appState != AppInformation.APP_RECORDING))
							{
								if (!clients.get(i).clientType.equals("STI"))
								{
									cancelledNonStimulusApps = true;
								}
								
								InetAddress address = InetAddress.getByName(clients.get(i).ip);
								OSCPortOut sender = new OSCPortOut(address, clients.get(i).getUniPort());
								ArrayList<Object> replyObject = new ArrayList<Object>(1);
								replyObject.add(clients.get(i).uniqueId);
								OSCMessage msg = new OSCMessage("/cancelstimuli", replyObject);
								sender.send(msg);
								sender.close();
							}
						} 
						catch (Exception ex) 
						{
							log("Error sending 'Cancel stimuli'", true);
							log(ex);
						}
					}
					try 
					{
						Thread.sleep(200);
					} catch (InterruptedException e) {
						log(e);
					}
					cancelledCount = 0;
					appCount = 0;
					for (int i = 0; i < clients.size(); i++)
					{
						if (clients.get(i).isContactedAndSelected() && UploadsData.isUploadClient(clients.get(i)))
						{
							appCount += 1;
							if (clients.get(i).appState == AppInformation.APP_RECORDING)
							{
								cancelledCount += 1;
							}
						}
					}
					updateGuiTaskResult(" " + cancelledCount + " / " + appCount);
				}
				
				updateGuiTaskResult(", cancelled playing on " + cancelledCount + " / " + appCount);
				
				if (cancelledNonStimulusApps)
				{
					fileManager.nextRecordingNumber();
				}
				
				setState(STATE_READY);
			}
		};
		if (serverState == STATE_READY)
		{
			setState(STATE_CANCELLING_STIMULI);
			updateGuiTaskResult("");
			Thread thread = new Thread(runner);
			thread.start();
			noteManager.addNote("Cancelled stimuli");
			saveNotes();
		}
	}

	public void uploadFile(final File[] selectedFiles, final Boolean isTouchSound,
			final String nameOnDevice, final Boolean alsoStopSelected) {
		log("Task: Upload file");
		
		Runnable runner = new Runnable()
		{
			public void run()
			{
				if (alsoStopSelected)
				{
					unselectNonUploadsClients();
					taskStopRecording();
				}
				int count = 0;
				while (serverState != STATE_READY && count < 200)
				{
					count += 1;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				fileManager.taskUploadFile(selectedFiles, isTouchSound, nameOnDevice);
			}
		};
		
		Thread thread = new Thread(runner);
		thread.start();
	}

	public void sendDisableScreenTouchSound() {
		for (int tries = 0; tries < 3; tries++)
		{
			for (int i = 0; i < clients.size(); i++)
			{
				if (clients.get(i).isContactedAndSelected() && UploadsData.isUploadClient(clients.get(i)))
				{
					sendDisableScreenTouchSound(clients.get(i));
				}
			}
			try 
			{
				Thread.sleep(100l);
			} catch (InterruptedException e) {
				log(e);
			}
		}
		log("Sent 'Disable screen touch sound'.");
	}
	
	public void sendDisableScreenTouchSound(AppInformation dev)
	{
		if (dev.isContactedAndSelected())
		{
			try {
				InetAddress address = InetAddress.getByName(dev.ip);
				OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
				ArrayList<Object> replyObject = new ArrayList<Object>(1);
				replyObject.add(dev.uniqueId);
				OSCMessage msg = new OSCMessage("/disabletouchsound", replyObject);
				sender.send(msg);
				sender.close();
			}
			catch (Exception ex) 
			{
				log("Error in sending 'Disable screen touch sound'.", true);
				log(ex);
			}
		}
	}
	
	public void sendDeleteFilesFromDevice(Boolean onlyThisExperiment, String experiment, Boolean onlyStimulusFiles) {
		for (int tries = 0; tries < 3; tries++)
		{
			for (int i = 0; i < clients.size(); i++)
			{
				if (onlyStimulusFiles)
				{
					if (clients.get(i).isContactedAndSelected() && UploadsData.isUploadClient(clients.get(i)))
					{
						sendDeleteFilesFromDevice(clients.get(i), onlyThisExperiment, experiment, onlyStimulusFiles);
					}
				}
				else
				{
					if (clients.get(i).isContactedAndSelected())
					{
						sendDeleteFilesFromDevice(clients.get(i), onlyThisExperiment, experiment, onlyStimulusFiles);
					}
				}
			}
			try 
			{
				Thread.sleep(100l);
			} catch (InterruptedException e) {
				log(e);
			}
		}
		log("Sent 'Delete files' message.");
	}
	
	public void sendDeleteFilesFromDevice(AppInformation dev, Boolean onlyThisExperiment, String experiment, Boolean onlyStimulusFiles)
	{
		if (dev.isContactedAndSelected())
		{
			try {
				InetAddress address = InetAddress.getByName(dev.ip);
				OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
				ArrayList<Object> replyObject = new ArrayList<Object>(3);
				replyObject.add(dev.uniqueId);
				replyObject.add(onlyThisExperiment);
				replyObject.add(experiment);
				replyObject.add(onlyStimulusFiles);
				OSCMessage msg = new OSCMessage("/deletefiles", replyObject);
				sender.send(msg);
				sender.close();
			}
			catch (Exception ex) 
			{
				log("Error in sending 'Delete files' message.", true);
				log(ex);
			}
		}
	}
	
	public void sendClockReset()
	{
		for (int tries = 0; tries < 3; tries++)
		{
			for (int i = 0; i < clients.size(); i++)
			{
				AppInformation dev = clients.get(i);
				if (dev.isContactedAndSelected())
				{
					try {
						InetAddress address = InetAddress.getByName(dev.ip);
						OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
						ArrayList<Object> replyObject = new ArrayList<Object>(2);
						replyObject.add(dev.uniqueId);
						replyObject.add(myId);
						OSCMessage msg = new OSCMessage("/clockreset", replyObject);
						sender.send(msg);
						sender.close();
					}
					catch (Exception ex) 
					{
						log("Error in sending 'Clock reset'.", true);
						log(ex);
					}
				}
				try 
				{
					Thread.sleep(100l);
				} catch (InterruptedException e) {
					log(e);
				}
			}
		}
	}
	
	public void sendCloseSelectedApps() {
		log("Sent 'Close apps' message.");
		for (int tries = 0; tries < 3; tries++)
		{
			for (int i = 0; i < clients.size(); i++)
			{
				if (clients.get(i).isContactedAndSelected())
				{
					sendCloseSelectedApps(clients.get(i));
				}
			}
			try 
			{
				Thread.sleep(100l);
			} catch (InterruptedException e) {
				log(e);
			}
		}
	}
	
	public void sendCloseSelectedApps(AppInformation dev)
	{
		if (dev.isContactedAndSelected())
		{
			try {
				InetAddress address = InetAddress.getByName(dev.ip);
				OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
				ArrayList<Object> replyObject = new ArrayList<Object>(1);
				replyObject.add(dev.uniqueId);
				OSCMessage msg = new OSCMessage("/closeapp", replyObject);
				sender.send(msg);
				sender.close();
			}
			catch (Exception ex) 
			{
				log("Error in sending 'Close apps' message.", true);
				log(ex);
			}
		}
	}
	
	public void sendEnableScreenTouchSound() {
		for (int tries = 0; tries < 3; tries++)
		{
			for (int i = 0; i < clients.size(); i++)
			{
				if (clients.get(i).isContactedAndSelected() && UploadsData.isUploadClient(clients.get(i)))
				{
					sendEnableScreenTouchSound(clients.get(i));
				}
			}
			try 
			{
				Thread.sleep(100l);
			} catch (InterruptedException e) {
				log(e);
			}
		}
		log("Sent 'Enable screen touch sound'.");
	}
	
	public void sendEnableScreenTouchSound(AppInformation dev)
	{
		if (dev.isContactedAndSelected())
		{
			try {
				InetAddress address = InetAddress.getByName(dev.ip);
				OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
				ArrayList<Object> replyObject = new ArrayList<Object>(1);
				replyObject.add(dev.uniqueId);
				OSCMessage msg = new OSCMessage("/enabletouchsound", replyObject);
				sender.send(msg);
				sender.close();
			}
			catch (Exception ex) 
			{
				log("Error in sending 'Enable screen touch sound'.", true);
				log(ex);
			}
		}
	}

	public void sendSetting(int selectedIndex)
	{
		if (selectedIndex < AppSettingInformation.appSettingsLabel.length)
		{
			for (int tries = 0; tries < 3; tries++)
			{
				for (int i = 0; i < clients.size(); i++)
				{
					if (clients.get(i).isContactedAndSelected() && UploadsData.isUploadClient(clients.get(i)))
					{
						sendSetting(clients.get(i),
								AppSettingInformation.appSettingsName[selectedIndex],
								AppSettingInformation.appSettingsValue[selectedIndex]);
					}
				}
				try 
				{
					Thread.sleep(100l);
				} catch (InterruptedException e) {
					log(e);
				}
			}
			log("Sent a setting: " + AppSettingInformation.appSettingsLabel[selectedIndex]);
		}
	}
	
	public void sendSetting(AppInformation dev, String setting, String value)
	{
		if (dev.isContactedAndSelected())
		{
			try {
				InetAddress address = InetAddress.getByName(dev.ip);
				OSCPortOut sender = new OSCPortOut(address, dev.getUniPort());
				ArrayList<Object> replyObject = new ArrayList<Object>(3);
				replyObject.add(dev.uniqueId);
				replyObject.add(setting);
				replyObject.add(value);
				OSCMessage msg = new OSCMessage("/setappsetting", replyObject);
				sender.send(msg);
				sender.close();
			}
			catch (Exception ex) 
			{
				log("Error in sending 'Enable screen touch sound'.", true);
				log(ex);
			}
		}
	}
	
	public void taskDownloadMetaFiles(final Boolean alsoStop)
	{
		log("Task: Downloading meta files");
		
		Runnable runner = new Runnable()
		{
			public void run()
			{
				if (alsoStop)
				{
					selectAllApps();
					taskStopRecording();
				}
				int count = 0;
				while (serverState != STATE_READY && count < 200)
				{
					count += 1;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				fileManager.taskDownloadMetaFiles();
			}
		};
		
		Thread thread = new Thread(runner);
		thread.start();
	}


}
