/*
FSenSync Server
Copyright (C) 2017-2018  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

public abstract class SafeAbstractTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	public void fireTableDataChanged()
	{
		if (javax.swing.SwingUtilities.isEventDispatchThread())
		{
			super.fireTableDataChanged();
		}
		else
		{
			final AbstractTableModel parent = this;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					parent.fireTableDataChanged();
				}
			});
		}
	}
	
	public void fireTableStructureChanged()
	{
		if (javax.swing.SwingUtilities.isEventDispatchThread())
		{
			super.fireTableStructureChanged();
		}
		else
		{
			final AbstractTableModel parent = this;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					parent.fireTableStructureChanged();
				}
			});
		}
	}
	
	public void fireTableChanged(final TableModelEvent e)
	{
		if (javax.swing.SwingUtilities.isEventDispatchThread())
		{
			super.fireTableChanged(e);
		}
		else
		{
			final AbstractTableModel parent = this;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					parent.fireTableChanged(e);
				}
			});
		}
	}
	
	public void fireTableRowsInserted(final int firstRow, final int lastRow)
	{
		if (javax.swing.SwingUtilities.isEventDispatchThread())
		{
			super.fireTableRowsInserted(firstRow, lastRow);
		}
		else
		{
			final AbstractTableModel parent = this;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					parent.fireTableRowsInserted(firstRow, lastRow);
				}
			});
		}
	}
	
	public void fireTableRowsDeleted(final int firstRow, final int lastRow)
	{
		if (javax.swing.SwingUtilities.isEventDispatchThread())
		{
			super.fireTableRowsDeleted(firstRow, lastRow);
		}
		else
		{
			final AbstractTableModel parent = this;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					parent.fireTableRowsDeleted(firstRow, lastRow);
				}
			});
		}
	}
	
	public void fireTableRowsUpdated(final int firstRow, final int lastRow)
	{
		if (javax.swing.SwingUtilities.isEventDispatchThread())
		{
			super.fireTableRowsUpdated(firstRow, lastRow);
		}
		else
		{
			final AbstractTableModel parent = this;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					parent.fireTableRowsUpdated(firstRow, lastRow);
				}
			});
		}
	}

}
