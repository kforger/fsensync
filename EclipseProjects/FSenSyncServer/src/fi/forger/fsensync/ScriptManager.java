/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ScriptManager extends SafeAbstractTableModel {

	private static final long serialVersionUID = 1L;
	private SyncServer server;
	private final String[] columnNames = {"Path", "Script name", "Is running"};
	public ArrayList<ScriptInformation> scriptList;
	
	public ScriptManager(SyncServer server)
	{
		this.server = server;
		scriptList = new ArrayList<ScriptInformation>();
		refreshScripts();
	}
	
	public void refreshScripts()
	{
		scriptList.clear();
		File path = new File(StartUpWindow.settingsPath + File.separator + "scripts");
		File[] files = path.listFiles();
		if (files != null)
		{
			for (int i = 0; i < files.length; i++)
			{
				scriptList.add(new ScriptInformation(files[i]));
			}
		}
	}
	
	public int getColumnCount()
	{
        return columnNames.length;
    }

	public int getRowCount()
	{
		return scriptList.size();
	}
	
	public String getColumnName(int col) {
        return columnNames[col];
    }
	
	public ScriptInformation getSelected(int row)
	{
		return scriptList.get(row);
	}

	public Object getValueAt(int row, int col) {
		if (scriptList.size() > row)
		{
			switch (col)
			{
			case 0: return scriptList.get(row).file.getParentFile().getPath();
			case 1: return scriptList.get(row).file.getName();
			case 2: return scriptList.get(row).isRunning;
			default: return null;
			}
		}
		else
		{
			return null;
		}
	}
	
	public Class<?> getColumnClass(int c) {
		if (scriptList.size() == 0)
		{
			return Object.class;
		}
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    public void runSelectedScript(final int firstRecording, final int lastRecording, final int scriptRow)
    {
    	final ScriptManager parent = this;
    	Runnable runner = new Runnable()
    	{
    		public void run()
    		{
    			try
    			{
    				if (!getSelected(scriptRow).isRunning)
    				{
    					File pythonExec = null;
    					if (StartUpWindow.anacondaPath != null)
    					{
    						String name = "python";
    						if (System.getProperty("os.name").startsWith("Windows"))
    						{
    							name = "python.exe";
    						}
    						
    						pythonExec = new File(StartUpWindow.anacondaPath.getAbsolutePath()
		    						+ File.separator + name);
    					}
    					
    					if (StartUpWindow.anacondaPath != null && StartUpWindow.anacondaPath.exists()
    							&& pythonExec != null && pythonExec.exists())
    					{
	    					getSelected(scriptRow).isRunning = true;
	    					parent.fireTableDataChanged();
		    				String[] command = new String[] {
		    						(StartUpWindow.anacondaPath.getAbsolutePath() + File.separator + "python"),
		    						getSelected(scriptRow).file.getAbsolutePath(),
		    						"-firstRecording", 
		    						Integer.toString(firstRecording),
		    						"-lastRecording" ,  
		    						Integer.toString(lastRecording),
		    						"-downloaded",
		    						(server.getExperimentPath() + File.separator + "downloaded"),
		    						"-synced",
		    						(server.getExperimentPath() + File.separator + "synced"),
		    						"-binpath",
		    						StartUpWindow.anacondaPath.getAbsolutePath()};
		    				
		    				server.gui.appendToScriptLog("\nStarting script:");
		    				
		    				String commandConcat = "" + command[0];
		    				for (int i = 1; i < command.length; i++)
		    				{
		    					commandConcat += " " + command[i].replace(" ", "\\ ");
		    				}
		    				server.gui.appendToScriptLog(commandConcat);
		
		    				Process p = Runtime.getRuntime().exec(command);
		    				getSelected(scriptRow).process = p;
		
		    				String line;
		    				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    				while ((line = input.readLine()) != null)
		    				{
		    					server.gui.appendToScriptLog(line);
		    				}
		    				input.close();
		
		    				BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		    				while ((line = error.readLine()) != null)
		    				{
		    					server.gui.appendToScriptLog(line);
		    				}
		    				error.close();
		
		    				server.gui.appendToScriptLog("End of script run.");
		    				
		    				getSelected(scriptRow).isRunning = false;
		        			parent.fireTableDataChanged();
    					}
    					else
    					{
    						if (StartUpWindow.anacondaPath == null)
    						{
    							server.gui.appendToScriptLog("\nAnaconda folder is not set.");
    						}
    						else
    						{
    							server.gui.appendToScriptLog("\nPython executable could not be found from: " +  StartUpWindow.anacondaPath.getAbsolutePath());
    						}
    					}
    				}
    				else
    				{
    					server.gui.appendToScriptLog("\nThe script is already running: " + getSelected(scriptRow).file.getAbsolutePath());
    				}
    			}
    			catch (IOException ex)
    			{
    				if (getSelected(scriptRow).isRunning)
    				{
    					server.gui.appendToScriptLog(ex.getMessage());
    					server.gui.appendToScriptLog("Script run ended with error.");
    				}
    				else
    				{
    					server.gui.appendToScriptLog("Forced end of script run.");
    				}
    				
    				getSelected(scriptRow).isRunning = false;
        			parent.fireTableDataChanged();
    			}
    		}
    	};
    	
    	Thread thread = new Thread(runner);
		thread.start();
    }
    
    public void stopScript(int scriptRow)
    {
    	if (getSelected(scriptRow).isRunning && getSelected(scriptRow).process != null)
    	{
    		getSelected(scriptRow).isRunning = false;
    		getSelected(scriptRow).process.destroy();
    	}
    }

}
