/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class TagManager extends SafeAbstractTableModel {

	private static final long serialVersionUID = -5766125209719123984L;

	private ArrayList<TagInformation> tagList;
	private SyncServer server;
	private final String[] columnNames = {"Type", "Tag 1", "Tag 2", "Tag 3", "App ID", "Device ID", "Contact to app"};
	
	public TagManager(SyncServer server)
	{
		this.server = server;
		tagList = new ArrayList<TagInformation>();
		File file = new File(server.getExperimentPath() + File.separator + server.getExperimentName() + "_tags.txt");
    	if (file.exists())
        {
            loadTags(file, false);
        }
    	else
    	{
    		assignTags();
        	fireTableDataChanged();
    	}
	}
	
	public int getColumnCount()
	{
        return columnNames.length;
    }

	public int getRowCount()
	{
		return tagList.size();
	}
	
	public String getColumnName(int col) {
        return columnNames[col];
    }

	@Override
	public Object getValueAt(int row, int col) {
		if (tagList.size() > row)
		{
    		switch (col)
    		{
    		case 0:	return tagList.get(row).desiredType;
    		case 1: return tagList.get(row).tags[0];
    		case 2: return tagList.get(row).tags[1];
    		case 3: return tagList.get(row).tags[2];
    		case 4:
    			if (tagList.get(row).getDev() != null)
    			{
    				return tagList.get(row).getDev().shortId;
    			}
    			else
    			{
    				return -1;
    			}
    		case 5:
    			if (tagList.get(row).getDev() != null)
    			{
    				return tagList.get(row).getDev().deviceId;
    			}
    			else
    			{
    				return "-";
    			}
    		case 6:
    			AppInformation dev = tagList.get(row).getDev();
    			if (dev != null)
    			{
	    			return dev.timeFromLastUpdate();
    			}
    			else
    			{
    				return new ContactAge("not assigned", -1L);
    			}
    		default: return null;
    		}
		}
		else
		{
			return null;
		}
	}
	
	public Class<?> getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    private void emptyTags(Boolean commandFromUser)
    {
    	for (int i = 0; i < server.clients.size(); i++)
    	{
    		server.clients.get(i).tag = null;
    		if (commandFromUser)
    		{
    			server.clients.get(i).needsToGiveAppTags = false;
    		}
    	}
    	tagList.clear();
    }
    
    public void createTagBackup(int recordingNumber)
    {
    	File oldFile = new File(server.getExperimentPath() + File.separator + server.getExperimentName() + "_tags.txt");
    	File backupFile = new File(server.getExperimentPath() + File.separator + "backups" + File.separator
    			+ server.getExperimentName() + "_"
    			+ String.format(Locale.ROOT, "%03d", recordingNumber) + "_tags.txt");
    	if (oldFile.exists())
        {
	    	backupFile.mkdirs();
    		try {
				Files.copy(oldFile.toPath(), backupFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				server.log(e);
			}
        }
    }
    
    public void loadTags(File file, Boolean isUserSupplied)
    {
    	final String[] allowedTypes = {"ACC", "STI", "VANNO", "VID", 
    			"BIO", "VPRO", "CART", "CLOCK", "EDA", "MOVE", "SERIAL", "DPLAY",
    			"PROC", "OTHER", ""};
    	List<String> allowedTypesList = Arrays.asList(allowedTypes);
    	
    	if (file.exists())
    	{
    		emptyTags(isUserSupplied);
    		
    		int nextSharedId = 1;
    		
    		try
    		{
    			BufferedReader br = new BufferedReader(new FileReader(file));
    			String line;
    			int lineCount = 0;
    			while ((line = br.readLine()) != null && line.length() > 0)
    			{
    				lineCount += 1;
    				String[] inputSplit = line.split(",");
    				String[] desiredTypes = inputSplit[0].split("/");
    				String[] outTags = new String[3];
    				for (int i = 0; i < 3; i++)
    				{
    					if (inputSplit.length > i+1)
    					{
    						outTags[i] = inputSplit[i+1];
    					}
    					else
    					{
    						outTags[i] = "";
    					}
    				}
    				
    				Boolean typesAreAllowed = true;
    				int sharedId = 0;
    				
    				String regexNumOnly = "[0-9]+";
    				if (desiredTypes.length > 0 && desiredTypes[0].matches(regexNumOnly))
    				{
    					if (desiredTypes[0].length() > 0)
    					{
	    					try
	    					{
	    						sharedId = Integer.parseInt(desiredTypes[0]);
	    						String[] old = desiredTypes;
	    						desiredTypes = new String[old.length - 1];
	    						System.arraycopy(old, 1, desiredTypes, 0, desiredTypes.length);
	    					}
	    					catch (Exception ex)
	    					{
	    						server.log(ex.toString());
	    						typesAreAllowed = false;
	    					}
    					}
    				}
    				else
    				{
    					if (!isUserSupplied && desiredTypes.length > 0)
    					{
    						String[] old = desiredTypes;
    						desiredTypes = new String[old.length - 1];
    						System.arraycopy(old, 1, desiredTypes, 0, desiredTypes.length);
    					}
    				}
    				
    				if (sharedId == 0)
    				{
    					sharedId = nextSharedId;
    					nextSharedId += 1;
    				}
    				else
    				{
    					nextSharedId = Math.max(nextSharedId, sharedId + 1);
    				}
    				
    				for (int i = 0; i < desiredTypes.length; i++)
    				{
    					if (!allowedTypesList.contains(desiredTypes[i]))
    					{
    						typesAreAllowed = false;
    					}
    				}
    				
    				if (typesAreAllowed)
					{
    					if (isUserSupplied)
    					{
							for (int i = 0; i < desiredTypes.length; i++)
							{
								TagInformation t = new TagInformation(outTags, desiredTypes[i], sharedId);
								tagList.add(t);
							}
    					}
    					else
    					{
    						TagInformation t = new TagInformation(outTags, desiredTypes[0], sharedId);
    						
	    					if (inputSplit.length > 4)
	        				{
	    						int appNumber = Integer.parseInt(inputSplit[4]);
	    						for (int k = 0; k < server.clients.size(); k++)
	    						{
	    							if (server.clients.get(k).shortId == appNumber
	    									&& server.clients.get(k).clientType.equals(t.desiredType))
	            					{
	            						t.setDev(server.clients.get(k));
	            					}
	    						}
	        				}
    						
    						tagList.add(t);
    					}
					}
    				else
    				{
    					server.log("Badly formatted tag on line: " + lineCount);
    				}
    			}
    			br.close();
    	    	server.log("Loaded tags from file: " + file.getName());
    		} catch (FileNotFoundException e) 
    		{
    			server.log(e);
    		} catch (IOException e) 
    		{
    			server.log(e);
    		}
    	}
    	
    	assignTags();
    	fireTableDataChanged();
    }
    
    private void saveTagInformation()
    {
    	File file = new File(server.getExperimentPath() + File.separator + server.getExperimentName() + "_tags.txt");
    	if (file.exists())
        {
            file.delete();
        }
    	PrintWriter output;
		try {
			output = new PrintWriter(file, "UTF-8");
			for (int i = 0; i < tagList.size(); i++)
			{
				TagInformation t = tagList.get(i);
				String devNum = "";
				if (t.getDev() != null)
				{
					devNum = String.valueOf(t.getDev().shortId);
				}
				output.println(t.sharedId + "/" + t.desiredType + "," 
				+ t.tags[0] + "," + t.tags[1] + "," + t.tags[2] + "," 
						+ devNum);
			}
            output.close();
		} catch (FileNotFoundException e) {
			server.log(e);
		} catch (UnsupportedEncodingException e) {
			server.log(e);
		}
    }
    
    public void assignTags()
    {
    	Boolean assignedNewTags = false;
    	
    	for (int indApp = 0; indApp < server.clients.size(); indApp++)
		{
    		AppInformation app = server.clients.get(indApp);
    		if (app.tag == null && !app.needsToGiveAppTags)
    		{
    			ArrayList<AppInformation> appsOnSameDevice = server.findAppsByLongDeviceId(app.longDeviceId);
	    		Boolean tagAssignedForThis = false;
	    		
	    		int requiredSharedId = 0;
	    		
	    		for (AppInformation otherOnSameDev : appsOnSameDevice)
	    		{
	    			if (otherOnSameDev.tag != null)
	    			{
	    				requiredSharedId = Math.max(requiredSharedId, otherOnSameDev.tag.sharedId);
	    			}
	    		}
	    		
	    		// Trying to find a previously used shared tag of correct type
	    		for (int indTag = 0; indTag < tagList.size() && !tagAssignedForThis; indTag++)
    			{
	    			TagInformation tag = tagList.get(indTag);
    				if (tag.getDev() == null && app.clientType.equals(tag.desiredType) && tag.sharedId == requiredSharedId)
					{
    					tagAssignedForThis = true;
    					assignedNewTags = true;
    					tag.setDev(app);
        				server.taskSendTags(app);
					}
    			}
	    		
	    		// Trying to find another free tag
	    		if (!tagAssignedForThis)
	    		{
	    			for (int indTag = 0; indTag < tagList.size() && !tagAssignedForThis; indTag++)
	    			{
		    			TagInformation tag = tagList.get(indTag);
	    				if (tag.getDev() == null && app.clientType.equals(tag.desiredType))
						{
	    					boolean canBeTaken = true;
	    					for (AppInformation other : server.clients)
	    					{
	    						if (other.tag != null && other.tag.sharedId == tag.sharedId)
	    						{
	    							canBeTaken = false;
	    						}
	    					}
	    					
	    					if (canBeTaken)
	    					{
		    					tagAssignedForThis = true;
		    					assignedNewTags = true;
		    					tag.setDev(app);
		        				server.taskSendTags(app);
	    					}
						}
	    			}
	    		}
	    		
	    		// Defaulting to an empty tag
	    		if (!tagAssignedForThis)
	    		{
	    			assignedNewTags = true;
					tagList.add(new TagInformation(app));
	    		}
    		}
    		else
    		{
    			if (app.tag == null)
    			{
	    			assignedNewTags = true;
					tagList.add(new TagInformation(app));
    			}
    		}
		}
    	
    	if (assignedNewTags)
    	{
    		saveTagInformation();
    		fireTableDataChanged();
    	}
    }

	public void updateTagFromClient(AppInformation app, String appTag1, String appTag2, String appTag3)
	{
		if (!app.tag.tags[0].equals(appTag1) || !app.tag.tags[1].equals(appTag2) || !app.tag.tags[2].equals(appTag3))
		{
			app.tag.tags[0] = appTag1;
			app.tag.tags[1] = appTag2;
			app.tag.tags[2] = appTag3;
			app.needsToGiveAppTags = false;
			saveTagInformation();
    		fireTableDataChanged();
		}
		else
		{
			app.tag.tags[0] = appTag1;
			app.tag.tags[1] = appTag2;
			app.tag.tags[2] = appTag3;
			app.needsToGiveAppTags = false;
		}
		server.taskSendTags(app);
	}
}
