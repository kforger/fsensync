/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

public class AppSettingInformation
{
	public String settingName;
	public String settingValue;
	
	public static final String[] appSettingsLabel = {"0: Circles", 	"1: Traces"};
	public static final String[] appSettingsName = 	{"DRAW_MODE", 	"DRAW_MODE"};
	public static final String[] appSettingsValue = {"0", 			"1"};
	
	public AppSettingInformation(String settingName, String settingValue)
	{
		this.settingName = settingName;
		this.settingValue = settingValue;
	}
}
