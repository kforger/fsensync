/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

public class StorageLevel  implements Comparable
{
	public long level;
	public String appType;
	
	public StorageLevel(long l, String appType)
	{
		this.level = l;
		this.appType = appType;
	}
	
	public boolean equals(Object other)
	{
	    if (other == null) return false;
	    if (other == this) return true;
	    if (other instanceof StorageLevel)
	    {
	    	StorageLevel otherStorage = (StorageLevel)other;
	    	
	    	if (this.level == otherStorage.level)
	    	{
	    		return true;
	    	}
	    	else
	    	{
	    		return false;
	    	}
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	public int compareTo(Object other)
	{
		if (other instanceof StorageLevel)
		{
			return compareTo((StorageLevel)other);
		}
		else
		{
			return 0;
		}
	}
	
	
	public int compareTo(StorageLevel other)
	{
		if (this.equals(other))
		{
			return 0;
		}
		else
		{
			return (int)(this.level - other.level);
		}
	}
}
