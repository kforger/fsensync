/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.io.File;

public class FileInformation {
	public String name;
	public long sizeInBytes;
	public AppInformation app;
	public int transferState;
	public String errorMessage;
	public int percentageTransferReady;
	public File file;
	public Boolean isTouchSound;
	public String nameOnDevice;
	public Boolean isSelected;
	public String metaTag1;
	public String metaTag2;
	public String metaTag3;
	public int metaRecordingNumber;
	public String metaAppType;
	
	
	public static final int FILE_STATE_NOT_TRANSFERRED = 0;
	public static final int FILE_STATE_TRANSFER_NOW = 1;
	public static final int FILE_STATE_TRANSFERRED = 2;
	
	public FileInformation(File file, AppInformation device, Boolean isTouchSound, String nameOnDevice)
	{
		this.name = file.getName();
		this.file = file;
		this.sizeInBytes = file.length();
		this.app = device;
		this.transferState = FILE_STATE_NOT_TRANSFERRED;
		this.errorMessage = "";
		percentageTransferReady = 0;
		this.isTouchSound = isTouchSound;
		this.nameOnDevice = nameOnDevice;
		this.isSelected = false;
		metaTag1 = "";
		metaTag2 = "";
		metaTag3 = "";
		metaRecordingNumber = -1;
		metaAppType = "";
	}
	
	public FileInformation(String name, long sizeInBytes, AppInformation device)
	{
		this.name = name;
		this.sizeInBytes = sizeInBytes;
		this.app = device;
		this.transferState = FILE_STATE_NOT_TRANSFERRED;
		this.errorMessage = "";
		percentageTransferReady = 0;
		this.isTouchSound = false;
		this.isSelected = false;
		metaTag1 = "";
		metaTag2 = "";
		metaTag3 = "";
		metaRecordingNumber = -1;
		metaAppType = "";
	}
}
