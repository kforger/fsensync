/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

public class CustomTable extends JTable {

	private static final long serialVersionUID = 1L;

	public CustomTable(AbstractTableModel model)
	{
		super(model);
		
		this.getTableHeader().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent event)
			{
				if (event.getClickCount() >= 2 && event.getButton() == MouseEvent.BUTTON1)
				{
					int col = CustomTable.this.columnAtPoint(event.getPoint());
					CustomTable.this.getColumnModel().getColumn(col).setPreferredWidth(
							CustomTable.this.getColumnModel().getColumn(col).getPreferredWidth() + 40);
				}
				
				if (event.getClickCount() >= 2 && event.getButton() == MouseEvent.BUTTON3)
				{
					int col = CustomTable.this.columnAtPoint(event.getPoint());
					CustomTable.this.getColumnModel().getColumn(col).setPreferredWidth(
							CustomTable.this.getColumnModel().getColumn(col).getPreferredWidth() - 40);
				}
			}
			
		});
	}

	public void tableChanged(TableModelEvent e)
	{
		int[] rowSelection = this.getSelectedRows();
		int columnSelection = this.getSelectedColumn();
		int numberOfRows = this.getRowCount();
		super.tableChanged(e);
		if (numberOfRows == this.getRowCount() && rowSelection.length == 1)
		{
			this.getSelectionModel().addSelectionInterval(rowSelection[0], rowSelection[0]);
		}
		if (columnSelection >= 0 && numberOfRows > 0 && rowSelection.length == 1)
		{
			this.setColumnSelectionInterval(columnSelection, columnSelection);
		}
	}
	
}
