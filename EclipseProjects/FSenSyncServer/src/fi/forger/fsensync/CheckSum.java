/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

public class CheckSum
{
    private MessageDigest myMd5;

    public CheckSum()
    {
        try
        {
            myMd5 = MessageDigest.getInstance("MD5");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void update(byte[] bytes, int count)
    {
        try
        {
            myMd5.update(bytes, 0, count);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public String getCheckSum()
    {
        String result = "";

        byte[] csum = myMd5.digest();

        for (int i = 0; i < csum.length; i++)
        {
            Byte b = csum[i];
            result += Integer.toHexString(b.intValue());
        }

        return result;
    }

    public static String getCheckSum(String filename)
    {
        String result = "";

        try {
            InputStream in =  new FileInputStream(filename);

            byte[] bytes = new byte[1024];
            MessageDigest md5 = MessageDigest.getInstance("MD5");

            int count;

            while ((count = in.read(bytes)) > 0)
            {
                md5.update(bytes, 0, count);
            }

            in.close();

            byte[] csum = md5.digest();

            for (int i = 0; i < csum.length; i++)
            {
                Byte b = csum[i];
                result += Integer.toHexString(b.intValue());
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return result;
    }
}
