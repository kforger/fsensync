/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

public class UploadsData extends SafeAbstractTableModel
{
	private static final long serialVersionUID = 4909298554883512680L;

	private SyncServer server;
	private final String[] columnNames = {"selection" ,"Type", "App Short ID", "Contact to app", "App state",
			"Tag 1", "Tag 2", "Tag 3", "Disk space (MB)", "Draw mode"};
	
	public UploadsData(SyncServer server)
	{
		this.server = server;
	}
	
	public static Boolean isUploadClient(AppInformation dev)
	{
		return dev.clientType.equals("STI") 
				|| dev.clientType.equals("CART") 
				|| dev.clientType.equals("VANNO")
				|| dev.clientType.equals("DEPLAY");
	}
	
	public int getColumnCount()
	{
		return columnNames.length;
	}
	
	public void setValueAt(Object value, int row, int col)
	{
		if (getRowCount() > row && col == 0)
		{
			getStimulusDevFromRow(row).isSelected = (Boolean)value;
		}
	}
	
	public void setSelectionByCell(int row, int column)
	{
		Object cellContent = getValueAt(row, column);
		if (cellContent != null && !(cellContent instanceof Boolean))
		{
			for (int i = 0; i < getRowCount(); i++)
			{
				if (cellContent.equals(getValueAt(i, column)))
				{
					getStimulusDevFromRow(i).isSelected = true;
				}
				else
				{
					getStimulusDevFromRow(i).isSelected = false;
				}
			}
		}
		for (int i = 0; i < server.clients.size(); i++)
		{
			if (!isUploadClient(server.clients.get(i)))
			{
				server.clients.get(i).isSelected = false;
			}
		}
		this.fireTableDataChanged();
	}
	
	private AppInformation getStimulusDevFromRow(int row)
	{
		AppInformation dev = null;
		int count = 0;
		for (int i = 0; i < server.clients.size(); i++)
		{
			if (isUploadClient(server.clients.get(i)))
			{
				if (count == row)
				{
					dev = server.clients.get(i);
				}
				count += 1;
			}
		}
		return dev;
	}

	public int getRowCount() {
		int count = 0;
		for (int i = 0; i < server.clients.size(); i++)
		{
			if (isUploadClient(server.clients.get(i)))
			{
				count += 1;
			}
		}
    	return count;
    }

	public Object getValueAt(int row, int col)
	{
		AppInformation dev = getStimulusDevFromRow(row);
		if (dev != null)
		{
			switch (col)
			{
			case 0: return dev.isSelected;
			case 1: return dev.clientType;
			case 2: return dev.shortId;
			case 3: return dev.timeFromLastUpdate();
			case 4: return server.appStates[dev.appState];
			case 5: return dev.tag.tags[0];
			case 6: return dev.tag.tags[1];
			case 7: return dev.tag.tags[2];
			case 8: 
    			if (dev.freeSpace == -1L)
    				return -1;
    			else
    				return (int)(dev.freeSpace / 1024 / 1024);
			case 9:
				return dev.getSetting("DRAW_MODE");
			default: return null;
			}
		}
		else
		{
			return null;
		}
	}
	
	public Class<?> getColumnClass(int c)
	{
		if (getRowCount() == 0)
		{
			return Object.class;
		}
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col)
    {
    	if (col == 0)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
	public String getColumnName(int col) {
        return columnNames[col];
    }

}
