/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoint;
import org.apache.commons.math3.fitting.WeightedObservedPoints;

public class FileManager extends SafeAbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	public static boolean cancelLongFileOperations = false;
	
	private SyncServer server;
	private final String[] columnNames = {
			"selection", "Name", "Size in bytes", "Short ID", 
			"Device ID", "Meta tag 1", "Meta tag 2", "Meta app type", "Rec num", "Device battery", "App state", "Downloaded" };
	private int currentRecordingNumber;
	private int recordingNumberSentToApps;
	
	public int uploadedNow = 0;

	public ArrayList<FileInformation> downloadList;
	public ArrayList<FileInformation> uploadList;
	
	public Boolean tableDataChanged;
	
	private ArrayList<MasterListItem> masterFileList;
	
	private ArrayList<KnownLag> lagList;
	
	public FileManager(SyncServer server)
	{
		this.server = server;
		loadRecordingNumber();
		downloadList = new ArrayList<FileInformation>();
		uploadList = new ArrayList<FileInformation>();
		tableDataChanged = true;
		masterFileList = new ArrayList<MasterListItem>();
		lagList = new ArrayList<KnownLag>();
		loadMasterFileList();
		combineFileLists();
		loadKnownLags();
	}
	
	private void loadKnownLags()
	{
		Boolean ok = true;
		File file = new File(StartUpWindow.settingsPath.getAbsolutePath() + File.separator + "knownLags.txt");
		if (file.exists())
		{
			lagList.clear();
			try
			{
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null)
				{
					String[] input = line.split(",");
					if (input.length == 3)
					{
						lagList.add(new KnownLag(input[0], input[1], Integer.parseInt(input[2])));
					}
					else
					{
						ok = false;
					}
				}
				br.close();
				if (!ok)
				{
					server.log("Badly formed lag list.");
				}
			} catch (FileNotFoundException e) 
			{
				server.log(e);
			} catch (IOException e) 
			{
				server.log(e);
			}
		}
	}
	
	private class KnownLag
	{
		public String deviceModel;
		public String appType;
		public int lag;
		
		public KnownLag(String deviceModel, String appType, int lag)
		{
			this.deviceModel = deviceModel;
			this.appType = appType;
			this.lag = lag;
		}
	}
	

	private int knowLagForFile(File file)
	{
		int lag = 0;
		
		File metaFile = new File(file.getAbsolutePath().replace(".csv", ".meta"));
		if (metaFile.exists())
		{
			String deviceModel = "none";
			String appType = "none";
			
			try 
			{
				BufferedReader br = new BufferedReader(new FileReader(metaFile));
				String line;
				while ((line = br.readLine()) != null)
				{
					if (line.startsWith("Device: "))
					{
						deviceModel = line.substring(8);
					}
					
					if (line.startsWith("App type: "))
					{
						appType = line.substring(10);
					}
				}
				br.close();
			}
			catch (IOException ex)
			{
				server.log(ex);
			}
			
			lag = knowLagForDeviceModel(deviceModel, appType);
		}
		
		return lag;
	}
	
	public int knowLagForDeviceModel(String deviceModel, String appType)
	{
		int lag = 0;
		if (lagList != null)
		{
			for (int i = 0; i < lagList.size(); i++)
			{
				KnownLag l = lagList.get(i);
				if (l.appType.equals(appType) && l.deviceModel.equals(deviceModel))
				{
					lag = l.lag;
				}
			}
		}
		return lag;
	}
	
	public void addToMasterFileList(String fileName, String uniqueId)
	{
		Boolean addedToList = false;
		MasterListItem item = new MasterListItem(fileName, uniqueId);
		if (!masterFileList.contains(item))
		{
			masterFileList.add(item);
			addedToList = true;
		}
		
		if (addedToList)
		{
			saveMasterFileList();
		}
		
		combineFileLists();
	}
	
	public void addToMasterFileList()
	{
		Boolean addedToList = false;
		for (int i = 0; i < downloadList.size(); i++)
		{
			MasterListItem item = new MasterListItem(downloadList.get(i).name, downloadList.get(i).app.uniqueId);
			if (!masterFileList.contains(item))
			{
				masterFileList.add(item);
				addedToList = true;
			}
		}
		
		if (addedToList)
		{
			saveMasterFileList();
		}
		
		combineFileLists();
	}
	
	public synchronized void combineFileLists()
	{
		// Add from masterFileList to downloadList
		for (int i = 0; i < masterFileList.size(); i++)
		{
			Boolean found = false;
			for (int k = 0; !found && k < downloadList.size(); k++)
			{
				if (masterFileList.get(i).name.equals(downloadList.get(k).name))
				{
					found = true;
				}
			}
			if (!found)
			{
				FileInformation info = new FileInformation(masterFileList.get(i).name,
						-1, server.findAppByUniqueId(masterFileList.get(i).uniqueId));
				File file = new File(server.getExperimentPath() + File.separator + "downloaded" + File.separator + info.name);
				if (file.exists())
				{
					info.transferState = FileInformation.FILE_STATE_TRANSFERRED;
					info.sizeInBytes = file.length();
				}
				downloadList.add(info);
			}
		}
	}
	
	private class MasterListItem
	{
		public String name;
		public String uniqueId;
		
		public MasterListItem(String name, String uniqueId)
		{
			this.name = name;
			this.uniqueId = uniqueId;
		}
		
		public boolean equals(Object other)
		{
		    if (other == null) return false;
		    if (other == this) return true;
		    if (other instanceof MasterListItem)
		    {
		    	MasterListItem otherMasterListItem = (MasterListItem)other;
		    	
		    	if (this.name.equals(otherMasterListItem.name))
		    	{
		    		return true;
		    	}
		    	else
		    	{
		    		return false;
		    	}
		    }
		    else
		    {
		    	return false;
		    }
		}
	}
	
	public void loadMasterFileList()
	{
		Boolean ok = true;
		File file = new File(server.getExperimentPath() + File.separator + "masterFileList.txt");
		if (file.exists())
		{
			masterFileList.clear();
			try
			{
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null)
				{
					String[] input = line.split(",");
					if (input.length == 2)
					{
						masterFileList.add(new MasterListItem(input[0], input[1]));
					}
					else
					{
						ok = false;
					}
				}
				br.close();
				if (!ok)
				{
					server.log("Badly formed master file list.");
				}
			} catch (FileNotFoundException e) 
			{
				server.log(e);
			} catch (IOException e) 
			{
				server.log(e);
			}
		}
	}
	
	public synchronized void saveMasterFileList()
	{
		PrintWriter masterFileListWriter;
		File listFile = new File(server.getExperimentPath() + File.separator + "masterFileList.txt");
		try {
			masterFileListWriter = new PrintWriter(new FileOutputStream(listFile, false));
			
			for (int i = 0; i < masterFileList.size(); i++)
			{
				masterFileListWriter.println(masterFileList.get(i).name + "," + masterFileList.get(i).uniqueId);
			}
			masterFileListWriter.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	public int getColumnCount()
	{
        return columnNames.length;
    }

	public int getRowCount()
	{
		return downloadList.size();
	}
	
	public String getColumnName(int col) {
        return columnNames[col];
    }

	public Object getValueAt(int row, int col) {
		if (downloadList.size() > row)
		{
    		switch (col)
    		{
    		case 0: return downloadList.get(row).isSelected;
    		case 1: return downloadList.get(row).name;
    		case 2: return (int)downloadList.get(row).sizeInBytes;
    		case 3: return downloadList.get(row).app.shortId;
    		case 4: return downloadList.get(row).app.deviceId;
    		case 5: return downloadList.get(row).metaTag1;
    		case 6: return downloadList.get(row).metaTag2;
    		case 7: return downloadList.get(row).metaAppType;
    		case 8: return downloadList.get(row).metaRecordingNumber;
    		case 9: return downloadList.get(row).app.batteryLevel;
    		case 10: return server.appStates[downloadList.get(row).app.appState];
    		case 11: 
    			if (downloadList.get(row).transferState == FileInformation.FILE_STATE_TRANSFERRED)
    			{
    				return "Yes";
    			}
    			if (downloadList.get(row).transferState == FileInformation.FILE_STATE_NOT_TRANSFERRED)
    			{
    				return "No" + downloadList.get(row).errorMessage;
    			}
    			if (downloadList.get(row).transferState == FileInformation.FILE_STATE_TRANSFER_NOW)
    			{
    				return "Downloading " + downloadList.get(row).percentageTransferReady + "%";
    			}
    		default: return null;
    		}
		}
		else
		{
			return null;
		}
	}
	
	public void selectAll()
	{
		for (int i = 0; i < downloadList.size(); i++)
		{
				downloadList.get(i).isSelected = true;
		}
		this.fireTableDataChanged();
	}
	
	public void selectNone()
	{
		for (int i = 0; i < downloadList.size(); i++)
		{
				downloadList.get(i).isSelected = false;
		}
		this.fireTableDataChanged();
	}
	
	public void selectMetaFiles()
	{
		for (int i = 0; i < downloadList.size(); i++)
		{
			if (downloadList.get(i).name.endsWith(".meta"))
			{
				downloadList.get(i).isSelected = true;
			}
			else
			{
				downloadList.get(i).isSelected = false;
			}
		}
	}
	
	public void setValueAt(Object value, int row, int col)
	{
		if (downloadList.size() > row && col == 0)
		{
			downloadList.get(row).isSelected = (Boolean)value;
		}
	}
	
    public boolean isCellEditable(int row, int col) {
    	if (col == 0)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    public void setSelectionByCell(int row, int column)
	{
		Object cellContent = getValueAt(row, column);
		if (cellContent != null && !(cellContent instanceof Boolean))
		{
			for (int i = 0; i < getRowCount(); i++)
			{
				if (cellContent.equals(getValueAt(i, column)))
				{
					downloadList.get(i).isSelected = true;
				}
				else
				{
					downloadList.get(i).isSelected = false;
				}
			}
		}
		this.fireTableDataChanged();
	}
    
    public void keepSelectionByCell(int row, int column)
	{
		Object cellContent = getValueAt(row, column);
		if (cellContent != null && !(cellContent instanceof Boolean))
		{
			for (int i = 0; i < getRowCount(); i++)
			{
				if (cellContent.equals(getValueAt(i, column)) && downloadList.get(i).isSelected)
				{
					downloadList.get(i).isSelected = true;
				}
				else
				{
					downloadList.get(i).isSelected = false;
				}
			}
		}
		this.fireTableDataChanged();
	}
	
	public Class<?> getColumnClass(int c) {
		if (downloadList.size() == 0)
		{
			return Object.class;
		}
        return getValueAt(0, c).getClass();
    }

    private synchronized AppInformation getNextFileListDev()
	{
    	AppInformation updateDev = null;
		for (int i = 0; i < server.clients.size(); i++)
		{
			AppInformation dev = server.clients.get(i);
			if (dev.isContacted()
					&& (dev.appState < AppInformation.APP_RECORDING ||
							dev.appState == AppInformation.APP_FILE_TRANSFER)
					&& dev.fileListUpdated == false
					&& dev.failedFileTransferCount < 3
					&& dev.activeConnection == false)
			{
				updateDev = dev;
			}
		}

		if (updateDev != null)
		{
			updateDev.activeConnection = true;
		}
		
		return updateDev;
	}
    
    private TimedRunnable fileListWorker()
	{
    	TimedRunnable runner = new TimedRunnable()
		{
			public void run()
			{
				AppInformation dev = null;

				while ((dev = getNextFileListDev()) != null)
				{
					if (dev.isContacted()
							&& (dev.appState < AppInformation.APP_RECORDING ||
									dev.appState == AppInformation.APP_FILE_TRANSFER)
							&& dev.fileListUpdated == false)
					{

						try
						{
							Socket socket = getTimedSocket(dev.ip, dev.getTcpPort());
							BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
							PrintWriter writer = new PrintWriter(socket.getOutputStream());

							writer.println("Give list of files.");
							writer.println(server.getExperimentName());
							writer.flush();
							String reply;
							int state = 0;
							while((reply = reader.readLine()) != null && state < 2)
							{
								this.delaySocketClosingTimeout();
								switch (state)
								{
								case 0:
									if (!reply.equals("FILES START"))
									{
										server.log("Invalid file transfer message:" + reply);
									}
									else
									{
										state = 1;
									}
									break;
								case 1:
									if (reply.equals("FILES END"))
									{
										state = 2;
										dev.fileListUpdated = true;
									}
									else
									{
										long length = Long.valueOf(reader.readLine());
										FileInformation info = new FileInformation(reply, length, dev);
										downloadList.add(info);

										File file = new File(server.getExperimentPath() + File.separator + "downloaded" + File.separator + info.name);
										if (file.exists() && file.length() == info.sizeInBytes)
										{
											info.transferState = FileInformation.FILE_STATE_TRANSFERRED;
										}
									}
									break;
								default:
									server.log("Error in file transfer connection.", true);
									break;
								}
							}
							reader.close();
							writer.close();
							socket.close();

						}
						catch (SocketTimeoutException ex)
						{
							server.log("Device at ip " + dev.ip + " not reached.", true);
							//server.log(ex);
						}
						catch (IOException ex)
						{
							server.log("Device at ip " + dev.ip + " did not accept connection.", true);
							//server.log(ex);
						}
						
						if (dev.fileListUpdated == false)
						{
							dev.failedFileTransferCount += 1;
						}
						
						dev.activeConnection = false;
					}

				}
			}
		};
		return runner;
	}
    
    private void sortDownloadedList()
    {
    	Collections.sort(downloadList, new Comparator<FileInformation>() {
    	        @Override
    	        public int compare(FileInformation info1, FileInformation info2)
    	        {
    	        	if (info1.transferState == info2.transferState)
    	        	{
    	        		return  info1.name.compareTo(info2.name);
    	        	}
    	        	else
    	        	{
    	        		return info1.transferState - info2.transferState;
    	        	}
    	        }
    	    });
    	
    	if (downloadList.size() > 0)
    	{
    		int i = 0;
	    	while (i + 1 < downloadList.size())
	    	{
	    		if (downloadList.get(i).name.equals(downloadList.get(i+1).name))
	    		{
	    			// Preserving the file entry where file type and app type matches
	    			if (downloadList.get(i).name.contains("_" + downloadList.get(i+1).app.clientType + "."))
	    			{
	    				downloadList.remove(i);
	    			}
	    			else
	    			{
	    				downloadList.remove(i+1);
	    			}
	    		}
	    		else
	    		{
	    			i += 1;
	    		}
	    	}
    	}
    }
    
    private Runnable getTaskFileListsRunner()
    {
    	Runnable runner = new Runnable()
		{
			public void run()
			{
				for (int i = 0; i < server.clients.size(); i++)
				{
					server.clients.get(i).fileListUpdated = false;
					server.clients.get(i).failedFileTransferCount = 0;
				}

				ArrayList<String> selectedFiles = new ArrayList<String>();
				for (int i = 0; i < downloadList.size(); i++)
				{
					if (downloadList.get(i).isSelected)
					{
						selectedFiles.add(downloadList.get(i).name);
					}	
				}
				
				ArrayList<FileInformation> oldDownloadList = downloadList;
				
				downloadList = new ArrayList<FileInformation>();

				int threadCount = 3;
				Thread threads[] = new Thread[threadCount];
				for (int i = 0; i < threadCount; i++)
				{
					threads[i] = new Thread(fileListWorker());
					threads[i].start();
				}
				try {
					for (int i = 0; i < threadCount; i++)
					{
						threads[i].join();
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				
				sortDownloadedList();
				addToMasterFileList();
				sortDownloadedList();
				
				// Keeping same selected files
				for (int i = 0; i < downloadList.size(); i++)
				{
					for (int k = 0; k < selectedFiles.size(); k++)
					{
						if (downloadList.get(i).name.equals(selectedFiles.get(k)))
						{
							downloadList.get(i).isSelected = true;
						}
					}
				}
				
				// Keeping previously read meta data
				for (int i = 0; i < downloadList.size(); i++)
				{
					FileInformation newFile = downloadList.get(i);
					for (int k = 0; k < oldDownloadList.size(); k++)
					{
						FileInformation oldFile = oldDownloadList.get(k);
						if (newFile.name.equals(oldFile.name))
						{
							newFile.metaTag1 = oldFile.metaTag1;
							newFile.metaTag2 = oldFile.metaTag2;
							newFile.metaTag3 = oldFile.metaTag3;
							newFile.metaAppType = oldFile.metaAppType;
							newFile.metaRecordingNumber = oldFile.metaRecordingNumber;
						}
					}
				}
				
				fireTableDataChanged();
				
				server.updateGuiTaskResult(", downloaded " + numDownloadedFromList() + "/" + downloadList.size());
				server.setState(SyncServer.STATE_READY);
			}
		};
		return runner;
    }
    
	public void taskGetFileLists()
	{
		if (server.serverState == SyncServer.STATE_READY)
		{
			cancelLongFileOperations = false;
			server.setState(SyncServer.STATE_DOWNLOADING);
			server.updateGuiTaskResult("");
			Thread thread = new Thread(getTaskFileListsRunner());
			thread.start();
		}
	}
	
	private int numDownloadedFromList()
	{
		int numDownloaded = 0;
		for (int i = 0; i < downloadList.size(); i++)
		{
			if (downloadList.get(i).transferState == FileInformation.FILE_STATE_TRANSFERRED)
			{
				numDownloaded += 1;
			}
		}
		return numDownloaded;
	}
	
	private int numDownloadedFromSelected()
	{
		int numDownloaded = 0;
		for (int i = 0; i < downloadList.size(); i++)
		{
			if (downloadList.get(i).transferState == FileInformation.FILE_STATE_TRANSFERRED && downloadList.get(i).isSelected)
			{
				numDownloaded += 1;
			}
		}
		return numDownloaded;
	}
	
	private int numSelected()
	{
		int numSelected = 0;
		for (int i = 0; i < downloadList.size(); i++)
		{
			if (downloadList.get(i).isSelected)
			{
				numSelected += 1;
			}
		}
		return numSelected;
	}
	
	private Runnable getDownloadFilesRunner(final Boolean onlySmallFiles, final Boolean onlySelected)
	{
		Runnable runner = new Runnable()
		{
			public void run()
			{
				getTaskFileListsRunner().run();
				
				server.setState(SyncServer.STATE_DOWNLOADING);
				
				for (int i = 0; i < server.clients.size(); i++)
				{
					server.clients.get(i).failedFileTransferCount = 0;
				}

				int threadCount = 3;
				Thread threads[] = new Thread[threadCount];
				for (int i = 0; i < threadCount; i++)
				{
					threads[i] = new Thread(fileDownloadWorker(onlySmallFiles, onlySelected));
					threads[i].start();
				}
				try {
					for (int i = 0; i < threadCount; i++)
					{
						threads[i].join();
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				
				sortDownloadedList();
				fireTableDataChanged();
				
				if (onlySelected)
				{
					server.updateGuiTaskResult(", downloaded " + numDownloadedFromSelected() + "/" + numSelected());
				}
				else
				{
					server.updateGuiTaskResult(", downloaded " + numDownloadedFromList() + "/" + downloadList.size());
				}
				server.setState(SyncServer.STATE_READY);
				server.taskRefreshApps(false);
			}
		};
		return runner;
	}
	
	private synchronized FileInformation getNextFileToBeDownloaded(Boolean onlySmallFiles, Boolean onlySelected)
	{
		FileInformation downloadThis = null;
		for (int i = 0; i < downloadList.size(); i++)
		{
			FileInformation info = downloadList.get(i);
			AppInformation dev = info.app;
			if (dev.isContacted()
					&& (dev.appState < AppInformation.APP_RECORDING ||
							dev.appState == AppInformation.APP_FILE_TRANSFER)
					&& info.transferState == FileInformation.FILE_STATE_NOT_TRANSFERRED
					&& info.app.failedFileTransferCount < 3
					&& info.app.activeConnection == false
					&& info.sizeInBytes != -1)
			{
				if (onlySmallFiles)
				{
					if (info.sizeInBytes < 100000000L)
					{
						downloadThis = info;
					}
				}
				else
				{
					if (onlySelected)
					{
						if (info.isSelected)
						{
							downloadThis = info;
						}
					}
					else
					{
						downloadThis = info;
					}
				}
			}
		}

		if (downloadThis != null)
		{
			downloadThis.transferState = FileInformation.FILE_STATE_TRANSFER_NOW;
			downloadThis.percentageTransferReady = 0;
			downloadThis.app.activeConnection = true;
		}
		return downloadThis;
	}
	
	private TimedRunnable fileDownloadWorker(final Boolean onlySmallFiles, final Boolean onlySelected)
	{
		TimedRunnable runner = new TimedRunnable()
		{
			public void run()
			{
				FileInformation info;
				
				while ((info = getNextFileToBeDownloaded(onlySmallFiles, onlySelected)) != null)
				{
					AppInformation dev = info.app;

					File downloadedFile = null;
					
					try
					{
						Socket socket = getTimedSocket(dev.ip, dev.getTcpPort());
						PrintWriter writer = new PrintWriter(socket.getOutputStream());

						writer.println("Give file.");
						writer.println(info.name);
						writer.flush();
						
						File path = new File(server.getExperimentPath() + File.separator + "downloaded");
			            path.mkdir();
			            downloadedFile = new File(path.getAbsolutePath() + File.separator + info.name);
			            FileOutputStream fileOut = new FileOutputStream(path.getAbsolutePath() + File.separator + info.name);
						
						InputStream in = socket.getInputStream();
						byte[] bytes = new byte[16*1024];
			            int count;
			            int accumCount = 0;
			            int lastPercentage = -1;
						while ((count = in.read(bytes)) > 0) {
							fileOut.write(bytes, 0, count);
							this.delaySocketClosingTimeout();
							accumCount += count;
							info.percentageTransferReady = (int)(((double)accumCount / (double)info.sizeInBytes) * 100.0);
							if (lastPercentage < info.percentageTransferReady)
							{
								lastPercentage = info.percentageTransferReady;
								fireTableDataChanged();
							}
						}
						
						in.close();
						fileOut.close();
						writer.close();
						socket.close();
						
						if (downloadedFile.exists() && downloadedFile.length() == info.sizeInBytes)
						{
							info.transferState = FileInformation.FILE_STATE_TRANSFERRED;
							info.errorMessage = "";
							syncFiles(new File[]{downloadedFile}, false);
						}
						else
						{
							if (!downloadedFile.exists())
							{
								server.log("File not downloaded: " + info.name);
								info.app.failedFileTransferCount += 1;
							}
							else
							{
								server.log("Wrong file size:" + info.name + " " + downloadedFile.length() +  "!=" + info.sizeInBytes);
								downloadedFile.delete();
								info.app.failedFileTransferCount += 1;
							}
						}
					}
					catch (SocketTimeoutException ex)
					{
						server.log("Device at ip " + dev.ip + " not reached.", true);
						info.errorMessage = ", device not reached";
						info.app.failedFileTransferCount += 1;
						if (downloadedFile != null && downloadedFile.exists())
						{
							downloadedFile.delete();
						}
					}
					catch (IOException ex)
					{
						if (cancelLongFileOperations)
						{
							info.app.failedFileTransferCount += 3;
							server.log("Cancelled in file download: " + info.name, true);
						}
						else
						{
							server.log("Error in file download: " + info.name, true);
						}
						info.app.failedFileTransferCount += 1;
						info.errorMessage = ", device not reached";
						//server.log(ex);
						if (downloadedFile != null && downloadedFile.exists())
						{
							downloadedFile.delete();
						}
					}
					if (info.transferState == FileInformation.FILE_STATE_TRANSFER_NOW)
					{
						info.transferState = FileInformation.FILE_STATE_NOT_TRANSFERRED;
					}
					fireTableDataChanged();
					info.app.activeConnection = false;
					server.updateGuiTaskResult(" " + numDownloadedFromList() + "/" + downloadList.size());
				}
			}
		};
		return runner;
	}
	
	public void taskDownloadFiles(Boolean onlySmallFiles, Boolean onlySelected)
	{
		if (server.serverState == SyncServer.STATE_READY)
		{
			cancelLongFileOperations = false;
			server.setState(SyncServer.STATE_DOWNLOADING);
			server.updateGuiTaskResult("");
			Thread thread = new Thread(getDownloadFilesRunner(onlySmallFiles, onlySelected));
			thread.start();
		}
	}
	
    public void nextRecordingNumber()
    {
    	currentRecordingNumber = Math.max(currentRecordingNumber, recordingNumberSentToApps + 1);
    	PrintWriter output;
		try {
			File file = new File(server.getExperimentPath() + File.separator + ".recordingCount.txt");
			output = new PrintWriter(file, "UTF-8");
			output.println(currentRecordingNumber);
			output.println(recordingNumberSentToApps);
            output.close();
		} catch (Exception e) {
			server.log(e);
		}
		if (server.gui != null)
        {
        	server.gui.setCurrentRecording(currentRecordingNumber);
        }
    }
    
    private void loadRecordingNumber()
    {
    	int recordingNumberStopped = 1;
    	int recordingNumberStarted = 0;
        File file = new File(server.getExperimentPath() + File.separator + ".recordingCount.txt");
        if (file.exists()) {
            try
            {
            	BufferedReader br = new BufferedReader(new FileReader(file));
            	recordingNumberStopped = Integer.valueOf(br.readLine());
            	recordingNumberStarted = Integer.valueOf(br.readLine());
            	br.close();
            }
            catch (Exception e) 
            {
				server.log(e);
			}
        }

    	PrintWriter output;
		try 
		{
			output = new PrintWriter(file, "UTF-8");
			output.println(recordingNumberStopped);
			output.println(recordingNumberStarted);
            output.close();
		} catch (FileNotFoundException e) {
			server.log(e);
		} catch (UnsupportedEncodingException e) {
			server.log(e);
		}
			
        currentRecordingNumber = recordingNumberStopped;
        recordingNumberSentToApps = recordingNumberStarted;
        if (server.gui != null)
        {
        	server.gui.setCurrentRecording(currentRecordingNumber);
        }
    }
    
    public int getCurrentRecordingNumber() {
		return currentRecordingNumber;
	}

	public void setRecordingNumberSentToApps(int minAtApps)
	{
		currentRecordingNumber = Math.max(currentRecordingNumber, minAtApps);
		if (server.gui != null)
        {
        	server.gui.setCurrentRecording(currentRecordingNumber);
        }
		recordingNumberSentToApps = currentRecordingNumber;
		PrintWriter output;
		try {
			File file = new File(server.getExperimentPath() + File.separator + ".recordingCount.txt");
			output = new PrintWriter(file, "UTF-8");
			output.println(currentRecordingNumber);
			output.println(recordingNumberSentToApps);
            output.close();
		} catch (FileNotFoundException e) {
			server.log(e);
		} catch (UnsupportedEncodingException e) {
			server.log(e);
		}
	}
	
	private Boolean syncGenericCsvFile(File file, File outPath) 
	{
		Boolean ok = true;
		try 
		{
			int knownLag = knowLagForFile(file);
			
			ArrayList<RecordLine> list = new ArrayList<RecordLine>();
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			String header = "Timestamp(milliseconds)";
			int origHeaderLength = 0;
			int headerColumnCount = 0;
			
			// Header line
			if ((line = br.readLine()) != null)
			{
				String[] v = line.split(",");
				headerColumnCount = v.length;
				if (v.length > 3)
				{
					origHeaderLength = v.length;
					for (int i = 3; i < v.length; i++)
					{
						header = header + "," + v[i];
					}
				}
			}
			String prevLine = null;
			
			while ((line = br.readLine()) != null)
			{
				int comma1 = line.indexOf(",");
				int comma2 = line.indexOf(",", comma1 + 1);
				int comma3 = line.indexOf(",", comma2 + 1);
				
				boolean lineOk = true;
				
				if (comma1 <= 0 || comma2 <= 0 || comma3 <= 0 || !br.ready())
				{
					if (line.split(",").length != headerColumnCount)
					{
						lineOk = false;
						if (comma3 == -1 && line.endsWith(".0000"))
						{
							// a sync line
						}
						else
						{
							server.log("Removed a partial line from file during syncing: " + file.getName());
						}
					}
				}
				
				if (lineOk)
				{
					String otherValues = line.substring(comma3, line.length());
					list.add(new RecordLine(
							Long.valueOf(line.substring(0, comma1)), 
							Long.valueOf(line.substring(comma1+1, comma2)), 
							Long.valueOf(line.substring(comma2+1, comma3)),
							otherValues));
				}
				
				prevLine = line;
			}
			
			// Checking for a post-sync related last line
			if (prevLine != null)
			{
				int comma1 = prevLine.indexOf(",");
				int comma2 = prevLine.indexOf(",", comma1 + 1);
				int comma3 = prevLine.indexOf(",", comma2 + 1);
				
				if (comma1 > 0 && comma2 > 0 && comma3 == -1)
				{
					if (prevLine.endsWith(".0000"))
					{
					list.add(new RecordLine(
							Long.valueOf(prevLine.substring(0, comma1)), 
							Long.valueOf(prevLine.substring(comma1+1, comma2).replace(".0000", "")), 
							Long.valueOf(prevLine.substring(comma2+1, prevLine.length()).replace(".0000", ""))));
					}
					else
					{
						server.log("Removed a partial last line from file during syncing: " + file.getName());
					}
				}
			}
			
			br.close();
			
			calculateLinearSync((ArrayList)list, file.getName(), knownLag);

			File outFile = new File(outPath.getAbsolutePath() + File.separator + file.getName());
			PrintWriter writer = new PrintWriter(outFile, "UTF-8");
			writer.println(header);
			for (int i = 0; i < list.size(); i++)
			{
				RecordLine r = list.get(i);
				if (r.otherValues != null)
				{
				writer.println(r.syncedTime + r.otherValues);
				}
			}
			writer.close();
		}
		catch (IOException e)
		{
			ok = false;
			server.log("Error in reading: " + file.getName());
			server.log(e);
		}
		catch (Exception e)
		{
			ok = false;
			server.log("Error in reading: " + file.getName());
			server.log(e);
		}

		return ok;
	}
	
	private Boolean syncVProFile(File vproFile, File outPath)
	{
		Boolean ok = true;
		try 
		{
			ArrayList<RecordLine> list = new ArrayList<RecordLine>();
			BufferedReader br = new BufferedReader(new FileReader(vproFile));
			String line;
			String header = "Timestamp(milliseconds)";
			int origHeaderLength = 0;
			
			// Header line
			if ((line = br.readLine()) != null)
			{
				String[] v = line.split(",");
				if (v.length > 3)
				{
					origHeaderLength = v.length;
					for (int i = 3; i < v.length; i++)
					{
						header = header + "," + v[i];
					}
				}
			}
			
			while ((line = br.readLine()) != null)
			{
				String[] v = line.split(",");
				String otherValues = line.substring(v[0].length() + v[1].length() + v[2].length() + 2, line.length());
				if (v.length == origHeaderLength)
				{
					list.add(new RecordLine(Long.valueOf(v[0]), Long.valueOf(v[1]), Long.valueOf(v[2]),
							otherValues));
				}
			}
			br.close();

			calculateLinearSync((ArrayList)list, vproFile.getName(), 0);
	
			// Corrections to the 1 second steps in the timestamps
			{
				long timeAtStart = list.get(0).syncedTime;
	
				WeightedObservedPoints obs = new WeightedObservedPoints();
				for (int i = 0; i < list.size(); i++)
				{
					if (i < list.size() - 1 && list.get(i).syncedTime != list.get(i+1).syncedTime)
					{
						// Here we work only with the last samples that have the same timestamp
						// as those should have the minimal amount of lag.
						RecordLine r = list.get(i);
						double index = i;
						double time = (double)(r.syncedTime - timeAtStart);
						obs.add(new WeightedObservedPoint(1.0, index, time));
					}
				}
	
				PolynomialCurveFitter fitter = PolynomialCurveFitter.create(1);
				double[] coeff = fitter.fit(obs.toList());
				
				for (int i = 0; i < list.size(); i++)
				{
					RecordLine r = list.get(i);
					r.syncedTime = ((long)((double)(i) * coeff[1]) + (long)(coeff[0])) + timeAtStart;
				}
			}
			
			// Writing the data to a file 
			
			NumberFormat f = NumberFormat.getInstance(Locale.ROOT);
			f.setGroupingUsed(false);
			f.setMinimumIntegerDigits(1);
			f.setMinimumFractionDigits(1);
			f.setMaximumFractionDigits(10);

			File outFile = new File(outPath.getAbsolutePath() + File.separator + vproFile.getName());
			PrintWriter writer = new PrintWriter(outFile, "UTF-8");
			writer.println(header);
			for (int i = 0; i < list.size(); i++)
			{
				RecordLine r = list.get(i);
				writer.println(r.syncedTime + r.otherValues);
			}
			writer.close();
		}
		catch (IOException e)
		{
			ok = false;
			server.log("Error in reading: " + vproFile.getName());
			server.log(e);
		}
		catch (Exception e)
		{
			ok = false;
			server.log("Error in reading: " + vproFile.getName());
			server.log(e);
		}

		return ok;
	}
	
	public void syncFiles(File[] files, boolean guiMessages)
	{
		File outPath = new File(server.getExperimentPath() + File.separator + "synced");
		if (!outPath.exists())
		{
			outPath.mkdir();
		}
		
		int numSynced = 0;
		int numFiles = 0;
		if (files != null)
		{
			for (int fileInd = 0; fileInd < files.length; fileInd++)
			{
				if (files[fileInd].isFile() && !files[fileInd].isDirectory())
				{
					numFiles += 1;
				}
			}
			
			for (int fileInd = 0; fileInd < files.length; fileInd++)
			{
				if (files[fileInd].isFile() && !files[fileInd].isDirectory())
				{
					Boolean ok = false;
					Boolean foundMatch = false;
					
					String[] genericTypes = {"ACC", "STI","VANNO", 
							"TOUCH", "VID", "CART", "BIO", "EDA", 
							"ECG", "DEPLAY", "SERIAL", "PROC", "OTHER"};
					
					for (int i = 0; i < genericTypes.length && foundMatch == false; i++)
					{
						if (files[fileInd].getName().matches(".+_" + genericTypes[i] + "\\.csv"))
						{
							foundMatch = true;
							ok = syncGenericCsvFile(files[fileInd], outPath);
						}
					}
					
					if (foundMatch == false && files[fileInd].getName().matches(".+_VPRO\\.csv"))
					{
						foundMatch = true;
						ok = syncVProFile(files[fileInd], outPath);
					}
					
					if (files[fileInd].getName().matches(".+\\.meta"))
					{
						foundMatch = true;
						
						try
						{
							File outFile = new File(outPath.getAbsolutePath() + File.separator + files[fileInd].getName());
							Files.copy(files[fileInd].toPath(), outFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
							ok = true;
						} 
						catch (IOException e)
						{
							server.log(e);
							ok = false;
						}
					}
					
					if (foundMatch == false 
							&& ((files[fileInd].getName().matches(".+\\.mp4"))
									|| (files[fileInd].getName().matches(".+_BIOACC\\.csv"))
									|| (files[fileInd].getName().matches(".+_BIOBRE\\.csv"))
									|| (files[fileInd].getName().matches(".+_BIOSUM\\.csv"))
									|| (files[fileInd].getName().matches(".+_BIOECG\\.csv")))
									)
					{
						foundMatch = true;
						ok = true;
						// Nothing to do with these files
					}

					if (foundMatch == false)
					{
						server.log("Could not recognize the type of the file: " + files[fileInd].getName());
					}

					if (guiMessages)
					{
						if (ok)
						{
							numSynced += 1;
							if (fileInd + 1 == files.length)
							{
								server.updateGuiTaskResult(", synced " + numSynced + " / " + numFiles);
							}
							else
							{
								server.updateGuiTaskResult(" " + numSynced + " / " + numFiles);
							}
						}
						else
						{
							server.updateGuiTaskResult(", Error in reading file.");
						}
					}
				}
			}
		}
		else
		{
			if (guiMessages)
			{
				server.updateGuiTaskResult(", synced " + numSynced + " / " + 0);
			}
		}
	}
	
	public void taskSyncDownloadedFiles()
	{
		Runnable runner = new Runnable()
		{
			public void run()
			{
				File path = new File(server.getExperimentPath() + File.separator + "downloaded");
				File[] files = path.listFiles();
				
				syncFiles(files, true);
				
				server.setState(SyncServer.STATE_READY);
				
			}
		};
		if (server.serverState == SyncServer.STATE_READY)
		{
			cancelLongFileOperations = false;
			server.setState(SyncServer.STATE_CALCULATING_SYNC);
			server.updateGuiTaskResult("");
			Thread thread = new Thread(runner);
			thread.start();
		}
	}
	private class RecordLine
	{
		long time;
		long upper;
		long lower;
		long syncedTime;
		String otherValues;
		
		public RecordLine(long time, long upper, long lower)
		{
			this.time = time;
			this.upper = upper;
			this.lower = lower;
			syncedTime = 0L;
		}
		
		public RecordLine(long time, long upper, long lower, String otherValues)
		{
			this(time, upper, lower);
			this.otherValues = otherValues;
		}
	}
	
	private void calculateLinearSync(ArrayList<RecordLine> list, String fileName, int knownLag)
	{
		long timeAtStart = 0;
		
		if (list.size() > 0)
		{
			timeAtStart = list.get(0).time;
			double[] coeff = {0.0, 0.0};
			
			WeightedObservedPoints obs = new WeightedObservedPoints();
			long lastUpper = list.get(0).upper;
			long lastLower = list.get(0).lower;
			long lastStamp = list.get(0).time;
			Boolean firstJump = true;
			for (int i = 0; i < list.size(); i++)
			{
				RecordLine r = list.get(i);
				if (lastUpper != r.upper || lastLower != r.lower || i == 0)
				{
					double weight = Math.max(100000.0 - (r.upper - r.lower), 0.0);
					double time = (double)(r.time - timeAtStart);
					double value = (r.upper + r.lower) / 2.0;
					long worstCaseDrift = (long)((r.time - lastStamp) * (28.0/1000000.0));
					lastStamp = r.time;
					if (r.lower >  lastUpper + 5000 + worstCaseDrift || r.upper < lastLower - 5000 - worstCaseDrift)
					{
						if (firstJump)
						{
							server.log("Unlikely jump in timing in file '" + fileName + "' at line " + (i+1));
						}
						firstJump = false;
					}
					else
					{
						lastUpper = r.upper;
						lastLower = r.lower;
						obs.add(new WeightedObservedPoint(weight, time, value));
					}
				}
			}

			PolynomialCurveFitter fitter = PolynomialCurveFitter.create(1);
			coeff = fitter.fit(obs.toList());
			
			for (int i = 0; i < list.size(); i++)
			{
				RecordLine r = list.get(i);
				r.syncedTime = ((r.time + (long)((r.time - timeAtStart) * coeff[1]) + (long)(coeff[0])) / 1000l) - (long)knownLag;
			}
		}
	}

	public void taskUploadFile(File[] selectedFiles, Boolean isTouchSound, String nameOnDevice)
	{
		if (server.serverState == SyncServer.STATE_READY)
		{
			cancelLongFileOperations = false;
			server.setState(SyncServer.STATE_UPLOADING);
			server.updateGuiTaskResult("");
			Thread thread = new Thread(getUploadFilesRunner(selectedFiles, isTouchSound, nameOnDevice));
			thread.start();
		}
	}

	private synchronized FileInformation getNextFileToBeUploaded()
	{
		FileInformation uploadThis = null;
		for (int i = 0; i < uploadList.size(); i++)
		{
			FileInformation info = uploadList.get(i);
			AppInformation dev = info.app;
			if (dev.isContacted()
					&& (dev.appState < AppInformation.APP_RECORDING ||
							dev.appState == AppInformation.APP_FILE_TRANSFER)
					&& info.transferState == FileInformation.FILE_STATE_NOT_TRANSFERRED
					&& info.app.failedFileTransferCount < 3
					&& info.app.activeConnection == false)
			{
				uploadThis = info;
			}
		}

		if (uploadThis != null)
		{
			uploadThis.transferState = FileInformation.FILE_STATE_TRANSFER_NOW;
			uploadThis.percentageTransferReady = 0;
			uploadThis.app.activeConnection = true;
		}
		return uploadThis;
	}
	
	private int totalUploadPercentage()
	{
		double bytesUploaded = 0;
		double totalBytes = 0;
		
		for (int i = 0; i < uploadList.size(); i++)
		{
			FileInformation info = uploadList.get(i);
			bytesUploaded += (int)(((double)info.percentageTransferReady / 100.0) * info.sizeInBytes);
			totalBytes += info.sizeInBytes;
		}
		
		if (totalBytes > 0)
		{
			return (int)((bytesUploaded / totalBytes) * 100);
		}
		else
		{
			return 0;
		}
	}
	
	private TimedRunnable fileUploadWorker()
	{
		TimedRunnable runner = new TimedRunnable()
		{
			public void run()
			{
				FileInformation info;
				
				while ((info = getNextFileToBeUploaded()) != null)
				{
					AppInformation dev = info.app;
					
					String checkSum = "";
					if (info.file.exists())
					{
						checkSum = CheckSum.getCheckSum(info.file.getAbsolutePath());
					}

					try
					{
						Socket socket = getTimedSocket(dev.ip, dev.getTcpPort());
						PrintWriter writer = new PrintWriter(socket.getOutputStream());
						BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
						
						Boolean receivedOk = false;
						
						writer.println("Receive stimulus file.");
						
						if (info.isTouchSound)
						{
							writer.println("Is touch sound file.");
						}
						else
						{
							writer.println("Not touch sound file.");
						}
						
						if (info.nameOnDevice == null || info.nameOnDevice.length() == 0)
						{
							writer.println(info.name);
						}
						else
						{
							writer.println(info.nameOnDevice);
						}
						
						writer.println(info.sizeInBytes);
						
						writer.println(checkSum);
						writer.flush();
						
						String readyLine = reader.readLine();
						while(readyLine != null && readyLine.equals("WAIT FOR IT."))
						{
							this.delaySocketClosingTimeout();
							server.log("Waiting for a slow MD5 check from a client.");
							readyLine = reader.readLine();
						}
						
						if (readyLine.equals("READY TO RECEIVE"))
						{
							if (info.file.exists())
							{
								byte[] bytes = new byte[16 * 1024];
								InputStream in = new FileInputStream(info.file);
								OutputStream out = socket.getOutputStream();
								int count;
								int accumCount = 0;
								int lastPercentage = -1;
								while ((count = in.read(bytes)) > 0) {
									out.write(bytes, 0, count);
									this.delaySocketClosingTimeout();
									accumCount += count;
									info.percentageTransferReady = (int)(((double)accumCount / (double)info.sizeInBytes) * 100.0);
									if (lastPercentage < info.percentageTransferReady)
									{
										lastPercentage = info.percentageTransferReady;
										server.updateGuiTaskResult(", " + totalUploadPercentage() + "%");
										//		+ ", " + info.percentageTransferReady + "%");
										//fireTableDataChanged();
									}
								}
								
								out.flush();
								
								String reply = reader.readLine();
								if (reply != null)
								{
									if (reply.equals("RECEIVED OK"))
									{
										receivedOk = true;
									}
									else
									{
										server.log("Failed to upload file due to mismatch: " + info.name);
									}
								}
								
								out.close();
								in.close();
							} else {
								server.log("Failed to upload a non-existing file: " + info.name);
							}
						}
						else
						{
							if (readyLine.equals("ALREADY HAD FILE"))
							{
								info.percentageTransferReady = 100;
								receivedOk = true;
							}
							else
							{
								server.log("Did not get a 'ready to receive' message.", true);
							}
						}
						
						
						if (info.percentageTransferReady == 100 && receivedOk)
						{
							info.transferState = FileInformation.FILE_STATE_TRANSFERRED;
							info.errorMessage = "";
							increaseUploadCount();
							server.log("File upload success.", false);
						}
						else
						{
							info.percentageTransferReady = 0;
							dev.failedFileTransferCount += 1;
						}
						
						writer.close();
						reader.close();
						socket.close();
					}
					catch (SocketTimeoutException ex)
					{
						server.log("Device at ip " + dev.ip + " not reached.", true);
						//info.errorMessage = ", device not reached";
						dev.failedFileTransferCount += 1;
						info.percentageTransferReady = 0;
					}
					catch (IOException ex)
					{
						if (cancelLongFileOperations)
						{
							dev.failedFileTransferCount += 3;
							server.log("Cancelled in file upload: " + info.name, true);
						}
						else
						{
							server.log("Error in file upload: " + info.name + " to ip " + dev.ip, true);
						}
						info.percentageTransferReady = 0;
						dev.failedFileTransferCount += 1;
						//server.log(ex);
					}
					
					if (info.transferState == FileInformation.FILE_STATE_TRANSFER_NOW)
					{
						info.transferState = FileInformation.FILE_STATE_NOT_TRANSFERRED;
					}
					
					dev.activeConnection = false;
					
					server.updateGuiTaskResult(", " + totalUploadPercentage() + "%");
					
				}
			}
		};
		return runner;
	}
	
	private synchronized void increaseUploadCount()
	{
		uploadedNow += 1;
	}
	
	
	private Runnable getUploadFilesRunner(final File[] selectedFiles, final Boolean isTouchSound, final String nameOnDevice) {
		Runnable runner = new Runnable()
		{
			public void run()
			{
				server.setState(SyncServer.STATE_UPLOADING);
				
				for (int i = 0; i < server.clients.size(); i++)
				{
					server.clients.get(i).failedFileTransferCount = 0;
				}
				
				uploadList.clear();
				
				for (int i = 0; i < server.clients.size(); i++)
				{
					if (UploadsData.isUploadClient(server.clients.get(i))
							&& server.clients.get(i).isContactedAndSelected())
					{
						for (int s = 0; s < selectedFiles.length; s++)
						{
							uploadList.add(new FileInformation(selectedFiles[s], server.clients.get(i), isTouchSound, nameOnDevice));
						}
					}
				}
				
				File pathBackups = new File(server.getExperimentPath() + File.separator + "uploadBackups");
				pathBackups.mkdir();
				
				for (int s = 0; s < selectedFiles.length; s++)
				{
					try
					{
						File dest = new File(pathBackups.getAbsolutePath()  + File.separator + selectedFiles[s].getName());
						Files.copy(selectedFiles[s].toPath(), dest.toPath());
					}
					catch (IOException ex)
					{
						
					}
				}
				
				uploadedNow = 0;
				
				int threadCount = 3;
				Thread threads[] = new Thread[threadCount];
				for (int i = 0; i < threadCount; i++)
				{
					threads[i] = new Thread(fileUploadWorker());
					threads[i].start();
				}
				try {
					for (int i = 0; i < threadCount; i++)
					{
						threads[i].join();
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				
				server.updateGuiTaskResult(", uploaded " + totalUploadPercentage() + "%");
				server.setState(SyncServer.STATE_READY);
				server.taskRefreshApps(false);
			}
		};
		return runner;
	}
	
	private class TimeoutTask extends TimerTask
    {
		private TimedRunnable r;
		
        public TimeoutTask(TimedRunnable r)
        {
            this.r = r;
        }

        @Override
        public void run()
        {
        	if (r.lastActivity < SyncServer.getCurrentTimeMillis() - 8000 || cancelLongFileOperations)
        	{
        		if (r.timedSocket != null && !r.timedSocket.isClosed())
        		{
        			if (cancelLongFileOperations)
        			{
        				server.log("File operation cancelled by user.");
        			}
        			else
        			{
        				server.log("File transfer timeout reached.");
        			}
        			try {
						r.timedSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
        		}
        		this.cancel();
        	}
        }
    }
	
	private class TimedRunnable implements Runnable {

		private long lastActivity;
		private Socket timedSocket;
		private Timer timer;
		
		public TimedRunnable ()
		{
			lastActivity = SyncServer.getCurrentTimeMillis();
			timer = new Timer();
		}
		
		public Socket getTimedSocket(String ip, int port) throws IOException
	    {
			if (cancelLongFileOperations)
			{
				throw new IOException("Operation cancelled by user.");
			}

			timer.cancel();
			timer = new Timer();
			timedSocket = new Socket();
			timedSocket.connect(new InetSocketAddress(ip, port), 1000);
			timedSocket.setSoTimeout(7000);
			lastActivity = SyncServer.getCurrentTimeMillis();
			timer.schedule(new TimeoutTask(this), 1000, 1000);
			return timedSocket;
	    }
		
		public void delaySocketClosingTimeout()
		{
			lastActivity = SyncServer.getCurrentTimeMillis();
		}
		
		@Override
		public void run() {}

	}

	public void installerConnection(final String ip)
	{
		server.log("Started installer connection.");
		TimedRunnable runner = new TimedRunnable()
		{
			public void run()
			{
				uploadList = new ArrayList<FileInformation>();
				if (StartUpWindow.settingsPath != null)
				{
					File folder = new File(StartUpWindow.settingsPath + File.separator + "apks");
					if (folder.exists())
					{
						File[] apks = folder.listFiles(new FilenameFilter() {
                            @Override
                            public boolean accept(File file, String name) {
                                if (name.endsWith(".apk"))
                                {
                                    return true;
                                }
                                else
                                {
                                	return false;
                                }
                            }
                        });
						for (int i = 0; i < apks.length; i++)
						{
							uploadList.add(new FileInformation(apks[i], null, false, ""));
						}
					}
				}
				else
				{
					return;
				}
				
				int failedFileTransferCount = 0;
				
				if (uploadList.size() == 0)
				{
					try
					{
						Socket socket = getTimedSocket(ip, 11199);
						PrintWriter writer = new PrintWriter(socket.getOutputStream());
						BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
						writer.println("Server has no apk files.");
						writer.close();
						reader.close();
						socket.close();
					}
					catch (SocketTimeoutException ex)
					{
					}
					catch (IOException ex)
					{
					}
				}
				
				for (int i = 0; i < uploadList.size(); i++)
				{
					FileInformation info = uploadList.get(i);
					
					String checkSum = "";
					if (info.file.exists())
					{
						checkSum = CheckSum.getCheckSum(info.file.getAbsolutePath());
					}

					try
					{
						Socket socket = getTimedSocket(ip, 11199);
						PrintWriter writer = new PrintWriter(socket.getOutputStream());
						BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
						
						Boolean receivedOk = false;
						
						writer.println("Receive installer file.");
						writer.println(uploadList.size());
						writer.println(info.name);
						writer.println(info.sizeInBytes);
						writer.println(checkSum);
						writer.flush();
						
						String line = reader.readLine();
						if (line.equals("READY TO RECEIVE"))
						{
							if (info.file.exists())
							{
								byte[] bytes = new byte[16 * 1024];
								InputStream in = new FileInputStream(info.file);
								OutputStream out = socket.getOutputStream();
								int count;
								int accumCount = 0;
								int lastPercentage = -1;
								while ((count = in.read(bytes)) > 0) {
									out.write(bytes, 0, count);
									this.delaySocketClosingTimeout();
									accumCount += count;
									info.percentageTransferReady = (int)(((double)accumCount / (double)info.sizeInBytes) * 100.0);
									if (lastPercentage < info.percentageTransferReady)
									{
										lastPercentage = info.percentageTransferReady;
										server.updateGuiTaskResult(", " + totalUploadPercentage() + "%");
									}
								}
								
								out.flush();
								
								String reply = reader.readLine();
								if (reply != null)
								{
									if (reply.equals("RECEIVED OK"))
									{
										receivedOk = true;
									}
									else
									{
										server.log("Failed to upload file due to mismatch: " + info.name);
									}
								}
								
								out.close();
								in.close();
							} else {
								server.log("Failed to upload a non-existing file: " + info.name);
							}
						}
						else
						{
							if (line.equals("ALREADY HAVE THE FILE"))
							{
								receivedOk = true;
								info.percentageTransferReady = 100;
								info.transferState = FileInformation.FILE_STATE_NOT_TRANSFERRED;
								server.updateGuiTaskResult(", to installer " + totalUploadPercentage() + "%");
							}
							else
							{
								server.log("Did not get a 'ready to receive' message.", true);
							}
						}
						
						
						if (info.percentageTransferReady == 100 && receivedOk)
						{
							info.transferState = FileInformation.FILE_STATE_TRANSFERRED;
							info.errorMessage = "";
							increaseUploadCount();
							server.log("File upload success.", false);
						}
						else
						{
							info.percentageTransferReady = 0;
							failedFileTransferCount += 1;
						}
						
						writer.close();
						reader.close();
						socket.close();
					}
					catch (SocketTimeoutException ex)
					{
						server.log("Device at ip " + ip + " not reached.", true);
						//info.errorMessage = ", device not reached";
						failedFileTransferCount += 1;
						info.percentageTransferReady = 0;
					}
					catch (IOException ex)
					{
						server.log("Error in file upload: " + info.name + " to ip " + ip, true);
						info.percentageTransferReady = 0;
						failedFileTransferCount += 1;
						//server.log(ex);
					}
					
					if (info.transferState == FileInformation.FILE_STATE_TRANSFER_NOW)
					{
						info.transferState = FileInformation.FILE_STATE_NOT_TRANSFERRED;
					}
					
					server.updateGuiTaskResult(", uploaded to installer " + totalUploadPercentage() + "%");
					
				}
				server.setState(SyncServer.STATE_READY);
			}
		};
		if (server.getServerState() == SyncServer.STATE_READY)
		{
			server.setState(SyncServer.STATE_UPLOADING);
			server.updateGuiTaskResult("");
			Thread t = new Thread(runner);
			t.start();
		}
	}

	public void taskDownloadMetaFiles()
	{
		selectMetaFiles();
		
		if (server.serverState == SyncServer.STATE_READY)
		{
			cancelLongFileOperations = false;
			server.setState(SyncServer.STATE_DOWNLOADING);
			server.updateGuiTaskResult("");
			Thread thread = new Thread(getDownloadFilesRunner(false, true));
			thread.start();
		}
	}

	public void readMetaData() {
		File path = new File(server.getExperimentPath() + File.separator + "downloaded");
		if (path.exists() && path.isDirectory())
		{
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++)
			{
				if (files[i].exists() && files[i].getName().endsWith(".meta"))
				{
					String filePrefix = files[i].getName().replaceAll("_[A-Z]+\\.[a-zA-Z]+$", "");
					String tag1 = "";
					String tag2 = "";
					String tag3 = "";
					String appType = "";
					int recordingNum = -1;
					try
					{
						String recNum = filePrefix.substring(filePrefix.length() - 7, filePrefix.length() - 4);
						recordingNum = Integer.parseInt(recNum);
					}
					catch (Exception ex) {}
					
					try
					{
						BufferedReader br = new BufferedReader(new FileReader(files[i]));
						String line;
						while ((line = br.readLine()) != null)
						{
							if (line.startsWith("Server tag 1:"))
							{
								tag1 = line.replace("Server tag 1:", "").trim();
							}
							if (line.startsWith("Server tag 2:"))
							{
								tag2 = line.replace("Server tag 2:", "").trim();
							}
							if (line.startsWith("Server tag 3:"))
							{
								tag3 = line.replace("Server tag 3:", "").trim();
							}
							if (line.startsWith("App type:"))
							{
								appType = line.replace("App type:", "").trim();
							}
						}
						br.close();
					} catch (FileNotFoundException e) 
					{
						server.log(e);
					} catch (IOException e) 
					{
						server.log(e);
					}
					
					for (int k = 0; k < downloadList.size(); k++)
					{
						if (downloadList.get(k).name.startsWith(filePrefix))
						{
							downloadList.get(k).metaTag1 = tag1;
							downloadList.get(k).metaTag2 = tag2;
							downloadList.get(k).metaTag3 = tag3;
							downloadList.get(k).metaRecordingNumber = recordingNum;
							downloadList.get(k).metaAppType = appType;
						}
					}
					
				}
			}
		}
	}
	
}
