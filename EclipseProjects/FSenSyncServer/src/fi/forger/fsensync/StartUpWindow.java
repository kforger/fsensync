/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.awt.EventQueue;
import java.awt.Font;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class StartUpWindow {

	private JFrame frame;
	
	private static float uiScaleFactor = 1.2f;
	private JTextField experimentNameText;
	private JList<String> list;
	private static File workingPath;
	public static File settingsPath;
	public static File anacondaPath;

	/*
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		loadSettings();
		
		Locale.setDefault(Locale.ROOT);
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		scaleFontSizes(uiScaleFactor);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartUpWindow window = new StartUpWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});		
	}
	
	private static void loadSettings()
    {
		try
		{
			try
			{
				settingsPath = new File(StartUpWindow.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
			}
			catch (Exception e)
			{
				settingsPath = new File(ClassLoader.getSystemClassLoader().getResource(".").getPath());
			}
			if (settingsPath.getAbsolutePath().endsWith("jar"))
			{
				settingsPath = settingsPath.getParentFile();
			}
			System.out.println("Settings path: " + settingsPath);
			File settingFile = new File(settingsPath.getAbsolutePath() + File.separator + "settings.txt");
			if (settingFile.exists())
			{
				BufferedReader br = new BufferedReader(new FileReader(settingFile));
				String pathText = br.readLine();
				if (pathText.equals("Default folder not set"))
				{
					workingPath = new File(System.getProperty("user.home"));
				}
				else
				{
					File userPath = new File(pathText);
					if (userPath.exists())
					{
						workingPath = userPath;
					}
					else
					{
						workingPath = new File(System.getProperty("user.home"));
					}
				}
				
				String s = br.readLine();
				if (s != null)
				{
					uiScaleFactor = Float.valueOf(s);
				}
				
				String pythonText = br.readLine();
				if (pythonText != null && !pythonText.equals("Anaconda path not set"))
				{
					anacondaPath = new File(pythonText);
				}
				
				br.close();
			}
			else
			{
				workingPath = new File(System.getProperty("user.home"));
			}
			saveFolderSettings();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			workingPath = new File(System.getProperty("user.home"));
		}
    }
	
	/**
	 * Create the application.
	 */
	public StartUpWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("FSenSync Server");
		frame.setBounds(100, 100, 564, 366);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {438};
		gridBagLayout.rowHeights = new int[] {0, 0, 0, 15, 29, 19, 0, 0, 0, (int)(200 * uiScaleFactor), 0};
		gridBagLayout.columnWeights = new double[]{1.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0};
		frame.getContentPane().setLayout(gridBagLayout);
		
		Component rigidArea_1 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_1 = new GridBagConstraints();
		gbc_rigidArea_1.insets = new Insets(0, 0, 5, 0);
		gbc_rigidArea_1.gridx = 0;
		gbc_rigidArea_1.gridy = 0;
		frame.getContentPane().add(rigidArea_1, gbc_rigidArea_1);
		
		JLabel lblNewLabel = new JLabel("Create a new experiment or choose an existing one.");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		frame.getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.insets = new Insets(0, 0, 5, 0);
		gbc_rigidArea.gridx = 0;
		gbc_rigidArea.gridy = 2;
		frame.getContentPane().add(rigidArea, gbc_rigidArea);
		
		JLabel lblNewLabel_1 = new JLabel("New experiment name:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 3;
		frame.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		experimentNameText = new JTextField();
		experimentNameText.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_experimentNameText = new GridBagConstraints();
		gbc_experimentNameText.insets = new Insets(0, 0, 5, 0);
		gbc_experimentNameText.anchor = GridBagConstraints.NORTH;
		gbc_experimentNameText.fill = GridBagConstraints.HORIZONTAL;
		gbc_experimentNameText.gridx = 0;
		gbc_experimentNameText.gridy = 4;
		frame.getContentPane().add(experimentNameText, gbc_experimentNameText);
		experimentNameText.setColumns(10);
		
		JButton startNewButton = new JButton("Create experiment");
		startNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{ 
				startNewExperiment(); 
			}
		});
		GridBagConstraints gbc_startNewButton = new GridBagConstraints();
		gbc_startNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_startNewButton.gridx = 0;
		gbc_startNewButton.gridy = 5;
		frame.getContentPane().add(startNewButton, gbc_startNewButton);
		
		Component rigidArea_2 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_2 = new GridBagConstraints();
		gbc_rigidArea_2.insets = new Insets(0, 0, 5, 0);
		gbc_rigidArea_2.gridx = 0;
		gbc_rigidArea_2.gridy = 6;
		frame.getContentPane().add(rigidArea_2, gbc_rigidArea_2);
		
		JLabel lblExistingExperiments = new JLabel("Existing experiments:");
		GridBagConstraints gbc_lblExistingExperiments = new GridBagConstraints();
		gbc_lblExistingExperiments.insets = new Insets(0, 0, 5, 0);
		gbc_lblExistingExperiments.gridx = 0;
		gbc_lblExistingExperiments.gridy = 7;
		frame.getContentPane().add(lblExistingExperiments, gbc_lblExistingExperiments);
		
		list = new JList<String>();
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event) {
				if (event.getClickCount() >= 2 && list.isSelectionEmpty() == false)
				{
					startExistingExperiment();
				}
			}
		});
		
		final JLabel lblCurrentDirectory = new JLabel(workingPath.getAbsolutePath());
		GridBagConstraints gbc_lblCurrentDirectory = new GridBagConstraints();
		gbc_lblCurrentDirectory.insets = new Insets(0, 0, 5, 0);
		gbc_lblCurrentDirectory.gridx = 0;
		gbc_lblCurrentDirectory.gridy = 8;
		frame.getContentPane().add(lblCurrentDirectory, gbc_lblCurrentDirectory);
		
		JScrollPane scrollPane = new JScrollPane(list);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 9;
		frame.getContentPane().add(scrollPane, gbc_scrollPane);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 10;
		frame.getContentPane().add(panel, gbc_panel);
		
		JButton btnNewButton = new JButton("Select directory");
		panel.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				JFileChooser chooser = new JFileChooser(workingPath);
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = chooser.showOpenDialog(frame);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					workingPath = new File(chooser.getSelectedFile().getAbsolutePath());
					lblCurrentDirectory.setText(workingPath.getAbsolutePath());
					setListItems();
					saveFolderSettings();
				}
			}
		});
		
		JButton openExperimentButton = new JButton("Open experiment");
		panel.add(openExperimentButton);
		openExperimentButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (list.isSelectionEmpty() == false)
				{
					startExistingExperiment();
				}
			}
		});
		
		setListItems();
		
		frame.pack();
		frame.setResizable(false);
	}
	
	
	public static void saveFolderSettings()
	{
		try
		{
			File settingFile = new File(settingsPath.getAbsolutePath() + File.separator + "settings.txt");
			if (settingFile.exists())
	        {
				settingFile.delete();
	        }
			PrintWriter output = new PrintWriter(settingFile, "UTF-8");
			output.println(workingPath);
			output.println(uiScaleFactor);
			if (anacondaPath != null && anacondaPath.exists())
			{
				output.print(anacondaPath);
			}
			else
			{
				output.print("Python path not set");
			}
			output.close();
		} catch (Exception e) {
			
		}
	}
	
	private void setListItems()
	{
		File path = workingPath;
		File[] directories = path.listFiles(new FilenameFilter() {
		    @Override
		    public boolean accept(File file, String name)
		    {
		    	Boolean ok = false;
		    	File candidateFile = new File(file.getAbsolutePath() + File.separator + name);
		    	if (candidateFile.listFiles() != null && candidateFile.listFiles().length >= 1)
		    	{
		    		File[] files = candidateFile.listFiles();
		    		for (int i = 0; i < files.length; i++)
		    		{
		    			if (files[i].getName().equals(".recordingCount.txt"))
		    			{
		    				ok = true;
		    			}
		    			if (files[i].getName().equals(".deviceInformation.txt"))
		    			{
		    				ok = true;
		    			}
		    		}
		    	}
		        return ok && !name.startsWith(".");
		    }
		});
		
		String[] experiments = new String[directories.length];
		for (int i = 0; i < directories.length; i++)
		{
			experiments[i] = directories[i].getName();
		}
		
		Arrays.sort(experiments);
		
		list.setListData(experiments);
	}
	
	public void startNewExperiment()
	{
		String name = experimentNameText.getText();
		name = name.replaceAll("\\s+","");
		if (name.length() == 0)
		{
			JOptionPane.showMessageDialog(frame, "The length of the name must be at least 1.");
			return;
		}
		else
		{
            File path = new File(workingPath.getAbsolutePath() + File.separator + name);
            path.mkdir();
            ServerGui.startServer(path.getAbsolutePath(), uiScaleFactor);
            frame.dispose();
		}
	}
	
	public void startExistingExperiment()
	{
		String name = list.getSelectedValue();
        File path = new File(workingPath.getAbsolutePath() + File.separator + name);
        ServerGui.startServer(path.getAbsolutePath(), uiScaleFactor);
        frame.dispose();
	}

	public static void scaleFontSizes(float size)
	{
        Set<Object> defKeys = UIManager.getLookAndFeelDefaults().keySet();
        Object[] keys = defKeys.toArray(new Object[defKeys.size()]);

        for (Object k : keys)
        {
            if (k != null && k.toString().toLowerCase().contains("font"))
            {
                Font font = UIManager.getDefaults().getFont(k);
                if (font != null) {
                    font = font.deriveFont(font.getSize2D() * size);
                    UIManager.put(k, font);
                }
            }
        }
    }
	
}
