/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

public class AppInformation {
	public String ip;
	public double lastSyncAccuracy = Double.NaN;
	public int batteryLevel;
	public int shortId;
	public String uniqueId;
	public int appState;
	public Boolean fileListUpdated;
	private long lastUpdate;
	public Boolean activeConnection;
	public int failedFileTransferCount;
	public TagInformation tag;
	public long freeSpace;
	public int recordingNumber;
	public Boolean isOnBackground;
	public Boolean recordingForOtherServer;
	public String clientType;
	public String deviceModel;
	public String deviceId;
	public String longDeviceId;
	public Boolean isSelected;
	public Boolean needsToGiveAppTags;
	public ArrayList<AppSettingInformation> settings;
	public double meanPing = Double.NaN;
	public int droppedPackagePercentage = -1;
	public boolean isStreaming = false;
	private long lastHealthUpdate;
	public int[] sensorHealth;
	private int portOffset;

	public static final int SENSOR_HEALTH_INITIAL = 0;
    public static final int SENSOR_HEALTH_BAD = 1;
    public static final int SENSOR_HEALTH_GOOD = 2;
	
	public static final int APP_CLOSED = 0;
	public static final int APP_ERROR = 1;
	public static final int APP_STARTED = 2;
	public static final int APP_2 = 3;
	public static final int APP_3 = 4;
	public static final int APP_INITIAL_SYNC = 5;
	public static final int APP_WAITING_FOR_SENSORS = 6;
	public static final int APP_READY = 7;
	public static final int APP_RECORDING = 8;
	public static final int APP_PREPARING_PLAY = 9;
	public static final int APP_PLAYING = 10;
	public static final int APP_FILE_TRANSFER = 11;
	public static final int APP_POST_SYNC = 12;
	
	public AppInformation(String ip, int shortId, String uniqueId, String clientType,
			String deviceModel, int port, String deviceId, String longDeviceId, Boolean needToGiveAppTags)
	{
		this.ip = ip;
		this.uniqueId = uniqueId;
		this.shortId = shortId;
		this.appState = APP_STARTED;
		this.clientType = clientType;
		this.deviceModel = deviceModel.replace("\\W", "");
		this.portOffset = port;
		this.deviceId = deviceId;
		this.longDeviceId = longDeviceId;
		this.needsToGiveAppTags = needToGiveAppTags;
		lastUpdate = -1L;
		batteryLevel = -1;
		lastHealthUpdate = 0;
		activeConnection = false;
		failedFileTransferCount = 0;
		tag = null;
		freeSpace = -1;
		recordingNumber = -1;
		isOnBackground = false;
		recordingForOtherServer = false;
		isSelected = true;
		settings = new ArrayList<AppSettingInformation>();
		sensorHealth = new int[0];
	}
	
	public Boolean isContactedAndSelected()
	{
		if (!ip.equals("") && appState > APP_CLOSED && isSelected)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public int getUniPort()
	{
		return portOffset + SyncServer.DEFAULT_CLIENT_UNICAST_PORT;
	}
	
	public int getTcpPort()
	{
		return portOffset + SyncServer.DEFAULT_CLIENT_TCP_PORT;
	}
	
	public void setPortOffset(int offset)
	{
		portOffset = offset;
	}
	
	public void addSetting(String settingName, String settingValue)
	{
		Boolean found = false;
		for (int i = 0; i < settings.size() && found == false; i++)
		{
			if (settings.get(i).settingName.equals(settingName))
			{
				found = true;
				settings.get(i).settingValue = settingValue;
			}
		}
		if (found == false)
		{
			settings.add(new AppSettingInformation(settingName, settingValue));
		}
	}
	
	public String getSetting(String settingName)
	{
		for (int i = 0; i < settings.size(); i++)
		{
			if (settings.get(i).settingName.equals(settingName))
			{
				return settings.get(i).settingValue;
			}
		}
		return "?";
	}
	
	public Boolean isContacted()
	{
		if (!ip.equals("") && portOffset != -1 && appState > APP_CLOSED)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void markAppAssumedClosed()
	{
		appState = APP_CLOSED;
		lastUpdate = -3L;
		lastHealthUpdate = 0;
		activeConnection = false;
		recordingForOtherServer = false;
		lastSyncAccuracy = Double.NaN;
		settings.clear();
		isStreaming = false;
	}
	
	public Boolean samePortOffset(int offset)
	{
		return portOffset == offset;
	}
	
	public void markAppClosed()
	{
		appState = APP_CLOSED;
		ip = "";
		portOffset = -1;
		lastUpdate = -2L;
		lastHealthUpdate = 0;
		activeConnection = false;
		recordingForOtherServer = false;
		lastSyncAccuracy = Double.NaN;
		settings.clear();
		isStreaming = false;
	}
	
	public void noteUpdate()
	{
		lastUpdate = SyncServer.getCurrentTimeMillis();
	}
	
	public void noteHealthUpdate()
	{
		lastHealthUpdate = SyncServer.getCurrentTimeMillis();
	}
	
	public boolean healthInfoIsCurrent()
	{
		if (lastHealthUpdate > 0)
		{
			if (SyncServer.getCurrentTimeMillis() - lastUpdate < 20000l)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public ContactAge timeFromLastUpdate()
	{
		long time = 0L;
		if (lastUpdate == -3L ||lastUpdate == -2L || lastUpdate == -1L)
		{
			time = lastUpdate;
		}
		else
		{
			time = SyncServer.getCurrentTimeMillis() - lastUpdate;
		}
		
		if (time == -3)
			return new ContactAge("disappeared", time);
		if (time == -2)
			return new ContactAge("app closed", time);
		if (time == -1)
			return new ContactAge("no contact", time);
		if (time < 15000)
			return new ContactAge("< 15s", time);
		if (time < 30000)
			return new ContactAge("< 30s", time);
		if (time < 60000)
			return new ContactAge("< 60s", time);
		return new ContactAge("~ " + (time / 60000L) + "min", time);
		
		
	}
}
