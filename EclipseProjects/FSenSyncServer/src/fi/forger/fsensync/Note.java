/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


package fi.forger.fsensync;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Note
{
	public String text;
	public long timeStamp;
	public String timeHuman;
	
	public Note(long timeStamp, String text)
	{
		this.timeStamp = timeStamp;
		this.timeHuman = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(timeStamp));
		this.text = text;
	}
	
	public Note(long timeStamp, String timeHuman, String text)
	{
		this.timeStamp = timeStamp;
		this.timeHuman = timeHuman;
		this.text = text;
	}
}
