/*
FSenSync Server
Copyright (C) 2017-2018  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.JScrollPane;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;

import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JComboBox;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JTextArea;
import javax.swing.JSplitPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

public class ServerGui {

	private JFrame frame;
	private JTextArea textAreaLog;
	private static SyncServer server;
	private JScrollPane scrollPane;
	private CustomTable tableApps;
	private JPanel panelApps;
	private JButton buttonStart;
	private JPanel panel_1;
	private JButton buttonStop;
	private static float uiScaleFactor;
	private JPanel panel_2;
	private JLabel lblNewLabel;
	private JLabel currentRecording;
	private JLabel lblState;
	private JLabel serverState1;
	private Component rigidArea;
	private JButton btnRefreshDevices;
	private JPanel panelDownloads;
	private JPanel panelStreaming;
	private JPanel panel_4;
	private JButton buttonFetchFileInfo;
	private JScrollPane scrollPane_1;
	private JTable tableFiles;
	private JButton buttonDownloadFiles;
	private JButton buttonDownloadSmallFiles;
	private JPanel panel_5;
	private JLabel label;
	private JLabel serverState2;
	private JPanel panelTags;
	private JPanel panel_7;
	private JButton buttonLoadTags;
	private JScrollPane scrollPane_2;
	private CustomTable tableTags;
	private JButton buttonSendTags;
	private JPanel panel_8;
	private JButton buttonClose;
	private JButton buttonDelete;
	private JButton buttonCalculateSync;
	private JPanel panelNotes;
	private JPanel panelScripts;
	private JPanel panelScriptsA;
	private JPanel panelScriptsB;
	private JScrollPane scrollPane_6;
	private JButton buttonRunScript;
	private CustomTable tableScripts;
	private JPanel panel_10;
	private JButton buttonNewNote;
	private JButton buttonSaveNotes;
	private JScrollPane scrollPane_3;
	private JTable tableNotes;
	private JButton buttonDeleteNote;
	private DefaultCellEditor noteEditor;
	private JLabel saveLabel;
	private JPanel panel_11;
	private JButton buttonUndoNoteChanges;
	private JPanel panel_12;
	private JButton btnStartStimuli;
	private JButton btnStopStimuli;
	private JPanel panelUploads;
	private JPanel panel_14;
	private JButton buttonUploadFile;
	private JScrollPane scrollPane_4;
	private JPanel panel_15;
	private JLabel label_1;
	private JLabel serverState3;
	private JLabel lblStimulusFileName;
	private JPanel textAreaAbout;
	private JLabel lblNewLabel_1;
	private JPanel panel_screen_touch;
	private JPanel panel_stim_settings;
	private JButton buttonSoundFile;
	private JButton buttonDisableSound;
	private JButton buttonEnableSound;
	private Component rigidArea_1;
	private JLabel networkStateLabel;
	private JLabel networkStateText;
	private JTextField textFileNameOnDevice;
	private JLabel lblFileNameOn;
	private JTable tableUploads;
	private JTable tableStreaming;
	private JButton buttonSendSetting;
	private JComboBox<String> comboBox;
	private JButton btnDeleteStimulusFiles;
	private Component rigidArea_2;
	private JPanel panel;
	private JButton buttonStartStreaming;
	private JButton buttonStopStreaming;
	private JPanel panel_3;
	private JPanel panel_6;
	private JLabel label_2;
	private JLabel serverState4;
	private JScrollPane scrollPane_5;
	private JLabel label_3;
	private JTextField textMaxFps;
	private JLabel lblMulticastIp;
	private JTextField textMultiIp;
	private Component rigidArea_3;
	private Component rigidArea_4;
	private JLabel lblMulticastPort;
	private JTextField textMultiPort;
	private Component rigidArea_5;
	private JButton buttonPython;
	private JLabel lblMinRecording;
	private JLabel lblMaxRecording;
	private JFormattedTextField textField_2;
	private JFormattedTextField textField_3;
	private JScrollPane scrollPane_7;
	private JTextArea textAreaScripts;
	private JSplitPane splitPane;
	private JScrollPane logScrollPane_8;
	private JButton buttonStopScript;
	private JButton buttonSelectedFiles;
	private JPanel panel_9;
	private JButton buttonDownloadMeta;
	private JButton buttonReadMeta;
	private JButton buttonCancelFileOperation;
	private JButton buttonCancelUploads;
	private JButton btnHideExtraColumns;
	
	private boolean extraColumnsHidden;
	private TableColumn[] extraColumns;
	private JComboBox<String> comboBoxUploads;
	
	public static void startServer(String path, float uiScaleFactor)
	{
		ServerGui.uiScaleFactor = uiScaleFactor;
		server = new SyncServer(path);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ServerGui window = new ServerGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		Thread thread = new Thread(server);
		thread.start();
	}

	/**
	 * Create the application.
	 */
	public ServerGui() {
		initialize();
		server.setGui(this);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("FSenSync Server - " + server.getExperimentPath());
		frame.setBounds(100, 100, 750, 481);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension((int)(750 * uiScaleFactor), 100));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		panelApps = new JPanel();
		tabbedPane.addTab("Connected apps", null, panelApps, null);
		panelApps.setLayout(new BoxLayout(panelApps, BoxLayout.Y_AXIS));
		
		panel_1 = new JPanel();
		panel_1.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelApps.add(panel_1);
		
		buttonStart = new JButton("Start recording");
		buttonStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { server.taskStartRecording(); }
		});
		panel_1.add(buttonStart);
		
		buttonStop = new JButton("Stop recording");
		buttonStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { server.taskStopRecording(); }
		});
		panel_1.add(buttonStop);
		
		btnRefreshDevices = new JButton("Refresh devices");
		btnRefreshDevices.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { server.taskRefreshApps(true); }
		});
		panel_1.add(btnRefreshDevices);
		
		panel_12 = new JPanel();
		panel_12.setMaximumSize(new Dimension(2147483647, 0));
		panelApps.add(panel_12);
		
		btnStartStimuli = new JButton("Start stimuli");
		btnStartStimuli.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { server.taskStartStimuli(); }
		});
		panel_12.add(btnStartStimuli);
		
		btnStopStimuli = new JButton("Cancel stimuli");
		btnStopStimuli.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { server.taskCancelStimuli(); };
		});
		panel_12.add(btnStopStimuli);
		
		lblStimulusFileName = new JLabel("Stimulus file name:");
		panel_12.add(lblStimulusFileName);
		
		comboBoxUploads = new JComboBox<String>();
		comboBoxUploads.setMaximumRowCount(10);
		comboBoxUploads.setEditable(true);
		panel_12.add(comboBoxUploads);
		
		
		
		buttonClose = new JButton("Close apps");
		buttonClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				int proceed = 0;
				Object[] options = {"Yes", "Cancel"};
				proceed = JOptionPane.showOptionDialog(frame,
				    "Are you sure you want to close all the selected apps?",
				    "",
				    JOptionPane.OK_CANCEL_OPTION,
				    JOptionPane.QUESTION_MESSAGE,
				    null,
				    options,
				    options[1]);
				if (proceed == 0)
				{
					server.sendCloseSelectedApps();
				}
			}
		});
		panel_1.add(buttonClose);
		
		btnHideExtraColumns = new JButton("Extra info");
		panel_1.add(btnHideExtraColumns);
		btnHideExtraColumns.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				hideShowExtraColumns();
			}
		});
		
		panel_2 = new JPanel();
		panel_2.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelApps.add(panel_2);
		
		lblState = new JLabel("State: ");
		panel_2.add(lblState);
		
		serverState1 = new JLabel(server.serverStates[server.getServerState()]);
		panel_2.add(serverState1);
		
		rigidArea = Box.createRigidArea(new Dimension(20, 20));
		panel_2.add(rigidArea);
		
		lblNewLabel = new JLabel("Recording no:");
		panel_2.add(lblNewLabel);
		
		currentRecording = new JLabel("?");
		panel_2.add(currentRecording);
		
		currentRecording.setText(String.valueOf(server.getCurrentRecordingNumber()));
		
		rigidArea_1 = Box.createRigidArea(new Dimension(20, 20));
		panel_2.add(rigidArea_1);
		
		networkStateLabel = new JLabel("Network error/success:");
		panel_2.add(networkStateLabel);
		
		networkStateText = new JLabel("0/0");
		panel_2.add(networkStateText);
		
		tableApps = new CustomTable(server);
		tableApps.setRowSelectionAllowed(false);
		tableApps.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event)
			{
				if (event.getClickCount() == 2)
				{
					server.setSelectionByCell(
							tableApps.convertRowIndexToModel(tableApps.getSelectedRow()),
							tableApps.convertColumnIndexToModel(tableApps.getSelectedColumn()));
				}
			}
		});
		tableApps.setFillsViewportHeight(true);
		tableApps.setRowHeight((int)(tableApps.getRowHeight() * uiScaleFactor));
		tableApps.setDefaultRenderer(Integer.class, new MyIntegerRenderer(true));
		tableApps.setDefaultRenderer(Double.class, new MyDoubleRenderer(true));
		tableApps.setDefaultRenderer(String.class, new MyStringRenderer(true));
		tableApps.setDefaultRenderer(Boolean.class, new MyBooleanRenderer());
		tableApps.setDefaultRenderer(ContactAge.class, new ContactAgeRenderer());
		tableApps.setDefaultRenderer(SensorHealthText.class, new SensorHealthRenderer());
		tableApps.setDefaultRenderer(BatteryLevel.class, new BatteryLevelRenderer());
		tableApps.setDefaultRenderer(StorageLevel.class, new StorageLevelRenderer());
		tableApps.setAutoCreateRowSorter(true);
		
		scrollPane = new JScrollPane(tableApps);
		panelApps.add(scrollPane);
		
		panelStreaming = new JPanel();
		tabbedPane.addTab("Streaming", null, panelStreaming, null);
		panelStreaming.setLayout(new BoxLayout(panelStreaming, BoxLayout.Y_AXIS));
		
		panel = new JPanel();
		panel.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelStreaming.add(panel);
		
		buttonStartStreaming = new JButton("Start streaming");
		buttonStartStreaming.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					String multicastIp = textMultiIp.getText();
					int multicastPort = Integer.parseInt(textMultiPort.getText());
					int maxFps = Integer.parseInt(textMaxFps.getText());
					server.taskStartStreaming(multicastIp, multicastPort, maxFps);
				}
				catch (NumberFormatException ex)
				{
					JOptionPane.showMessageDialog(frame, "Bad number format.", 
							"Warning",
						    JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		panel.add(buttonStartStreaming);
		
		buttonStopStreaming = new JButton("Stop streaming");
		buttonStopStreaming.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				server.taskStopStreaming();
			}
		});
		panel.add(buttonStopStreaming);
		
		panel_3 = new JPanel();
		panel_3.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelStreaming.add(panel_3);
		
		label_3 = new JLabel("Maximum fps:");
		panel_3.add(label_3);
		
		textMaxFps = new JTextField();
		textMaxFps.setText("110");
		textMaxFps.setColumns(4);
		panel_3.add(textMaxFps);
		
		rigidArea_3 = Box.createRigidArea(new Dimension(20, 20));
		panel_3.add(rigidArea_3);
		
		lblMulticastIp = new JLabel("Multicast ip:");
		panel_3.add(lblMulticastIp);
		
		textMultiIp = new JTextField();
		textMultiIp.setText("239.98.98.1");
		textMultiIp.setColumns(10);
		panel_3.add(textMultiIp);
		
		rigidArea_4 = Box.createRigidArea(new Dimension(20, 20));
		panel_3.add(rigidArea_4);
		
		lblMulticastPort = new JLabel("Multicast port:");
		panel_3.add(lblMulticastPort);
		
		textMultiPort = new JTextField();
		textMultiPort.setText("10333");
		textMultiPort.setColumns(6);
		panel_3.add(textMultiPort);
		
		panel_6 = new JPanel();
		panel_6.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelStreaming.add(panel_6);
		
		label_2 = new JLabel("State: ");
		panel_6.add(label_2);
		
		serverState4 = new JLabel(server.serverStates[server.getServerState()]);
		panel_6.add(serverState4);
		
		tableStreaming = new CustomTable(server.streamingData);
		tableStreaming.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event)
			{
				if (event.getClickCount() == 2)
				{
					server.streamingData.setSelectionByCell(
							tableStreaming.convertRowIndexToModel(tableStreaming.getSelectedRow()), 
							tableStreaming.convertColumnIndexToModel(tableStreaming.getSelectedColumn()));
				}
			}
		});
		tableStreaming.setFillsViewportHeight(true);
		tableStreaming.setRowHeight((int)(tableStreaming.getRowHeight() * uiScaleFactor));
		tableStreaming.setDefaultRenderer(Integer.class, new MyIntegerRenderer(true));
		tableStreaming.setDefaultRenderer(Double.class, new MyDoubleRenderer(true));
		tableStreaming.setDefaultRenderer(String.class, new MyStringRenderer(true));
		tableStreaming.setDefaultRenderer(Boolean.class, new MyBooleanRenderer());
		tableStreaming.setDefaultRenderer(ContactAge.class, new ContactAgeRenderer());
		tableStreaming.setAutoCreateRowSorter(true);
		
		scrollPane_5 = new JScrollPane(tableStreaming);
		panelStreaming.add(scrollPane_5);
		
		panelDownloads = new JPanel();
		tabbedPane.addTab("Downloads", null, panelDownloads, null);
		panelDownloads.setLayout(new BoxLayout(panelDownloads, BoxLayout.Y_AXIS));
		
		panel_4 = new JPanel();
		panel_4.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelDownloads.add(panel_4);
		
		panel_8 = new JPanel();
		panel_8.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelDownloads.add(panel_8);
		
		buttonDownloadSmallFiles = new JButton("Download only small files");
		buttonDownloadSmallFiles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Boolean alsoStop = false;
				
				if (server.appsAreBusy())
				{
					int confirmation = -1;

					Object[] options2 = {"Yes", "No"};
					confirmation = JOptionPane.showOptionDialog(frame,
							"Do you want to stop all recordings to allow downloads from all devices?",
							"",
							JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE,
							null,
							options2,
							options2[1]);
					
					if (confirmation == 0)
					{
						alsoStop = true;
					}
					
					if (server.appsAreDoingPostSync())
					{
						JOptionPane.showMessageDialog(frame, 
								"Warning: Some apps are doing post sync thus all files may not be downloaded.");
					}
				}

				server.taskDownloadFiles(true, false, alsoStop);
			}
		});
		panel_8.add(buttonDownloadSmallFiles);
		
		buttonDelete = new JButton("Delete files from devices");
		panel_8.add(buttonDelete);
		
		buttonCancelFileOperation = new JButton("Cancel");
		buttonCancelFileOperation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				FileManager.cancelLongFileOperations = true;
			}
		});
		buttonCancelFileOperation.setEnabled(false);
		panel_8.add(buttonCancelFileOperation);
		buttonDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				int proceed = -1;
				Object[] options = {"Only this experiment",
	                    "All files",
	                    "Cancel"};
				proceed = JOptionPane.showOptionDialog(frame,
				    "Delete files only from this experiment or all of them?",
				    "",
				    JOptionPane.OK_CANCEL_OPTION,
				    JOptionPane.QUESTION_MESSAGE,
				    null,
				    options,
				    options[2]);
				int confirmation = -1;
				if (proceed == 0 || proceed == 1)
				{
					Object[] options2 = {"Yes", "Cancel"};
					confirmation = JOptionPane.showOptionDialog(frame,
					    "Are you absolutely sure you want to delete files?",
					    "",
					    JOptionPane.OK_CANCEL_OPTION,
					    JOptionPane.QUESTION_MESSAGE,
					    null,
					    options2,
					    options2[1]);
				}
				
				if (confirmation == 0 && proceed == 0)
				{
					server.sendDeleteFilesFromDevice(true, server.getExperimentName(), false);
				}
				if (confirmation == 0 && proceed == 1)
				{
					server.sendDeleteFilesFromDevice(false, server.getExperimentName(), false);
				}
			}
		});
		
		buttonFetchFileInfo = new JButton("Update file information");
		buttonFetchFileInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				server.taskGetFileLists();
			}
		});
		panel_4.add(buttonFetchFileInfo);
		
		buttonDownloadFiles = new JButton("Download all files");
		buttonDownloadFiles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Boolean alsoStop = false;

				if (server.appsAreBusy())
				{
					int confirmation = -1;

					Object[] options2 = {"Yes", "No"};
					confirmation = JOptionPane.showOptionDialog(frame,
							"Do you want to stop all recordings to allow downloads from all devices?",
							"",
							JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE,
							null,
							options2,
							options2[1]);

					if (confirmation == 0)
					{
						alsoStop = true;
					}
					
					if (server.appsAreDoingPostSync())
					{
						JOptionPane.showMessageDialog(frame, 
								"Warning: Some apps are doing post sync thus all files may not be downloaded.");
					}
				}

				server.taskDownloadFiles(false, false, alsoStop);
			}
		});
		panel_4.add(buttonDownloadFiles);
		
		buttonCalculateSync = new JButton("Re-calculate sync");
		buttonCalculateSync.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				server.taskCalculateSync();
			}
		});
		panel_4.add(buttonCalculateSync);
		
		tableFiles = new CustomTable(server.getFileManager());
		tableFiles.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event)
			{
				if (event.getClickCount() == 2)
				{
					server.getFileManager().setSelectionByCell(
							tableFiles.convertRowIndexToModel(tableFiles.getSelectedRow()),
							tableFiles.convertColumnIndexToModel(tableFiles.getSelectedColumn()));
				}
			}
			
			public void mousePressed(MouseEvent event) {
				if (event.isPopupTrigger())
				{
					popup(event);
				}
            }
			
			public void mouseReleased(MouseEvent event) {
				if (event.isPopupTrigger())
				{
					popup(event);
				}
            }
			
			private void popup(MouseEvent event){
				
				int row = tableFiles.rowAtPoint(event.getPoint());
				int col = tableFiles.columnAtPoint(event.getPoint());
		        if (row >= 0 && row < tableFiles.getRowCount() && col >= 0 && col < tableFiles.getColumnCount()) 
		        {
		        	tableFiles.setRowSelectionInterval(row, row);
		        	tableFiles.setColumnSelectionInterval(col, col);
		        	
		        	JPopupMenu menu = new JPopupMenu();
					
					JMenuItem item = new JMenuItem("Select all similar");
				    item.addActionListener(new ActionListener() {
				      public void actionPerformed(ActionEvent e) {
				        server.getFileManager().setSelectionByCell(
								tableFiles.convertRowIndexToModel(tableFiles.getSelectedRow()),
								tableFiles.convertColumnIndexToModel(tableFiles.getSelectedColumn()));
				        
				      }
				    });
				    menu.add(item);
				    
				    item = new JMenuItem("Keep similar");
				    item.addActionListener(new ActionListener() {
				      public void actionPerformed(ActionEvent e) {
				        server.getFileManager().keepSelectionByCell(
								tableFiles.convertRowIndexToModel(tableFiles.getSelectedRow()),
								tableFiles.convertColumnIndexToModel(tableFiles.getSelectedColumn()));
				        
				      }
				    });
				    menu.add(item);
				    
				    item = new JMenuItem("Select all");
				    item.addActionListener(new ActionListener() {
				      public void actionPerformed(ActionEvent e) {
				        server.getFileManager().selectAll();
				      }
				    });
				    menu.add(item);
					
				    item = new JMenuItem("Unselect all");
				    item.addActionListener(new ActionListener() {
				      public void actionPerformed(ActionEvent e) {
				        server.getFileManager().selectNone();
				      }
				    });
				    menu.add(item);

			        menu.show(event.getComponent(), event.getX(), event.getY());
		        }
		    }
		});
		tableFiles.setRowSelectionAllowed(true);
		tableFiles.setFillsViewportHeight(true);
		tableFiles.setRowHeight((int)(tableApps.getRowHeight() * uiScaleFactor));
		tableFiles.setDefaultRenderer(Boolean.class, new MyBooleanRenderer());
		tableFiles.setDefaultRenderer(Integer.class, new MyIntegerRenderer(true));
		tableFiles.setDefaultRenderer(Double.class, new MyDoubleRenderer(true));
		tableFiles.setDefaultRenderer(String.class, new MyStringRenderer(true));
//		tableFiles.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		tableFiles.setRowHeight((int)(tableApps.getRowHeight() * uiScaleFactor));
		
		panel_9 = new JPanel();
		panel_9.setMaximumSize(new Dimension(2147483647, 0));
		panelDownloads.add(panel_9);
		
		buttonDownloadMeta = new JButton("Download meta files");
		buttonDownloadMeta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Boolean alsoStop = false;

				if (server.appsAreBusy())
				{
					int confirmation = -1;

					Object[] options2 = {"Yes", "No"};
					confirmation = JOptionPane.showOptionDialog(frame,
							"Do you want to stop recordings from all apps to allow downloading from all of them?",
							"",
							JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE,
							null,
							options2,
							options2[1]);

					if (confirmation == 0)
					{
						alsoStop = true;
					}
					
					if (server.appsAreDoingPostSync())
					{
						JOptionPane.showMessageDialog(frame, 
								"Warning: Some apps are doing post sync thus all files may not be downloaded.");
					}
				}

				server.taskDownloadMetaFiles(alsoStop);
			}
		});
		panel_9.add(buttonDownloadMeta);
		
		buttonReadMeta = new JButton("Read meta data");
		buttonReadMeta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				server.getFileManager().readMetaData();
			}
		});
		panel_9.add(buttonReadMeta);
		
		buttonSelectedFiles = new JButton("Download selected files");
		panel_9.add(buttonSelectedFiles);
		buttonSelectedFiles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Boolean alsoStop = false;

				if (server.appsAreBusy())
				{
					int confirmation = -1;

					Object[] options2 = {"Yes", "No"};
					confirmation = JOptionPane.showOptionDialog(frame,
							"Do you want to stop recordings from apps holding selected files to allow downloading them all?",
							"",
							JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE,
							null,
							options2,
							options2[1]);

					if (confirmation == 0)
					{
						alsoStop = true;
					}
					
					if (server.appsAreDoingPostSync())
					{
						JOptionPane.showMessageDialog(frame, 
								"Warning: Some apps are doing post sync thus all files may not be downloaded.");
					}
				}

				server.taskDownloadFiles(false, true, alsoStop);

			}
		});
		
		panel_5 = new JPanel();
		panel_5.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelDownloads.add(panel_5);
		
		label = new JLabel("State: ");
		panel_5.add(label);
		
		serverState2 = new JLabel(server.serverStates[server.getServerState()]);
		panel_5.add(serverState2);
		
		scrollPane_1 = new JScrollPane(tableFiles);
		panelDownloads.add(scrollPane_1);
		
		panelUploads = new JPanel();
		tabbedPane.addTab("Uploads", null, panelUploads, null);
		panelUploads.setLayout(new BoxLayout(panelUploads, BoxLayout.Y_AXIS));
		
		panel_14 = new JPanel();
		panel_14.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelUploads.add(panel_14);
		
		buttonUploadFile = new JButton("Upload stimulus file");
		buttonUploadFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				final JFileChooser fileChooser = new JFileChooser(server.getExperimentPath());
				fileChooser.setMultiSelectionEnabled(true);
				int i = fileChooser.showOpenDialog(frame);
				if (i == JFileChooser.APPROVE_OPTION)
				{
					Boolean alsoStopSelected = false;

					if (server.selectedUploadAppsAreBusy())
					{
						int confirmation = -1;

						Object[] options2 = {"Yes", "No"};
						confirmation = JOptionPane.showOptionDialog(frame,
								"Do you want to stop recordings from selected devices to allow uploads?",
								"",
								JOptionPane.OK_CANCEL_OPTION,
								JOptionPane.QUESTION_MESSAGE,
								null,
								options2,
								options2[1]);

						if (confirmation == 0)
						{
							alsoStopSelected = true;
						}
					}
					
					File[] files = fileChooser.getSelectedFiles();
					server.uploadFile(files, false, textFileNameOnDevice.getText(), alsoStopSelected);
					updateUploadsComboBox(files);
				}
			}
		});
		panel_14.add(buttonUploadFile);
		
		lblFileNameOn = new JLabel("File name on device:");
		panel_14.add(lblFileNameOn);
		
		textFileNameOnDevice = new JTextField();
		panel_14.add(textFileNameOnDevice);
		textFileNameOnDevice.setColumns(10);
		
		buttonCancelUploads = new JButton("Cancel");
		buttonCancelUploads.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileManager.cancelLongFileOperations = true;
			}
		});
		buttonCancelUploads.setEnabled(false);
		panel_14.add(buttonCancelUploads);
		
		panel_screen_touch = new JPanel();
		panel_screen_touch.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelUploads.add(panel_screen_touch);
		
		panel_stim_settings = new JPanel();
		panel_stim_settings.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelUploads.add(panel_stim_settings);
		
		buttonSoundFile = new JButton("Upload screen touch sound");
		buttonSoundFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				final JFileChooser fileChooser = new JFileChooser(server.getExperimentPath());
				int i = fileChooser.showOpenDialog(frame);
				if (i == JFileChooser.APPROVE_OPTION)
				{
					File[] files = new File[1];
					files[0] = fileChooser.getSelectedFile();
					
					Boolean alsoStopSelected = false;

					if (server.selectedUploadAppsAreBusy())
					{
						int confirmation = -1;

						Object[] options2 = {"Yes", "No"};
						confirmation = JOptionPane.showOptionDialog(frame,
								"Do you want to stop recordings from selected devices to allow uploads?",
								"",
								JOptionPane.OK_CANCEL_OPTION,
								JOptionPane.QUESTION_MESSAGE,
								null,
								options2,
								options2[1]);

						if (confirmation == 0)
						{
							alsoStopSelected = true;
						}
					}
					
					server.uploadFile(files, true, "", alsoStopSelected);
				}
			}
		});
		panel_screen_touch.add(buttonSoundFile);
		
		buttonEnableSound = new JButton("Enable touch sound");
		buttonEnableSound.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				server.sendEnableScreenTouchSound();
			}
		});
		panel_screen_touch.add(buttonEnableSound);
		
		buttonDisableSound = new JButton("Disable touch sound");
		buttonDisableSound.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				server.sendDisableScreenTouchSound();
			}
		});
		panel_screen_touch.add(buttonDisableSound);
		
		buttonSendSetting = new JButton("Send setting");
		buttonSendSetting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				server.sendSetting(comboBox.getSelectedIndex());;
			}
		});
		panel_stim_settings.add(buttonSendSetting);
		
		comboBox = new JComboBox(AppSettingInformation.appSettingsLabel);
		panel_stim_settings.add(comboBox);
		
		rigidArea_2 = Box.createRigidArea(new Dimension(20, 20));
		panel_stim_settings.add(rigidArea_2);
		
		btnDeleteStimulusFiles = new JButton("Delete stimulus files from devices");
		btnDeleteStimulusFiles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				int confirmation = -1;
				Object[] options = {"Yes", "Cancel"};
				confirmation = JOptionPane.showOptionDialog(frame,
				    "Are you sure you want to delete stimulus files from the selected devices?",
				    "",
				    JOptionPane.OK_CANCEL_OPTION,
				    JOptionPane.QUESTION_MESSAGE,
				    null,
				    options,
				    options[1]);

				
				if (confirmation == 0)
				{
					server.sendDeleteFilesFromDevice(false, "-", true);
				}
			}
		});
		panel_stim_settings.add(btnDeleteStimulusFiles);
		
		panel_15 = new JPanel();
		panel_15.setMaximumSize(new Dimension(Integer.MAX_VALUE, 0));
		panelUploads.add(panel_15);
		
		label_1 = new JLabel("State: ");
		panel_15.add(label_1);
		
		serverState3 = new JLabel(server.serverStates[server.getServerState()]);
		panel_15.add(serverState3);
		
		tableUploads = new CustomTable(server.uploadsData);
		tableUploads.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event)
			{
				if (event.getClickCount() == 2)
				{
					server.uploadsData.setSelectionByCell(
							tableUploads.convertRowIndexToModel(tableUploads.getSelectedRow()),
							tableUploads.convertColumnIndexToModel(tableUploads.getSelectedColumn()));
				}
			}
		});
		tableUploads.setFillsViewportHeight(true);
		tableUploads.setRowHeight((int)(tableUploads.getRowHeight() * uiScaleFactor));
		tableUploads.setDefaultRenderer(Integer.class, new MyIntegerRenderer(true));
		tableUploads.setDefaultRenderer(Double.class, new MyDoubleRenderer(true));
		tableUploads.setDefaultRenderer(String.class, new MyStringRenderer(true));
		tableUploads.setDefaultRenderer(Boolean.class, new MyBooleanRenderer());
		tableUploads.setDefaultRenderer(ContactAge.class, new ContactAgeRenderer());
		tableUploads.setAutoCreateRowSorter(true);
		
		scrollPane_4 = new JScrollPane(tableUploads);
		panelUploads.add(scrollPane_4);
		
		panelTags = new JPanel();
		tabbedPane.addTab("Tags", null, panelTags, null);
		panelTags.setLayout(new BoxLayout(panelTags, BoxLayout.Y_AXIS));
		
		panel_7 = new JPanel();
		panel_7.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelTags.add(panel_7);
		
		buttonLoadTags = new JButton("Load and assign tags");
		buttonLoadTags.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				int proceed = 0;
				if (server.getTagManager().getRowCount() > 0)
				{
					Object[] options = {"Yes", "Cancel"};
					proceed = JOptionPane.showOptionDialog(frame,
					    "Are you sure you want to replace the existing tags with new ones?",
					    "",
					    JOptionPane.OK_CANCEL_OPTION,
					    JOptionPane.QUESTION_MESSAGE,
					    null,
					    options,
					    options[1]);
				}
				if (proceed == 0)
				{
					final JFileChooser fileChooser = new JFileChooser(server.getExperimentPath());
					int i = fileChooser.showOpenDialog(frame);
					if (i == JFileChooser.APPROVE_OPTION)
					{
						server.loadTags(fileChooser.getSelectedFile(), true);
					}
				}
			}
		});
		panel_7.add(buttonLoadTags);
		
		buttonSendTags = new JButton("Resend tags to apps");
		buttonSendTags.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				server.sendAllTags();
			}
		});
		panel_7.add(buttonSendTags);
		
		tableTags = new CustomTable(server.getTagManager());
		tableTags.setFillsViewportHeight(true);
		tableTags.setDefaultRenderer(Integer.class, new MyIntegerRenderer(false));
		tableTags.setDefaultRenderer(Double.class, new MyDoubleRenderer(false));
		tableTags.setDefaultRenderer(String.class, new MyStringRenderer(false));
		tableTags.setDefaultRenderer(ContactAge.class, new ContactAgeRenderer());
		tableTags.setRowHeight((int)(tableTags.getRowHeight() * uiScaleFactor));
		
		scrollPane_2 = new JScrollPane(tableTags);
		panelTags.add(scrollPane_2);
		
		panelNotes = new JPanel();
		tabbedPane.addTab("Notes", null, panelNotes, null);
		panelNotes.setLayout(new BoxLayout(panelNotes, BoxLayout.Y_AXIS));
		
		panel_10 = new JPanel();
		panel_10.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelNotes.add(panel_10);
		
		buttonNewNote = new JButton("New note");
		buttonNewNote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				server.addNewNote();
			}
		});
		panel_10.add(buttonNewNote);
		
		buttonDeleteNote = new JButton("Delete selected notes");
		buttonDeleteNote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				int proceed = 1;
				int[] selection = tableNotes.getSelectedRows();
				if (selection.length > 0)
				{
					Object[] options = {"Yes", "Cancel"};
					proceed = JOptionPane.showOptionDialog(frame,
					    "Are you sure you want to delete the selected notes?",
					    "",
					    JOptionPane.OK_CANCEL_OPTION,
					    JOptionPane.QUESTION_MESSAGE,
					    null,
					    options,
					    options[1]);
				}
				if (proceed == 0)
				{
					server.deleteNotes(selection);
				}
			}
		});
		panel_10.add(buttonDeleteNote);
		
		buttonSaveNotes = new JButton("Save notes");
		buttonSaveNotes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				noteEditor.stopCellEditing();
				server.saveNotes();
			}
		});
		panel_10.add(buttonSaveNotes);
		
		buttonUndoNoteChanges = new JButton("Undo changes");
		buttonUndoNoteChanges.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				server.loadNotes();
			}
		});
		panel_10.add(buttonUndoNoteChanges);
		
		tableNotes = new JTable(server.getNoteManager());
		tableNotes.setFillsViewportHeight(true);
		tableNotes.setRowHeight((int)(tableNotes.getRowHeight() * uiScaleFactor));
		tableNotes.setDefaultRenderer(Integer.class, new MyIntegerRenderer(false));
		tableNotes.setDefaultRenderer(Double.class, new MyDoubleRenderer(false));
		tableNotes.setDefaultRenderer(String.class, new MyNoteStringRenderer());
		tableNotes.setDefaultRenderer(Long.class, new MyLongRenderer(false));
		tableNotes.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		
		TableColumn textColumn = tableNotes.getColumnModel().getColumn(2);
		JTextField textField = new JTextField();
		noteEditor = new DefaultCellEditor(textField);
		textColumn.setCellEditor(noteEditor);
		
		panel_11 = new JPanel();
		panel_11.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelNotes.add(panel_11);
		
		saveLabel = new JLabel("Ready.");
		panel_11.add(saveLabel);
		
		scrollPane_3 = new JScrollPane(tableNotes);
		panelNotes.add(scrollPane_3);
		
		panelScripts = new JPanel();
		tabbedPane.addTab("Scripts", null, panelScripts, null);
		panelScripts.setLayout(new BoxLayout(panelScripts, BoxLayout.Y_AXIS));
		
		panelScriptsA = new JPanel();
		panelScriptsA.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelScripts.add(panelScriptsA);
		
		buttonRunScript = new JButton("Run selected script");
		buttonRunScript.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int row = tableScripts.getSelectedRow();
				if (row >= 0)
				{
					int firstRecording = 1;
					int lastRecording = Integer.MAX_VALUE;
					try
					{
						firstRecording = Integer.valueOf(textField_2.getText());
					}
					catch (Exception ex) {}
					try
					{
						lastRecording = Integer.valueOf(textField_3.getText());
					}
					catch (Exception ex) {}
					
					server.scriptManager.runSelectedScript(firstRecording, lastRecording, row);
				}
			}
		});
		panelScriptsA.add(buttonRunScript);
		
		buttonPython = new JButton("Set Anaconda bin folder"); 
		buttonPython.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				final JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int i = fileChooser.showOpenDialog(frame);
				if (i == JFileChooser.APPROVE_OPTION)
				{
					File file = fileChooser.getSelectedFile();
					StartUpWindow.anacondaPath = file;
					StartUpWindow.saveFolderSettings();
					appendToScriptLog("Set Anaconda folder: " + file.getAbsolutePath());
				}
			}
		});
		
		buttonStopScript = new JButton("Stop selected script");
		buttonStopScript.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = tableScripts.getSelectedRow();
				if (row >= 0)
				{
					server.scriptManager.stopScript(row);
				}
			}
		});
		panelScriptsA.add(buttonStopScript);
		panelScriptsA.add(buttonPython);
		
		panelScriptsB = new JPanel();
		panelScriptsB.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		panelScripts.add(panelScriptsB);
		
		lblMinRecording = new JLabel("First recording:");
		panelScriptsB.add(lblMinRecording);
		
		NumberFormat format = NumberFormat.getNumberInstance();
		format.setMinimumFractionDigits(0);
		format.setMinimumIntegerDigits(0);
		format.setGroupingUsed(false);
		
		textField_2 = new JFormattedTextField(format);
		textField_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				checkScriptExperimentRangeLowerSet();
			}
		});
		textField_2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				checkScriptExperimentRangeLowerSet();
			}
		});
		panelScriptsB.add(textField_2);
		textField_2.setColumns(5);
		
		rigidArea_5 = Box.createRigidArea(new Dimension(20, 20));
		panelScriptsB.add(rigidArea_5);
		
		lblMaxRecording = new JLabel("Last recording:");
		panelScriptsB.add(lblMaxRecording);
		
		textField_3 = new JFormattedTextField(format);
		textField_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				checkScriptExperimentRangeHigherSet();
			}
		});
		textField_3.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				checkScriptExperimentRangeHigherSet();
			}
		});
		panelScriptsB.add(textField_3);
		textField_3.setColumns(5);
		
		tableScripts = new CustomTable(server.getScriptManager());
		tableScripts.setFillsViewportHeight(true);
		tableScripts.setRowHeight((int)(tableScripts.getRowHeight() * uiScaleFactor));
		tableScripts.setDefaultRenderer(Integer.class, new MyIntegerRenderer(false));
		tableScripts.setDefaultRenderer(Double.class, new MyDoubleRenderer(false));
		tableScripts.setDefaultRenderer(String.class, new MyNoteStringRenderer());
		tableScripts.setDefaultRenderer(Long.class, new MyLongRenderer(false));
		
		textAreaScripts = new JTextArea();
		textAreaScripts.setText("");
		textAreaScripts.setRows(5);
		textAreaScripts.setLineWrap(true);
		textAreaScripts.setEditable(false);
		
		scrollPane_6 = new JScrollPane(tableScripts);
		scrollPane_7 = new JScrollPane(textAreaScripts);
		scrollPane_7.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollPane_6, scrollPane_7);
		splitPane.setAlignmentY(Component.CENTER_ALIGNMENT);
		splitPane.setAlignmentX(Component.CENTER_ALIGNMENT);
		splitPane.setResizeWeight(0.5);
		panelScripts.add(splitPane);
		
		Dimension minimumSize = new Dimension(100, 50);
		scrollPane_6.setMinimumSize(minimumSize);
		scrollPane_7.setMinimumSize(minimumSize);
		
		textAreaLog = new JTextArea();
		textAreaLog.setEditable(false);
		textAreaLog.setLineWrap(true);
		
		logScrollPane_8 = new JScrollPane(textAreaLog);
		logScrollPane_8.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		logScrollPane_8.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tabbedPane.addTab("Log", null, logScrollPane_8, null);

		textAreaAbout = new JPanel();
		tabbedPane.addTab("About", null, textAreaAbout, null);
		
		lblNewLabel_1 = new JLabel("<html><div style:'text-align: center'><br />" 
				+ "FSenSync Server by Förger Analytics<br/>"
				+ "<br/>"
				+ "Copyright 2017-2019, Klaus Förger, <br />klaus@forger.fi<br />"
				+ "<br/>"
				+ "Server version: " + SyncServer.serverVersion + "<br />"
				+ "Minimum supported app major version: " + server.minClientVersionNumber + "<br />"
				+ "Maximum supported app major version: " + server.maxClientVersionNumber + "<br />"
				+ "<br/>"
				+ "This program is free software: you can redistribute it and/or modify<br/>"
				+ "it under the terms of the GNU General Public License as published by<br/>"
				+ "the Free Software Foundation, either version 3 of the License, or<br/>"
				+ "(at your option) any later version.<br/>"
				+ "<br/>"
				+ "</div></html>");
				
				
		textAreaAbout.add(lblNewLabel_1);
		
		extraColumnsHidden = false;
		extraColumns = new TableColumn[3];
		TableColumnModel tcm = tableApps.getColumnModel();
		extraColumns[0] = tcm.getColumn(1);
		extraColumns[1] = tcm.getColumn(3);
		extraColumns[2] = tcm.getColumn(4);
		hideShowExtraColumns();
	}
	
	private void checkScriptExperimentRangeHigherSet()
	{
		if (textField_3.getText().length() > 0 && textField_2.getText().length() > 0)
		{
			try
			{
				if (Integer.valueOf(textField_3.getText()) < Integer.valueOf(textField_2.getText()))
				{
					textField_2.setText(textField_3.getText());
				}
			}
			catch (Exception ex) {}
		}
	}
	
	private void checkScriptExperimentRangeLowerSet()
	{
		if (textField_2.getText().length() > 0 && textField_3.getText().length() > 0)
		{
			try
			{
				if (Integer.valueOf(textField_2.getText()) > Integer.valueOf(textField_3.getText()))
				{
					textField_3.setText(textField_2.getText());
				}
			}
			catch (Exception ex) {}
		}
	}
	
	public void appendToScriptLog(final String text)
	{
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	if (textAreaScripts != null)
				{
					textAreaScripts.append(text + "\n");
					JScrollBar v = scrollPane_7.getVerticalScrollBar();
					v.setValue(v.getMaximum());
				}
		    }
		});
	}

	public void addToLog(final String t)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				textAreaLog.append(t);
			}
		});
	}
	
	public void setCurrentRecording(final int i)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				currentRecording.setText(String.valueOf(i));
			}
		});
	}
	
	public void setNetworkStateText(final String text)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				networkStateText.setText(text);
			}
		});
	}

	public void setState(final String text)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				serverState1.setText(text);
				serverState2.setText(text);
				serverState3.setText(text);
				serverState4.setText(text);
			}
		});
	}
	
	public void setSavedText(final String text)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				saveLabel.setText(text);
			}
		});
	}
	
	private class SelectionRenderer extends DefaultTableCellRenderer
	{
		private static final long serialVersionUID = 1L;
		Boolean renderSelection;
		
		public SelectionRenderer(Boolean renderSelection)
		{
			this.renderSelection = renderSelection;
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
	    {
	        final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        TableModel model = table.getModel();
	        if (renderSelection && (Boolean)model.getValueAt(table.convertRowIndexToModel(row), 0))
	        {
	        	c.setBackground(Color.CYAN);
	        }
	        else
	        {
	        	c.setBackground(Color.WHITE);
	        }
	        return c;
	    }
	}
	
	private class MyIntegerRenderer extends SelectionRenderer 
	{
		private static final long serialVersionUID = 1L;
		public MyIntegerRenderer(Boolean renderSelection)
		{
			super(renderSelection);
			setHorizontalAlignment( JLabel.RIGHT );
		}

	    public void setValue(Object value) {
	    	if (value != null)
	    	{
	    		if ((int)value == -1)
	    		{
	    			setText("?");
	    		}
	    		else
	    		{
	    			setText(String.valueOf(value));
	    		}
	    	}
	    	else
	    	{
	    		setText("?");
	    	}
	    }
	    
	    
	}
	
	private class MyStringRenderer extends SelectionRenderer
	{
		private static final long serialVersionUID = 1L;

		public MyStringRenderer(Boolean renderSelection)
		{
			super(renderSelection);
			setHorizontalAlignment( JLabel.CENTER );
		}
	}
	
	private class MyNoteStringRenderer extends DefaultTableCellRenderer
	{
		private static final long serialVersionUID = 1L;

		public MyNoteStringRenderer()
		{
			super();
			setHorizontalAlignment( JLabel.LEFT );
		}
	}
	
	private class MyLongRenderer extends SelectionRenderer
	{
		private static final long serialVersionUID = 1L;

		public MyLongRenderer(Boolean renderSelection)
		{
			super(renderSelection);
			setHorizontalAlignment( JLabel.CENTER );
		}
	}
	
	private class MyBooleanRenderer extends JCheckBox implements TableCellRenderer
	{
		private static final long serialVersionUID = 1L;

		public MyBooleanRenderer()
		{
			super();
			setHorizontalAlignment(JLabel.CENTER);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
	    {
	        TableModel model = table.getModel();
	        if ((Boolean)model.getValueAt(table.convertRowIndexToModel(row), 0))
	        {
	        	setBackground(Color.CYAN);
	        	setSelected(true);
	        }
	        else
	        {
	        	setBackground(Color.WHITE);
	        	setSelected(false);
	        }
	        return this;
	    }
	}
	
	private class ContactAgeRenderer extends DefaultTableCellRenderer
	{
		private static final long serialVersionUID = 1L;
		private Color green;

		public ContactAgeRenderer()
		{
			super();
			setHorizontalAlignment( JLabel.CENTER);
			green = new Color(0.4f, 1.0f, 0.4f);
		}
		
		public void setValue(Object value)
		{
	    	if (value != null)
	    	{
	    		setText(((ContactAge)value).text);
	    		if (((ContactAge)value).age < 0l)
	    		{
	    			setBackground(Color.WHITE);
	    		}
	    		else
	    		{
	    			if (((ContactAge)value).age < 15000L)
		    		{
		    			setBackground(green);
		    		}
	    			else
	    			{
	    				setBackground(Color.WHITE);
	    			}
	    		}
	    	}
	    	else
	    	{
	    		setText("?");
	    		
	    	}
	    }
	}
	
	private class SensorHealthRenderer extends DefaultTableCellRenderer
	{
		private static final long serialVersionUID = 1L;

		public SensorHealthRenderer()
		{
			super();
			setHorizontalAlignment( JLabel.CENTER);
		}
		
		public void setValue(Object value)
		{
			setBackground(Color.WHITE);
	    	if (value != null)
	    	{
	    		setText(((SensorHealthText)value).text);
	    	}
	    	else
	    	{
	    		setText("?");
	    	}
	    }
	}
	
	private class BatteryLevelRenderer extends DefaultTableCellRenderer
	{
		private static final long serialVersionUID = 1L;
		private Color warn1;
		private Color warn2;
		private Color warn3;

		public BatteryLevelRenderer()
		{
			super();
			setHorizontalAlignment( JLabel.RIGHT);
			warn1 = new Color(1.0f, 0.8f, 0.7f);
			warn2 = new Color(1.0f, 0.6f, 0.4f);
			warn3 = new Color(1.0f, 0.3f, 0.1f);
		}
		
		public void setValue(Object value)
		{
	    	if (value != null)
	    	{
	    		int level = ((BatteryLevel)value).level;
	    		if (((BatteryLevel)value).level >= 0)
	    		{
	    			setText(String.valueOf(level));
	    		}
	    		else
	    		{
	    			setText("?");
	    		}
	    		
	    		if (level > 35  || level < 0)
	    		{
	    			setBackground(Color.WHITE);
	    		}
	    		else
	    		{
	    			if (level > 20)
		    		{
		    			setBackground(warn1);
		    		}
	    			else
	    			{
	    				if (level > 10)
			    		{
			    			setBackground(warn2);
			    		}
		    			else
		    			{
		    				setBackground(warn3);
		    			}
	    			}
	    		}
	    	}
	    	else
	    	{
	    		setText("?");
	    	}
	    }
	}
	
	private class StorageLevelRenderer extends DefaultTableCellRenderer
	{
		private static final long serialVersionUID = 1L;
		private Color warn1;
		private Color warn2;
		private Color warn3;

		public StorageLevelRenderer()
		{
			super();
			setHorizontalAlignment( JLabel.RIGHT);
			warn1 = new Color(1.0f, 0.8f, 0.7f);
			warn2 = new Color(1.0f, 0.6f, 0.4f);
			warn3 = new Color(1.0f, 0.3f, 0.1f);
		}
		
		public void setValue(Object value)
		{
	    	if (value != null)
	    	{
	    		long level = ((StorageLevel)value).level;
	    		if (((StorageLevel)value).level >= 0)
	    		{
	    			setText(String.valueOf(level));
	    		}
	    		else
	    		{
	    			setText("?");
	    		}
	    		
	    		if ((level > 350 && !((StorageLevel)value).appType.equals("VID")) || level > 1000 || level < 0)
	    		{
	    			setBackground(Color.WHITE);
	    		}
	    		else
	    		{
	    			if ((level > 200 && !((StorageLevel)value).appType.equals("VID")) || level > 500 )
		    		{
		    			setBackground(warn1);
		    		}
	    			else
	    			{
	    				if (level > 110)
			    		{
			    			setBackground(warn2);
			    		}
		    			else
		    			{
		    				setBackground(warn3);
		    			}
	    			}
	    		}
	    	}
	    	else
	    	{
	    		setText("?");
	    	}
	    }
	}
	
	private class MyDoubleRenderer extends SelectionRenderer 
	{
		private static final long serialVersionUID = 1L;
		private NumberFormat formatter;
		public MyDoubleRenderer(Boolean renderSelection) 
		{
			super(renderSelection);
			setHorizontalAlignment( JLabel.RIGHT );
			formatter = new DecimalFormat("#0.0");
		}

	    public void setValue(Object value) {
	    	if (value != null)
	    	{
	    		if (((Double)value).isNaN())
	    		{
	    			setText("?");
	    		}
	    		else
	    		{
	    			setText(formatter.format(value));
	    		}
	    	}
	    	else
	    	{
	    		setText("?");
	    	}
	    }
	}
	
	public String getStimulusFileName()
	{
		if (comboBoxUploads.getSelectedItem() == null)
		{
			return "";
		}
		else
		{
			return (String)comboBoxUploads.getSelectedItem();
		}
	}
	
	public void updateUploadsComboBox(File[] files)
	{
		while (comboBoxUploads.getItemCount() > 0)
		{
			comboBoxUploads.removeItemAt(0);
		}
		comboBoxUploads.addItem("");
		for (File f : files)
		{
			comboBoxUploads.addItem(f.getName());
		}
	}
	
	public void disableButtons()
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				buttonStart.setEnabled(false);
				buttonStop.setEnabled(false);
				btnRefreshDevices.setEnabled(false);
				buttonFetchFileInfo.setEnabled(false);
				buttonDownloadFiles.setEnabled(false);
				buttonLoadTags.setEnabled(false);
				buttonSendTags.setEnabled(false);
				buttonClose.setEnabled(false);
				buttonDelete.setEnabled(false);
				buttonCalculateSync.setEnabled(false);
				btnStartStimuli.setEnabled(false);
				btnStopStimuli.setEnabled(false);
				buttonUploadFile.setEnabled(false);
				buttonSoundFile.setEnabled(false);
				buttonDownloadSmallFiles.setEnabled(false);
				buttonDisableSound.setEnabled(false);
				buttonEnableSound.setEnabled(false);
				btnDeleteStimulusFiles.setEnabled(false);
				buttonSendSetting.setEnabled(false);
				buttonSelectedFiles.setEnabled(false);
				buttonDownloadMeta.setEnabled(false);
				buttonReadMeta.setEnabled(false);
				
				buttonCancelFileOperation.setEnabled(true);
				buttonCancelUploads.setEnabled(true);
			}
		});
	}
	
	public void enableButtons()
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				buttonStart.setEnabled(true);
				buttonStop.setEnabled(true);
				btnRefreshDevices.setEnabled(true);
				buttonFetchFileInfo.setEnabled(true);
				buttonDownloadFiles.setEnabled(true);
				buttonLoadTags.setEnabled(true);
				buttonSendTags.setEnabled(true);
				buttonClose.setEnabled(true);
				buttonDelete.setEnabled(true);
				buttonCalculateSync.setEnabled(true);
				btnStartStimuli.setEnabled(true);
				btnStopStimuli.setEnabled(true);
				buttonUploadFile.setEnabled(true);
				buttonSoundFile.setEnabled(true);
				buttonDownloadSmallFiles.setEnabled(true);
				buttonDisableSound.setEnabled(true);
				buttonEnableSound.setEnabled(true);
				btnDeleteStimulusFiles.setEnabled(true);
				buttonSendSetting.setEnabled(true);
				buttonSelectedFiles.setEnabled(true);
				buttonDownloadMeta.setEnabled(true);
				buttonReadMeta.setEnabled(true);
				
				buttonCancelFileOperation.setEnabled(false);
				buttonCancelUploads.setEnabled(false);
			}
		});
	}
	
	public void hideShowExtraColumns()
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				if (extraColumnsHidden)
				{
					TableColumnModel tcm = tableApps.getColumnModel();
					for (TableColumn t : extraColumns)
					{
						tcm.addColumn(t);
					}
				}
				else
				{
					TableColumnModel tcm = tableApps.getColumnModel();
					for (TableColumn t : extraColumns)
					{
						tcm.removeColumn(t);
					}
				}
				extraColumnsHidden = !extraColumnsHidden;
			}
		});
	}
}
