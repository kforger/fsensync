/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class NoteManager  extends SafeAbstractTableModel
{
	private static final long serialVersionUID = 1L;
	
	public ArrayList<Note> notes;
	private SyncServer server;
	private final String[] columnNames = {"Timestamp", "Time", "Text"};

	public NoteManager(SyncServer server)
	{
		this.server = server;
		notes = new ArrayList<Note>();
		loadNotes();
	}
	
	public int getColumnCount()
	{
        return columnNames.length;
    }

	public int getRowCount()
	{
		return notes.size();
	}
	
	public String getColumnName(int col) {
        return columnNames[col];
    }
	
	@Override
	public Object getValueAt(int row, int col) {
		if (notes.size() > row)
		{
    		switch (col)
    		{
    		case 0: return notes.get(row).timeStamp;
    		case 1: return notes.get(row).timeHuman;
    		case 2: return notes.get(row).text;
    		default: return null;
    		}
		}
		else
		{
			return null;
		}
	}
	
	public Class<?> getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col)
    {
        if (col == 2)
        {
        	return true;
        }
        else
        {
        	return false;
        }
    }
	
    public void addNote(String text)
    {
    	notes.add(new Note(SyncServer.getCurrentTimeMillis(), text));
    	saveNotes();
		fireTableDataChanged();
		server.gui.setSavedText("Unsaved changes.");
    }
    
	public void createNewEmptyNote()
	{
		notes.add(new Note(SyncServer.getCurrentTimeMillis(), ""));
		fireTableDataChanged();
		server.gui.setSavedText("Unsaved changes.");
	}

	@Override
	public void setValueAt(Object value, int row, int col)
	{
		if (notes.size() > row && col == 2)
		{
			String s = (String)value;
			s = s.replace(",", ".");
			notes.get(row).text = s;
		}
		server.gui.setSavedText("Unsaved changes.");
	}
	
	public void deleteNotes(int[] rows)
	{
		Arrays.sort(rows);
		for (int i = rows.length - 1; i >= 0; i--)
		{
			if (notes.size() > rows[i])
			{
				notes.remove(rows[i]);
			}
		}
		fireTableDataChanged();
		server.gui.setSavedText("Unsaved changes.");
	}

	public Boolean saveNotes()
	{
		Boolean ok = true;
		File file = new File(server.getExperimentPath() + File.separator + server.getExperimentName() + "_notes.txt");
    	if (file.exists())
        {
            file.delete();
        }
    	PrintWriter output;
		try {
			output = new PrintWriter(file, "UTF-8");
			for (int i = 0; i < notes.size(); i++)
			{
				Note n = notes.get(i);
				output.println(n.timeStamp + "," + n.timeHuman + "," + n.text);
			}
            output.close();
		} catch (FileNotFoundException e) {
			server.log(e);
			ok = false;
		} catch (UnsupportedEncodingException e) {
			server.log(e);
			ok = false;
		}
		return ok;
	}
	
	public Boolean loadNotes()
    {
		Boolean ok = true;
		File file = new File(server.getExperimentPath() + File.separator + server.getExperimentName() + "_notes.txt");
    	if (file.exists())
    	{
    		notes = new ArrayList<Note>();
    		try
    		{
    			BufferedReader br = new BufferedReader(new FileReader(file));
    			String line;
    			while ((line = br.readLine()) != null)
    			{
    				String[] input = line.split(",");
    				if (input.length == 3)
    				{
	    				long timestamp = Long.valueOf(input[0]);
	    				notes.add(new Note(timestamp, input[1], input[2]));
    				}
    				else if (input.length == 2)
    				{
	    				long timestamp = Long.valueOf(input[0]);
	    				notes.add(new Note(timestamp, input[1], ""));
    				}
    				else
    				{
    					ok = false;
    				}
    			}
    			br.close();
    			if (!ok)
    			{
    				server.log("Badly formed notes in the text file.");
    			}
    		} catch (FileNotFoundException e) 
    		{
    			server.log(e);
    		} catch (IOException e) 
    		{
    			server.log(e);
    		}
    	}
    	fireTableDataChanged();
    	return ok;
    }
	
	public void createNoteBackup(int recordingNumber)
    {
    	File oldFile = new File(server.getExperimentPath() + File.separator + server.getExperimentName() + "_notes.txt");
    	File backupFile = new File(server.getExperimentPath() + File.separator + "backups" + File.separator
    			+ server.getExperimentName() + "_"
    			+ String.format(Locale.ROOT, "%03d", recordingNumber) + "_notes.txt");
    	if (oldFile.exists())
        {
	    	backupFile.mkdirs();
    		try {
				Files.copy(oldFile.toPath(), backupFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				server.log(e);
			}
        }
    }
}
