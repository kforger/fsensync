/*
FSenSync Server
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

public class SensorHealthText  implements Comparable {
	public String text;
	
	public SensorHealthText(String text)
	{
		this.text = text;
	}
	
	public boolean equals(Object other)
	{
	    if (other == null) return false;
	    if (other == this) return true;
	    if (other instanceof SensorHealthText)
	    {
	    	SensorHealthText otherText = (SensorHealthText)other;
	    	
	    	if (this.text.equals(otherText.text))
	    	{
	    		return true;
	    	}
	    	else
	    	{
	    		return false;
	    	}
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	public int compareTo(Object other)
	{
		if (other instanceof SensorHealthText)
		{
			return compareTo((SensorHealthText)other);
		}
		else
		{
			return 0;
		}
	}
	
	
	public int compareTo(SensorHealthText other)
	{
		if (this.equals(other))
		{
			return 0;
		}
		else
		{
			return this.text.compareTo(other.text);
		}
	}
}
