/*
FSenSync Desktop Libraries
Copyright (C) 2019  Klaus Förger, Förger Analytics, klaus@forger.fi

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


package fi.forger.desktopsynclibrary;

import java.io.File;
import java.io.PrintWriter;


/**
 * 
 * This class provides a very simple way to get timestamps synced with the FSenSync Server, 
 * and through it with other synced apps.
 * The resulting app shows in the FSenSync Server in state "Waiting for sensors",
 * thus no control/recording/downloads are possible. To get those extend the 
 * {@link fi.forger.desktopsynclibrary.SyncedClient}.
 * 
 * @author Klaus Förger, klaus@forger.fi
 *
 */
public class SimpleSync {

	private SyncWrapper wrapper;

	/**
	 * @param settingsFolder path to folder where FSenSync setting files will be stored
	 */
	public SimpleSync(String settingsFolder)
	{
		this(settingsFolder, "", "", "");
	}

	/**
	 * @param settingsFolder path to folder where FSenSync setting files will be stored
	 * @param tag1 tag number one that will sent to the FSenSync Server
	 */
	public SimpleSync(String settingsFolder, String tag1)
	{
		this(settingsFolder, tag1, "", "");
	}

	/**
	 * @param settingsFolder path to folder where FSenSync setting files will be stored
	 * @param tag1 tag number one that will sent to the FSenSync Server
	 * @param tag2 tag number two that will sent to the FSenSync Server
	 */
	public SimpleSync(String settingsFolder, String tag1, String tag2)
	{
		this(settingsFolder, tag1, tag2, "");
	}

	/**
	 * @param settingsFolder path to folder where FSenSync setting files will be stored
	 * @param tag1 tag number one that will sent to the FSenSync Server
	 * @param tag2 tag number two that will sent to the FSenSync Server
	 * @param tag3 tag number three that will sent to the FSenSync Server
	 */
	public SimpleSync(String settingsFolder, String tag1, String tag2, String tag3) {
		File file = new File(settingsFolder);
		wrapper = new SyncWrapper(settingsFolder, tag1, tag2, tag3);
	}

	/**
	 * 
	 * Returns the server time in milliseconds if the isSynced() has returned true.
	 * There can be jumps of a few milliseconds that correct the drifts between the
	 * server clock and the client clock.
	 * 
	 * @return the server time in milliseconds
	 */
	public long serverTimeInMillis()
	{
		return (System.nanoTime() + wrapper.syncServ.getOffsetNanosToServerStreaming()) / SyncService.NANOS_IN_MILLI;
	}

	/**
	 * 
	 * Tells if the time from the serverTimeInMillis() represents the server time.
	 * 
	 * @return is true if the clock sync over LAN has been achieved
	 */
	public boolean isSynced()
	{
		return wrapper.syncServ.getAppState() >= SyncService.STATE_SYNCED_AND_WAITING_FOR_SENSORS;
	}

	private class SyncWrapper implements SyncedClient {

		public SyncWrapper(String settingsFolder, String tag1, String tag2, String tag3) {
			File file = new File(settingsFolder);
			SyncService.appTag1 = tag1;
			SyncService.appTag2 = tag2;
			SyncService.appTag3 = tag3;
			syncServ = new SyncService(this, "PROC", 12, file);
		}

		public SyncService syncServ;
		public Boolean getIsOnBackGround() { return false; };
		public Boolean getRecordingServiceReady() { return false; };
		public void setTagsToUi() {};
		public void updateExperimentInfoToUi() {};
		public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream) { return false; };
		public Boolean stopRecording() { return false; };
		public void newSyncState(int syncState, int oldState, String stateString) {};
		public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {};
		public void cancelStimuli() {};
		public void showExtraMessageOnScreen(String message) {};
		public String getAppSetting(String setting) { return ""; };
		public void setAppSetting(String setting, String value) {};
		public void closeApp() {}
	}
}
