/*
FSenSync Desktop player
Copyright (C) 2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.desktopplayer;

import processing.core.PApplet;
import processing.video.*;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.PrintWriter;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import fi.forger.desktopsynclibrary.*;

public class DesktopPlayer extends PApplet implements SyncedClient
{

	public static final int VERSION_CODE = 102;
	public static SyncService syncServ;
	public PrintWriter outputStreamStimulus;
    private String stimuliName;
    private Timer timer;
    private static File mainFolder;
    
    private boolean movieReady;
    private long nanosLoadedFrame;
    private long nanosLastShownFrame;
    private float timeLoadedFrame;
    
    private boolean isAudioOnly;
	
	public static void main(String[] args)
	{
		mainFolder = null;
		try
		{
			mainFolder = new File(DesktopPlayer.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
		}
		catch (Exception e)
		{
			mainFolder = new File(ClassLoader.getSystemClassLoader().getResource(".").getPath());
		}
		if (mainFolder.getAbsolutePath().endsWith("jar"))
		{
			mainFolder = mainFolder.getParentFile();
		}
		
		PApplet.main("fi.forger.desktopplayer.DesktopPlayer");
	}

	Movie movie;
	private int vidOrigWidth;
	private int vidOrigHeight;
	private int vidDesiredWidth;
	private int vidDesiredHeight;
	private int vidXOffset;
	private int vidYOffset;
	private float timeLastFrame = 0.0f;
	private boolean firstFrame = true;
	private int rotation;
	private boolean cancelStimulus;
	private String extraMessage;

	public void settings()
	{
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] devices = env.getScreenDevices();
		int opt1 = JOptionPane.YES_NO_OPTION;
		Object[] opt2 = {"window", "1: fullscreen"};
		if (devices.length > 1)
		{
			opt1 = JOptionPane.YES_NO_CANCEL_OPTION;
			opt2 = new String[]{"window", "1: fullscreen", "2: fullscreen"};
		}
		
		int screen = JOptionPane.showOptionDialog(null,
		    "Which display you want to use?",
		    "DesktopPlayer",
		    opt1,
		    JOptionPane.QUESTION_MESSAGE,
		    null,
		    opt2,
		    opt2[0]);
		
		if (screen == 0)
		{
			size(600,400,P3D);
		}
		else
		{
			println(screen);
			fullScreen(P3D, screen);
		}
		
		rotation = 0;
	}

	public void setup() {
		
		if (syncServ == null)
        {
            syncServ = new SyncService(this, "DEPLAY", 12, mainFolder);
        }
		timer = new Timer(true);
		
		background(0);
		frameRate(60);
		movieReady = false;
		surface.setResizable(true);
		extraMessage = "";
	}

	public void movieEvent(Movie m)
	{
		m.read();
		nanosLoadedFrame = System.nanoTime();
		timeLoadedFrame = movie.time();
	}
	
	public void keyPressed()
	{
		if (this.key == 'q')
		{
			if (syncServ != null)
			{
				syncServ.sendClosingAppToServer();
			}
			exit();
		}
		if (this.key == 'r')
		{
			rotation = (rotation + 1) % 4;
		}
	}

	public void draw()
	{
		if (movieReady && !isAudioOnly)
		{
			if (vidOrigWidth == 0 || vidOrigHeight == 0)
			{
				vidOrigWidth = movie.width;
				vidOrigHeight = movie.height;
				
			}

			if (rotation == 0 || rotation == 2)
			{
				double vidAspect = (double)vidOrigWidth / (double)vidOrigHeight;
	            double screenAspect = (double)width / (double)height;

	            if (vidAspect > screenAspect)
	            {
	            	vidDesiredWidth = width;
	            	vidDesiredHeight = vidOrigHeight * width / vidOrigWidth;
	            	vidXOffset = 0;
	            	vidYOffset = (height - vidDesiredHeight) / 2;
	            }
	            else
	            {
	            	vidDesiredHeight = height;
	            	vidDesiredWidth = vidOrigWidth * height / vidOrigHeight;
	            	vidXOffset = (width - vidDesiredWidth) / 2;
	            	vidYOffset = 0;
	            }
			}
			else
			{
				double vidAspect = (double)vidOrigWidth / (double)vidOrigHeight;
	            double screenAspect = (double)height / (double)width;

	            if (vidAspect > screenAspect)
	            {
	            	vidDesiredWidth = vidOrigHeight * height / vidOrigWidth;
	            	vidDesiredHeight = height;
	            	vidXOffset = (width - vidDesiredWidth) / 2;
	            	vidYOffset = 0;
	            }
	            else
	            {
	            	vidDesiredHeight = vidOrigWidth * width / vidOrigHeight;
	            	vidDesiredWidth = width;
	            	vidXOffset = 0;
	            	vidYOffset = (height - vidDesiredHeight) / 2;
	            }
			}

			if (nanosLoadedFrame != nanosLastShownFrame)
			{
				background(0);
				nanosLastShownFrame = nanosLoadedFrame;
				switch (rotation)
				{
					case 0:
						image(movie, vidXOffset, vidYOffset, vidDesiredWidth, vidDesiredHeight);
						break;
					case 1:
						translate(width, 0);
						rotate(HALF_PI);
						image(movie, vidYOffset, vidXOffset, vidDesiredHeight, vidDesiredWidth);
						break;
					case 2:
						translate(width, height);
						rotate(HALF_PI * 2f);
						image(movie, vidXOffset, vidYOffset, vidDesiredWidth, vidDesiredHeight);
						break;
					case 3:
						translate(0, height);
						rotate(-HALF_PI);
						image(movie, vidYOffset, vidXOffset, vidDesiredHeight, vidDesiredWidth);
						break;
					default:
						break;
				}
			}
		}
		else
		{
			background(0);
			if (movieReady && isAudioOnly)
			{
				// Only the black background
			}
			else
			{
				textSize(28);
				text("FSenSync DesktopPlayer", 10, 30);
				text("State: " + syncServ.getStateString(), 10, 60);
				text("rotation (r): " + (rotation * 90), 10, 90);
				text("quit (q)", 10, 120);
				text(extraMessage, 10, 150);
			}
			
		}
	}
	
	// FSenSync methods
	
	@Override
	public Boolean getIsOnBackGround() {
		return false;
	}

	@Override
	public Boolean getRecordingServiceReady() {
		return true;
	}

	@Override
	public void setTagsToUi() {}

	@Override
	public void updateExperimentInfoToUi() {}

	@Override
	public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream) {
		Boolean ok = true;
		extraMessage = "";

        try
        {
            metaOutputStream.println("App version: " + VERSION_CODE);
            metaOutputStream.close();
        }
        catch (Exception ex)
        {
            System.out.println("Error when writing meta output file.");
        }

        try {
            File fileStimulus = new File(fileBaseName + "DEPLAY.csv");
            if (fileStimulus.exists()) {
                syncServ.sendErrorToServer("Refused to overwrite old recording with number " + recordingNumber);
                return false;
            }
            fileStimulus.createNewFile();
            outputStreamStimulus = new PrintWriter(fileStimulus, "UTF-8");
            try {
                String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,EventType,PlayProgress(milliseconds),FileName";
                outputStreamStimulus.println(header);
                outputStreamStimulus.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to file.");
            }
        } catch (Exception ex) {
            ok = false;
            System.out.println("Error when opening output file.");
        }
        return ok;
	}

	@Override
	public Boolean stopRecording() {
		outputStreamStimulus.close();
        return true;
	}

	@Override
	public void newSyncState(int syncState, int oldState, String stateString) {}

	@Override
	public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {
		this.stimuliName = stimuliName;
		cancelStimulus = false;
		
		if (stimuliName.endsWith(".mp3") || stimuliName.endsWith(".wav") || stimuliName.endsWith(".ogg"))
		{
			isAudioOnly = true;
		}
		else
		{
			isAudioOnly = false;
		}
		
		showVideo();
	}

	@Override
	public void cancelStimuli()
	{
		cancelStimulus = true;
		
		long actualTime = System.nanoTime();
    	long playTime = (long)(movie.time() * 1000);
    	String output= getTimeAndBounds(actualTime) + ",cancelled," + playTime + "," + stimuliName;
    	try {
            outputStreamStimulus.println(output);
            outputStreamStimulus.flush();
        } catch (Exception ex) {
            System.out.println("Error while writing to stimulus log file.");
        }
    
		movie.stop();

		movieReady = false;
	}

	@Override
	public void showExtraMessageOnScreen(String message)
	{
		extraMessage = message;
	}

	@Override
	public void enableScreenTouchSound() {}

	@Override
	public void disableScreenTouchSound() {}

	@Override
	public String getAppSetting(String setting) {
		return "";
	}

	@Override
	public void setAppSetting(String setting, String value) {}

	@Override
	public void closeApp() {
		if (syncServ != null)
		{
			syncServ.sendClosingAppToServer();
		}
		exit();
	}
	
	public void showVideo()
	{
		extraMessage = "";
		File test = new File(SyncService.getStimulusDirectory().getAbsolutePath() + File.separator + stimuliName);
		if (!test.exists())
		{
			System.out.println("Did not have stimulus file: '" + stimuliName + "'");
            showExtraMessageOnScreen("Did not have stimulus file: '" + stimuliName + "'");
            syncServ.delayedCancelAndStop();
            syncServ.sendErrorToServer("Did not have stimulus file: '" + stimuliName + "'");
            return;
		}
		
		try
		{
			movie = new Movie(this, SyncService.getStimulusDirectory().getAbsolutePath() + File.separator + stimuliName);
			movie.play();
			movie.jump(0);
			movie.pause();
			vidOrigWidth = movie.width;
			vidOrigHeight = movie.height;
			movie.play();
		}
		catch (Exception ex)
		{
			System.out.println("Error when starting stimuli, , a GStreamer issue?: '" + stimuliName + "'");
            showExtraMessageOnScreen("Error when starting stimuli, a GStreamer issue?");
            syncServ.delayedCancelAndStop();
            syncServ.sendErrorToServer("Error when starting stimuli, , a GStreamer issue?: '" + stimuliName + "'");
            return;
		}
		
        movieReady = true;
        
        long actualTime = System.nanoTime();
        long currentPlayTime = (long)(movie.time() * 1000);
        
        DesktopPlayer.syncServ.setStatePlayingStimuli();
        
        String output = getTimeAndBounds(actualTime) + ",started," + currentPlayTime + "," + stimuliName;
        try {
            outputStreamStimulus.println(output);
            outputStreamStimulus.flush();
        } catch (Exception ex) {
            System.out.println("Error while writing to stimulus log file.");
        }
        
        timer.schedule(new LogPlayTask(), 100, 100);
	}
	
    public String getTimeAndBounds(long actualTime)
    {
        long reportedTimeStamp = (actualTime + DesktopPlayer.syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
        long reportedUpperBound = DesktopPlayer.syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
        long reportedLowerBound = DesktopPlayer.syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;
        return reportedTimeStamp + "," + reportedUpperBound + "," + reportedLowerBound;
    }
    
    private class LogPlayTask extends TimerTask {

    	int count;
    	int countSame;
    	float timeLastFrame;
    	boolean isStopped;
    	float audioTime;
    	float audioTimeLast;
    	
        public LogPlayTask() 
        {
        	count = -1;
        	timeLastFrame = 0;
        	isStopped = false;
        	audioTime = 0;
        	audioTimeLast = 0;
        }

        @Override
        public void run() {
        	
        	if (cancelStimulus)
        	{
        		this.cancel();
        		return;
        	}
        	
        	count = (count + 1) % 10;
        	
        	if (!isAudioOnly)
        	{
        		if (timeLoadedFrame == timeLastFrame)
    	    	{
    	    		countSame += 1;
    	    	}
    	    	else
    	    	{
    	    		countSame = 0;
    	    	}
    	    	timeLastFrame = timeLoadedFrame;
        	}
        	else
        	{
        		audioTime = movie.time();
        		if (audioTime == audioTimeLast)
        		{
        			countSame += 1;
        		}
        		else
        		{
        			nanosLoadedFrame = System.nanoTime();
        			timeLoadedFrame = movie.time();
        		}
        		audioTimeLast = movie.time();
        	}
        	
        	if (countSame > 3)
	    	{
	    		isStopped = true;
	    	}
		    	
	    	if (count == 0 && !isStopped)
	    	{
	            if (movie != null) 
	            {
	                long actualTime = System.nanoTime();
	                float fTime = movie.time();
	                
	                if (fTime < movie.duration() - 0.5f)
	                {
		                long playTime = (long)(fTime * 1000);
	
		                String output = getTimeAndBounds(actualTime) + ",playing," + playTime + "," + stimuliName;
		                try {
		                    outputStreamStimulus.println(output);
		                    outputStreamStimulus.flush();
		                } catch (Exception ex) {
		                    System.out.println("Error while writing to stimulus log file.");
		                }
	                }
	            }
	    	}

		    	
        	if (isStopped)
            {
            	movie.stop();
            	movieReady = false;
            	
            	long actualTime = nanosLoadedFrame;
            	long playTime = (long)(timeLoadedFrame * 1000);
            	String output= getTimeAndBounds(actualTime) + ",stopped," + playTime + "," + stimuliName;
            	try {
                    outputStreamStimulus.println(output);
                    outputStreamStimulus.flush();
                } catch (Exception ex) {
                    System.out.println("Error while writing to stimulus log file.");
                }
            	
            	syncServ.playingStimuliEnded();
            	this.cancel();
            }
        }
    }

}
