This product includes software developed at
Förger Analytics (http://www.forger.fi/) (Copyright 2018-2019)
(Licensed under the GNU General Public License version 3)

This product includes software (FSenSync Desktop Libraries) developed at
Förger Analytics (http://www.forger.fi/) (Copyright 2018-2019)
(Licensed under the 2-Clause BSD License)

This product includes software developed by
Chandrasekhar Ramakrishnan / Illposed Software (Copyright (c) 2002-2014)
(Licensed under the 3-Clause BSD License)

This product includes software from the Processing distribution (core, serial, jSSC)
(Licensed under the GNU Lesser General Public License)
Source code is available from:
  http://www.forger.fi/downloads/files/processing-master.zip
  http://www.forger.fi/downloads/files/java-simple-serial-connector-processing.zip
