/*
FSenSync Serial Reader
Copyright (C) 2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.serialread;

import fi.forger.desktopsynclibrary.SyncService;
import processing.core.PApplet;
import processing.serial.Serial;

public class SerialPApplet extends PApplet {

	public boolean acceptSerial = false;
	public long lastsSerialDataNanoTime = -1;
	private SerialReader main;

	public SerialPApplet(SerialReader main)
	{
		super();
		this.main = main;
	}
	
	public void serialEvent(Serial port)
	{
		long actualTime = System.nanoTime();
		if (acceptSerial && port != null && SerialReader.syncServ != null)
		{
			while (port.available() > 0)
			{
				int val = port.read();
				
				if (main != null)
            	{
            		main.logReceivedSerial(val);
            	}
				
				String output;
	            if (SerialReader.syncServ.getAppState() == SyncService.STATE_RECORDING_DATA
	            		&& SerialReader.outputStream != null)
	            {
	            	output = getTimeAndBounds(actualTime) + "," + val;
	            	try {
	                    SerialReader.outputStream.println(output);
	                    SerialReader.outputStream.flush();
	                } catch (Exception ex) {
	                    System.out.println("Error while writing to log file.");
	                }
	            }

	            lastsSerialDataNanoTime = actualTime;
			}
		}
	}
	
    public String getTimeAndBounds(long actualTime)
    {
        long reportedTimeStamp = (actualTime + SerialReader.syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
        long reportedUpperBound = SerialReader.syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
        long reportedLowerBound = SerialReader.syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;
        return reportedTimeStamp + "," + reportedUpperBound + "," + reportedLowerBound;
    }
	
}
