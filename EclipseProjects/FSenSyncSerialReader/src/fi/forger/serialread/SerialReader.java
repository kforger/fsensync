/*
FSenSync Serial Reader
Copyright (C) 2018  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.serialread;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import fi.forger.desktopsynclibrary.SyncService;
import fi.forger.desktopsynclibrary.SyncedClient;
import processing.serial.*;

public class SerialReader implements SyncedClient {

	public static final int VERSION_CODE = 101;
	public static SyncService syncServ;
	public static PrintWriter outputStream;
	private Serial port;
	private static File mainFolder = null;
	private SerialGui gui;
	private SerialPApplet applet;
	public static String newline = System.getProperty("line.separator");
	private String messages;
	private boolean nativeLibsLoaded = false;
	private String selectedPortName;
	private Timer timer;
	private ArrayList<String> oldPortList;
	private static final String EMPTY_PORT_STRING = "-";
	
	public static void main(String[] args) {
		
		try
		{
			mainFolder = new File(SerialReader.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
		}
		catch (Exception e)
		{
			mainFolder = new File(ClassLoader.getSystemClassLoader().getResource(".").getPath());
		}
		if (mainFolder.getAbsolutePath().endsWith("jar"))
		{
			mainFolder = mainFolder.getParentFile();
		}
		
		SerialReader client = new SerialReader();
		if (syncServ == null)
        {
            syncServ = new SyncService(client, "SERIAL", 12, mainFolder);
        }
	}
	
	public SerialReader()
	{
		SerialGui.startGui(this);
		
		messages = "";
		selectedPortName = EMPTY_PORT_STRING;
        
        String osName = System.getProperty("os.name");
        String architecture = System.getProperty("os.arch");
        String fileSeparator = System.getProperty("file.separator");
        
        log("OS/architecture: " + osName + "/" + architecture);
        log("Main folder: " + mainFolder);

        String serialLib = "NativeLibraryNotSelected";
        
        if(osName.equals("Linux"))
        {
        	if(architecture.equals("i386") || architecture.equals("i686")){
        		serialLib = "linux32" + fileSeparator + "libjSSC-2.8.so";
            }
            else if(architecture.equals("amd64") || architecture.equals("universal")){
            	serialLib = "linux64" + fileSeparator + "libjSSC-2.8.so";
            }
        }
        else if(osName.startsWith("Win"))
        {
        	if(architecture.equals("i386") || architecture.equals("i686") || architecture.equals("x86")){
        		serialLib = "windows32" + fileSeparator + "jSSC-2.8.dll";
            }
            else if(architecture.equals("amd64") || architecture.equals("universal")){
            	serialLib = "windows64" + fileSeparator + "jSSC-2.8.dll";
            }
        }
        else if(osName.equals("Mac OS X") || osName.equals("Darwin"))
        {
        	serialLib = "macosx" + fileSeparator + "libjSSC-2.8.jnilib";
        }

        
        String lib = mainFolder + fileSeparator + "native_libs" + fileSeparator + serialLib;
        try {
        	System.load(lib);
            log("Native lib: " + lib);
            nativeLibsLoaded = true;
        } catch (UnsatisfiedLinkError e) {
        	e.printStackTrace();
        	log("Native lib problem: " + lib);
        	log(e.toString());
        	nativeLibsLoaded = false;
        }
		
		applet = new SerialPApplet(this);

		// the delay is needed to avoid a Processing related initialization issue
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		applet.acceptSerial = true;
		refreshPorts();
		
		timer = new Timer(true);
		timer.schedule(new CheckPorts(), 1000, 1000);
	}
	
	@Override
	public Boolean getIsOnBackGround() {
		return false;
	}

	@Override
	public Boolean getRecordingServiceReady() {
		return nativeLibsLoaded && port != null && port.active();
	}

	@Override
	public void setTagsToUi() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateExperimentInfoToUi() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream) {
		Boolean ok = true;

        try
        {
            metaOutputStream.println("App version: " + VERSION_CODE);
            metaOutputStream.close();
        }
        catch (Exception ex)
        {
            System.out.println("Error when writing meta output file.");
        }

        try {
            File fileStimulus = new File(fileBaseName + "SERIAL.csv");
            if (fileStimulus.exists()) {
                syncServ.sendErrorToServer("Refused to overwrite old recording with number " + recordingNumber);
                return false;
            }
            fileStimulus.createNewFile();
            outputStream = new PrintWriter(fileStimulus, "UTF-8");
            try {
                String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,SerialEvent";
                outputStream.println(header);
                outputStream.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to file.");
            }
        } catch (Exception ex) {
            ok = false;
            System.out.println("Error when opening output file.");
        }
        return ok;
	}

	@Override
	public Boolean stopRecording() {
		outputStream.close();
        return true;
	}

	@Override
	public void newSyncState(int syncState, int oldState, String stateString)
	{
		if (syncState != oldState)
		{
			log("App state: " + stateString);
		}
		if (gui != null)
		{
			gui.setAppStateLabel(stateString);
		}
	}

	@Override
	public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {}

	@Override
	public void cancelStimuli() {}

	@Override
	public void showExtraMessageOnScreen(String message) {}

	@Override
	public void enableScreenTouchSound() {}

	@Override
	public void disableScreenTouchSound() {}

	@Override
	public String getAppSetting(String setting) {
		return "";
	}

	@Override
	public void setAppSetting(String setting, String value) {}

	@Override
	public void closeApp() {
		if (syncServ != null)
		{
			syncServ.sendClosingAppToServer();
		}
		if (port != null)
		{
			port.dispose();
		}
		System.exit(0);
	}
	
	public void connectToPort(String portName)
	{
		disconnectSerial();
		try
		{
			applet.acceptSerial = false;
			port = new Serial(applet, portName, 9600);
			try {
				// We need this delay so that old buffered messages that would get wrong
				// timestamps will be ignored.
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			port.clear();
			applet.acceptSerial = true;
			
			syncServ.sensorStateChanged();
			selectedPortName = portName;
			log("Connected to serial port: " + portName);
		}
		catch (Exception ex)
		{
			log(ex.getMessage());
		}
		
		if (gui != null)
		{
			gui.setSerialInterfaceLabel(selectedPortName);
		}
	}
	
	public void refreshPorts()
	{
		if (gui != null)
		{
			ArrayList<String> postList = new ArrayList<String>(); 
			postList.addAll(Arrays.asList(Serial.list()));
		
			boolean isChanged = false;
			
			if (oldPortList == null)
			{
				isChanged = true;
			}
			else
			{
				if (postList.size() != oldPortList.size())
				{
					isChanged = true;
				}
					
				if (selectedPortName != null 
						&& !selectedPortName.equals(EMPTY_PORT_STRING) 
						&& !postList.contains(selectedPortName))
				{
					log("Suddenly lost connection to serial interface: " + selectedPortName);
					disconnectSerial();
				}
				
				for (String s : postList)
				{
					if (!oldPortList.contains(s))
					{
						isChanged = true;
						log("Found new serial interface: " + s);
					}
				}
				}
			
			if (isChanged)
			{
				gui.addPorts(Serial.list());
			}
			
			oldPortList = postList;
		}
	}
	
	public void log(String text)
	{
		if (gui != null)
		{
			gui.addToLog(text + newline);
		}
		else
		{
			messages = messages + text + newline;
		}
		
		System.out.println(text);
	}
	
	public void setGui(SerialGui gui)
	{
		this.gui = gui;
		gui.addToLog(messages);
	}
	
	public void logReceivedSerial(int number)
	{
		long time = System.currentTimeMillis();
		log("Received serial data: " + number + " at " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(time)));
	}

	public void disconnectSerial() {
		if (port != null)
		{
			port.dispose();
			if (!selectedPortName.equals(EMPTY_PORT_STRING))
			{
				log("Disconnected from serial port: " + selectedPortName);
			}
			selectedPortName = EMPTY_PORT_STRING;
		}
		if (gui != null)
		{
			gui.setSerialInterfaceLabel(selectedPortName);
		}
	}
	
    private class CheckPorts extends TimerTask {

        public CheckPorts() 
        {
        }

        @Override
        public void run()
        {
        	refreshPorts();
        	syncServ.sensorStateChanged();
        }
    }

}
