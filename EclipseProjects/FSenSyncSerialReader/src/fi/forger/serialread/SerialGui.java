/*
FSenSync Serial Reader
Copyright (C) 2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.serialread;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.border.EmptyBorder;

import fi.forger.desktopsynclibrary.SyncService;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Component;
import javax.swing.Box;

public class SerialGui extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private SerialReader parent;
	private JButton btnRefreshSerialPorts;
	private JComboBox<String> comboBox;
	private JButton btnConnectToSelected;
	private JPanel panel;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	
	private static float uiScaleFactor = 1.0f;
	private JPanel panel_1;
	private JLabel label;
	private JLabel labelAppState;
	private JButton button;
	private Component rigidArea;
	private JLabel lblSerialInterface;
	private JLabel labelSerialInterface;
	
	/**
	 * Launch the application.
	 */
	public static void startGui(final SerialReader parent) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SerialGui frame = new SerialGui(parent);
					frame.setVisible(true);
					parent.setGui(frame);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SerialGui(SerialReader parent) {
		setTitle("FSenSync Serial Reader");
		this.parent = parent;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 326);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		panel = new JPanel();
		panel.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		contentPane.add(panel);
		
		btnRefreshSerialPorts = new JButton("Refresh serial ports");
		btnRefreshSerialPorts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SerialGui.this.parent.refreshPorts();
			}
		});
		panel.add(btnRefreshSerialPorts);
		
		comboBox = new JComboBox<String>();
		panel.add(comboBox);
		
		btnConnectToSelected = new JButton("Connect");
		btnConnectToSelected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (comboBox != null)
				{
					String selected = (String)comboBox.getSelectedItem();
					SerialGui.this.parent.connectToPort(selected);
				}
			}
		});
		panel.add(btnConnectToSelected);
		
		button = new JButton("Disconnect");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SerialGui.this.parent.disconnectSerial();
			}
		});
		panel.add(button);
		
		panel_1 = new JPanel();
		panel_1.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)(200 * uiScaleFactor)));
		contentPane.add(panel_1);
		
		label = new JLabel("State: ");
		panel_1.add(label);
		
		labelAppState = new JLabel("-");
		panel_1.add(labelAppState);
		
		rigidArea = Box.createRigidArea(new Dimension(20, 20));
		panel_1.add(rigidArea);
		
		lblSerialInterface = new JLabel("Serial interface:");
		panel_1.add(lblSerialInterface);
		
		labelSerialInterface = new JLabel("-");
		panel_1.add(labelSerialInterface);
		
		scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		scrollPane.setViewportView(textArea);
		
		this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
            	SerialGui.this.parent.closeApp();
            }
        });
	}
	
	public void addToLog(final String t)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				textArea.append(t);
				JScrollBar v = scrollPane.getVerticalScrollBar();
				v.setValue(v.getMaximum());
			}
		});
	}
	
	public void addPorts(String[] portNames)
	{
		comboBox.removeAllItems();
		for (String item : portNames)
		{
			comboBox.addItem(item);
		}
	}
	
	public void setAppStateLabel(final String text)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				labelAppState.setText(text);
			}
		});
	}
	
	public void setSerialInterfaceLabel(final String text)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				labelSerialInterface.setText(text);
			}
		});
	}

}
