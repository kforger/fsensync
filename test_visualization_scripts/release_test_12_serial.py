#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 12:57:03 2017

@author: klaus
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob
import re

#%%

data_file = '/home/klaus/FSenSyncExp/release_test_v100/synced/release_test_v100_033_042_SERIAL.csv'

m = re.search('/synced/', data_file)
note_file = data_file[0:m.start()]
m = re.search('/\w*$', note_file)
note_file = note_file + "/" + note_file[m.start():m.end()] + "_notes.txt"

plt.figure(1)
plt.clf()

names = []

time_divider = 1000

all_data = []
for ind, file in enumerate(glob.glob(data_file)):
    record = {}
    record['data'] = pd.read_csv(file)
    m = re.search('\d\d\d_[A-Z]{1,6}\.csv$', file)
    record['short_id'] = int(file[m.start():m.start()+3])
    record['file'] = file
    record['tag'] = ''
    if (len(record['data']) > 0):
      all_data.append(record)
      m = re.search('\d\d\d_\d\d\d_[A-Z]{1,6}\.csv$', file)
      names.append(file[m.start():m.end()])

    meta_file = (record['file'][0:-3] + "meta").replace("synced", "downloaded", 1)
    file  = open(meta_file, "r")
    file.readline() # TODO: Read the tags
    file.close()
  
min_stamp = []
for ind, record in enumerate(all_data):
  min_stamp.append(record['data'].iloc[0, 0])
min_stamp = np.min(min_stamp)

for ind, record in enumerate(all_data):
    plt.plot(([record['data'].iloc[0, 0], record['data'].iloc[-1, 0]] - min_stamp) / time_divider, np.array([ind, ind]) + 0.1)
    plt.plot((record['data'].iloc[:, 0] - min_stamp) / time_divider, np.ones((len(record['data'].iloc[:, 0]))) * ind, 'x')
    
plt.ylim(plt.ylim()[0] -1, plt.ylim()[1] +1)
plt.yticks(range(len(all_data)), names)
plt.xlabel('Time in seconds')

#%%

plt.figure(2)
plt.clf()

for ind, record in enumerate(all_data):
  plt.plot((record['data'].iloc[:, 0] - min_stamp) / time_divider, record['data'].iloc[:, 1], '-o')
plt.legend()


# Plot notes
notes = pd.read_csv(note_file, header=None)
ylimits = plt.ylim()
xlimits = plt.xlim()
for index, row in notes.iterrows():
    row_time = (row[0] - min_stamp)/ time_divider
    if (row_time > xlimits[0] and row_time < xlimits[1]):
        plt.plot((row_time, row_time), ylimits, 'k--')
        plt.text(row_time, ylimits[0], row[2], rotation='vertical', va='bottom', ha='right')
        
#%% Times between triggers

for ind, record in enumerate(all_data):
  for i in range(1, len(record['data'])):
    print(str(record['data'].iloc[i, 0]) + ' ' + str(record['data'].iloc[i, 0] - record['data'].iloc[i-1, 0]) )
    

