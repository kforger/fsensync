#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 12:57:03 2017

@author: klaus
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob
import re
from matplotlib.pyplot import cm 

plt.close('all')

#%%

data_file = '/home/klaus/FSenSyncExp/release_test_v100/synced/release_test_v100_004_*_TOUCH.csv'

m = re.search('/synced/', data_file)
note_file = data_file[0:m.start()]
m = re.search('/\w*$', note_file)
note_file = note_file + "/" + note_file[m.start():m.end()] + "_notes.txt"

plt.figure(1)
plt.clf()

names = []

all_data = []
for ind, file in enumerate(glob.glob(data_file)):
  record = {}
  record['data'] = pd.read_csv(file)
  m = re.search('\d\d\d_[A-Z]{1,6}\.csv$', file)
  record['short_id'] = int(file[m.start():m.start()+3])
  record['file'] = file
  record['tag'] = ''
  if (len(record['data']) > 0):
    all_data.append(record)
    m = re.search('\d\d\d_\d\d\d_[A-Z]{1,6}\.csv$', file)
    names.append(file[m.start():m.end()])

  meta_file = record['file'][0:-9] + "STI.meta"
  lines = []
  with open(meta_file) as f:
    lines = f.readlines()
    
  for ind_line, line in enumerate(lines):
    l = line.rstrip()
    if (l.startswith('Device:')):
      record['model'] = l[8:]
    if (l.startswith('View width:')):
      record['view_width'] = float(l[12:])
    if (l.startswith('View height:')):
      record['view_height'] = float(l[13:])
  
min_stamp = []
for ind, record in enumerate(all_data):
  min_stamp.append(record['data'].iloc[0, 0])
min_stamp = np.min(min_stamp)

for ind, record in enumerate(all_data):
    plt.plot(([record['data'].iloc[0, 0], record['data'].iloc[-1, 0]] - min_stamp) / 60000, np.array([ind, ind]) + 0.1)
    plt.plot((record['data'].iloc[:, 0] - min_stamp) / 60000, np.ones((len(record['data'].iloc[:, 0]))) * ind, 'x')
    
plt.ylim(plt.ylim()[0] -1, plt.ylim()[1] +1)
plt.yticks(range(len(all_data)), names)
plt.xlabel('Time in minutes')

plt.title('Timings of all touches')

#%% Plotting X coordinates over time

plt.figure(2)
plt.clf()

for ind, record in enumerate(all_data):
  plt.plot((record['data'].iloc[:, 0] - min_stamp) / 1000, record['data'].iloc[:, 2], 'o',label=record['model'])
plt.legend()


# Plot notes
notes = pd.read_csv(note_file, header=None)
ylimits = plt.ylim()
xlimits = plt.xlim()
for index, row in notes.iterrows():
    row_time = (row[0] - min_stamp)/ 1000
    if (row_time > xlimits[0] and row_time < xlimits[1]):
        plt.plot((row_time, row_time), ylimits, 'k--')
        plt.text(row_time, ylimits[0], row[2], rotation='vertical', va='bottom', ha='right')
plt.xlabel('Time in seconds')

plt.title('X coordinates over time')

#%% Plotting all touches on X and Y coordinates

plt.figure(3)
plt.clf()

color=iter(cm.rainbow(np.linspace(0,1,len(all_data))))
for ind, record in enumerate(all_data):
  c=next(color)
  inds_down =  (record['data'].iloc[:, 4] == -1).ravel().nonzero()[0]
  inds_up =  (record['data'].iloc[:, 4] == 1).ravel().nonzero()[0]
  for i in range(len(inds_down)):
    s_range = range(inds_down[i], inds_up[i])
    if (i == 0):
      plt.plot(record['data'].iloc[s_range, 2], record['data'].iloc[s_range, 3], '-', c=c,label=record['model'])
    else:
      plt.plot(record['data'].iloc[s_range, 2], record['data'].iloc[s_range, 3], '-', c=c, label='')
plt.gca().invert_yaxis()
plt.legend()

plt.title('All touches on original X and Y coordinates')

#%% Plotting all touches on X and Y coordinates

plt.figure(4)
plt.clf()

color=iter(cm.rainbow(np.linspace(0,1,len(all_data))))
for ind, record in enumerate(all_data):
  c=next(color)
  inds_down =  (record['data'].iloc[:, 4] == -1).ravel().nonzero()[0]
  inds_up =  (record['data'].iloc[:, 4] == 1).ravel().nonzero()[0]
  for i in range(len(inds_down)):
    s_range = range(inds_down[i], inds_up[i])
    if (i == 0):
      plt.plot(record['data'].iloc[s_range, 2] / record['view_width'], record['data'].iloc[s_range, 3] / record['view_height'], '-', c=c,label=record['model'])
    else:
      plt.plot(record['data'].iloc[s_range, 2] / record['view_width'], record['data'].iloc[s_range, 3]  / record['view_height'], '-', c=c, label='')
plt.gca().invert_yaxis()
plt.legend()

plt.title('All touches on normalized X and Y coordinates')
  
#%% Plotting network sync information from downloaded folder

# Note: This showns also the constant offset added to some devices by the server

for ind, record in enumerate(all_data):
  downloaded = record['file'].replace('synced', 'downloaded')
  data_d = pd.read_csv(downloaded)
  start_time = data_d.iloc[0, 0]
  time_diff = record['data'].iloc[:, 0].values - (data_d.iloc[:, 0]/1000.0)
  plt.figure()
  plt.clf()
  l = [] 
  l1, = plt.plot((data_d.iloc[:,0] - start_time) / 1000, data_d.iloc[:,1] / 1000, 'b-', label="upper bound")
  l2, = plt.plot((data_d.iloc[:,0] - start_time) / 1000, data_d.iloc[:,2] / 1000, 'r-', label="lower bound")
  l3, = plt.plot((data_d.iloc[:,0] - start_time) / 1000, time_diff, 'k-', label="synced time")
  plt.xlabel("time (milliseconds)")
  plt.ylabel("clock drift (milliseconds)")
  plt.legend(handles=[l1, l3, l2])
  plt.title(record['file'][-15:])
