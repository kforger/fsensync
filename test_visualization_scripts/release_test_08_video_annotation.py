#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 12:57:03 2017

@author: klaus
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob
import re
from scipy import interpolate
import time
from matplotlib import animation
from PIL import Image
import os

plt.close('all')

#%%

data_file = '/home/klaus/FSenSyncExp/release_test_v100/synced/release_test_v100_013_032_STI.csv'

m = re.search('/synced/', data_file)
note_file = data_file[0:m.start()]
m = re.search('/\w*$', note_file)
note_file = note_file + "/" + note_file[m.start():m.end()] + "_notes.txt"

names = []

time_divider = 1000

all_data = []
for ind, file in enumerate(glob.glob(data_file)):
    record = {}
    record['play_data'] = pd.read_csv(file)
    m = re.search('\d\d\d_[A-Z]{1,6}\.csv$', file)
    record['short_id'] = int(file[m.start():m.start()+3])
    record['file'] = file
    record['tag'] = ''
    if (len(record['play_data']) > 0):
      all_data.append(record)
      m = re.search('\d\d\d_\d\d\d_[A-Z]{1,6}\.csv$', file)
      names.append(file[m.start():m.end()])

    meta_file = record['file'].replace("STI.csv", "VANNO.meta", 1).replace("synced", "downloaded", 1)
    file  = open(meta_file, "r")
    lines = file.readlines()
    file.close()
    for i_line, line in enumerate(lines):
      if (line.startswith('Device: ')):
        record['model'] = line.replace('Device: ', '', 1).strip()
      if (line.startswith('Video file: ')):
        record['video_file'] = line.replace('Video file: ', '', 1).strip()
      if (line.startswith('Image file: ')):
        record['image_file'] = line.replace('Image file: ', '', 1).strip()
      if (line.startswith('Touch width: ')):
        record['touch_width'] = int(line.replace('Touch width: ', ''))
      if (line.startswith('Touch height: ')):
        record['touch_height'] = int(line.replace('Touch height: ', ''))

    
    touch_file = record['file'].replace("STI", "TOUCH", 1)
    record['touch_data'] = pd.read_csv(touch_file)
  
min_stamp = []
for ind, record in enumerate(all_data):
  min_stamp.append(record['play_data'].iloc[0, 0])
min_stamp = np.min(min_stamp)

#%% Plotting raw play/touch data

plt.figure(1)
plt.clf()
f, axarr = plt.subplots(2, sharex=True, num=1)

plt.sca(axarr[0])

for ind, record in enumerate(all_data):
  plt.plot((record['play_data'].iloc[:, 0] - min_stamp) / time_divider, record['play_data'].iloc[:, 2], '-o', label=record['model'])
plt.legend()


# Plot notes
notes = pd.read_csv(note_file, header=None)
ylimits = plt.ylim()
xlimits = plt.xlim()
for index, row in notes.iterrows():
    row_time = (row[0] - min_stamp)/ time_divider
    if (row_time > xlimits[0] and row_time < xlimits[1]):
        plt.plot((row_time, row_time), ylimits, 'k--')
        plt.text(row_time, ylimits[0], row[2], rotation='vertical', va='bottom', ha='right')
        
plt.sca(axarr[1])

for ind, record in enumerate(all_data):
  plt.plot((record['touch_data'].iloc[:, 0] - min_stamp) / 1000, record['touch_data'].iloc[:, 2], 'o',label='X touch')
  plt.plot((record['touch_data'].iloc[:, 0] - min_stamp) / 1000, record['touch_data'].iloc[:, 3], 'o',label='Y touch')
plt.legend()

axarr[0].set_title('Video playback in annotation time')
axarr[1].set_title('Touches in annotation time')

  
#%% Calculating touches over original video

for ind, record in enumerate(all_data):
  f = interpolate.interp1d(record['play_data'].iloc[:, 0], record['play_data'].iloc[:, 2], kind='linear')
  touch_progress = f(record['touch_data'].iloc[:, 0])
  record['touch_data']
  record['touch_data'].loc[:,'Progress'] = pd.Series(touch_progress, index=record['touch_data'].index)

plt.figure(2)
plt.clf()

for ind, record in enumerate(all_data):
  plt.plot(record['touch_data'].iloc[:, 5] / 1000, record['touch_data'].iloc[:, 2], 'o',label='X touch')
  plt.plot(record['touch_data'].iloc[:, 5] / 1000, record['touch_data'].iloc[:, 3], 'o',label='Y touch')
plt.legend()

plt.ylabel('Coordinates')
plt.xlabel('Original video time in seconds')
plt.title('Touches projected to original video time')

#%% Rendering a video of the touches in original video timeline

def new_figure():
  plt.close(100)
  plt.figure(num=100, figsize=(6,4.5), dpi=100)
  plt.clf()
  plt.subplots_adjust(top=0.95,bottom=0.05,
                      left=0.05,right=0.95,
                      hspace=0.2,wspace=0.2)
  
def animate_touches(start_time_seconds, end_time_seconds, frame_length, fps, filename, touch_data):
    f = plt.gcf()
    ax = plt.gca()
    line, = ax.plot([], [], 'ko')
    frames = int(round((end_time_seconds - start_time_seconds) / frame_length))
    global time_last
    time_last = time.time()
    def init():
        line.set_data([], [])
        return line,
    def animate(i):
        global time_last
        if (time_last < time.time() - 1.0 or i==frames-1):
            time_last = time.time()
            print("\r" + str(i+1) + "/" + str(frames), end='')
        offset = i*frame_length
        
        inds_touch = touch_data['Progress'] / 1000 > start_time_seconds + offset - 0.5
        inds_touch = np.logical_and(inds_touch, touch_data['Progress'] / 1000 < start_time_seconds + offset + 0.5)
        if (np.sum(inds_touch) > 0):
          line.set_data(touch_data['Xcoordinate'][inds_touch], touch_data['Ycoordinate'][inds_touch])
        else:
          line.set_data([], [])
        
        return line,
    print(str(0) + "/" + str(frames), end='')
    anim = animation.FuncAnimation(f, animate, init_func=init,
                                   frames=frames, interval=1, blit=True,
                                   repeat=False)
    anim.save(filename, fps=fps, extra_args=['-vcodec', 'libx264', '-r', '30'])
    print("")

for ind, record in enumerate(all_data):

  video_max_time = np.max(record['touch_data'].iloc[:, 5] / 1000)
  
  new_figure()
  plt.gca().xaxis.tick_top()
  
  img = Image.open(re.sub(r"/synced/.*", '', record['file']) + '/uploadBackups/' + record['image_file'])
  img = img.resize((record['touch_width'],record['touch_height']), Image.ANTIALIAS)
  imgplot = plt.imshow(img)
  
  new_name = record['video_file'].replace('.mp4', '_') + record['file'][-15:-4] + '.mp4'
  animate_touches(0, video_max_time + 1, 1/15, 15, new_name, record['touch_data'])
  
  # Composite with original video and the annotation
  orig_video_file = re.sub(r"/synced/.*", '', record['file']) + '/uploadBackups/' + record['video_file']
  new_name_2 = new_name.replace('.mp4', '_final.mp4')
  
  command = 'ffmpeg -y'
  command = command + ' -i ' + orig_video_file
  command = command + ' -i ' + new_name
  command = command + ' -filter_complex '
  
  command = command + '"'
  command = command + '[0:v:0]pad=iw:ih+' + str(450) + '[a0]; '
  command = command + '[a0][1:v:0]overlay=0:' + str(450)
  command = command + '"'
  
  command = command + ' -preset superfast'
  #command = command + ' -an'
  #command = command + ' -to ' + to_string
  command = command + ' ' + new_name_2
  
  print(command, flush=True)
  
  os.system(command)
