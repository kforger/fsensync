#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 12:57:03 2017

@author: klaus
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob
import re

plt.close('all')

#%%

data_file = '/home/klaus/FSenSyncExp/release_test_v100/synced/release_test_v100_012_*_EDA.csv'

m = re.search('/synced/', data_file)
note_file = data_file[0:m.start()]
m = re.search('/\w*$', note_file)
note_file = note_file + "/" + note_file[m.start():m.end()] + "_notes.txt"

plt.figure(1)
plt.clf()

names = []

all_data = []
for ind, file in enumerate(glob.glob(data_file)):
  record = {}
  record['data'] = pd.read_csv(file)
  m = re.search('\d\d\d_\d\d\d_([^\W^_]{12})?_?[A-Z]{1,6}\.csv$', file)
  record['short_id'] = int(file[m.start()+4:m.start()+7])
  record['recording_num'] = int(file[m.start():m.start()+3])
  record['mac'] = file[m.start()+8:m.start()+20]
  record['file'] = file
  
  if (len(record['data']) > 0):
    all_data.append(record)
    m = re.search('\d\d\d_\d\d\d_([^\W^_]{12})?_?[A-Z]{1,6}\.csv$', file)
    names.append(file[m.start():m.end()])

  meta_file = (record['file'][0:-20] + "EDA.meta")
  lines = []
  with open(meta_file) as f:
    lines = f.readlines()
    
  for ind_line, line in enumerate(lines):
    l = line.rstrip()
    if (l.startswith('Device:')):
      record['model'] = l[8:]
    if (l.startswith('Tag for unit: "' + record['mac'] + '" = ')):
      record['ring_tag'] = l[32:-1]
  
min_stamp = []
for ind, record in enumerate(all_data):
  min_stamp.append(record['data'].iloc[0, 0])
min_stamp = np.min(min_stamp)

for ind, record in enumerate(all_data):
  plt.plot(([record['data'].iloc[0, 0], record['data'].iloc[-1, 0]] - min_stamp) / 60000, np.array([ind, ind]) + 0.1)
  plt.plot((record['data'].iloc[:, 0] - min_stamp) / 60000, np.ones((len(record['data'].iloc[:, 0]))) * ind, 'x')
    
plt.ylim(plt.ylim()[0] -1, plt.ylim()[1] +1)
plt.yticks(range(len(all_data)), names)
plt.xlabel('Time in minutes')

#%%

f, axarr = plt.subplots(6) 

for ind, record in enumerate(all_data):
  this_label = record['mac']  + ' ' + record['ring_tag']
  axarr[0].plot((record['data'].iloc[:, 0] - min_stamp) / 1000, record['data'].iloc[:, 4], label=this_label)
  axarr[1].plot((record['data'].iloc[:, 0] - min_stamp) / 1000, record['data'].iloc[:, 5], label=this_label)
  axarr[2].plot((record['data'].iloc[:, 0] - min_stamp) / 1000, record['data'].iloc[:, 6], label=this_label)
  axarr[3].plot((record['data'].iloc[:, 0] - min_stamp) / 1000, record['data'].iloc[:, 3], label=this_label)
  axarr[4].plot((record['data'].iloc[:, 0] - min_stamp) / 1000, record['data'].iloc[:, 2], label=this_label)
  axarr[5].plot((record['data'].iloc[1:, 0] - min_stamp) / 1000, 
       (record['data'].iloc[1:, 0].values - record['data'].iloc[:-1, 0].values) / 1000, 
       label=record['mac'] + record['ring_tag'])
plt.legend()

axarr[0].set_ylabel('accel\nX')
axarr[1].set_ylabel('accel\nY')
axarr[2].set_ylabel('accel\nZ')
axarr[3].set_ylabel('resistance')
axarr[4].set_ylabel('MoodNumber')
axarr[5].set_ylabel('time\nbetween\nsamples')

# Plot notes
notes = pd.read_csv(note_file, header=None)
ylimits = plt.ylim()
xlimits = plt.xlim()
for index, row in notes.iterrows():
    row_time = (row[0] - min_stamp)/ 1000
    if (row_time > xlimits[0] and row_time < xlimits[1]):
        plt.plot((row_time, row_time), ylimits, 'k--')
        plt.text(row_time, ylimits[0], row[2], rotation='vertical', va='bottom', ha='right')
plt.xlabel("time (seconds)")
  
#%%

for ind, record in enumerate(all_data):
  downloaded = record['file'].replace('synced', 'downloaded')
  data_d = pd.read_csv(downloaded)
  start_time = data_d.iloc[0, 0]
  time_diff = record['data'].iloc[:, 0].values - (data_d.iloc[:, 0]/1000.0)
  plt.figure()
  plt.clf()
  l = [] 
  l1, = plt.plot((data_d.iloc[:,0] - start_time) / 1000, data_d.iloc[:,1] / 1000, 'b-', label="upper bound")
  l2, = plt.plot((data_d.iloc[:,0] - start_time) / 1000, data_d.iloc[:,2] / 1000, 'r-', label="lower bound")
  l3, = plt.plot((data_d.iloc[:,0] - start_time) / 1000, time_diff, 'k-', label="synced time")
  plt.xlabel("time (milliseconds)")
  plt.ylabel("clock drift (milliseconds)")
  plt.legend(handles=[l1, l3, l2])
  plt.title(record['file'][-15:])
