#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 19 10:57:44 2017

@author: klaus
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob
import re
from PIL import Image

#%%

plt.close('all')

#%%

data_file = '/home/klaus/FSenSyncExp/release_test_v100/synced/release_test_v100_011_030_CART.csv'
game_file = '/home/klaus/FSenSyncExp/release_test_v100/uploadBackups/cart_example.csv'
picture_folder = '/home/klaus/FSenSyncExp/release_test_v100/uploadBackups/'

#%% Timing plot

plt.figure(1)
plt.clf()

names = []

time_divider = 1000

all_data = []
for ind, file in enumerate(glob.glob(data_file)):
  record = {}
  record['data'] = pd.read_csv(file)
  m = re.search('\d\d\d_[A-Z]{1,6}\.csv$', file)
  record['short_id'] = int(file[m.start():m.start()+3])
  record['file'] = file
  record['tag'] = ''
  if (len(record['data']) > 0):
    all_data.append(record)
    m = re.search('\d\d\d_\d\d\d_[A-Z]{1,6}\.csv$', file)
    names.append(file[m.start():m.end()])
  
  meta_file = (record['file'][0:-3] + "meta").replace("synced", "downloaded", 1)
  file  = open(meta_file, "r")
  file.readline() 
  file.readline() 
  file.readline() 
  record['model'] = file.readline()[8:-1]
  for line in file:
    if (line.startswith('View width: ')):
      record['view_width'] = int(line[12:])
    if (line.startswith('View height: ')):
      record['view_height'] = int(line[13:])
  file.close()
  
  touch_file = record['file'].replace("CART", "TOUCH", 1)
  record['touch'] = pd.read_csv(touch_file)

min_stamp = []
for ind, record in enumerate(all_data):
  min_stamp.append(record['data'].iloc[0, 0])
min_stamp = np.min(min_stamp)

cart_markers = ('ro', 'r.', 'rs')
touch_markers = ('bo', 'b.', 'bs')
for ind, record in enumerate(all_data):
  plt.plot(([record['touch'].iloc[0, 0],
             record['touch'].iloc[-1, 0]] - min_stamp) / time_divider, np.array([ind, ind]) + 0.3)
  for ind_b in range(len(record['touch'].iloc[:, 0])):
    if (record['touch'].iloc[ind_b, 4] != 0):
      plt.plot((record['touch'].iloc[ind_b, 0] - min_stamp) / time_divider,
               ind + 0.2, touch_markers[record['touch'].iloc[ind_b, 4]+1])
  plt.plot(([record['data'].iloc[0, 0],
             record['data'].iloc[-1, 0]] - min_stamp) / time_divider, np.array([ind, ind]) + 0.1)
  for ind_b in range(len(record['data'].iloc[:, 0])):
    plt.plot((record['data'].iloc[ind_b, 0] - min_stamp) / time_divider, 
             ind, cart_markers[record['data'].iloc[ind_b, 6]])
    
plt.ylim(plt.ylim()[0] -1, plt.ylim()[1] +1)
plt.yticks(range(len(all_data)), names)
plt.xlabel('Time in seconds')

#%% Reconstruction of the trials

file  = open(game_file, "r")
file.readline()

trial_num = -1;
for line in file:
  trial_num += 1
  items = line.split(',')
  if (len(items) > 7):
    
    back_img = picture_folder + items[0]
    other_img = []
    for ind in range(7,len(items)):
      other_img.append(picture_folder + items[ind].rstrip())
    
    
    f, axarr = plt.subplots(2)
    plt.subplots_adjust(hspace=0.3)
    
    # start positions
    plt.axes(axarr[0])
    
    img = Image.open(back_img)
    img = img.resize((record['view_width'],record['view_height']), Image.ANTIALIAS)
    imgplot = plt.imshow(img, extent=(0,record['view_width'],record['view_height'],0))
    

    inds_a = record['data'].iloc[:, 8] == trial_num
    inds_b = record['data'].iloc[:, 7] == 0
    inds = np.logical_and(inds_a, inds_b)
    start_data = record['data'].values[inds, :]
    for ind in range(len(start_data)):
      position = (start_data[ind, 2]-(start_data[ind, 4]/2),
                  start_data[ind, 2]+(start_data[ind, 4]/2),
                  start_data[ind, 3]+(start_data[ind, 5]/2),
                  start_data[ind, 3]-(start_data[ind, 5]/2))
      img = Image.open(other_img[start_data[ind, 1]])
      try:
        exif_data = img._getexif()
        if (274 in exif_data):
          if (exif_data[274] == 6):
            img = img.rotate(270, expand=True)
          if (exif_data[274] == 3):
            img = img.rotate(180, expand=True)
          if (exif_data[274] == 8):
            img = img.rotate(90, expand=True)
      except AttributeError:
        # No exif data
        pass
      
      plt.imshow(img, extent=position)
    
    plt.ylim((0, record['view_height']))
    plt.xlim((0, record['view_width']))
    plt.title('Trial start: ' + str(trial_num))
    plt.gca().invert_yaxis()
    
    # middle positions with lines
    
    for ind in range(len(other_img)):
      inds_a = record['data'].iloc[:, 8] == trial_num
      inds_b = record['data'].iloc[:, 1] == ind
      inds = np.logical_and(inds_a, inds_b)
      middle_data = record['data'].values[inds, :]
      plt.plot(middle_data[:, 2], middle_data[:, 3], '-o')
      
    # touches
    ind_this_trial = record['data'].iloc[:, 8] == trial_num
    inds_a = record['touch'].iloc[:, 0] >= np.min(record['data'].values[ind_this_trial, 0])
    inds_b = record['touch'].iloc[:, 0] <= np.max(record['data'].values[ind_this_trial, 0])
    inds_touched_this = np.logical_and(inds_a, inds_b)
    touches = record['touch'].values[inds_touched_this, :]
    t_inds = np.unique(touches[:, 1])
    for t_ind in t_inds:
      touches_fing = touches[touches[:, 1] == t_ind]
      traject_x = []
      traject_y = []
      for i_sample in range(len(touches_fing[:, 1])):
        if (touches_fing[i_sample, 4] <= 0):
          traject_x.append(touches_fing[i_sample, 2])
          traject_y.append(touches_fing[i_sample, 3])
        if (touches_fing[i_sample, 4] == 1):
          traject_x.append(touches_fing[i_sample, 2])
          traject_y.append(touches_fing[i_sample, 3])
          plt.plot(traject_x, traject_y, '-')
          traject_x = []
          traject_y = []
      if (len(traject_x) > 0):
        plt.plot(traject_x, traject_y, '-')
    
    # end positions
    plt.axes(axarr[1])
    
    img = Image.open(back_img)
    img = img.resize((record['view_width'],record['view_height']), Image.ANTIALIAS)
    imgplot = plt.imshow(img, extent=(0,record['view_width'],record['view_height'],0))
    
    inds_a = record['data'].iloc[:, 8] == trial_num
    inds_b = record['data'].iloc[:, 7] == 2
    inds = np.logical_and(inds_a, inds_b)
    start_data = record['data'].values[inds, :]
    for ind in range(len(start_data)):
      position = (start_data[ind, 2]-(start_data[ind, 4]/2),
                  start_data[ind, 2]+(start_data[ind, 4]/2),
                  start_data[ind, 3]+(start_data[ind, 5]/2),
                  start_data[ind, 3]-(start_data[ind, 5]/2))
      img = Image.open(other_img[start_data[ind, 1]])
      try:
        exif_data = img._getexif()
        if (274 in exif_data):
          if (exif_data[274] == 6):
            img = img.rotate(270, expand=True)
          if (exif_data[274] == 3):
            img = img.rotate(180, expand=True)
          if (exif_data[274] == 8):
            img = img.rotate(90, expand=True)
      except AttributeError:
        # No exif data
        pass
      
      plt.imshow(img, extent=position)
    
    plt.ylim((0, record['view_height']))
    plt.xlim((0, record['view_width']))
    plt.title('Trial end: ' + str(trial_num))
    plt.gca().invert_yaxis()
    

file.close()
