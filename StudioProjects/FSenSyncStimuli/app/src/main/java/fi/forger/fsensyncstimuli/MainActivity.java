/*
FSenSync Stimuli
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensyncstimuli;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.media.SyncParams;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import fi.forger.remotecontrollibrary.ControlledActivity;
import fi.forger.synclibrary.*;

public class MainActivity extends ControlledActivity implements SyncedClient, MediaPlayer.OnCompletionListener {

    private static final String clientType = "STI";
    private static final int majorVersion = BuildConfig.MAJOR_VERSION;

    private static PrintWriter outputStreamTouch;
    private static PrintWriter outputStreamStimulus;


    private static int STORAGE_PERMISSION_REQUEST = 1;

    private static SyncService syncServ;
    private static TouchService touchService;

    private static DrawView drawView;

    private static MediaPlayer mediaPlayer;
    private Timer myUiTimer;
    private static VideoSurfaceView videoSurface;
    private static PictureView pictureView;

    private TextView myTextView;
    private TextView textExperiment;
    private TextView textExtra;
    private TextView textExtra2;
    private TextView textTag1;
    private TextView textTag2;
    private TextView textTag3;

    private long prevLoggedTime = 0l;

    private static String stimulusName1;
    private static String stimulusName2;

    private long globalDesiredStartTime;

    private Boolean soundStimulus;
    private Boolean pictureStimulus;
    private Boolean videoStimulus;

    private static SoundPool soundPool;
    private int soundInt;
    private static MemorySoundSource soundSource;
    private int nativeSoundRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setTryToHideActionBar(false);
        super.onCreate(savedInstanceState);
        startServices();

        // UI stuff
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textBox);
        textExperiment = (TextView) findViewById(R.id.textExperiment);
        textExtra = (TextView) findViewById(R.id.textExtra);
        textExtra2 = (TextView) findViewById(R.id.textExtra2);
        textTag1 = (TextView) findViewById(R.id.textTag1);
        textTag2 = (TextView) findViewById(R.id.textTag2);
        textTag3 = (TextView) findViewById(R.id.textTag3);
        drawView = (DrawView) findViewById(R.id.drawView);
        pictureView = (PictureView) findViewById(R.id.pictureView);
        updateExperimentInfoToUi();
        myUiTimer = new Timer();
        myUiTimer.schedule(new UiUpdateTask(this), 1000,10);
        myUiTimer.schedule(new DrawTask(), 1000,50);
        soundStimulus = false;
        pictureStimulus = false;
        videoStimulus = false;

        AudioManager audioManager = (AudioManager) this.getSystemService(getApplicationContext().AUDIO_SERVICE);
        nativeSoundRate = Integer.valueOf(audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE));
    }

    public void setTagsToUi()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (textTag1 != null && textTag2 != null && textTag3 != null) {
                    textTag1.setText(syncServ.getTag1());
                    textTag2.setText(syncServ.getTag2());
                    textTag3.setText(syncServ.getTag3());
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (readyToExit)
        {
            syncServ.sendClosingAppToServer();
            System.exit(0);
        }
    }

    private class StartPlayTask extends TimerTask {

        MainActivity main;
        public long desiredStart;

        public StartPlayTask(MainActivity main, long desiredStart) {
            this.main = main;
            this.desiredStart = desiredStart;
        }

        @Override
        public void run() {
            long actualTime = SystemClock.elapsedRealtimeNanos();

            if (pictureStimulus || soundStimulus)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pictureView.invalidate();
                        RelativeLayout layout = (RelativeLayout) findViewById(R.id.pictureLayout);
                        layout.bringToFront();
                        RelativeLayout layout2 = (RelativeLayout) findViewById(R.id.otherLayout);
                        layout2.setVisibility(View.INVISIBLE);
                        RelativeLayout layoutDraw = (RelativeLayout) findViewById(R.id.drawLayout);
                        layoutDraw.bringToFront();
                    }
                });
            }
            else
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RelativeLayout layout = (RelativeLayout) findViewById(R.id.videoLayout);
                        layout.bringToFront();
                        RelativeLayout layout2 = (RelativeLayout) findViewById(R.id.otherLayout);
                        layout2.setVisibility(View.INVISIBLE);
                        RelativeLayout layoutDraw = (RelativeLayout) findViewById(R.id.drawLayout);
                        layoutDraw.bringToFront();
                    }
                });
            }



            if (soundStimulus || videoStimulus)
            {
                mediaPlayer.start();
            }

            syncServ.setStatePlayingStimuli();
            long printedTime = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MILLI;
            System.out.println("Desired:" + desiredStart + " Actual: " + printedTime + " Diff:" + (printedTime - desiredStart));

            int currentPlayTime = 0;
            if (soundStimulus || videoStimulus)
            {
                currentPlayTime = mediaPlayer.getCurrentPosition();
            }
            String output = getTimeAndBounds(actualTime) + ",started," + currentPlayTime + ","
                    + MainActivity.stimulusName1 + "," + MainActivity.stimulusName2;
            try {
                outputStreamStimulus.println(output);
                outputStreamStimulus.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to stimulus log file.");
            }
            myUiTimer.schedule(new LogPlayTask(main), 100, 1000);

        }
    }

    private class DrawTask extends TimerTask {

        public DrawTask() {

            }

        @Override
        public void run()
        {
            if (drawView != null)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        drawView.invalidate();
                    }
                });
            }
        }
    }

    private class LogPlayTask extends TimerTask {

        MainActivity main;
        int numAdjust;

        public LogPlayTask(MainActivity main)
        {
            numAdjust = 0;
            this.main = main;
        }

        @Override
        public void run() {
            if (pictureStimulus || (mediaPlayer != null && mediaPlayer.isPlaying())) {

                long actualTime = SystemClock.elapsedRealtimeNanos();
                int currentPlayTime = 0;
                if (soundStimulus || videoStimulus)
                {
                    currentPlayTime = mediaPlayer.getCurrentPosition();
                }

                String output = getTimeAndBounds(actualTime) + ",playing," + currentPlayTime + ","
                        + MainActivity.stimulusName1 + "," + MainActivity.stimulusName2;
                try {
                    if (pictureStimulus || soundStimulus || videoStimulus) {
                        outputStreamStimulus.println(output);
                        outputStreamStimulus.flush();
                    }
                } catch (Exception ex) {
                    System.out.println("Error while writing to stimulus log file.");
                }
            }
            else
            {
                this.cancel();
            }
        }
    }

    public String processInputFile(String name)
    {
        String error = "";
        boolean ok = false;

        File file = new File(syncServ.getStimulusDirectory().getAbsolutePath() + File.separator + name);

        if (!file.exists())
        {
            return "File was not found: " + name;
        }

        if (name.endsWith(".mp4") ||
                name.endsWith(".3gp") ||
                name.endsWith(".webm") ||
                name.endsWith(".mkv"))
        {
            soundStimulus = false;
            pictureStimulus = false;
            videoStimulus = true;
            MainActivity.stimulusName1 = name;
            Uri uri = Uri.fromFile(file);
            initMediaPlayer(uri);
            ok = true;
        }

        if (name.endsWith(".bmp") ||
                name.endsWith(".gif") ||
                name.endsWith(".jpg") ||
                name.endsWith(".png"))
        {
            pictureStimulus = true;
            videoStimulus = false;
            MainActivity.stimulusName2 = name;
            pictureView.loadBitmap(file);
            ok = true;
        }

        if (name.endsWith(".ogg") ||
                name.endsWith(".wav") ||
                name.endsWith(".mp3") ||
                name.endsWith(".flac") ||
                name.endsWith(".aac"))
        {
            soundStimulus = true;
            MainActivity.stimulusName1 = name;
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                soundSource = new MemorySoundSource(file);
                initMediaPlayer(null);
                ok = true;
            }
            else
            {
                return "Android version is too old for sound stimuli. Minimum is version 6.0.";
            }
        }

        if (ok)
        {
            return "";
        }
        else {
            return "File type not recognized: " + name;
        }
    }

    public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {
        if (syncServ != null) {
            // TODO: allow this only when surface is ready

            globalDesiredStartTime = desiredStartTime;
            soundStimulus = false;
            pictureStimulus = false;
            videoStimulus = false;
            MainActivity.stimulusName1 = "-";
            MainActivity.stimulusName2 = "-";

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (!videoStimulus)
                    {
                        pictureView.setToBlack();
                    }

                }
            });

            File file = new File(syncServ.getStimulusDirectory().getAbsolutePath() + File.separator + stimuliName);
            if (file.exists() && stimuliName.length() > 0)
            {
                // Reading file names from a text file
                if (file.getName().endsWith(".txt"))
                {
                    boolean ok = true;
                    String errorMessage = "";


                    try
                    {
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        String line = br.readLine();

                        if (!line.startsWith("Stimulus file list:")) {
                            ok = false;
                            errorMessage = "Unrecognized first line in settings: " + stimuliName;
                        }

                        line = br.readLine();
                        if (line != null && ok)
                        {
                            String[] fileNames = line.split(",");
                            for (int i = 0; i < fileNames.length; i++)
                            {
                                String name = fileNames[i].trim();
                                errorMessage = processInputFile(name);
                                if (errorMessage.length() > 0)
                                {
                                    ok = false;
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ok = false;
                    }

                    if (!ok)
                    {
                        if (errorMessage.length() == 0)
                        {
                            errorMessage = "Error in reading file: " + stimuliName;
                        }
                        showExtraMessageOnScreen(errorMessage);
                        syncServ.sendErrorToServer(errorMessage);
                        syncServ.delayedCancelAndStop();
                        return;
                    }
                }
                else
                {
                    String errorMessage = processInputFile(file.getName().replace(",", ""));
                    if (errorMessage.length() > 0)
                    {
                        showExtraMessageOnScreen(errorMessage);
                        syncServ.sendErrorToServer(errorMessage);
                        syncServ.delayedCancelAndStop();
                        return;
                    }
                }

                long delay = desiredStartTime - ((SystemClock.elapsedRealtimeNanos() + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MILLI);
                if (delay > 0) {
                    System.out.println("Start task set: " + delay);
                    myUiTimer.schedule(new StartPlayTask(this, desiredStartTime), delay);
                } else {
                    System.out.println("Start task not set: " + delay);
                    syncServ.delayedCancelAndStop();
                    syncServ.sendErrorToServer("Loading stimulus file '" + stimuliName + "' was too slow.");
                }
            }
            else
            {
                System.out.println("Did not have stimulus file: '" + stimuliName + "'");
                showExtraMessageOnScreen("Did not have stimulus file: '" + stimuliName + "'");
                syncServ.delayedCancelAndStop();
                syncServ.sendErrorToServer("Did not have stimulus file: '" + stimuliName + "'");
            }
        }
    }

    private void initMediaPlayer(Uri uri) {

        mediaPlayer = new MediaPlayer();
        videoSurface = (VideoSurfaceView) findViewById(R.id.surfaceView);
        mediaPlayer.setSurface(videoSurface.getHolder().getSurface());
        mediaPlayer.setOnCompletionListener(this);
        if (uri != null)
        {
            if (uri.toString().endsWith(".mp4") ||
                    uri.toString().endsWith(".3gp") ||
                    uri.toString().endsWith(".webm") ||
                    uri.toString().endsWith(".mkv")) {
                MediaPlayer.OnVideoSizeChangedListener listenerVideoSize = new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        videoSurface.aspectRatioFitting(mp, width, height, MainActivity.this);
                    }
                };
                mediaPlayer.setOnVideoSizeChangedListener(listenerVideoSize);
            }
        }
        try {
            if (uri != null) {
                mediaPlayer.setDataSource(getApplicationContext(), uri);
            }
            else
            {
                if (android.os.Build.VERSION.SDK_INT >= 23)
                {
                    mediaPlayer.setDataSource(soundSource);
                }
            }
            mediaPlayer.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            if (android.os.Build.VERSION.SDK_INT >= 23) {
                SyncParams p = mediaPlayer.getSyncParams();
                p.setSyncSource(SyncParams.SYNC_SOURCE_SYSTEM_CLOCK);
                p.setTolerance(0.044f);
                mediaPlayer.setSyncParams(p);
                System.out.println("Sync source: " + p.getSyncSource());
                System.out.println("Tolerance: " + p.getTolerance());
            }

            mediaPlayer.prepare(); // might take long! (for buffering, etc)
            mediaPlayer.start();
            mediaPlayer.pause();
            mediaPlayer.seekTo(0);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getTimeAndBounds(long actualTime)
    {
        long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
        long reportedUpperBound = syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
        long reportedLowerBound = syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;
        return reportedTimeStamp + "," + reportedUpperBound + "," + reportedLowerBound;
    }

    public void onCompletion(MediaPlayer mp)
    {
        if (pictureStimulus || soundStimulus || videoStimulus) {
            pictureStimulus = false;
            soundStimulus = false;
            videoStimulus = false;

            System.out.println("Reached end of stimulus");
            long actualTime = SystemClock.elapsedRealtimeNanos();
            int endProgress = mp.getDuration();
            String output = getTimeAndBounds(actualTime) + ",stopped," + endProgress + ","
                    + MainActivity.stimulusName1 + "," + MainActivity.stimulusName2;
            try {
                outputStreamStimulus.println(output);
                outputStreamStimulus.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to stimulus log file.");
            }

            MainActivity.stimulusName1 = "-";
            MainActivity.stimulusName2 = "-";


            syncServ.playingStimuliEnded();

            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.reset();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RelativeLayout layout = (RelativeLayout) findViewById(R.id.otherLayout);
                        layout.setVisibility(View.VISIBLE);
                        layout.bringToFront();
                        RelativeLayout layoutDraw = (RelativeLayout) findViewById(R.id.drawLayout);
                        layoutDraw.bringToFront();
                    }
                });
            }
        }
    }

    public void cancelStimuli() {
        System.out.println("Cancelling stimuli");
        if (pictureStimulus || mediaPlayer != null) {
            int currentPlayTime = 0;
            if (soundStimulus || videoStimulus)
            {
                currentPlayTime = mediaPlayer.getCurrentPosition();
                mediaPlayer.stop();
                mediaPlayer.reset();
            }

            long actualTime = SystemClock.elapsedRealtimeNanos();
            String output = getTimeAndBounds(actualTime) + ",cancelled," + currentPlayTime + ","
                    + MainActivity.stimulusName1 + "," + MainActivity.stimulusName2;
            try {
                if (pictureStimulus || soundStimulus || videoStimulus) {
                    pictureStimulus = false;
                    soundStimulus = false;
                    videoStimulus = false;
                    outputStreamStimulus.println(output);
                    outputStreamStimulus.flush();
                }
            } catch (Exception ex) {
                System.out.println("Error while writing to stimulus log file.");
            }

            MainActivity.stimulusName1 = "-";
            MainActivity.stimulusName2 = "-";

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    RelativeLayout layout = (RelativeLayout) findViewById(R.id.otherLayout);
                    layout.setVisibility(View.VISIBLE);
                    layout.bringToFront();
                    RelativeLayout layoutDraw = (RelativeLayout) findViewById(R.id.drawLayout);
                    layoutDraw.bringToFront();
                }
            });
        }
    }

    private void startServices() {
        if (syncServ == null || syncServ.getAppState() == SyncService.STATE_ERROR) {
            syncServ = new SyncService(this, this, clientType, majorVersion);
            if (touchService == null && syncServ.getAppState() > SyncService.STATE_ERROR) {
                touchService = new TouchService(this);
            }
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_REQUEST);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_REQUEST && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                System.out.println("Required permission was not granted by user.");
            } else {
                System.out.println("Permission granted");
                startServices();
            }
        }
    }

    public void updateExperimentInfoToUi()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (syncServ.getAppState() < SyncService.STATE_FIRST_SYNC) {
                    textExperiment.setText("STI, Device ID: " + syncServ.getDeviceId());
                }
                else
                {
                    String ph = syncServ.getDeviceId();
                    if (ph.equals("-")) {
                        textExperiment.setText("STI, App ID: " + syncServ.getMyShortId());
                    }
                    else
                    {
                        textExperiment.setText("STI, App ID: " + syncServ.getMyShortId() + " (" + syncServ.getDeviceId() + ")");
                    }
                }
            }
        });
    }

    public void showExtraMessageOnScreen(final String message)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textExtra.setText(message);
            }
        });
    }

    public void enableScreenTouchSound()
    {
        disableScreenTouchSound();
        File file = new File(SyncService.getStimulusDirectory().getAbsolutePath() + File.separator + "screen_touch_sound_file");
        if (file.exists())
        {
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
            soundInt = soundPool.load(file.getAbsolutePath(), 1);
        }
    }

    public void disableScreenTouchSound()
    {
        if (soundPool != null)
        {
            soundPool.unload(soundInt);
        }
        soundPool = null;
    }

    private class UiUpdateTask extends TimerTask {
        MainActivity main;

        public UiUpdateTask(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String s = syncServ.getStateString();
                    if (syncServ.getAppState() > SyncService.STATE_FIRST_SYNC) {
                        String time = "\n" + (SystemClock.elapsedRealtimeNanos() + syncServ.getOffsetNanosToServer());
                        s = s + time.substring(0, Math.max(0, time.length() - 6));
                    }
                    myTextView.setText(s);

                    Point p = new Point();
                    getWindowManager().getDefaultDisplay().getSize(p);
                    Integer screenWidth = p.x;
                    Integer screenHeight = p.y;
                    if (android.os.Build.VERSION.SDK_INT >= 23) {
                        textExtra2.setText("View size: " + screenWidth + "x" + screenHeight + ", Sound rate: " + nativeSoundRate + "Hz");
                    }
                    else
                    {
                        textExtra2.setText("View size: " + screenWidth + "x" + screenHeight + ", Sounds not supported");
                    }
                }
            });

        }
    }

    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream) {
        Boolean ok = true;

        try
        {
            metaOutputStream.println("App version: " + BuildConfig.VERSION_CODE);
            metaOutputStream.println("Native sound rate: " + nativeSoundRate);
            Point p = new Point();
            getWindowManager().getDefaultDisplay().getSize(p);
            Integer screenWidth = p.x;
            Integer screenHeight = p.y;
            metaOutputStream.println("View width: " + screenWidth);
            metaOutputStream.println("View height: " + screenHeight);
            metaOutputStream.close();
        }
        catch (Exception ex)
        {
            System.out.println("Error when writing meta output file.");
        }

        try {
            File fileTouch = new File(fileBaseName + "TOUCH.csv");
            File fileStimulus = new File(fileBaseName + "STI.csv");
            if (fileTouch.exists() || fileStimulus.exists()) {
                syncServ.sendErrorToServer("Refused to overwrite old recording with number " + recordingNumber);
                return false;
            }
            fileTouch.createNewFile();
            fileStimulus.createNewFile();
            outputStreamTouch = new PrintWriter(fileTouch, "UTF-8");
            try {
                String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,TouchIndex,Xcoordinate,Ycoordinate,UpDownEvent";
                outputStreamTouch.println(header);
                outputStreamTouch.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to file.");
            }
            outputStreamStimulus = new PrintWriter(fileStimulus, "UTF-8");
            try {
                String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,EventType,PlayProgress(milliseconds),FileName,ImageName";
                outputStreamStimulus.println(header);
                outputStreamStimulus.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to file.");
            }
        } catch (Exception ex) {
            ok = false;
            System.out.println("Error when opening output file.");
        }
        return ok;
    }

    public Boolean stopRecording() {
        outputStreamTouch.close();
        outputStreamStimulus.close();
        return true;
    }

    public void setAppSetting(String setting, String value)
    {
        switch (setting)
        {
            case "DRAW_MODE":
                try
                {
                    int mode = Integer.valueOf(value);
                    if (mode >= 0 && mode <= 2) {
                        drawView.setDrawMode(Integer.valueOf(value));
                    }
                    else
                    {
                        syncServ.sendErrorToServer("Bad draw mode value: " + value);
                    }
                }
                catch (Exception ex)
                {
                    syncServ.sendErrorToServer("Bad draw mode value: " + value);
                }
                break;
            default:
                break;
        }
    }

    public String getAppSetting(String setting)
    {
        switch (setting)
        {
            case "DRAW_MODE":
                return "" + drawView.getDrawMode();
            default:
                return "INVALID_SETTING";
        }
    }

    public Boolean getRecordingServiceReady() {
        return touchService.touchListeningReady;
    }

    public void sensorStateChanged() {
        syncServ.sensorStateChanged();
    }

    public boolean dispatchTouchEvent(MotionEvent event)
    {
        if (touchService != null)
        {
            touchService.processTouch(event);
        }
        return super.dispatchTouchEvent(event);
    }

    private class TouchService {
        public Boolean touchListeningReady;
        private NumberFormat formatter;
        private MainActivity main;
        private long diffNanosToUpTime;
        private long previousStreamedTime[];
        private int pressStartTime[];

        public TouchService(MainActivity main) {
            this.main = main;
            formatter = NumberFormat.getInstance(Locale.ROOT);
            formatter.setGroupingUsed(false);
            formatter.setMinimumIntegerDigits(1);
            formatter.setMinimumFractionDigits(1);
            touchListeningReady = true;

            previousStreamedTime = new long[30];
            pressStartTime = new int[30];
            for (int i = 0; i < 30; i++)
            {
                previousStreamedTime[i] = Long.MIN_VALUE;
                pressStartTime[i] = Integer.MIN_VALUE;
            }

            diffNanosToUpTime = 0l;
            for (int i = 0; i < 10; i++)
            {
                long t0 = SystemClock.elapsedRealtimeNanos() / SyncService.NANOS_IN_MILLI;
                long t1 = SystemClock.uptimeMillis();
                long t2 = SystemClock.elapsedRealtimeNanos() / SyncService.NANOS_IN_MILLI;
                diffNanosToUpTime += ((t0 + t2) / 2L) - t1;
            }
            diffNanosToUpTime = diffNanosToUpTime / 10L;
        }

        public void processTouch(MotionEvent event)
        {
            long reportedUpperBound = syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
            long reportedLowerBound = syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;

            int pointerCount = event.getPointerCount();

            int upDownEvent = 0;
            int upDownPointerId = -1;

            if (event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN || event.getAction() == MotionEvent.ACTION_DOWN)
            {
                upDownEvent = -1;
                upDownPointerId = event.getPointerId(event.getActionIndex());
                int index = event.getPointerId(event.getActionIndex());
                if (index < drawView.isDown.length)
                {
                    drawView.isDown[index] = true;
                }
            }

            if (event.getActionMasked() == MotionEvent.ACTION_POINTER_UP || event.getAction() == MotionEvent.ACTION_UP)
            {
                upDownEvent = 1;
                upDownPointerId = event.getPointerId(event.getActionIndex());
                int index = event.getPointerId(event.getActionIndex());
                if (index < drawView.isDown.length)
                {
                    drawView.isDown[index] = false;
                }

            }

            if (syncServ.getAppState() >= SyncService.STATE_RECORDING_DATA && syncServ.getAppState() <= SyncService.STATE_PLAYING)
            {
                for (int p = 0; p < pointerCount; p++)
                {
                    for (int i = 0; i < event.getHistorySize(); i++) {
                        long actualTime = (event.getHistoricalEventTime(i) + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                        long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
                        printEventToFile(reportedTimeStamp, reportedUpperBound, reportedLowerBound,
                                event.getHistoricalX(p, i), event.getHistoricalY(p, i), event.getPointerId(p), 0);
                    }

                    long actualTime = (event.getEventTime() + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                    long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
                    int upDownForThis = 0;
                    if (event.getPointerId(p) == upDownPointerId)
                    {
                        upDownForThis = upDownEvent;
                    }
                    printEventToFile(reportedTimeStamp, reportedUpperBound, reportedLowerBound,
                            event.getX(p), event.getY(p), event.getPointerId(p), upDownForThis);
                }
            }

            if (syncServ.getAppState() >= SyncService.STATE_SYNCED_AND_READY
                    && syncServ.getAppState() <= SyncService.STATE_PLAYING
                    && syncServ.getStreamState())
            {
                for (int p = 0; p < pointerCount; p++) {
                    int upDownForThis = 0;
                    if (event.getPointerId(p) == upDownPointerId) {
                        upDownForThis = upDownEvent;
                    }
                    if (event.getEventTime() > previousStreamedTime[event.getPointerId(p)] + (SyncService.MILLIS_IN_SECOND / syncServ.getMaxMulticastFps())
                            || upDownForThis != 0)
                    {
                        previousStreamedTime[event.getPointerId(p)] = event.getEventTime();
                        long actualTime = (event.getEventTime() + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                        int reportedTimeStamp = (int)(
                                ((actualTime + syncServ.getOffsetNanosToServerStreaming())
                                        / SyncService.NANOS_IN_MILLI)
                                        - syncServ.getStreamingZeroTime());

                        if (upDownForThis == -1)
                        {
                            pressStartTime[event.getPointerId(p)] = reportedTimeStamp;
                        }

                        ArrayList<Object> data = new ArrayList<Object>();
                        data.add(reportedTimeStamp);
                        data.add(event.getPointerId(p));
                        data.add(upDownForThis);
                        data.add(event.getX(p));
                        data.add(event.getY(p));

                        Point po = new Point();
                        getWindowManager().getDefaultDisplay().getSize(po);
                        Integer screenWidth = po.x;
                        Integer screenHeight = po.y;

                        data.add(screenWidth);
                        data.add(screenHeight);

                        data.add(reportedTimeStamp - pressStartTime[event.getPointerId(p)]);

                        Boolean ok = syncServ.streamData(data);
                    }
                }
            }

            if (soundPool != null && event.getActionMasked() == MotionEvent.ACTION_DOWN)
            {
                MainActivity.soundPool.play(main.soundInt, 1.0f, 1.0f, 10, 0, 1.0f);
            }

            if (drawView != null)
            {
                for (int p = 0; p < pointerCount; p++)
                {
                    drawView.updateCoordinates((int) event.getX(p), (int) event.getY(p), event.getPointerId(p) % 10);
                }
            }
        }

        private void printEventToFile(long reportedTimeStamp, long reportedUpperBound, long reportedLowerBound, float x, float y, int pointerId, int UpDown)
        {
            String output = reportedTimeStamp + ",";
            output = output + reportedUpperBound + "," + reportedLowerBound + ",";
            output = output + pointerId + ",";
            output = output + formatter.format(x);
            output = output + "," + formatter.format(y);
            output = output + "," + UpDown;
            try {
                outputStreamTouch.println(output);
                outputStreamTouch.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing touch to file.");
            }
            prevLoggedTime = reportedTimeStamp;
        }

    }
}
