/*
FSenSync Installer
Copyright (C) 2017  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensyncinstaller;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortIn;
import com.illposed.osc.OSCPortOut;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import fi.forger.synclibrary.*;

public class MainActivity extends AppCompatActivity {

    private Timer myUiTimer;
    private TextView myTextView;
    private static int STORAGE_PERMISSION_REQUEST = 1;
    private static String messages = "";
    private Thread fileTransferThread;
    private ServerSocket fileTransferSocket;
    private static final int DEFAULT_TCP_PORT = 11199;
    private static final int DEFAULT_OSC_PORT = 11198;
    private static ArrayList<ApkInfo> apks;
    private static int numToInstall = -1;
    private static Boolean reportPrinted = false;
    public static Boolean permissionsOk = false;
    public static Boolean userOk = false;
    private static Boolean downloadStarted = false;
    private static Boolean downloadReady = false;
    private static Boolean installReady = false;
    private String broadcastAddress = "";
    private String serverIp = "";
    private String myIp = "";
    private static final String apkDirectoryName = "FSenSyncApks";
    private static Boolean readyToProcess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_REQUEST);
        } else {
            permissionsOk = true;
        }

        messages = "Installer starting.\nFinding server.";

        apks = new ArrayList<ApkInfo>();

        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled() == false) {
            wifiManager.setWifiEnabled(true);
        }

        // UI stuff
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textBox);
        myTextView.setMovementMethod(new ScrollingMovementMethod());
        myUiTimer = new Timer();
        myUiTimer.schedule(new UiUpdateTask(this), 1000, 1000);
        myUiTimer.schedule(new DownloadTask(this), 1000, 1000);

        fileTransferThread = new Thread(fileTransferRunner());
        fileTransferThread.start();

        OSCListener listenerServerAddress = new OSCListener() {
            public void acceptMessage(java.util.Date time, OSCMessage message) {
                List<Object> a = message.getArguments();
                if (a.size() != 1) {
                    return;
                }
                serverIp = (String) a.get(0);
            }
        };
        try {
            OSCPortIn portUnicast = new OSCPortIn(DEFAULT_OSC_PORT);
            portUnicast.addListener("/serveraddress", listenerServerAddress);
            portUnicast.startListening();
        } catch (Exception ex) {
            System.out.println("Network port " + DEFAULT_OSC_PORT + " is reserved.");
        }

        Button buttonStart = (Button) findViewById(R.id.button);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                userOk = true;
                Button buttonStart = (Button) findViewById(R.id.button);
                buttonStart.setEnabled(false);
                Button button2 = (Button) findViewById(R.id.button2);
                button2.setEnabled(false);
            }
        });

        Button button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                File path = Environment.getExternalStorageDirectory();

                path = new File(path.getAbsolutePath() + File.separator + apkDirectoryName);
                File[] files = path.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File file, String name) {
                        return name.endsWith(".apk");
                    }
                });

                for (int i = 0; i < files.length; i++) {
                    files[i].delete();
                }

                Button button2 = (Button) findViewById(R.id.button2);
                button2.setEnabled(false);
                addMessage("Deleted old apk files.");
            }
        });

        tryToGetOldVersionsUninstalled();
    }

    private Runnable fileTransferRunner() {
        Runnable runner = new Runnable() {
            public void run() {
                fileTransferSocket = null;

                while (fileTransferSocket == null) {
                    int number = DEFAULT_TCP_PORT;
                    try {
                        fileTransferSocket = new ServerSocket(number);
                    } catch (IOException ex) {
                        String message = "Cannot listen to port " + number + ".";
                        System.out.println(message);
                        addMessage(message);
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex2) {
                    }
                }

                while (true) {
                    try {
                        //serverSocket.setSoTimeout(60000);
                        Socket socket = fileTransferSocket.accept();
                        socket.setSoTimeout(7000);
                        installerConnection(socket);
                    } catch (Exception ex) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex2) {

                        }
                    }
                }
            }
        };
        return runner;
    }

    public void addMessage(String text) {
        messages = messages + "\n" + text;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_REQUEST && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                System.out.println("Required permission was not granted by user.");
            } else {
                System.out.println("Permission granted");
                permissionsOk = true;
            }
        }
    }

    public void onResume() {
        super.onResume();
        setReadyToProcess(true);
    }

    public void setReadyToProcess(Boolean value)
    {
        readyToProcess = value;
        if (readyToProcess) {
            System.out.println("ready");
        }
        else
        {
            System.out.println("not ready");
        }
    }


    public void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }

    private class UiUpdateTask extends TimerTask {

        MainActivity main;
        Boolean serverFound = false;

        public UiUpdateTask(MainActivity main) {
            this.main = main;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (myTextView != null) {

                        myTextView.setText(messages);
                    }
                }
            });
        }
    }

    private void findMyIp() {
        try {

            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                NetworkInterface ni = en.nextElement();
                Enumeration<InetAddress> ee = ni.getInetAddresses();
                while (ee.hasMoreElements()) {
                    InetAddress ia = ee.nextElement();
                    if (ia.isSiteLocalAddress() && ia instanceof Inet4Address) {
                        myIp = ia.getHostAddress();
                    }
                }
                if (!ni.isLoopback() && ni.isUp()) {
                    Iterator<InterfaceAddress> eBr = ni.getInterfaceAddresses().iterator();
                    while (eBr.hasNext()) {
                        InterfaceAddress ia = eBr.next();
                        if (ia != null && ia.getBroadcast() != null) {
                            broadcastAddress = ia.getBroadcast().toString();
                            broadcastAddress = broadcastAddress.replaceAll("/", "");
                        }
                    }
                }
            }
        } catch (SocketException e) {
            System.out.println("Error while finding my own ip.");
            e.printStackTrace();
        }
    }

    private class DownloadTask extends TimerTask {

        MainActivity main;
        Boolean serverFound = false;

        public DownloadTask(MainActivity main) {
            this.main = main;
        }

        @Override
        public void run()
        {
            if (myIp.equals(""))
            {
                findMyIp();
            }
            if (serverIp.length() == 0 && myIp.length() > 0)
            {
                try {
                    InetAddress address = InetAddress.getByName(broadcastAddress);
                    OSCPortOut sender = new OSCPortOut(address, 11200);
                    ArrayList<Object> a = new ArrayList<Object>(1);
                    a.add(myIp);
                    OSCMessage msg = new OSCMessage("/giveserverip", a);
                    sender.send(msg);
                } catch (Exception ex) {
                    System.out.println("Error sending broadcast message.");
                    ex.printStackTrace();
                }
            }
            if (serverFound == false && serverIp.length() > 0)
            {
                serverFound = true;
                main.addMessage("Server found.");
                if (!userOk)
                {
                    main.addMessage("Waiting for user to start downloads.");
                }
            }
            if (serverFound && downloadStarted == false && permissionsOk && userOk)
            {
                try {
                    InetAddress address = InetAddress.getByName(serverIp);
                    OSCPortOut sender = new OSCPortOut(address, 11100);
                    ArrayList<Object> a = new ArrayList<Object>(1);
                    a.add(myIp);
                    OSCMessage msg = new OSCMessage("/installerconnection", a);
                    sender.send(msg);
                } catch (Exception ex) {
                    System.out.println("Error when sending unicast message.");
                    ex.printStackTrace();
                }
            }
            if (downloadReady)
            {
                myUiTimer.schedule(new InstallTask(main), 100, 100);
                this.cancel();
            }
        }
    }

    private class InstallTask extends TimerTask {

        MainActivity main;

        public InstallTask(MainActivity main) {
            this.main = main;
        }

        @Override
        public void run()
        {
            if (installReady == false && readyToProcess == true)
            {
                tryInstallApk();
            }
            if (!installReady && apks.size() == numToInstall && numToInstall > 0)
            {
                Boolean allAnswered = true;
                for (int i = 0; i < apks.size(); i++)
                {
                    if (apks.get(i).installAnswered == false)
                    {
                        allAnswered = false;
                    }
                }
                if (allAnswered && readyToProcess == true)
                {
                    installReady = true;
                    addMessage("All installations processed.");
                }
            }
            if (installReady && !reportPrinted && readyToProcess == true)
            {
                reportPrinted = true;
                printReport();
            }
        }
    }

    public void installerConnection(Socket socket) {
        downloadStarted = true;
        {
            Timer timer = new Timer(true);
            TimeoutTask timeout = new TimeoutTask(socket);
            timer.schedule(timeout, 1000, 1000);

            try {
                BufferedReader netReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter netWriter = new PrintWriter(socket.getOutputStream());

                String command = netReader.readLine();

                switch (command) {
                    case "Server has no apk files.": {
                        addMessage("Server has no apk files.");
                        break;
                    }
                    case "Receive installer file.": {
                        numToInstall = Integer.parseInt(netReader.readLine());
                        String fileName = netReader.readLine();
                        long fileLength = Long.valueOf(netReader.readLine());
                        String serverCheckSum = netReader.readLine();

                        File path = Environment.getExternalStorageDirectory();
                        path = new File(path.getAbsolutePath() + File.separator + apkDirectoryName);

                        if (!path.exists()) {
                            path.mkdir();
                        }

                        File file = new File(path + File.separator + fileName);

                        Boolean needToDownload = true;
                        if (file.exists()) {
                            if (file.length() == fileLength && CheckSum.getCheckSum(file.getAbsolutePath()).equals(serverCheckSum)) {
                                netWriter.println("ALREADY HAVE THE FILE");
                                netWriter.flush();

                                needToDownload = false;
                                addMessage("Same file found: " + fileName);
                                apks.add(new ApkInfo(file));
                            } else {
                                file.delete();
                            }
                        }

                        if (needToDownload) {
                            addMessage("Started downloading file " + apks.size() + "/" + numToInstall);

                            netWriter.println("READY TO RECEIVE");
                            netWriter.flush();

                            CheckSum check = new CheckSum();

                            byte[] bytes = new byte[16 * 1024];
                            OutputStream out = new FileOutputStream(file);
                            InputStream in = socket.getInputStream();
                            int count = 0;
                            int accumCount = 0;
                            while (accumCount < fileLength && (count = in.read(bytes)) > 0) {
                                out.write(bytes, 0, count);
                                check.update(bytes, count);
                                timeout.delaySocketClosingTimeout();
                                accumCount += count;
                            }

                            out.close();

                            String myCheckSum = check.getCheckSum();

                            String message;
                            if (file.exists() && file.length() == fileLength && myCheckSum.equals(serverCheckSum)) {
                                netWriter.println("RECEIVED OK");
                                netWriter.flush();
                                message = "File '" + fileName + "' downloaded successfully.";
                                apks.add(new ApkInfo(file));
                            } else {
                                if (file.exists() == false) {
                                    message = "File '" + fileName + "' downloaded failed.";
                                } else {
                                    message = "File '" + fileName + "' had a mismatch in size or MD5 checksum.";
                                    file.delete();
                                }
                            }

                            in.close();

                            System.out.println(message);
                            addMessage(message);

                        }

                        break;
                    }
                    default:
                        System.out.println("Invalid file command:" + command);
                        break;
                }

                netReader.close();
                netWriter.close();
                socket.close();
            } catch (SocketTimeoutException ex) {
                System.out.println("File transfer timeout.");
            } catch (IOException ex) {
                System.out.println("Error in file transfer connection.");
                ex.printStackTrace();
            }

            timer.cancel();
        }

        if (numToInstall == apks.size()) {
            addMessage("All files downloaded.");
            downloadReady = true;
        }
    }

    private void tryToGetOldVersionsUninstalled()
    {
        try
        {
            PackageManager pm = getPackageManager();
            List<ApplicationInfo> apps = pm.getInstalledApplications(0);

            ArrayList<String> oldApps = new ArrayList<String>();
            oldApps.add("forger.analytics.fsensyncinstaller");
            oldApps.add("fi.forger.fsensyncvpro");
            oldApps.add("fi.forger.fsensyncstimuli");
            oldApps.add("forger.analytics.fsensyncaccelerometer");
            oldApps.add("forger.analytics.fsensyncbio");
            oldApps.add("fi.forger.fsensynccartography");
            oldApps.add("forger.analytics.fsensyncvideo");
            oldApps.add("forger.analytics.fsensyncbigclock");

            for (int i = 0; i < apps.size(); i++)
            {
                System.out.println(apps.get(i).packageName);
                for (int k = 0; k < oldApps.size(); k++) {
                    if (apps.get(i).packageName.equals(oldApps.get(k)))
                    {
                        Intent intent = new Intent(Intent.ACTION_DELETE);
                        intent.setData(Uri.parse("package:" + apps.get(i).packageName));
                        setReadyToProcess(false);
                        startActivity(intent);
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void tryInstallApk()
    {
        if (numToInstall != apks.size())
        {
            return;
        }

        int nextToInstall = -1;
        Boolean foundNewInstall = false;
        for (int i = 0; i < apks.size(); i++)
        {
            if (foundNewInstall == false && apks.get(i).installStarted == false)
            {
                apks.get(i).installStarted = true;
                foundNewInstall = true;
                nextToInstall = i;
            }
        }

        if (foundNewInstall)
        {
            {
                try {
                    // Checking if the apk was installed
                    PackageManager pm = getPackageManager();
                    PackageInfo info = pm.getPackageArchiveInfo(apks.get(nextToInstall).file.getAbsolutePath(), 0);
                    PackageInfo installed = pm.getPackageInfo(info.packageName, PackageManager.GET_META_DATA);
                    if (installed.versionCode == info.versionCode) {
                        apks.get(nextToInstall).installAnswered = true;
                        if (installReady == false)
                        {
                            tryInstallApk();
                        }
                        return;
                    }
                } catch (Exception ex) {

                }

                if (apks.get(nextToInstall).file.exists()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        try {
                            Uri apkUri = FileProvider.getUriForFile(getBaseContext(),
                                    getBaseContext().getApplicationContext().getPackageName() + ".provider",
                                    apks.get(nextToInstall).file);
                            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                            intent.setData(apkUri);
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            setReadyToProcess(false);

                            if (apks.get(nextToInstall).file.getName().startsWith("FSenSyncInstaller"))
                            {
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                this.startActivityForResult(intent, nextToInstall);
                                this.finish();
                            }
                            else
                            {
                                this.startActivityForResult(intent, nextToInstall);
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }


                    } else {

                        String idString = Build.MANUFACTURER + "_" + Build.MODEL;

                        if (idString.equals("alps_U18") || idString.equals("alps_X01S")) {

                            // Special case of the Alps smart watches
                            try {
                                final PackageManager pm = getPackageManager();
                                PackageInfo info = pm.getPackageArchiveInfo(apks.get(nextToInstall).file.getAbsolutePath(), 0);
                                PackageInfo oldApp = pm.getPackageInfo(info.packageName, PackageManager.GET_META_DATA);
                                if (oldApp != null) {
                                    apks.get(nextToInstall).installStarted = false;
                                    Intent intent = new Intent(Intent.ACTION_DELETE);
                                    intent.setData(Uri.parse("package:" + info.packageName)); // "package:fi.forger.fsensync.accelerometer"
                                    setReadyToProcess(false);
                                    startActivity (intent);
                                    return;
                                }
                            } catch (Exception ex) {

                            }

                            Intent install = new Intent(Intent.ACTION_VIEW);
                            install.setDataAndType(Uri.fromFile(apks.get(nextToInstall).file),
                                    "application/vnd.android.package-archive");
                            //install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            setReadyToProcess(false);
                            this.startActivityForResult(install, nextToInstall);


                        }
                        else {
                            Intent install = new Intent(Intent.ACTION_VIEW);
                            install.setDataAndType(Uri.fromFile(apks.get(nextToInstall).file),
                                    "application/vnd.android.package-archive");
                            //install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            setReadyToProcess(false);
                            if (apks.get(nextToInstall).file.getName().startsWith("FSenSyncInstaller"))
                            {
                                install.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                this.startActivityForResult(install, nextToInstall);
                                this.finish();
                            }
                            else
                            {
                                this.startActivityForResult(install, nextToInstall);
                            }
                        }
                    }
                } else {
                    apks.get(nextToInstall).installAnswered = true;
                    addMessage("Not installed: " + apks.get(nextToInstall).file.getName());
                }
            }
        }
    }

    private void printReport() {
        final int STATUS_UNKNOWN = 0;
        final int STATUS_UPDATED = 1;
        final int STATUS_OLD_VERSION = 2;
        final int STATUS_NEWER_VERSION = 3;
        final int STATUS_NOT_INSTALLED = 4;

        for (int i = 0; i < apks.size(); i++) {
            ApkInfo r = apks.get(i);
            try {
                // Checking if the apk was installed
                final PackageManager pm = getPackageManager();
                PackageInfo info = pm.getPackageArchiveInfo(apks.get(i).file.getAbsolutePath(), 0);
                r.info = info;

                try {
                    PackageInfo installed = pm.getPackageInfo(info.packageName, PackageManager.GET_META_DATA);
                    r.installed = installed;
                    if (installed.versionCode == info.versionCode)
                    {
                        r.status = STATUS_UPDATED;
                    }
                    else
                    {
                        if (installed.versionCode < info.versionCode)
                        {
                            r.status = STATUS_OLD_VERSION;
                        }
                        else
                        {
                            r.status = STATUS_NEWER_VERSION;
                        }
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    r.status = STATUS_NOT_INSTALLED;
                }
            } catch (Exception ex)
            {
                r.status = STATUS_UNKNOWN;
            }
        }

        if (apks.contains(new ApkInfo(STATUS_UPDATED)))
        {
            addMessage("");
            addMessage("Updated to server version:");

            int countUpdated = 0;

            for (int i = 0; i < apks.size(); i++) {
                if (apks.get(i).equals(STATUS_UPDATED))
                {
                    addMessage(strip(apks.get(i).info.packageName) + " "  + apks.get(i).info.versionName);
                    countUpdated += 1;
                }
            }

            if (countUpdated == numToInstall)
            {
                addMessage("");
                addMessage("All apps are up to date.");
            }
        }
        else
        {
            addMessage("");
            addMessage("No apps were updated.");
        }

        if (apks.contains(new ApkInfo(STATUS_OLD_VERSION)))
        {
            addMessage("");
            addMessage("Currently older version than server:");
            for (int i = 0; i < apks.size(); i++) {
                if (apks.get(i).equals(STATUS_OLD_VERSION))
                {
                    addMessage(strip(apks.get(i).info.packageName));
                    addMessage("Downloaded: " + apks.get(i).info.versionName + " Installed: " + apks.get(i).installed.versionName);
                }
            }
        }

        if (apks.contains(new ApkInfo(STATUS_NEWER_VERSION)))
        {
            addMessage("");
            addMessage("Currently newer version than server:");
            for (int i = 0; i < apks.size(); i++) {
                if (apks.get(i).equals(STATUS_NEWER_VERSION))
                {
                    addMessage(strip(apks.get(i).info.packageName));
                    addMessage("Downloaded: " + apks.get(i).info.versionName + " Installed: " + apks.get(i).installed.versionName);
                }
            }
        }

        if (apks.contains(new ApkInfo(STATUS_NOT_INSTALLED)))
        {
            addMessage("");
            addMessage("Not installed:");
            for (int i = 0; i < apks.size(); i++) {
                if (apks.get(i).equals(STATUS_NOT_INSTALLED))
                {
                    addMessage(strip(apks.get(i).info.packageName));
                }
            }
        }

        if (apks.contains(new ApkInfo(STATUS_UNKNOWN)))
        {
            addMessage("");
            addMessage("Status is unknown:");
            for (int i = 0; i < apks.size(); i++) {
                if (apks.get(i).equals(STATUS_UNKNOWN))
                {
                    addMessage(apks.get(i).file.getName());
                }
            }
        }
    }

    private String strip(String s)
    {
        return s.replace("fi.forger.", "");
    }

    private class ApkInfo
    {
        int status;
        PackageInfo info;
        PackageInfo installed;
        File file;
        Boolean installStarted = false;
        Boolean installAnswered = false;

        public ApkInfo() {}

        public ApkInfo(int status)
        {
            this.status = status;
        }

        public ApkInfo(File file) { this.file = file; }

        public boolean equals(Object other)
        {
            if (other == null) return false;
            if (other == this) return true;
            if (other instanceof ApkInfo)
            {
                return ((ApkInfo) other).status == this.status;
            }
            return false;
        }

        public boolean equals(int statusOther)
        {
            return statusOther == status;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode < apks.size())
        {
            apks.get(requestCode).installAnswered = true;
        }
    }

    private class TimeoutTask extends TimerTask {
        private long lastActivity;
        private Socket timedSocket;

        public TimeoutTask(Socket s) {
            lastActivity = System.currentTimeMillis();
            timedSocket = s;

        }

        @Override
        public void run() {
            if (lastActivity < System.currentTimeMillis() - 10000) {
                if (timedSocket != null && !timedSocket.isClosed()) {
                    System.out.println("File transfer timeout reached.");
                    try {
                        timedSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                this.cancel();
            }
        }

        public void delaySocketClosingTimeout() {
            lastActivity = System.currentTimeMillis();
        }
    }

    public Boolean stopRecording() {return false; }
    public void newSyncState(int syncState, int oldState, String syncStateString) {}
    public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {}
    public void cancelStimuli() {}
    public void showExtraMessageOnScreen(String message) {}
    public void enableScreenTouchSound() {}
    public void disableScreenTouchSound() {}
    public String getAppSetting(String setting)
    {
        return "INVALID_SETTING";
    }
    public void setAppSetting(String setting, String value) {}
    public Boolean getIsOnBackGround()
    {
        return false;
    }
    public Boolean getRecordingServiceReady() { return false; }
    public void closeApp()
    {
        this.finish();
    }
    public void setTagsToUi() {}
    public void updateExperimentInfoToUi() {}
    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream)
    {
        metaOutputStream.close();
        return false;
    }

    @Override
    protected void onStop()
    {
        //setReadyToProcess(false);
        super.onStop();
    }
}
