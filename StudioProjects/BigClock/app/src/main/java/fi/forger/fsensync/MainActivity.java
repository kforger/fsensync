/*
FSenSync BigClock
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import fi.forger.remotecontrollibrary.ControlledActivity;
import fi.forger.synclibrary.*;

public class MainActivity extends ControlledActivity {

    private static final String appFullName = "FSenSyncBigClock";

    private Timer myUiTimer;
    private TextView myTextView;
    private static int STORAGE_PERMISSION_REQUEST = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startServices();

        // UI stuff
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textBox);
        myTextView.setText(appFullName);
        setTagsToUi();
        updateExperimentInfoToUi();
        myUiTimer = new Timer();
        myUiTimer.schedule(new UiUpdateTask(this), 1000,5);
    }

    private void startServices()
    {

        Intent intent = new Intent(MainActivity.this, BigClockService.class);
        intent.setAction(RecordingService.MSG_START);
        startService(intent);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_REQUEST);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        if (requestCode == STORAGE_PERMISSION_REQUEST && grantResults.length > 0)
        {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
            {
                System.out.println("Required permission was not granted by user.");
            }
            else
            {
                System.out.println("Permission granted");
                startServices();
            }
        }
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (readyToExit)
        {
            if (BigClockService.syncServ != null) {
                BigClockService.syncServ.sendClosingAppToServer();
            }
            System.exit(0);
        }
    }

    private class UiUpdateTask extends TimerTask {
        MainActivity main;

        public UiUpdateTask(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            if (BigClockService.syncServ != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String s = "";
                        if (BigClockService.syncServ.getAppState() > SyncService.STATE_FIRST_SYNC) {
                            long t = (SystemClock.elapsedRealtimeNanos() + BigClockService.syncServ.getOffsetNanosToServer());
                            String time = "" + (SystemClock.elapsedRealtimeNanos() + BigClockService.syncServ.getOffsetNanosToServer());
                            s = s + time.substring(0, Math.max(0, time.length() - 6)) + "\n";
                            long count = (t % 10000000000L) / 1000000000L;
                            for (int i = 0; i < count + 1; i++)
                            {
                                s = s + (i);
                            }
                            s = s + "\n";
                            long count2 = (t % 1000000000L) / 100000000L;
                            for (int i = 0; i < count2 + 1; i++)
                            {
                                s = s + (i);
                            }
                        }
                        myTextView.setText(s);

                        myTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, myTextView.getWidth() / 8);
                        System.out.println((myTextView.getWidth() / 8));
                    }
                });
            }
        }
    }

}
