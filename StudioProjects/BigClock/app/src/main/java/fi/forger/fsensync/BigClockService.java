/*
FSenSync BigClock
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.File;
import java.io.PrintWriter;

import fi.forger.synclibrary.RecordingService;
import fi.forger.synclibrary.SyncService;
import fi.forger.synclibrary.SyncedClient;

public class BigClockService extends RecordingService implements SyncedClient {
    private static final String TAG = "BigClockService";

    private static final String clientType = "CLOCK";
    private static final int majorVersion = BuildConfig.MAJOR_VERSION;

    public static SyncService syncServ;

    private static PrintWriter outputStream;

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId)
    {
        final BigClockService parent = this;


        Runnable runner = new Runnable()
        {
            public void run()
            {
                if (intent.getAction().equals(RecordingService.MSG_START)) {
                    Log.i(TAG, "Service onStartCommand");

                    if (syncServ == null || syncServ.getAppState() == syncServ.STATE_ERROR)
                    {
                        syncServ = new SyncService(parent, parent, clientType, majorVersion);

                    }


                    Intent activityIntent = new Intent(parent, MainActivity.class);
                    PendingIntent resultPendingIntent =
                            PendingIntent.getActivity(parent, 0, activityIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT);

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(parent)
                                    .setSmallIcon(R.drawable.ic_stat_name)
                                    .setContentTitle("FSenSync Acceleration")
                                    .setContentText("Service is currently running.")
                                    .setContentIntent(resultPendingIntent);

                    startForeground(123456, mBuilder.build());
                }
                else if (intent.getAction().equals(RecordingService.MSG_STOP))
                {
                    stopForeground(true);
                    stopSelf();
                }
            }
        };

        Thread thread = new Thread(runner);
        thread.start();

        return Service.START_NOT_STICKY;
    }

    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream)
    {
        Boolean ok = true;

        try
        {
            metaOutputStream.println("App version: " + BuildConfig.VERSION_CODE);
            metaOutputStream.close();
        }
        catch (Exception ex)
        {
            System.out.println("Error when writing meta output file.");
        }

        try
        {
            File file = new File(fileBaseName + "ACC.csv");
            if (file.exists())
            {
                syncServ.sendErrorToServer("Refused to overwrite old recording with number " + recordingNumber);
                return false;
            }
            file.createNewFile();
            outputStream = new PrintWriter(file, "UTF-8");
            try {
                String header = "Timestamp(microseconds),DriftLowerBound,DriftUpperBound,Xacceleration,Yacceleration,Zacceleration";
                outputStream.println(header);
                outputStream.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing data to file.");
            }
        }
        catch (Exception ex)
        {
            ok = false;
            System.out.println("Error when opening output file.");
        }
        return ok;
    }

    public Boolean stopRecording()
    {
        outputStream.close();
        return true;
    }

    public Boolean getRecordingServiceReady()
    {
        return false;
    }

    public void sensorStateChanged()
    {
        syncServ.sensorStateChanged();
    }


}
