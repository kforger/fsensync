/*
FSenSync Cartography
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensynccartography;

import java.io.File;
import java.util.ArrayList;


public class TrialInfo
{
    public File backgroundImage;
    public ArrayList<File> imgList;
    public Boolean overlapAllowed;
    public Boolean selfEnded;
    public Boolean multiTouch;
    public int trialDuration;
    public int breakBeforeTrial;
    public double imgScale;
    public int trialNumber;


    public TrialInfo(File backgroundImage, ArrayList<File> imgList, Boolean overlapAllowed,
                     Boolean selfEnded, Boolean multiTouch, int trialDuration, int breakBeforeTrial,
                     double imgScale, int trialNumber)
    {
        this.backgroundImage = backgroundImage;
        this.imgList = imgList;
        this.overlapAllowed = overlapAllowed;
        this.selfEnded = selfEnded;
        this.multiTouch = multiTouch;
        this.trialDuration = trialDuration;
        this.breakBeforeTrial = breakBeforeTrial;
        this.imgScale = imgScale;
        this.trialNumber = trialNumber;
    }
}
