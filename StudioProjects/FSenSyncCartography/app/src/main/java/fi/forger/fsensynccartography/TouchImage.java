/*
FSenSync Cartography
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensynccartography;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.File;
import java.io.IOException;

public class TouchImage
{
    public int centerX;
    public int centerY;
    public Bitmap bitmap;
    public int height;
    public int width;
    public Boolean isMovable;
    public double areaScale;
    public int maxWidth;
    public int maxHeight;
    public Boolean isRecycled;
    public int imageNumber;
    public boolean overlapped = false;

    public TouchImage(int x, int y, File file, double areaScale, int maxWidth, int maxHeight, int imageNumber)
    {
        centerX = x;
        centerY = y;
        isMovable = true;
        this.areaScale =  areaScale;
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
        this.imageNumber = imageNumber;

        BitmapFactory.Options shapeInfo = readBitmapShape(file.getAbsolutePath());
        double linearScale = Math.sqrt((maxWidth * maxHeight * areaScale) / (double)(shapeInfo.outHeight * shapeInfo.outWidth));
        bitmap = loadBitmap(file.getAbsolutePath(), (int)(shapeInfo.outWidth * linearScale), (int)(shapeInfo.outHeight * linearScale));
        linearScale = Math.sqrt((maxWidth * maxHeight * areaScale) / (double)(bitmap.getHeight() * bitmap.getWidth()));
        this.width = (int)(bitmap.getWidth() * linearScale);
        this.height = (int)(bitmap.getHeight() * linearScale);

        isRecycled = false;

    }

    public static BitmapFactory.Options readBitmapShape(String path)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        return options;
    }

    public static Bitmap loadBitmap(String path, int desiredWidth, int desiredHeight)
    {
        // This is done to avoid out of memory errors
        BitmapFactory.Options options = readBitmapShape(path);
        options.inSampleSize = calculateInSampleSize(options, desiredWidth, desiredHeight);
        options.inJustDecodeBounds = false;

        //Checking the rotation of the image
        ExifInterface exif = null;
        try {
            File pictureFile = new File(path);
            exif = new ExifInterface(pictureFile.getAbsolutePath());
        } catch (IOException e) {
            //e.printStackTrace();
        }

        int orientation = ExifInterface.ORIENTATION_NORMAL;

        if (exif != null)
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        Bitmap notRotated = BitmapFactory.decodeFile(path, options);
        Bitmap rotated = null;

//        System.out.println("Desired: " + desiredWidth + " " + desiredHeight);
//        System.out.println("After: " + notRotated.getWidth() + " " + notRotated.getHeight());

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotated = rotateBitmap(notRotated, 90);
                notRotated.recycle();
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotated = rotateBitmap(notRotated, 180);
                notRotated.recycle();
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotated = rotateBitmap(notRotated, 270);
                notRotated.recycle();
                break;
            default:
                rotated = notRotated;
                break;
        }

        return rotated;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (reqWidth > 1000)
        {
            reqWidth = 1000;
        }

        if (reqHeight > 1000)
        {
            reqHeight = 1000;
        }

        if (height > reqHeight || width > reqWidth) {

            int halfHeight = height / 2;
            int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public void recycleBitmap()
    {
        isRecycled = true;
        if (bitmap != null)
        {
            bitmap.recycle();
        }
    }

    public TouchImage(int x, int y, int w, int h)
    {
        isRecycled = true;
        centerX = x;
        centerY = y;
        height = h;
        width = w;
        isMovable = false;
        this.imageNumber = -1;
    }

    public int getOriginalWidth()
    {
        if (bitmap != null && !isRecycled)
        {
            return bitmap.getWidth();
        }
        else
        {
            return width;
        }
    }

    public int getOriginalHeight()
    {
        if (bitmap != null && !isRecycled)
        {
            return bitmap.getHeight();
        }
        else
        {
            return height;
        }
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

}
