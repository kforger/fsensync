/*
FSenSync Cartography
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensynccartography;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

/**
 * Created by klaus on 30.5.2017.
 */

public class TouchImageView extends View {

    private Paint paint;
    private Bitmap backgroundBitmap;
    private Boolean isBlack = true;
    private int movedIds[];
    private Random rand;
    private boolean allowOverlap = false;
    private boolean multiTouch = false;

    public ArrayList<TouchImage> tImages;
    private ArrayList<ImageOrder> orders;

    private MainActivity parent;

    public TouchImageView(Context context)
    {
        super(context);
        init();
    }

    public TouchImageView(Context context, AttributeSet attrs)
    {
        super( context, attrs);
        init();
    }

    public TouchImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super( context, attrs, defStyleAttr);
        init();
    }

    public void onSizeChanged (int w, int h, int oldW, int oldH)
    {

    }

    public void setParent(MainActivity parent)
    {
        this.parent = parent;
    }

    public void setAllowOverlap(Boolean allow)
    {
        allowOverlap = allow;
    }

    public void clearImages()
    {
        if (backgroundBitmap != null)
        {
            backgroundBitmap.recycle();
        }

        if (tImages != null)
        {
            for (int i = 0; i < tImages.size(); i++)
            {
                tImages.get(i).recycleBitmap();
            }
            tImages.clear();
        }

        orders.clear();

        //java.lang.System.gc();

        tImages = new ArrayList<TouchImage>();
        movedIds = new int[10];
        for (int i = 0; i < 10; i++)
        {
            movedIds[i] = -1;
        }
    }

    public void addUnmovingBlock(int x, int y, int w, int h)
    {
        tImages.add(new TouchImage(x, y, w, h));
    }

    private void init()
    {
        rand = new Random();
        tImages = new ArrayList<TouchImage>();
        orders = new ArrayList<ImageOrder>();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(0.0f);
        movedIds = new int[10];
        for (int i = 0; i < 10; i++)
        {
            movedIds[i] = -1;
        }
    }

    public void setToBlack()
    {
        isBlack = true;
        invalidate();
    }

    public void setMultiTouch(Boolean multiTouch)
    {
        this.multiTouch = multiTouch;
    }

    public void requestRedraw()
    {
        invalidate();
    }

    public void loadBackgroundBitmap(File file)
    {
        backgroundBitmap = TouchImage.loadBitmap(file.getAbsolutePath(), getHeight(), getWidth());
        isBlack = false;
    }

    public void imagesToRandomLocations()
    {
        for (int i = 0; i < tImages.size(); i++)
        {
            int tries = 0;
            while (tries < 40 && tImages.get(i).centerX == 0 && tImages.get(i).centerX == 0)
            {
                tries += 1;
                moveImageTo(i, (int) (rand.nextDouble() * this.getWidth()), (int) (rand.nextDouble() * this.getHeight()));
            }
        }
    }

    public void addTouchImage(File file, double scale, int imageNumber)
    {
        tImages.add(new TouchImage(0, 0, file, scale, this.getWidth(), this.getHeight(), imageNumber));
        orders.add(new ImageOrder(tImages.size()-1, tImages.size()));
    }

    private int maxImagePriority()
    {
        int max = -1;
        for (int i = 0; i < orders.size(); i++)
        {
            max = Math.max(max, orders.get(i).imagePriority);
        }
        return max;
    }

    public void moveImageTo(int imgIndex, int newPosX, int newPosY)
    {
        if (imgIndex >= 0 && tImages.get(imgIndex).isMovable)
        {
            TouchImage thisImage = tImages.get(imgIndex);

            TouchImagePosition pos = new TouchImagePosition(newPosX, newPosY, thisImage.getWidth(),  thisImage.getHeight(), imgIndex);
            pos.constrainToArea(this.getWidth(), this.getHeight());
            pos.setOriginalLocation(pos.x, pos.y);

            ArrayList<TouchImagePosition> posList = new ArrayList<TouchImagePosition>();
            posList.add(pos);

            if (!allowOverlap) {
                int iterations = 0;
                while (iterations < 4) {
                    iterations++;
                    int numToCheck = posList.size();
                    for (int i = 0; i < numToCheck; i++) {
                        //posList.get(i).constrainToArea(this.getWidth(), this.getHeight());
                        posList.get(i).checkAndAddNew(posList, tImages, this.getWidth(), this.getHeight());
                    }

                    //System.out.println(iterations + ": " + posList.size());
                }

                ArrayList<TouchImagePosition> toRemove = new ArrayList<TouchImagePosition>();
                for (int i = 0; i < posList.size(); i++) {
                    if (posList.get(i).isValid == false) {
                        toRemove.add(posList.get(i));
                    }
                }

                posList.removeAll(toRemove);

                Collections.sort(posList, new Comparator<TouchImagePosition>() {
                    public int compare(TouchImagePosition p1, TouchImagePosition p2) {
                        return p1.getDistanceFromOrig() - p2.getDistanceFromOrig();
                    }
                });
            }
            else
            {
                posList.get(0).checkAndAddNew(posList, tImages, this.getWidth(), this.getHeight());
            }

            if (posList.size() > 0) {
                if (posList.get(0).isValid || allowOverlap)
                {
                    thisImage.centerX = posList.get(0).x;
                    thisImage.centerY = posList.get(0).y;
                    thisImage.overlapped = posList.get(0).isValid;
                }
            }
        }
    }

    public String[] giveAllPositions()
    {
        ArrayList<String> posList = new ArrayList<String>();
        for (int i = 0; i < tImages.size(); i++)
        {
            // ImgIndex,CenterX,CenterY,Width,Height,TouchIndex

            TouchImage img = tImages.get(i);
            if (img.imageNumber > -1) {
                posList.add(img.imageNumber + "," + img.centerX + "," + img.centerY + "," + img.getWidth() + "," + img.getHeight() + ",-1");
            }
        }
        return posList.toArray(new String[0]);
    }

    public void moveTouchImage(int touchedX, int touchedY, int pointerId, int upDown, long time)
    {
        if (parent != null && parent.appMode == MainActivity.APP_MODE_CARTOGRAPHY) {
            if ((!multiTouch && pointerId == 0) || (multiTouch && pointerId < 10)) {
                int imgIndex = -1;

                if (upDown == 0) {
                    imgIndex = movedIds[pointerId];
                } else {
                    double minDistance = Double.MAX_VALUE;
                    for (int i = 0; i < tImages.size(); i++) {
                        TouchImage t = tImages.get(i);
                        if (t.isMovable) {
                            double xDiff = touchedX - t.centerX;
                            double yDiff = touchedY - t.centerY;
                            double distance = Math.sqrt((xDiff * xDiff) + (yDiff * yDiff));

                            if (distance < minDistance
                                    && Math.abs(t.centerX - touchedX) < Math.max(t.getWidth() / 2, 100)
                                    && Math.abs(t.centerY - touchedY) < Math.max(t.getHeight() / 2, 100)) {
                                imgIndex = i;
                                minDistance = distance;
                            }
                        }
                    }
                }

                for (int i = 0; i < orders.size(); i++) {
                    if (orders.get(i).imageIndex == imgIndex) {
                        orders.get(i).imagePriority = maxImagePriority() + 1;
                    }
                }

                if (upDown == -1 && imgIndex >= 0 && imgIndex < tImages.size())
                {
                    // Finger down event
                    // ImgIndex,CenterX,CenterY,Width,Height,TouchIndex
                    TouchImage img = tImages.get(imgIndex);
                    String s = img.imageNumber + "," + img.centerX + "," + img.centerY + "," + img.getWidth() + "," + img.getHeight() + "," + pointerId;
                    parent.logCartographyPosition(time, s, MainActivity.TRIAL_PHASE_MIDDLE);
                }

                movedIds[pointerId] = imgIndex;
                moveImageTo(imgIndex, touchedX, touchedY);

                if (upDown == 1 && imgIndex >= 0 && imgIndex < tImages.size())
                {
                    // Finger up event
                    // ImgIndex,CenterX,CenterY,Width,Height,TouchIndex
                    TouchImage img = tImages.get(imgIndex);
                    String s = img.imageNumber + "," + img.centerX + "," + img.centerY + "," + img.getWidth() + "," + img.getHeight() + "," + pointerId;
                    parent.logCartographyPosition(time, s, MainActivity.TRIAL_PHASE_MIDDLE);
                }
            }
        }
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        if (parent != null && parent.appMode == MainActivity.APP_MODE_CARTOGRAPHY) {
            paint.setAlpha(255);
            if (!isBlack && backgroundBitmap != null) {
                canvas.drawBitmap(backgroundBitmap, new Rect(0, 0, backgroundBitmap.getWidth(), backgroundBitmap.getHeight()),
                        new Rect(0, 0, canvas.getWidth(), canvas.getHeight()), paint);
            } else {
                canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), paint);
            }

            if (orders != null) {
                Collections.sort(orders, new Comparator<ImageOrder>() {
                    public int compare(ImageOrder p1, ImageOrder p2) {
                        return p1.imagePriority - p2.imagePriority;
                    }
                });
            }

            for (int i = 0; i < orders.size(); i++) {
                TouchImage t = tImages.get(orders.get(i).imageIndex);
                if (t.overlapped)
                {
                    paint.setAlpha(255);
                }
                else
                {
                    paint.setAlpha((int)(255 - (40 * (i/(double)orders.size()))));
                }
                if (t.bitmap != null) {
                    int xOffset = -(t.getWidth() / 2);
                    int yOffset = -(t.getHeight() / 2);

                    canvas.drawBitmap(t.bitmap,
                            new Rect(0, 0, t.getOriginalWidth(), t.getOriginalHeight()),
                            new Rect(t.centerX + xOffset,
                                    t.centerY + yOffset,
                                    t.centerX + t.getWidth() + xOffset,
                                    t.centerY + t.getHeight() + yOffset),
                            paint);
                }
            }
        }
    }

    private class ImageOrder
    {
        public int imageIndex;
        public int imagePriority;

        public ImageOrder(int imageIndex, int imagePriority)
        {
            this.imageIndex = imageIndex;
            this.imagePriority = imagePriority;
        }
    }

}
