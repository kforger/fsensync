/*
FSenSync Cartography
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensynccartography;

import java.util.ArrayList;

public class TouchImagePosition
{
    int x;
    int y;
    int origX;
    int origY;
    int myWidth;
    int myHeight;
    Boolean isChecked;
    Boolean isValid;
    int imageIndex;
    Boolean upAllowed;
    Boolean downAllowed;
    Boolean leftAllowed;
    Boolean rightAllowed;


    public TouchImagePosition(int x, int y, int myWidth, int myHeight, int imageIndex)
    {
        this.x = x;
        this.y = y;
        this.imageIndex = imageIndex;
        this.myWidth = myWidth;
        this.myHeight = myHeight;
        isChecked = false;
        isValid =  false;
        upAllowed = true;
        downAllowed = true;
        leftAllowed = true;
        rightAllowed = true;
    }

    public void setOriginalLocation(int xNew, int yNew)
    {
        origX = xNew;
        origY = yNew;
    }

    public int getDistanceFromOrig()
    {
        int xDiff = x - origX;
        int yDiff = y - origY;
        return (xDiff * xDiff) + (yDiff * yDiff);
    }

    public void checkAndAddNew(ArrayList<TouchImagePosition> posList, ArrayList<TouchImage> images, int areaWidth, int areaHeight)
    {
        if (isChecked == false)
        {
            isChecked = true;
            isValid = true;

            if (x < myWidth / 2
                    || y < myHeight / 2
                    || x > areaWidth - (myWidth / 2)
                    || y > areaHeight - (myHeight / 2)) {
                isValid = false;
            }

            if (isValid) {
                for (int i = 0; i < images.size(); i++) {
                    TouchImage other = images.get(i);

                    if (i != imageIndex) {
                        int xDist = Math.abs(x - other.centerX);
                        int yDist = Math.abs(y - other.centerY);
                        int xMinDist = (myWidth + other.getWidth()) / 2;
                        int yMinDist = (myHeight + other.getHeight()) / 2;

                        if (xDist < xMinDist && yDist < yMinDist) {
                            isValid = false;

                            if (upAllowed)
                            {
                                TouchImagePosition up = new TouchImagePosition(x, other.centerY - ((myHeight + other.getHeight()) / 2), myWidth, myHeight, imageIndex);
                                up.setOriginalLocation(origX, origY);
                                up.inheritAllowed(this);
                                up.downAllowed = false;
                                posList.add(up);
                            }


                            if (downAllowed)
                            {
                                TouchImagePosition down = new TouchImagePosition(x, other.centerY + ((myHeight + other.getHeight()) / 2), myWidth, myHeight, imageIndex);
                                down.setOriginalLocation(origX, origY);
                                down.inheritAllowed(this);
                                down.upAllowed = false;
                                posList.add(down);
                            }

                            if (leftAllowed)
                            {
                                TouchImagePosition left = new TouchImagePosition(other.centerX + ((myWidth + other.getWidth()) / 2), y, myWidth, myHeight, imageIndex);
                                left.setOriginalLocation(origX, origY);
                                left.inheritAllowed(this);
                                left.rightAllowed = false;
                                posList.add(left);
                            }

                            if (rightAllowed)
                            {
                                TouchImagePosition right = new TouchImagePosition(other.centerX - ((myWidth + other.getWidth()) / 2), y, myWidth, myHeight, imageIndex);
                                right.setOriginalLocation(origX, origY);
                                right.inheritAllowed(this);
                                right.leftAllowed = false;
                                posList.add(right);
                            }
                        }
                    }
                }
            }
        }
    }

    public void inheritAllowed(TouchImagePosition parent)
    {
        upAllowed = parent.upAllowed;
        downAllowed = parent.downAllowed;
        leftAllowed = parent.leftAllowed;
        rightAllowed = parent.rightAllowed;
    }

    public void constrainToArea(int areaWidth, int areaHeight)
    {
        if (x < myWidth / 2)
        {
            x = myWidth / 2;
        }

        if (y < myHeight / 2)
        {
            y = myHeight / 2;
        }

        if (x > areaWidth - (myWidth / 2))
        {
            x = areaWidth - (myWidth / 2);
        }

        if (y > areaHeight - (myHeight / 2))
        {
            y = areaHeight - (myHeight / 2);
        }
    }

}
