/*
FSenSync Cartography
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensynccartography;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import fi.forger.remotecontrollibrary.ControlledActivity;
import fi.forger.synclibrary.*;

public class MainActivity extends ControlledActivity implements SyncedClient {

    private static final String clientType = "CART";
    private static final int majorVersion = BuildConfig.MAJOR_VERSION;

    private static PrintWriter outputStreamTouch;
    private static PrintWriter outputStreamGame;
    private static PrintWriter outputStreamMeta;


    private static int STORAGE_PERMISSION_REQUEST = 1;

    private static SyncService syncServ;
    private static TouchService touchService;

    private Timer myTimer;
    private static TouchImageView touchImageView;

    private TextView myTextView;
    private TextView textExperiment;
    private TextView textExtra;
    private TextView textExtra2;
    private TextView textTag1;
    private TextView textTag2;
    private TextView textTag3;
    private Button button;

    public static ArrayList<TrialInfo> trialList;
    public static int currentTrial = -1;

    public static final int APP_MODE_BREAK = -1;
    public static final int APP_MODE_WAITING = 0;
    public static final int APP_MODE_CARTOGRAPHY = 2;
    public int appMode = APP_MODE_WAITING;

    public static final int TRIAL_PHASE_START = 0;
    public static final int TRIAL_PHASE_MIDDLE = 1;
    public static final int TRIAL_PHASE_END = 2;

    private int nativeSoundRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setTryToHideActionBar(false);
        super.onCreate(savedInstanceState);
        startServices();

        // UI stuff
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textBox);
        textExperiment = (TextView) findViewById(R.id.textExperiment);
        textExtra = (TextView) findViewById(R.id.textExtra);
        textExtra2 = (TextView) findViewById(R.id.textExtra2);
        textTag1 = (TextView) findViewById(R.id.textTag1);
        textTag2 = (TextView) findViewById(R.id.textTag2);
        textTag3 = (TextView) findViewById(R.id.textTag3);
        touchImageView = (TouchImageView) findViewById(R.id.pictureView);
        touchImageView.setParent(this);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (syncServ != null && syncServ.getAppState() == SyncService.STATE_PLAYING)
                            startNextTrial();
                    }
                });
            }
        });
        updateExperimentInfoToUi();
        myTimer = new Timer();
        myTimer.schedule(new UiUpdateTask(this), 1000,10);
        myTimer.schedule(new DrawTask(), 1000,50);


        AudioManager audioManager = (AudioManager) this.getSystemService(getApplicationContext().AUDIO_SERVICE);
        nativeSoundRate = Integer.valueOf(audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE));
    }

    public void setTagsToUi()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textTag1.setText(syncServ.getTag1());
                textTag2.setText(syncServ.getTag2());
                textTag3.setText(syncServ.getTag3());
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (readyToExit)
        {
            syncServ.sendClosingAppToServer();
            System.exit(0);
        }
    }

    private class StartPlayTask extends TimerTask {

        MainActivity main;
        public long desiredStart;

        public StartPlayTask(MainActivity main, long desiredStart) {
            this.main = main;
            this.desiredStart = desiredStart;
        }

        @Override
        public void run() {
            syncServ.setStatePlayingStimuli();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    touchImageView.invalidate();
                    RelativeLayout layout = (RelativeLayout) findViewById(R.id.pictureLayout);
                    layout.bringToFront();
                    RelativeLayout layout2 = (RelativeLayout) findViewById(R.id.otherLayout);
                    layout2.setVisibility(View.INVISIBLE);
                    RelativeLayout layoutDraw = (RelativeLayout) findViewById(R.id.drawLayout);
                    layoutDraw.bringToFront();

                    currentTrial = -1;
                    startNextTrial();
                }
            });
        }
    }

    private void startNextTrial()
    {
        appMode = APP_MODE_BREAK;

        if (currentTrial >= 0)
        {
            long actualTime = SystemClock.elapsedRealtimeNanos();
            String[] s = touchImageView.giveAllPositions();
            for (int i = 0; i < s.length; i++)
            {
                logCartographyPosition(actualTime, s[i], TRIAL_PHASE_END);
            }
        }

        currentTrial += 1;

        if (currentTrial < trialList.size())
        {
            final long time = System.currentTimeMillis();

            final TrialInfo info = trialList.get(currentTrial);
            touchImageView.clearImages();
            touchImageView.requestRedraw();
            touchImageView.setMultiTouch(info.multiTouch);

            button.setEnabled(false);
            if (info.selfEnded)
            {
                button.bringToFront();
            }
            else
            {
                touchImageView.bringToFront();
            }

            Runnable runner = new Runnable() {
                public void run() {
                    touchImageView.setAllowOverlap(false);
                    touchImageView.loadBackgroundBitmap(info.backgroundImage);
                    if (info.selfEnded) {
                        touchImageView.addUnmovingBlock((int) button.getX() + (button.getWidth() / 2),
                                (int) button.getY() + (button.getHeight() / 2),
                                (int) (button.getMeasuredWidth() * 1.1),
                                (int) (button.getMeasuredHeight() * 1.1));
                    }
                    for (int i = 0; i < info.imgList.size(); i++) {
                        File picture = info.imgList.get(i);
                        if (picture.exists()) {
                            touchImageView.addTouchImage(picture, info.imgScale, i);
                        }
                    }
                    touchImageView.imagesToRandomLocations();
                    touchImageView.setAllowOverlap(info.overlapAllowed);


                    long elapsedTime = System.currentTimeMillis() - time;
                    myTimer.schedule(new CartographyDelayedStartTask(info), Math.max(info.breakBeforeTrial - elapsedTime,0));
                }
            };

            Thread t = new Thread(runner);
            t.start();
        }
        else
        {
            syncServ.playingStimuliEnded();
            appMode = APP_MODE_WAITING;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    RelativeLayout layout = (RelativeLayout) findViewById(R.id.otherLayout);
                    layout.setVisibility(View.VISIBLE);
                    layout.bringToFront();
                    RelativeLayout layoutDraw = (RelativeLayout) findViewById(R.id.drawLayout);
                    layoutDraw.bringToFront();
                }
            });

            syncServ.clientWantsToStopRecording();
        }
    }



    private class DrawTask extends TimerTask {

        MainActivity main;

        public DrawTask() {

        }

        @Override
        public void run()
        {

        }
    }

    public void logCartographyPosition(long time, String position, int trialPhase)
    {
        if (trialPhase == TRIAL_PHASE_START || trialPhase == TRIAL_PHASE_END || (trialPhase == TRIAL_PHASE_MIDDLE && appMode == APP_MODE_CARTOGRAPHY))
        {
            String output = getTimeAndBounds(time) + "," + position + "," + trialPhase + "," + trialList.get(currentTrial).trialNumber;
            outputStreamGame.println(output);
            outputStreamGame.flush();
        }
    }

    private class CartographyDelayedStartTask extends TimerTask {

        TrialInfo info;

        public CartographyDelayedStartTask(TrialInfo info)
        {
            this.info = info;
        }

        @Override
        public void run() {

            long actualTime = SystemClock.elapsedRealtimeNanos();
            String[] s = touchImageView.giveAllPositions();
            for (int i = 0; i < s.length; i++)
            {
                logCartographyPosition(actualTime, s[i], TRIAL_PHASE_START);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    button.setEnabled(false);
                    touchImageView.requestRedraw();
                    appMode = APP_MODE_CARTOGRAPHY;
                    if (info.selfEnded)
                    {
                        button.setEnabled(true);
                    }
                    else
                    {
                        myTimer.schedule(new CartographyNextTrialTask(), info.trialDuration);
                    }
                }
            });
        }
    }

    private class CartographyNextTrialTask extends TimerTask {

        public CartographyNextTrialTask() {
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startNextTrial();
                }
            });
        }
    }

    public boolean startCartography(File file, String stimuliName, long desiredStartTime)
    {
        Boolean ok = true;
        String errorMessage = "-";
        ArrayList<File> images = new ArrayList<File>();

        try {
            trialList = new ArrayList<TrialInfo>();
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = br.readLine();

            String headerModel = "BackgroundImage,OverlapAllowed,SelfEnded,MultiTouch,TrialDuration,BreakBeforeTrial,ImgScale,Img";
            headerModel = headerModel.toLowerCase();

            if (!(line.toLowerCase()).startsWith(headerModel)) {
                ok = false;
                errorMessage = "Unrecognized header line in cartography file.";
            }

            if (ok) {
                while ((line = br.readLine()) != null && ok) {
                    String[] v = line.split(",");

                    if (v.length > 6) {
                        File backgroundImage = null;
                        ArrayList trialImages = new ArrayList<File>();
                        Boolean overlapAllowed = false;
                        Boolean selfEnded = false;
                        Boolean multiTouch = false;
                        int trialDuration = 10000;
                        int breakBeforeTrial = 0;
                        double imgScale = 0.1;
                        if (v[0].length() > 0) {
                            File f = new File(syncServ.getStimulusDirectory().getAbsolutePath() + File.separator + v[0]);
                            backgroundImage = f;
                            images.add(f);
                        }
                        if (v[1].toLowerCase().equals("true")) {
                            overlapAllowed = true;
                        }
                        if (v[2].toLowerCase().equals("true")) {
                            selfEnded = true;
                        }
                        if (v[3].toLowerCase().equals("true")) {
                            multiTouch = true;
                        }
                        if (v[4].length() > 0) {
                            try {
                                trialDuration = Integer.valueOf(v[4]);
                            } catch (NumberFormatException ex) {
                                ok = false;
                                errorMessage = "Bad number format on line: " + line;
                            }
                        }
                        if (v[5].length() > 0) {
                            try {
                                breakBeforeTrial = Integer.valueOf(v[5]);
                            } catch (NumberFormatException ex) {
                                ok = false;
                                errorMessage = "Bad number format on line: " + line;
                            }
                        }
                        if (v[6].length() > 0) {
                            try {
                                imgScale = Double.valueOf(v[6]);
                            } catch (NumberFormatException ex) {
                                ok = false;
                                errorMessage = "Bad number format on line: " + line;
                            }
                        }

                        for (int i = 7; i < v.length; i++) {
                            if (v[i].length() > 0) {
                                File f = new File(syncServ.getStimulusDirectory().getAbsolutePath() + File.separator + v[i]);
                                if (f.exists()) {
                                    trialImages.add(f);
                                    images.add(f);
                                } else {
                                    ok = false;
                                    if (f.getName().contains("."))
                                    {
                                        errorMessage = "Image file does not exist: " + v[i];
                                    }
                                    else
                                    {
                                        errorMessage = "Image file does not exist: " + v[i] + " Missing file type e.g. '.png'?";
                                    }
                                }
                            }
                        }

                        if (ok) {
                            TrialInfo info = new TrialInfo(backgroundImage, trialImages,
                                    overlapAllowed, selfEnded, multiTouch, trialDuration,
                                    breakBeforeTrial, imgScale, trialList.size());
                            trialList.add(info);
                        }
                    } else {
                        if (line.length() > 0) {
                            ok = false;
                            errorMessage = "Bad trial info: " + line;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ok = false;
            errorMessage = "Error in reading file: " + stimuliName;
        }

        System.out.println("Read " + trialList.size() + " trials.");

        long delay = desiredStartTime - ((SystemClock.elapsedRealtimeNanos() + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MILLI);

        if (ok && delay < 0) {
            ok = false;
            errorMessage = "Loading stimulus file '" + stimuliName + "' was too slow.";
        }

        if (ok && trialList.size() == 0) {
            ok = false;
            errorMessage = "Trial file '" + stimuliName + "' did not contain any valid trials.";
        }

        if (ok)
        {
            try {
                String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,ImgIndex,CenterX,CenterY,Width,Height,TouchIndex,TrialPhase,TrialNum";
                outputStreamGame.println(header);
                outputStreamGame.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to file.");
            }

            System.out.println("Start task set: " + delay);
            myTimer.schedule(new StartPlayTask(this, desiredStartTime), delay);

        } else {
            System.out.println("Start task not set: " + delay);
            showExtraMessageOnScreen(errorMessage);
            syncServ.sendErrorToServer(errorMessage);
        }

        return ok;
    }

    public String getTimeAndBounds(long actualTime)
    {
        long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
        long reportedUpperBound = syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
        long reportedLowerBound = syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;
        return reportedTimeStamp + "," + reportedUpperBound + "," + reportedLowerBound;
    }

    public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {

        boolean successfulStart = false;

        if (syncServ != null) {

            File file = new File(syncServ.getStimulusDirectory().getAbsolutePath() + File.separator + stimuliName);
            if (file.exists() && stimuliName.length() > 0) {

                Boolean ok = false;
                String errorMessage = "-";

                // Cartography
                if (stimuliName.endsWith(".csv"))
                {
                    ok = true;
                    successfulStart = startCartography(file, stimuliName, desiredStartTime);
                    if (successfulStart)
                    {
                        appMode = APP_MODE_CARTOGRAPHY;

                        outputStreamMeta.println("Cartography definition file: " + stimuliName);
                    }
                }

                if (!ok)
                {
                    errorMessage = "Did not recognize file: " + stimuliName;
                    showExtraMessageOnScreen("");
                    syncServ.sendErrorToServer(errorMessage);
                }
            }
            else
            {
                showExtraMessageOnScreen("Did not have file: '" + stimuliName + "'");
                syncServ.sendErrorToServer("Did not have file: '" + stimuliName + "'");
            }
        }

        if (!successfulStart)
        {
            syncServ.delayedCancelAndStop();
            appMode = APP_MODE_WAITING;
        }
    }

    public void cancelStimuli() {
        System.out.println("Cancelling stimuli");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.otherLayout);
                layout.setVisibility(View.VISIBLE);
                layout.bringToFront();
                RelativeLayout layoutDraw = (RelativeLayout) findViewById(R.id.drawLayout);
                layoutDraw.bringToFront();
            }
        });

        syncServ.clientWantsToStopRecording();
        appMode = APP_MODE_WAITING;
    }

    private void startServices() {
        if (syncServ == null || syncServ.getAppState() == SyncService.STATE_ERROR) {
            syncServ = new SyncService(this, this, clientType, majorVersion);
            if (touchService == null && syncServ.getAppState() > SyncService.STATE_ERROR) {
                touchService = new TouchService(this);
            }
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_REQUEST);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_REQUEST && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                System.out.println("Required permission was not granted by user.");
            } else {
                System.out.println("Permission granted");
                startServices();
            }
        }
    }

    public void updateExperimentInfoToUi()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (syncServ.getAppState() < SyncService.STATE_FIRST_SYNC) {
                    textExperiment.setText("CART, Device ID: " + syncServ.getDeviceId());
                }
                else
                {
                    String ph = syncServ.getDeviceId();
                    if (ph.equals("-")) {
                        textExperiment.setText("CART, App ID: " + syncServ.getMyShortId());
                    }
                    else
                    {
                        textExperiment.setText("CART, App ID: " + syncServ.getMyShortId() + " (" + syncServ.getDeviceId() + ")");
                    }
                }
            }
        });
    }

    public void showExtraMessageOnScreen(final String message)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textExtra.setText(message);
            }
        });
    }

    private class UiUpdateTask extends TimerTask {
        MainActivity main;

        public UiUpdateTask(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String s = syncServ.getStateString();
                    if (syncServ.getAppState() > SyncService.STATE_FIRST_SYNC) {
                        String time = "\n" + (SystemClock.elapsedRealtimeNanos() + syncServ.getOffsetNanosToServer());
                        s = s + time.substring(0, Math.max(0, time.length() - 6));
                    }
                    myTextView.setText(s);

                    Point p = new Point();
                    getWindowManager().getDefaultDisplay().getSize(p);
                    Integer screenWidth = p.x;
                    Integer screenHeight = p.y;
                    if (android.os.Build.VERSION.SDK_INT >= 23) {
                        textExtra2.setText("View size: " + screenWidth + "x" + screenHeight + ", Sound rate: " + nativeSoundRate + "Hz");
                    }
                    else
                    {
                        textExtra2.setText("View size: " + screenWidth + "x" + screenHeight + ", Sounds not supported");
                    }
                }
            });

        }
    }

    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream) {
        Boolean ok = true;

        try
        {
            metaOutputStream.println("App version: " + BuildConfig.VERSION_CODE);
            metaOutputStream.println("Native sound rate: " + nativeSoundRate);
            Point p = new Point();
            getWindowManager().getDefaultDisplay().getSize(p);
            Integer screenWidth = p.x;
            Integer screenHeight = p.y;
            metaOutputStream.println("View width: " + screenWidth);
            metaOutputStream.println("View height: " + screenHeight);

            outputStreamMeta = metaOutputStream;
        }
        catch (Exception ex)
        {
            System.out.println("Error when writing meta output file.");
        }

        try {
            File fileTouch = new File(fileBaseName + "TOUCH.csv");
            File fileStimulus = new File(fileBaseName + "CART.csv");
            if (fileTouch.exists() || fileStimulus.exists()) {
                syncServ.sendErrorToServer("Refused to overwrite old recording with number " + recordingNumber);
                return false;
            }
            fileTouch.createNewFile();
            fileStimulus.createNewFile();
            outputStreamTouch = new PrintWriter(fileTouch, "UTF-8");
            try {
                String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,TouchIndex,Xcoordinate,Ycoordinate,UpDownEvent";
                outputStreamTouch.println(header);
                outputStreamTouch.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to file.");
            }
            outputStreamGame = new PrintWriter(fileStimulus, "UTF-8");
        } catch (Exception ex) {
            ok = false;
            System.out.println("Error when opening output file.");
        }
        return ok;
    }

    public Boolean stopRecording() {
        outputStreamTouch.close();
        outputStreamGame.close();
        outputStreamMeta.close();

        appMode = APP_MODE_WAITING;
        return true;
    }

    public Boolean getRecordingServiceReady() {
        return touchService.touchListeningReady;
    }

    public void sensorStateChanged() {
        syncServ.sensorStateChanged();
    }

    public boolean dispatchTouchEvent(MotionEvent event)
    {
        if (touchService != null)
        {
            touchService.processTouch(event);
        }
        return super.dispatchTouchEvent(event);
    }

    private class TouchService {
        public Boolean touchListeningReady;
        private NumberFormat formatter;
        private MainActivity main;
        private long diffNanosToUpTime;
        private long previousStreamedTime[];
        private int pressStartTime[];

        public TouchService(MainActivity main) {
            this.main = main;
            formatter = NumberFormat.getInstance(Locale.ROOT);
            formatter.setGroupingUsed(false);
            formatter.setMinimumIntegerDigits(1);
            formatter.setMinimumFractionDigits(1);
            touchListeningReady = true;

            previousStreamedTime = new long[30];
            pressStartTime = new int[30];
            for (int i = 0; i < 30; i++)
            {
                previousStreamedTime[i] = Long.MIN_VALUE;
                pressStartTime[i] = Integer.MIN_VALUE;
            }

            diffNanosToUpTime = 0l;
            for (int i = 0; i < 10; i++)
            {
                long t0 = SystemClock.elapsedRealtimeNanos() / SyncService.NANOS_IN_MILLI;
                long t1 = SystemClock.uptimeMillis();
                long t2 = SystemClock.elapsedRealtimeNanos() / SyncService.NANOS_IN_MILLI;
                diffNanosToUpTime += ((t0 + t2) / 2l) - t1;
            }
            diffNanosToUpTime = diffNanosToUpTime / 10l;
        }

        public void processTouch(MotionEvent event)
        {

            long reportedUpperBound = syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
            long reportedLowerBound = syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;

            int pointerCount = event.getPointerCount();

            int upDownEvent = 0;
            int upDownPointerId = -1;

            if (event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN || event.getAction() == MotionEvent.ACTION_DOWN) {
                upDownEvent = -1;
                upDownPointerId = event.getPointerId(event.getActionIndex());
            }

            if (event.getActionMasked() == MotionEvent.ACTION_POINTER_UP || event.getAction() == MotionEvent.ACTION_UP) {
                upDownEvent = 1;
                upDownPointerId = event.getPointerId(event.getActionIndex());

            }

            if (appMode == APP_MODE_CARTOGRAPHY)
            {
                for (int p = 0; p < pointerCount; p++)
                {
                    int upDownForThis = 0;
                    if (event.getPointerId(p) == upDownPointerId)
                    {
                        upDownForThis = upDownEvent;
                    }

                    if (touchImageView != null)
                    {
                        long actualTime = (event.getEventTime() + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                        touchImageView.moveTouchImage((int)event.getX(p), (int)event.getY(p), event.getPointerId(p),
                                upDownForThis, actualTime);
                        touchImageView.requestRedraw();
                    }
                }
            }

            if (syncServ.getAppState() >= SyncService.STATE_RECORDING_DATA && syncServ.getAppState() <= SyncService.STATE_PLAYING)
            {
                for (int p = 0; p < pointerCount; p++)
                {
                    for (int i = 0; i < event.getHistorySize(); i++) {
                        long actualTime = (event.getHistoricalEventTime(i) + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                        long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
                        printEventToFile(reportedTimeStamp, reportedUpperBound, reportedLowerBound,
                                event.getHistoricalX(p, i), event.getHistoricalY(p, i), event.getPointerId(p), 0);
                    }

                    long actualTime = (event.getEventTime() + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                    long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
                    int upDownForThis = 0;
                    if (event.getPointerId(p) == upDownPointerId)
                    {
                        upDownForThis = upDownEvent;
                    }
                    printEventToFile(reportedTimeStamp, reportedUpperBound, reportedLowerBound,
                            event.getX(p), event.getY(p), event.getPointerId(p), upDownForThis);

                }
            }

            if (syncServ.getAppState() >= SyncService.STATE_SYNCED_AND_READY
                    && syncServ.getAppState() <= SyncService.STATE_PLAYING
                    && syncServ.getStreamState())
            {
                for (int p = 0; p < pointerCount; p++) {
                    int upDownForThis = 0;
                    if (event.getPointerId(p) == upDownPointerId) {
                        upDownForThis = upDownEvent;
                    }
                    if (event.getEventTime() > previousStreamedTime[event.getPointerId(p)] + (SyncService.MILLIS_IN_SECOND / syncServ.getMaxMulticastFps())
                            || upDownForThis != 0)
                    {
                        previousStreamedTime[event.getPointerId(p)] = event.getEventTime();
                        long actualTime = (event.getEventTime() + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                        int reportedTimeStamp = (int)(
                                ((actualTime + syncServ.getOffsetNanosToServerStreaming())
                                        / SyncService.NANOS_IN_MILLI)
                                        - syncServ.getStreamingZeroTime());

                        if (upDownForThis == -1)
                        {
                            pressStartTime[event.getPointerId(p)] = reportedTimeStamp;
                        }

                        ArrayList<Object> data = new ArrayList<Object>();
                        data.add(reportedTimeStamp);
                        data.add(event.getPointerId(p));
                        data.add(upDownForThis);
                        data.add(event.getX(p));
                        data.add(event.getY(p));

                        Point po = new Point();
                        getWindowManager().getDefaultDisplay().getSize(po);
                        Integer screenWidth = po.x;
                        Integer screenHeight = po.y;

                        data.add(screenWidth);
                        data.add(screenHeight);

                        data.add(reportedTimeStamp - pressStartTime[event.getPointerId(p)]);

                        Boolean ok = syncServ.streamData(data);
                    }
                }
            }

        }

        private void printEventToFile(long reportedTimeStamp, long reportedUpperBound, long reportedLowerBound, float x, float y, int pointerId, int UpDown)
        {
            String output = reportedTimeStamp + ",";
            output = output + reportedUpperBound + "," + reportedLowerBound + ",";
            output = output + pointerId + ",";
            output = output + formatter.format(x);
            output = output + "," + formatter.format(y);
            output = output + "," + UpDown;
            try {
                outputStreamTouch.println(output);
                outputStreamTouch.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing touch to file.");
            }
        }

    }
}
