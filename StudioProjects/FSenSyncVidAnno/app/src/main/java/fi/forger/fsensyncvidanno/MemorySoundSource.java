/*
FSenSync Video Annotation
Copyright (C) 2018-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensyncvidanno;

import android.annotation.TargetApi;
import android.media.MediaDataSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by klaus on 2.6.2017.
 */

@TargetApi(23)
public class MemorySoundSource extends MediaDataSource
{

    private byte[] data;

    public MemorySoundSource(File file)
    {
        try {
            FileInputStream in = new FileInputStream(file);
            int fileSize = (int)file.length();
            data = new byte[fileSize];
            int readBytes = in.read(data, 0, fileSize);
            if (readBytes != fileSize)
            {
                System.out.println("The sound file was read successfully.");
            }
            else
            {
                System.out.println("The sound file was read successfully.");
            }
        }
        catch (IOException ex)
        {
            System.out.println("Could not read the sound file.");
        }
    }

    public long getSize()
    {
        return data.length;
    }

    public int readAt(long position, byte[] buffer, int offset, int size)
    {
        if (size == 0)
        {
            return 0;
        }

        if (data.length < position)
        {
            return -1;
        }

        int actualSize = size;

        if (data.length < position + size)
        {
            actualSize = data.length - (int)position;
        }

        try
        {
            System.arraycopy(data,
                    (int) position,
                    buffer,
                    offset,
                    actualSize);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return -1;
        }

        return actualSize;
    }

    public void close()
    {
        data = new byte[0];
    }
}

