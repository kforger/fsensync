/*
FSenSync Video Annotation
Copyright (C) 2018-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensyncvidanno;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import fi.forger.synclibrary.SyncService;

public class StartScreen extends AppCompatActivity {

    private static int PERMISSION_REQUEST = 1;
    private Button buttonId;
    private Button buttonTag;
    private Button buttonStartApp;
    private Timer myTimer;
    private Boolean permissionsGranted;
    private Boolean mainIsLaunched;
    private Boolean otherActivityLaunched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);

        permissionsGranted = false;
        mainIsLaunched = false;
        otherActivityLaunched = false;

        buttonTag = (Button) findViewById(R.id.buttonTag);
        buttonTag.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        otherActivityLaunched = true;
                        Intent i = new Intent(getApplicationContext(), fi.forger.remotecontrollibrary.AppTagActivity.class);
                        startActivity(i);
                    }
                });
            }
        });

        buttonId = (Button) findViewById(R.id.buttonId);
        buttonId.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        otherActivityLaunched = true;
                        Intent i = new Intent(getApplicationContext(), fi.forger.remotecontrollibrary.DeviceIdActivity.class);
                        startActivity(i);
                    }
                });
            }
        });

        buttonStartApp = (Button) findViewById(R.id.buttonStartApp);
        buttonStartApp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startMain();
                    }
                });
            }
        });

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST);
        }
        else
        {
            permissionsGranted = true;
        }

        myTimer = new Timer();
        myTimer.schedule(new StartTask(this), 3000,200);
    }

    public void onResume()
    {
        super.onResume();
        TextView idText = (TextView) findViewById(R.id.textDeviceId);
        idText.setText("Device ID: " + SyncService.loadDeviceId());
        if (!SyncService.appTag1.equals("")
                || !SyncService.appTag2.equals("")
                || !SyncService.appTag3.equals(""))
        {
            TextView tagText = (TextView) findViewById(R.id.textViewTags);
            String text = "Custom tags: \n";
            text = text + SyncService.appTag1 + "\n";
            text = text + SyncService.appTag2 + "\n";
            text = text + SyncService.appTag3;
            tagText.setText(text);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        int count = 0;
        for (int i = 0; i < grantResults.length; i++) {
            if (requestCode == PERMISSION_REQUEST) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    System.out.println("Required permission was not granted by user.");
                } else {
                    System.out.println("Permission granted");
                    count++;
                }
            }
        }
        if (count == 1)
        {
            permissionsGranted = true;
        }
    }

    private class StartTask extends TimerTask {
        StartScreen main;

        public StartTask(StartScreen main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (permissionsGranted && !mainIsLaunched && !otherActivityLaunched)
                    {
                        startMain();
                    }
                }
            });
            if (otherActivityLaunched || mainIsLaunched)
            {
                this.cancel();
            }
        }
    }

    public void startMain()
    {
        mainIsLaunched = true;
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        this.finish();
    }
}
