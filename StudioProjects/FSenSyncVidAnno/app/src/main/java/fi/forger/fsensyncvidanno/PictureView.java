/*
FSenSync Video Annotation
Copyright (C) 2018-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensyncvidanno;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import java.io.File;

/**
 * Created by klaus on 30.5.2017.
 */

public class PictureView extends View {

    private Paint paint;
    private Bitmap bitmap;
    private Boolean isBlack = true;

    public PictureView(Context context)
    {
        super(context);
        init();
    }

    public PictureView(Context context, AttributeSet attrs)
    {
        super( context, attrs);
        init();
    }

    public PictureView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super( context, attrs, defStyleAttr);
        init();
    }

    public void onSizeChanged (int w, int h, int oldW, int oldH)
    {

    }

    private void init()
    {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(0.0f);
    }

    public void setToBlack()
    {
        isBlack = true;
        invalidate();
    }

    public void loadBitmap(File path)
    {
        bitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
        isBlack = false;
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        if (!isBlack && bitmap != null)
        {
            canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                    new Rect(0, 0, canvas.getWidth(), canvas.getHeight()), paint);
        }
        else
        {
            canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), paint);
        }
    }

}
