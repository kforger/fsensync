/*
FSenSync Video Annotation
Copyright (C) 2018-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensyncvidanno;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by klaus on 30.5.2017.
 */

public class DrawView extends View implements View.OnTouchListener {

    private Paint paint;
    private int[] x;
    private int[] y;
    private long[] lastUpdateTime;
    public Boolean[] isDown;
    private ArrayList<Integer> traceX;
    private ArrayList<Integer> traceY;
    private float dpiMult;

    private int drawMode;
    public static final int DRAW_MODE_CIRCLE = 0;
    public static final int DRAW_MODE_TRACE = 1;

    public DrawView(Context context)
    {
        super(context);
        init();
    }

    public DrawView(Context context, AttributeSet attrs)
    {
        super( context, attrs);
        init();
    }

    public DrawView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super( context, attrs, defStyleAttr);
        init();
    }

    public void onSizeChanged (int w, int h, int oldW, int oldH)
    {

    }

    private void init()
    {
        dpiMult = getResources().getDisplayMetrics().density;

        traceX = new ArrayList<Integer>();
        traceY = new ArrayList<Integer>();
        drawMode = DRAW_MODE_CIRCLE;
        x = new int[10];
        y = new int[10];
        isDown = new Boolean[10];
        lastUpdateTime = new long[10];
        for (int i = 0; i < 10; i++)
        {
            isDown[i] = false;
            lastUpdateTime[i] = System.currentTimeMillis() - 1000000;
        }

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(0.0f);
    }

    public void updateCoordinates(int x, int y, int index)
    {
        lastUpdateTime[index] = System.currentTimeMillis();
        this.x[index] = x;
        this.y[index] = y;
        this.postInvalidate();
    }

    public void setDrawMode(int drawMode)
    {
        this.drawMode =  drawMode;
    }

    public int getDrawMode()
    {
        return drawMode;
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        long timeNow = System.currentTimeMillis();

        switch (drawMode)
        {
            case DRAW_MODE_CIRCLE:
                int size = (int)(30 * dpiMult);
                for (int i = 0; i < 10; i++) {
                    if (lastUpdateTime[i] > timeNow - 250) {
                        paint.setARGB(255, 255 - (int) (255 * ((i) / 10f)), 0, (int) (255 * (i / 10f)));
                        RectF rect = new RectF(x[i] - size, y[i] - size, x[i] + size, y[i] + size);
                        canvas.drawOval(rect, paint);
                    }
                }
                break;
            case DRAW_MODE_TRACE:
                paint.setARGB(255, 255, 0, 0);
                paint.setStrokeWidth(4.0f);
                if (lastUpdateTime[0] > timeNow - 2000)
                {
                    if (traceX.size() > 1)
                    {
                        if (traceX.get(traceX.size()-1).equals(x[0]) && traceY.get(traceY.size()-1).equals(y[0]))
                        {
                            // Add nothing, if the new point is same as the old
                        }
                        else
                        {
                            traceX.add(x[0]);
                            traceY.add(y[0]);
                        }
                    }
                    else
                    {
                        traceX.add(x[0]);
                        traceY.add(y[0]);
                    }
                }
                else
                {
                    traceX.clear();
                    traceY.clear();
                }
                for (int i = 1; i < traceX.size(); i++)
                {
                    canvas.drawLine((float)traceX.get(i-1), (float)traceY.get(i-1), (float)traceX.get(i), (float)traceY.get(i), paint);
                }
                if (traceX.size() > 0)
                {
                    RectF rect = new RectF(x[0] - 6, y[0] - 6, x[0] + 6, y[0] + 6);
                    canvas.drawOval(rect, paint);
                }

                break;
            default:
                break;
        }
    }

    public boolean onTouch(View v, MotionEvent event)
    {
        return true;
    }

    public boolean dispatchTouchEvent(MotionEvent event)
    {
        if (MainActivity.touchService != null)
        {
            MainActivity.touchService.processTouch(event);
        }
        return true;
    }

}
