/*
FSenSync Video Annotation
Copyright (C) 2018-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensyncvidanno;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.media.SyncParams;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import fi.forger.remotecontrollibrary.ControlledActivity;
import fi.forger.synclibrary.*;

public class MainActivity extends ControlledActivity implements SyncedClient, MediaPlayer.OnCompletionListener {

    private static final String clientType = "VANNO";
    private static final int majorVersion = BuildConfig.MAJOR_VERSION;

    private static PrintWriter outputStreamTouch;
    private static PrintWriter outputStreamStimulus;
    private static PrintWriter metaOutputStream;


    private static int STORAGE_PERMISSION_REQUEST = 1;

    private static SyncService syncServ;
    public static TouchService touchService;

    private static DrawView drawView;

    private static MediaPlayer mediaPlayer;
    private Timer myUiTimer;
    private static VideoSurfaceView videoSurface;
    private static PictureView pictureView;

    private TextView myTextView;
    private TextView textExperiment;
    private TextView textExtra;
    private TextView textExtra2;
    private TextView textTag1;
    private TextView textTag2;
    private TextView textTag3;
    private AnnotationTimeBar annoTimeBar;

    private SeekBar seekBar;
    private AppCompatImageButton playButton;
    private AppCompatImageButton pauseButton;
    private boolean startAfterSeek;
    private boolean mediaPlayedIsPrepared = false;
    private boolean currentlySeeking = false;
    private boolean madeSeek = false;
    private boolean userSeekingAllowed = true;
    private boolean showOldAnnotationTimes = true;

    private String videoFileName = "";
    private String imageFileName = "";

    private static SoundPool soundPool;
    private int soundInt;
    private int nativeSoundRate;

    private int currentPlayProgress;
    private int maxPlayProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setTryToHideActionBar(false);
        super.onCreate(savedInstanceState);
        startServices();

        // UI stuff
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textBox);
        textExperiment = (TextView) findViewById(R.id.textExperiment);
        textExtra = (TextView) findViewById(R.id.textExtra);
        textExtra2 = (TextView) findViewById(R.id.textExtra2);
        textTag1 = (TextView) findViewById(R.id.textTag1);
        textTag2 = (TextView) findViewById(R.id.textTag2);
        textTag3 = (TextView) findViewById(R.id.textTag3);
        drawView = (DrawView) findViewById(R.id.drawView);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        playButton = (AppCompatImageButton) findViewById(R.id.playButton);
        pauseButton = (AppCompatImageButton) findViewById(R.id.pauseButton);
        pictureView = (PictureView) findViewById(R.id.pictureView);
        annoTimeBar = (AnnotationTimeBar) findViewById(R.id.annoTimeBar);
        updateExperimentInfoToUi();
        myUiTimer = new Timer();
        myUiTimer.schedule(new UiUpdateTask(this), 1000,10);
        myUiTimer.schedule(new DrawTask(), 1000,50);
        startAfterSeek = false;


        seekBar.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (userSeekingAllowed) {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser && userSeekingAllowed) {
                    try {
                        mediaPlayer.seekTo(seekBar.getProgress());
                    } catch (Exception ex) {
                    }
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar)
            {
                if (mediaPlayer != null)
                {
                    currentlySeeking = true;
                    try
                    {
                        if (mediaPlayer.isPlaying()) {
                            startAfterSeek = true;
                        }
                        else
                        {
                            startAfterSeek = false;
                        }
                        pausePlayAndLog();
                    }
                    catch (Exception ex) {}
                }
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer != null)
                {
                    currentlySeeking = false;
                    try
                    {
                        if (startAfterSeek && mediaPlayer.isPlaying() == false)
                        {
                            startPlayAndLog();
                        }
                    }
                    catch (Exception ex) {}
                }
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mediaPlayer != null)
                {
                    try
                    {
                        startPlayAndLog();
                    }
                    catch (Exception ex) {}
                }
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mediaPlayer != null)
                {
                    try
                    {
                        pausePlayAndLog();
                    }
                    catch (Exception ex) {}
                }
            }
        });

        AudioManager audioManager = (AudioManager) this.getSystemService(getApplicationContext().AUDIO_SERVICE);
        nativeSoundRate = Integer.valueOf(audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE));
    }

    public void setTagsToUi()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (textTag1 != null && textTag2 != null && textTag3 != null) {
                    textTag1.setText(syncServ.getTag1());
                    textTag2.setText(syncServ.getTag2());
                    textTag3.setText(syncServ.getTag3());
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (readyToExit)
        {
            syncServ.sendClosingAppToServer();
            System.exit(0);
        }
    }

    private class DrawTask extends TimerTask {

        MainActivity main;

        public DrawTask() {

            }

        @Override
        public void run()
        {
            if (drawView != null)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        drawView.invalidate();

                        if (mediaPlayer != null && !mediaPlayer.isPlaying() && !currentlySeeking && madeSeek)
                        {
                            madeSeek = false;
                            mediaPlayer.start();
                            mediaPlayer.pause();
                        }

                        if (mediaPlayer != null && mediaPlayedIsPrepared)
                        {
                            maxPlayProgress = mediaPlayer.getDuration();
                            seekBar.setMax(mediaPlayer.getDuration());
                            seekBar.setProgress(getMediaPosition());
                        }
                    }
                });
            }
        }
    }

    public int getMediaPosition()
    {
        int pos = 0;
        if (mediaPlayer != null && mediaPlayedIsPrepared)
        {
            pos = mediaPlayer.getCurrentPosition();
        }
        return pos;
    }

    private class LogPlayTask extends TimerTask {

        MainActivity main;

        public LogPlayTask(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            if (mediaPlayer != null && mediaPlayedIsPrepared)
            {
                if (syncServ.getAppState() == SyncService.STATE_PLAYING) {
                    if (mediaPlayer.isPlaying()) {
                        logStimulusPlay("playing");
                    } else {
                        logStimulusPlay("paused");
                    }
                }
            }

            if (syncServ.getAppState() == SyncService.STATE_SYNCED_AND_READY)
            {
                this.cancel();
            }
        }
    }

    public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {

        boolean successfulStart = false;

        if (syncServ != null) {

            File file = new File(syncServ.getStimulusDirectory().getAbsolutePath() + File.separator + stimuliName);
            if (file.exists() && stimuliName.length() > 0) {

                Boolean ok = false;
                String errorMessage = "-";

                if (stimuliName.endsWith(".txt"))
                {
                    ok = true;
                    successfulStart = startAnnotation(file, stimuliName, desiredStartTime);
                }
                if (!ok)
                {
                    errorMessage = "Did not recognize stimulus file: " + stimuliName;
                    showExtraMessageOnScreen("");
                    syncServ.sendErrorToServer(errorMessage);
                }
            }
            else
            {
                showExtraMessageOnScreen("Did not have stimulus file: '" + stimuliName + "'");
                syncServ.sendErrorToServer("Did not have stimulus file: '" + stimuliName + "'");
            }
        }

        if (successfulStart)
        {
            long actualTime = SystemClock.elapsedRealtimeNanos();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pictureView.invalidate();
                    RelativeLayout layout = (RelativeLayout) findViewById(R.id.pictureLayout);
                    layout.bringToFront();
                    RelativeLayout layout2 = (RelativeLayout) findViewById(R.id.otherLayout);
                    layout2.setVisibility(View.INVISIBLE);
                    RelativeLayout layoutDraw = (RelativeLayout) findViewById(R.id.drawLayout);
                    layoutDraw.bringToFront();

                    try
                    {
                        Integer screenWidth = -1;
                        Integer screenHeight = -1;

                        if (drawView != null)
                        {
                            screenWidth = drawView.getWidth();
                            screenHeight = drawView.getHeight();
                        }
                        metaOutputStream.println("Touch width: " + screenWidth);
                        metaOutputStream.println("Touch height: " + screenHeight);
                        metaOutputStream.println("Video file: " + videoFileName);
                        metaOutputStream.println("Image file: " + imageFileName);
                        metaOutputStream.close();
                    }
                    catch (Exception ex)
                    {
                        System.out.println("Error when writing meta output file.");
                    }
                }
            });

            syncServ.setStatePlayingStimuli();

            logStimulusPlay("paused");

            myUiTimer.schedule(new LogPlayTask(this), 100, 100);
        }
        else
        {
            syncServ.delayedCancelAndStop();
        }
    }

    public boolean startAnnotation(File settingFile, String stimuliName, long desiredStartTime) {
        Boolean ok = true;
        String errorMessage = "-";
        File videoFile = null;
        File imageFile = null;
        int touchTraceSetting = 0;
        float videoAreaMult = 1.0f;
        Boolean ignoreSettings = false;
        Boolean seekingAllowed = true;
        Boolean showOldAnnoTimes = true;

        annoTimeBar.initialize();

        try {
            BufferedReader br = new BufferedReader(new FileReader(settingFile));
            String line = br.readLine();

            if (!line.startsWith("Video annotation settings:")) {
                ok = false;
                errorMessage = "Unrecognized first line in settings: " + stimuliName;
            }

            while ((line = br.readLine()) != null && ok)
            {
                boolean lineProcessed = false;

                if (line.startsWith("Required tag 1:"))
                {
                    String rTag = line.replace("Required tag 1:", "").trim();
                    if (syncServ.getTag1().trim().equals(rTag))
                    {
                        ignoreSettings = false;
                    }
                    else
                    {
                        ignoreSettings = true;
                    }
                    lineProcessed = true;
                }

                if (!ignoreSettings && ok) {

                    if (line.startsWith("Video:")) {
                        String v = line.replace("Video:", "").trim();
                        videoFile = new File(syncServ.getStimulusDirectory().getAbsolutePath() + File.separator + v);
                        if (!videoFile.exists()) {
                            ok = false;
                            errorMessage = "Error in reading video file: " + v;
                        }
                        lineProcessed = true;
                    }

                    if (line.startsWith("Seeking video allowed:")) {
                        String v = line.replace("Seeking video allowed:", "").trim();
                        boolean booleanFound = false;
                        if (v.toLowerCase().equals("true") || v.toLowerCase().equals("yes"))
                        {
                            booleanFound = true;
                            seekingAllowed = true;
                        }
                        if (v.toLowerCase().equals("false") || v.toLowerCase().equals("no"))
                        {
                            booleanFound = true;
                            seekingAllowed = false;
                        }
                        if (!booleanFound) {
                            ok = false;
                            errorMessage = "Error in reading annotation setting: " + v;
                        }
                        lineProcessed = true;
                    }

                    if (line.startsWith("Show annotation times:")) {
                        String v = line.replace("Show annotation times:", "").trim();
                        boolean booleanFound = false;
                        if (v.toLowerCase().equals("true") || v.toLowerCase().equals("yes"))
                        {
                            booleanFound = true;
                            showOldAnnoTimes = true;
                        }
                        if (v.toLowerCase().equals("false") || v.toLowerCase().equals("no"))
                        {
                            booleanFound = true;
                            showOldAnnoTimes = false;
                        }
                        if (!booleanFound) {
                            ok = false;
                            errorMessage = "Error in reading video seeking setting: " + v;
                        }
                        lineProcessed = true;
                    }

                    if (line.startsWith("Annotation time:")) {
                        String v[] = line.replace("Annotation time:", "").split("-");
                        if (v.length == 2) {
                            String[] time0 = v[0].trim().split(":");
                            String[] time1 = v[1].trim().split(":");
                            if (time0.length != 3 || time1.length != 3 ) {
                                ok = false;
                                errorMessage = "Error in reading time setting: " + line;
                            }
                            else
                            {
                                try
                                {
                                    int t0 = (Integer.parseInt(time0[0]) * 60 * 60) + (Integer.parseInt(time0[1]) * 60) + Integer.parseInt(time0[2]);
                                    int t1 = (Integer.parseInt(time1[0]) * 60 * 60) + (Integer.parseInt(time1[1]) * 60) + Integer.parseInt(time1[2]);
                                    annoTimeBar.addStartEnd(t0 * 1000, t1 * 1000);
                                }
                                catch (NumberFormatException ex)
                                {
                                    ok = false;
                                    errorMessage = "Error in reading time setting: " + line;
                                }
                            }
                        }
                        else
                        {
                            ok = false;
                            errorMessage = "Error in reading time setting: " + line;
                        }

                        lineProcessed = true;
                    }

                    if (line.startsWith("Background image:")) {
                        String v = line.replace("Background image:", "").trim();
                        imageFile = new File(syncServ.getStimulusDirectory().getAbsolutePath() + File.separator + v);
                        if (!imageFile.exists()) {
                            ok = false;
                            errorMessage = "Error in reading image file: " + v;
                        }
                        lineProcessed = true;
                    }

                    if (line.startsWith("Touch trace setting:")) {
                        String v = line.replace("Touch trace setting:", "").trim();
                        try {
                            touchTraceSetting = Integer.parseInt(v);
                        } catch (NumberFormatException ex) {
                            ok = false;
                            errorMessage = "Error in touch trace setting";
                        }
                        lineProcessed = true;
                    }

                    if (line.startsWith("Video area multiplier:")) {
                        String v = line.replace("Video area multiplier:", "").trim();
                        v = v .replace(",", ".");
                        try {
                            videoAreaMult = Float.parseFloat(v);
                        } catch (NumberFormatException ex) {

                            try
                            {
                                // This should not be needed, but some Android devices seem to have
                                // strange locale behaviours
                                v = v .replace(".", ",");
                                videoAreaMult = Float.parseFloat(v);
                            }
                            catch (NumberFormatException ex2)
                            {
                                ok = false;
                                errorMessage = "Error in video area multiplier setting";
                            }
                        }
                        lineProcessed = true;
                    }

                    if (!lineProcessed && line.trim().length() > 1 && ok)
                    {
                        ok = false;
                        errorMessage = "Unrecognised line in settings: " + line;
                    }
                }

            }

        } catch (Exception ex) {
            ok = false;
            errorMessage = "Error in reading file: " + stimuliName;
        }

        userSeekingAllowed = seekingAllowed;
        showOldAnnotationTimes = showOldAnnoTimes;


        if (ok) {

            drawView.setDrawMode(touchTraceSetting);

            final float vidFloat = videoAreaMult;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (showOldAnnotationTimes)
                    {
                        annoTimeBar.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        annoTimeBar.setVisibility(View.INVISIBLE);
                    }

                    RelativeLayout videoLayout = (RelativeLayout) findViewById(R.id.videoLayout);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            0,
                            vidFloat * 5.0f
                    );
                    videoLayout.setLayoutParams(params);
                }
            });

            if (videoFile != null &&
                    (videoFile.getName().endsWith(".mp4") ||
                            videoFile.getName().endsWith(".3gp") ||
                            videoFile.getName().endsWith(".webm") ||
                            videoFile.getName().endsWith(".mkv")
                    )) {

                Uri uriVideo = Uri.fromFile(videoFile);
                initMediaPlayer(uriVideo);
            }
            else {
                ok = false;
                if (videoFile != null) {
                    errorMessage = "Error in reading video file: " + videoFile.getName();
                }
                else
                {
                    errorMessage = "Error: A video file matching the tag of the app was not given.";
                }
            }


            if (imageFile != null &&
                    (imageFile.getName().endsWith(".bmp") ||
                            imageFile.getName().endsWith(".gif") ||
                            imageFile.getName().endsWith(".jpg") ||
                            imageFile.getName().endsWith(".png")
                    )) {
                pictureView.loadBitmap(imageFile);
            }
            else
            {
                if (videoFile != null) {
                    errorMessage = "Error in reading image file: " + imageFile.getName();
                }
                else
                {
                    errorMessage = "Error: Image file not given to the app.";
                }

                ok = false;
            }

            if (ok)
            {
                videoFileName = videoFile.getName();
                imageFileName = imageFile.getName();
            }
            else
            {
                showExtraMessageOnScreen(errorMessage);
                syncServ.sendErrorToServer(errorMessage);
            }

        }
        else
        {
            showExtraMessageOnScreen(errorMessage);
            syncServ.sendErrorToServer(errorMessage);
        }

        if (!ok)
        {
            final RelativeLayout videoLayout = (RelativeLayout) findViewById(R.id.videoLayout);
            final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    0,
                    4.0f
            );
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    videoLayout.setLayoutParams(params);
                }
            });
        }

        return ok;
    }

    private void initMediaPlayer(Uri uri) {

        mediaPlayedIsPrepared = false;

        mediaPlayer = new MediaPlayer();
        videoSurface = (VideoSurfaceView) findViewById(R.id.surfaceView);
        mediaPlayer.setSurface(videoSurface.getHolder().getSurface());
        mediaPlayer.setOnCompletionListener(this);

        mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                madeSeek = true;
            }
        });

        if (uri != null)
        {
            if (uri.toString().endsWith(".mp4") ||
                    uri.toString().endsWith(".3gp") ||
                    uri.toString().endsWith(".webm") ||
                    uri.toString().endsWith(".mkv")) {
                MediaPlayer.OnVideoSizeChangedListener listenerVideoSize = new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        videoSurface.aspectRatioFitting(mp, width, height, MainActivity.this);
                    }
                };
                mediaPlayer.setOnVideoSizeChangedListener(listenerVideoSize);
            }
        }
        try {
            if (uri != null) {
                mediaPlayer.setDataSource(getApplicationContext(), uri);
            }

            mediaPlayer.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            if (android.os.Build.VERSION.SDK_INT >= 23) {
                SyncParams p = mediaPlayer.getSyncParams();
                p.setSyncSource(SyncParams.SYNC_SOURCE_SYSTEM_CLOCK);
                p.setTolerance(0.044f);
                mediaPlayer.setSyncParams(p);
                System.out.println("Sync source: " + p.getSyncSource());
                System.out.println("Tolerance: " + p.getTolerance());
            }

            mediaPlayer.prepare(); // might take long! (for buffering, etc)
            mediaPlayer.start(); // Start+pause is required for making the seekBar work before pressing play button.
            mediaPlayer.pause();
            mediaPlayer.seekTo(0);
            mediaPlayedIsPrepared = true;
            if (annoTimeBar != null)
            {
                annoTimeBar.setVideoStartAndEndTimes(0, mediaPlayer.getDuration());
                annoTimeBar.postInvalidate();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.videoLayout);
                layout.setVisibility(View.VISIBLE);
            }
        });

//        if (android.os.Build.VERSION.SDK_INT > 21)
//        {
//            this.startLockTask();
//        }
    }

    public String getTimeAndBounds(long actualTime)
    {
        long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
        long reportedUpperBound = syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
        long reportedLowerBound = syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;
        return reportedTimeStamp + "," + reportedUpperBound + "," + reportedLowerBound;
    }

    public void startPlayAndLog()
    {
        if (mediaPlayer != null && mediaPlayedIsPrepared && !mediaPlayer.isPlaying()) {
            logStimulusPlay("paused");
            mediaPlayer.start();
            myUiTimer.schedule(new LogPlayTask(this), 5);
            myUiTimer.schedule(new LogPlayTask(this), 50);
        }
    }

    public void pausePlayAndLog()
    {
        if (mediaPlayer != null && mediaPlayedIsPrepared) {
            mediaPlayer.pause();
            logStimulusPlay("paused");
        }
    }

    public void logStimulusPlay(String state)
    {
        if (mediaPlayer != null && mediaPlayedIsPrepared) {
            long actualTime = SystemClock.elapsedRealtimeNanos();
            int progress = getMediaPosition();
            currentPlayProgress = progress;
            String output = getTimeAndBounds(actualTime) + "," + state + "," + progress;
            try {
                outputStreamStimulus.println(output);
                outputStreamStimulus.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to stimulus log file.");
            }
        }
    }

    public void onCompletion(MediaPlayer mp)
    {
        System.out.println("Reached end of stimulus");
        logStimulusPlay("paused");
    }

    public void cancelStimuli() {
        System.out.println("Cancelling stimuli");
        if (mediaPlayer != null) {

            logStimulusPlay("paused");
            mediaPlayedIsPrepared = false;
            seekBar.setProgress(0);

            mediaPlayer.stop();
            mediaPlayer.reset();

//            if (android.os.Build.VERSION.SDK_INT > 21)
//            {
//                this.stopLockTask();
//            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    RelativeLayout layout = (RelativeLayout) findViewById(R.id.otherLayout);
                    layout.setVisibility(View.VISIBLE);
                    layout.bringToFront();
                    RelativeLayout layoutDraw = (RelativeLayout) findViewById(R.id.drawLayout);
                    layoutDraw.bringToFront();
                    RelativeLayout vidLayout = (RelativeLayout) findViewById(R.id.videoLayout);
                    vidLayout.setVisibility(View.INVISIBLE);

                    RelativeLayout videoLayout = (RelativeLayout) findViewById(R.id.videoLayout);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            0,
                            4.0f
                    );
                    videoLayout.setLayoutParams(params);

                    annoTimeBar.setVisibility(View.VISIBLE);
                    annoTimeBar.initialize();

                }
            });
        }

        syncServ.delayedCancelAndStop();
    }

    private void startServices() {
        if (syncServ == null || syncServ.getAppState() == SyncService.STATE_ERROR) {
            syncServ = new SyncService(this, this, clientType, majorVersion);
            if (touchService == null && syncServ.getAppState() > SyncService.STATE_ERROR) {
                touchService = new TouchService(this);
            }
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_REQUEST);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_REQUEST && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                System.out.println("Required permission was not granted by user.");
            } else {
                System.out.println("Permission granted");
                startServices();
            }
        }
    }

    public void updateExperimentInfoToUi()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (syncServ.getAppState() < SyncService.STATE_FIRST_SYNC) {
                    textExperiment.setText("Device ID: " + syncServ.getDeviceId());
                }
                else
                {
                    String ph = syncServ.getDeviceId();
                    if (ph.equals("-")) {
                        textExperiment.setText("App ID: " + syncServ.getMyShortId());
                    }
                    else
                    {
                        textExperiment.setText("App ID: " + syncServ.getMyShortId() + " (" + syncServ.getDeviceId() + ")");
                    }
                }
            }
        });
    }

    public void showExtraMessageOnScreen(final String message)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textExtra.setText(message);
            }
        });
    }

    public void enableScreenTouchSound()
    {
        disableScreenTouchSound();
        File file = new File(syncServ.getStimulusDirectory().getAbsolutePath() + File.separator + "screen_touch_sound_file");
        if (file.exists())
        {
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
            soundInt = soundPool.load(file.getAbsolutePath(), 1);
        }
    }

    public void disableScreenTouchSound()
    {
        if (soundPool != null)
        {
            soundPool.unload(soundInt);
        }
        soundPool = null;
    }

    private class UiUpdateTask extends TimerTask {
        MainActivity main;

        public UiUpdateTask(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String s = syncServ.getStateString();
                    if (syncServ.getAppState() > SyncService.STATE_FIRST_SYNC) {
                        String time = "\n" + (SystemClock.elapsedRealtimeNanos() + syncServ.getOffsetNanosToServer());
                        s = s + time.substring(0, Math.max(0, time.length() - 6));
                    }
                    myTextView.setText(s);

                    Integer screenWidth = -1;
                    Integer screenHeight = -1;

                    if (drawView != null)
                    {
                        screenWidth = drawView.getWidth();
                        screenHeight = drawView.getHeight();
                    }

                    if (android.os.Build.VERSION.SDK_INT >= 23) {
                        textExtra2.setText("Touch size: " + screenWidth + "x" + screenHeight + ", Sound rate: " + nativeSoundRate + "Hz");
                    }
                    else
                    {
                        textExtra2.setText("Touch size: " + screenWidth + "x" + screenHeight + ", Sounds not supported");
                    }
                }
            });

        }
    }

    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream) {
        Boolean ok = true;

        this.metaOutputStream = metaOutputStream;

        try
        {
            metaOutputStream.println("App version: " + BuildConfig.VERSION_CODE);
            metaOutputStream.println("Native sound rate: " + nativeSoundRate);
        }
        catch (Exception ex)
        {
            System.out.println("Error when writing meta output file.");
        }

        try {
            File fileTouch = new File(fileBaseName + "TOUCH.csv");
            File fileStimulus = new File(fileBaseName + "STI.csv");
            if (fileTouch.exists() || fileStimulus.exists()) {
                syncServ.sendErrorToServer("Refused to overwrite old recording with number " + recordingNumber);
                return false;
            }
            fileTouch.createNewFile();
            fileStimulus.createNewFile();
            outputStreamTouch = new PrintWriter(fileTouch, "UTF-8");
            try {
                String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,TouchIndex,Xcoordinate,Ycoordinate,UpDownEvent";
                outputStreamTouch.println(header);
                outputStreamTouch.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to file.");
            }
            outputStreamStimulus = new PrintWriter(fileStimulus, "UTF-8");
            try {
                String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,EventType,PlayProgress(milliseconds)";
                outputStreamStimulus.println(header);
                outputStreamStimulus.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing to file.");
            }
        } catch (Exception ex) {
            ok = false;
            System.out.println("Error when opening output file.");
        }
        return ok;
    }

    public Boolean stopRecording() {
        outputStreamTouch.close();
        outputStreamStimulus.close();
        return true;
    }

    public void setAppSetting(String setting, String value)
    {
        switch (setting)
        {
            case "DRAW_MODE":
                try
                {
                    int mode = Integer.valueOf(value);
                    if (mode >= 0 && mode <= 2) {
                        drawView.setDrawMode(Integer.valueOf(value));
                    }
                    else
                    {
                        syncServ.sendErrorToServer("Bad draw mode value: " + value);
                    }
                }
                catch (Exception ex)
                {
                    syncServ.sendErrorToServer("Bad draw mode value: " + value);
                }
                break;
            default:
                break;
        }
    }

    public String getAppSetting(String setting)
    {
        switch (setting)
        {
            case "DRAW_MODE":
                return "" + drawView.getDrawMode();
            default:
                return "INVALID_SETTING";
        }
    }

    public Boolean getRecordingServiceReady() {
        return touchService.touchListeningReady;
    }

    public void sensorStateChanged() {
        syncServ.sensorStateChanged();
    }



    public class TouchService {
        public Boolean touchListeningReady;
        private NumberFormat formatter;
        private MainActivity main;
        private long diffNanosToUpTime;
        private long previousStreamedTime[];
        private int pressStartTime[];

        public TouchService(MainActivity main) {
            this.main = main;
            formatter = NumberFormat.getInstance(Locale.ROOT);
            formatter.setGroupingUsed(false);
            formatter.setMinimumIntegerDigits(1);
            formatter.setMinimumFractionDigits(1);
            touchListeningReady = true;

            previousStreamedTime = new long[30];
            pressStartTime = new int[30];
            for (int i = 0; i < 30; i++)
            {
                previousStreamedTime[i] = Long.MIN_VALUE;
                pressStartTime[i] = Integer.MIN_VALUE;
            }

            diffNanosToUpTime = 0l;
            for (int i = 0; i < 10; i++)
            {
                long t0 = SystemClock.elapsedRealtimeNanos() / SyncService.NANOS_IN_MILLI;
                long t1 = SystemClock.uptimeMillis();
                long t2 = SystemClock.elapsedRealtimeNanos() / SyncService.NANOS_IN_MILLI;
                diffNanosToUpTime += ((t0 + t2) / 2l) - t1;
            }
            diffNanosToUpTime = diffNanosToUpTime / 10l;
        }

        public void processTouch(MotionEvent event)
        {
            long reportedUpperBound = syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
            long reportedLowerBound = syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;

            int pointerCount = event.getPointerCount();

            int upDownEvent = 0;
            int upDownPointerId = -1;

            if (event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN || event.getAction() == MotionEvent.ACTION_DOWN)
            {
                upDownEvent = -1;
                upDownPointerId = event.getPointerId(event.getActionIndex());
                int index = event.getPointerId(event.getActionIndex());
                if (index < drawView.isDown.length)
                {
                    drawView.isDown[index] = true;
                }
            }

            if (event.getActionMasked() == MotionEvent.ACTION_POINTER_UP || event.getAction() == MotionEvent.ACTION_UP)
            {
                upDownEvent = 1;
                upDownPointerId = event.getPointerId(event.getActionIndex());
                int index = event.getPointerId(event.getActionIndex());
                if (index < drawView.isDown.length)
                {
                    drawView.isDown[index] = false;
                }

            }

            if (syncServ.getAppState() >= SyncService.STATE_RECORDING_DATA && syncServ.getAppState() <= SyncService.STATE_PLAYING)
            {
                for (int p = 0; p < pointerCount; p++)
                {
                    for (int i = 0; i < event.getHistorySize(); i++) {
                        long actualTime = (event.getHistoricalEventTime(i) + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                        long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
                        printEventToFile(reportedTimeStamp, reportedUpperBound, reportedLowerBound,
                                event.getHistoricalX(p, i), event.getHistoricalY(p, i), event.getPointerId(p), 0);
                    }

                    long actualTime = (event.getEventTime() + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                    long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
                    int upDownForThis = 0;
                    if (event.getPointerId(p) == upDownPointerId)
                    {
                        upDownForThis = upDownEvent;
                    }
                    printEventToFile(reportedTimeStamp, reportedUpperBound, reportedLowerBound,
                            event.getX(p), event.getY(p), event.getPointerId(p), upDownForThis);
                }
            }

            if (syncServ.getAppState() >= SyncService.STATE_SYNCED_AND_READY
                    && syncServ.getAppState() <= SyncService.STATE_PLAYING
                    && syncServ.getStreamState())
            {
                for (int p = 0; p < pointerCount; p++) {
                    int upDownForThis = 0;
                    if (event.getPointerId(p) == upDownPointerId) {
                        upDownForThis = upDownEvent;
                    }
                    if (event.getEventTime() > previousStreamedTime[event.getPointerId(p)] + (SyncService.MILLIS_IN_SECOND / syncServ.getMaxMulticastFps())
                            || upDownForThis != 0)
                    {
                        previousStreamedTime[event.getPointerId(p)] = event.getEventTime();
                        long actualTime = (event.getEventTime() + diffNanosToUpTime) * SyncService.NANOS_IN_MILLI;
                        int reportedTimeStamp = (int)(
                                ((actualTime + syncServ.getOffsetNanosToServerStreaming())
                                        / SyncService.NANOS_IN_MILLI)
                                        - syncServ.getStreamingZeroTime());

                        if (upDownForThis == -1)
                        {
                            pressStartTime[event.getPointerId(p)] = reportedTimeStamp;
                        }

                        ArrayList<Object> data = new ArrayList<Object>();
                        data.add(reportedTimeStamp);
                        data.add(event.getPointerId(p));
                        data.add(upDownForThis);
                        data.add(event.getX(p));
                        data.add(event.getY(p));

                        Point po = new Point();
                        getWindowManager().getDefaultDisplay().getSize(po);
                        Integer screenWidth = po.x;
                        Integer screenHeight = po.y;

                        data.add(screenWidth);
                        data.add(screenHeight);

                        data.add(reportedTimeStamp - pressStartTime[event.getPointerId(p)]);

                        Boolean ok = syncServ.streamData(data);
                    }
                }
            }

            if (soundPool != null && event.getActionMasked() == MotionEvent.ACTION_DOWN)
            {
                main.soundPool.play(main.soundInt, 1.0f, 1.0f, 10, 0, 1.0f);
            }

            if (drawView != null)
            {
                for (int p = 0; p < pointerCount; p++)
                {
                    drawView.updateCoordinates((int) event.getX(p), (int) event.getY(p), event.getPointerId(p) % 10);
                }
            }
        }

        private void printEventToFile(long reportedTimeStamp, long reportedUpperBound, long reportedLowerBound, float x, float y, int pointerId, int UpDown)
        {
            String output = reportedTimeStamp + ",";
            output = output + reportedUpperBound + "," + reportedLowerBound + ",";
            output = output + pointerId + ",";
            output = output + formatter.format(x);
            output = output + "," + formatter.format(y);
            output = output + "," + UpDown;
            try {
                outputStreamTouch.println(output);
                outputStreamTouch.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing touch to file.");
            }

            if (annoTimeBar != null && showOldAnnotationTimes)
            {
                annoTimeBar.addTime(currentPlayProgress, maxPlayProgress);
            }
        }

    }

    public boolean dispatchTouchEvent(MotionEvent event)
    {
        return super.dispatchTouchEvent(event);
    }
}
