/*
FSenSync Video Annotation
Copyright (C) 2018-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensyncvidanno;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.SurfaceView;
import android.view.ViewGroup;

/**
 * Created by klaus on 19.5.2017.
 */

public class VideoSurfaceView extends SurfaceView {

    public VideoSurfaceView(Context context)
    {
        super(context);
    }

    public VideoSurfaceView(Context context, AttributeSet attrs)
    {
        super( context, attrs);
    }

    public VideoSurfaceView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super( context, attrs, defStyleAttr);
    }

    public VideoSurfaceView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super( context, attrs, defStyleAttr, defStyleRes);
    }

    public void aspectRatioFitting(MediaPlayer mp, int videoWidth, int videoHeight, Activity activity)
    {
        if(mp != null)
        {
            Point p = new Point();
            activity.getWindowManager().getDefaultDisplay().getSize(p);
            Integer screenWidth = p.x;
            Integer screenHeight = p.y;
            ViewGroup.LayoutParams videoParams = getLayoutParams();

            if (videoWidth > videoHeight)
            {
                videoParams.width = screenWidth;
                videoParams.height = screenWidth * videoHeight / videoWidth;
            }
            else
            {
                videoParams.width = screenHeight * videoWidth / videoHeight;
                videoParams.height = screenHeight;
            }

            setLayoutParams(videoParams);
        }
    }
}
