package fi.forger.fsensyncvidanno;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

public class AnnotationTimeBar extends View {

    private Paint paint;
    private ArrayList<Integer> times;
    private ArrayList<StartEnd> desiredTimes;
    private int startTime;
    private int endTime;
    private int lastTime;
    private float annotationVisibleLength = 0.0075f;

    public AnnotationTimeBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public void setVideoStartAndEndTimes(int startTime, int endTime)
    {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    private void init(AttributeSet attrs) {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
    }

    public void initialize()
    {
        times = new ArrayList<Integer>();
        desiredTimes = new ArrayList<StartEnd>();
        startTime = 0;
        endTime = 0;
        lastTime = -1;
        super.postInvalidate();
    }

    public void addTime(int input, int maxTime)
    {
        if (input != lastTime && times != null)
        {
            lastTime = input;

            // TODO: remove some entries if the ArrayList gets large

            times.add(input);
            endTime = maxTime;
            super.postInvalidate();
        }
    }

    public void addStartEnd(int start, int end)
    {
        if (desiredTimes != null)
        {
            desiredTimes.add(new StartEnd(start, end));
        }
        super.postInvalidate();
    }

    @Override
    public void onDraw(Canvas canvas) {

        if (times != null && desiredTimes != null)
        {
            float y = 0f;
            float xMax = canvas.getWidth();

            y = canvas.getHeight() * 0.3f;
            paint.setARGB(255, 255, 0, 0);
            paint.setStrokeWidth(6.0f);
            for (int i = 0; i < desiredTimes.size(); i++)
            {
                float x1 = (((desiredTimes.get(i).startMilli - startTime) / (float)endTime) * xMax);
                float x2 = (((desiredTimes.get(i).endMilli - startTime) / (float)endTime) * xMax);
                float y1 = y;
                float y2 = y;
                canvas.drawLine(x1, y1, x2, y2, paint);
            }

            y = canvas.getHeight() * 0.7f;
            paint.setARGB(255, 0, 255, 255);
            paint.setStrokeWidth(6.0f);
            for (int i = 0; i < times.size(); i++)
            {
                float x1 = (((times.get(i) - startTime) / (float)endTime) * xMax) - (xMax*annotationVisibleLength);
                float x2 = (((times.get(i) - startTime) / (float)endTime) * xMax) + (xMax*annotationVisibleLength);
                float y1 = y;
                float y2 = y;
                canvas.drawLine(x1, y1, x2, y2, paint);
            }


        }

    }

    private class StartEnd
    {
        public int startMilli;
        public int endMilli;

        public StartEnd(int startMilli, int endMilli)
        {
            this.startMilli = startMilli;
            this.endMilli = endMilli;
        }
    }

}
