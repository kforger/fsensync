/*
FSenSync Common Libraries
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package fi.forger.synclibrary;


import java.io.PrintWriter;

public interface SyncedClient
{
    public Boolean getIsOnBackGround();
    public Boolean getRecordingServiceReady();
    public void setTagsToUi();
    public void updateExperimentInfoToUi();
    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream);
    public Boolean stopRecording();
    public void newSyncState(int syncState, int oldState, String stateString);
    public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime);
    public void cancelStimuli();
    public void showExtraMessageOnScreen(String message);
    public void enableScreenTouchSound();
    public void disableScreenTouchSound();
    public String getAppSetting(String setting);
    public void setAppSetting(String setting, String value);
    public void closeApp();
    public boolean doesPostSync();
    public int[] giveSensorHealth();
}
