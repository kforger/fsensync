/*
FSenSync Common Libraries
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package fi.forger.synclibrary;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.StrictMode;

public class RecordingService extends Service
{

    public static final String MSG_START = "startAction";
    public static final String MSG_STOP = "stopAction";

    public static SyncedClient userActivity;
    public static SyncService syncServ;

    public static Boolean serviceIsRunning = false;

    @Override
    public void onCreate() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        serviceIsRunning = true;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    public void setTagsToUi() {}

    public void updateExperimentInfoToUi() {}
    public void newSyncState(int syncState, int oldState, String syncStateString) {}
    public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {}
    public void cancelStimuli() {}
    public void showExtraMessageOnScreen(String message) {}
    public void enableScreenTouchSound() {}
    public void disableScreenTouchSound() {}
    public String getAppSetting(String setting)
    {
        return "INVALID_SETTING";
    }
    public void setAppSetting(String setting, String value) {}
    public Boolean getIsScreenDim()
    {
        return false;
    }
    public Boolean getIsOnBackGround()
    {
        return true;
    }
    public void setScreenTo(Boolean dim) {}
    public void closeApp()
    {
        if (syncServ != null)
        {
            syncServ.sendClosingAppToServer();
        }
        if (userActivity != null)
        {
            userActivity.closeApp();
        }
        {
            serviceIsRunning = false;
            stopForeground(true);
            userActivity = null;
            stopSelf();
            System.exit(0);
        }
    };
    public boolean doesPostSync()
    {
        return false;
    }
    public int[] giveSensorHealth() { return new int[0]; }
}
