/*
FSenSync Common Libraries
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package fi.forger.synclibrary;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;

import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortIn;
import com.illposed.osc.OSCPortOut;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.regex.Pattern;

import static android.content.Context.POWER_SERVICE;


public class SyncService
{

    public static final long NANOS_IN_MILLI = 1000000L;
    public static final long NANOS_IN_MICRO = 1000L;
    public static final long MICROS_IN_MILLI = 1000L;
    public static final long NANOS_IN_SECOND = 1000000000L;
    public static final long MILLIS_IN_SECOND = 1000L;
    public static final int STATE_CLOSING = 0;
    public static final int STATE_ERROR = 1;
    public static final int STATE_FIRST_START = 2;
    public static final int STATE_WAITING_FOR_WIFI = 3;
    public static final int STATE_FINDING_SERVER = 4;
    public static final int STATE_FIRST_SYNC = 5;
    public static final int STATE_SYNCED_AND_WAITING_FOR_SENSORS = 6;
    public static final int STATE_SYNCED_AND_READY = 7;
    public static final int STATE_RECORDING_DATA = 8;
    public static final int STATE_PREPARING_PLAY = 9;
    public static final int STATE_PLAYING = 10;
    public static final int STATE_FILE_TRANSFER = 11;
    public static final int STATE_POST_SYNC = 12;

    public static final int SENSOR_HEALTH_INITIAL = 0;
    public static final int SENSOR_HEALTH_BAD = 1;
    public static final int SENSOR_HEALTH_GOOD = 2;

    private static final String[] stateStrings = {"Closing", "Error", "Starting", "Waiting for Wifi", "Finding server",
            "First sync", "Synced, waiting for sensors", "Ready", "Recording", "Preparing play", "Playing", "File transfer",
            "Post sync"};
    private static final int ADDRESS_ALTERNATIVES = 10;
    private static final int DEFAULT_UNICAST_PORT = 11101;
    private static final int DEFAULT_TCP_PORT = 11201;

    private static PowerManager.WakeLock wakeLock;
    private static WifiManager.MulticastLock wifiMulticastLock;
    private static WifiManager.WifiLock wifiLock;
    public static String appTag1 = "";
    public static String appTag2 = "";
    public static String appTag3 = "";
    private int appState;

    private String serverIp;
    private String myIp;
    private Timer myNetworkTimer;
    private String uniqueId;
    private int syncMessageCount;
    private long offsetNanosToServer;
    private long offsetNanosToServerStreaming;
    private Boolean offsetNanosToServerHasBeenUpdatedOnce;
    private long errorNanosUpperBound;
    private long timeOfLastDriftIncrement;
    private long errorNanosLowerBound;
    private String broadcastAddress;
    private String previousExperimentName;
    private int previousRecordingNumber;
    private String tag1;
    private String tag2;
    private String tag3;
    private int myShortId;
    private String clientType;
    private String errorDescription;
    private String deviceId;
    private String longDeviceId ;
    private Boolean usesAppTags ;
    private Boolean hasReceivedTags;
    private int selectedPortOffset;
    private Boolean workingForOtherServer = false;
    private int recordingServerId;
    private int timeServerId;
    private int versionNumber;

    private boolean isStreaming;
    private String multicastIp;
    private int multicastPort;
    private int maxMulticastFps;
    private int multicastPacketNumber;
    private HashMap<String, Integer> multicastPacketNumberDict;
    private OSCPortOut multicastSender;
    private long streamingZeroTime;
    private int knownSensorLag;

    private SyncedClient client;
    private ContextWrapper activity;

    private OSCPortIn portUnicast;
    private OSCPortIn portBroadcast;
    private Thread fileTransferThread;
    private ServerSocket fileTransferSocket;
    private Boolean closeServices = false;

    private ArrayList<SyncMessageInfo> syncMessagesSent;
    private ArrayList<Double> pingTimeList;

    private class SyncMessageInfo
    {
        public long sentTime;
        public Boolean replied = false;

        public SyncMessageInfo(long sentTime)
        {
            this.sentTime = sentTime;
        }
    }

    private synchronized int processSyncMessagesSent(SyncMessageInfo message, long replyTime, Boolean onlyCountAmount)
    {
        // These actions needs to be synchronized as items could be are deleted and read in parallel

        if (onlyCountAmount)
        {
            int amountReplied = 0;
            int amountTotal = 0;
            long timeNow = SystemClock.elapsedRealtimeNanos();
            for (int i = 0; i < syncMessagesSent.size(); i++)
            {
                if (amountTotal <= 100 && syncMessagesSent.get(i).sentTime < timeNow - (2000 * NANOS_IN_MILLI))
                {
                    amountTotal += 1;
                    if (syncMessagesSent.get(i).replied) {
                        amountReplied += 1;
                    }
                }
            }
            return (int)((1.0 - ((amountReplied) / (double)amountTotal)) * 100.0);
        }

        if (message != null)
        {
            syncMessagesSent.add(message);
            while (syncMessagesSent.size() > 101)
            {
                syncMessagesSent.remove(0);
            }
        }
        else
        {
            int index = syncMessagesSent.size();
            Boolean replyFound = false;
            while (index > 0 && !replyFound)
            {
                index -= 1;
                if (!syncMessagesSent.get(index).replied && syncMessagesSent.get(index).sentTime == replyTime) {
                    replyFound = true;
                    syncMessagesSent.get(index).replied = true;
                }
            }
        }
        return -1;
    }

    public String getStateString()
    {
        if (appState != STATE_ERROR) {
            return stateStrings[appState];
        }
        else
        {
            return "Error: " + errorDescription;
        }
    }

    public int getMaxMulticastFps()
    {
        return maxMulticastFps;
    }

    public Boolean streamData(ArrayList<Object> data)
    {
        return streamData(data, null);
    }

    public synchronized Boolean streamData(ArrayList<Object> data, String extraOscAddress)
    {
        Boolean ok = false;
        int packetNumber;

        if (extraOscAddress == null)
        {
            packetNumber = multicastPacketNumber;
            multicastPacketNumber += 1;
            extraOscAddress = "";
        }
        else
        {
            if (multicastPacketNumberDict.containsKey(extraOscAddress))
            {
                packetNumber = multicastPacketNumberDict.get(extraOscAddress);
            }
            else
            {
                packetNumber = 0;
            }
            multicastPacketNumberDict.put(extraOscAddress, packetNumber + 1);
        }

        try
        {
            if (multicastSender != null)
            {
                data.add(0, myShortId);
                data.add(1, tag1 + "," + tag2 + "," + tag3);
                data.add(2, packetNumber);
                OSCMessage msg = new OSCMessage("/fsensync/" + clientType.toLowerCase() + extraOscAddress, data);
                multicastSender.send(msg);
                ok = true;
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error in OSC streaming.");
        }

        return ok;
    }

    public SyncService(SyncedClient client, ContextWrapper activity, String clientType, int versionNumber)
    {
        this.client = client;
        this.activity = activity;
        this.clientType = clientType;
        this.versionNumber = versionNumber;
        this.pingTimeList = new ArrayList<Double>();
        this.syncMessagesSent = new  ArrayList<SyncMessageInfo>();

        multicastPacketNumberDict = new HashMap<String, Integer>();

        appState = STATE_FIRST_START;
        closeServices = false;
        serverIp = "";
        myIp = "";
        uniqueId = "";
        syncMessageCount = 0;
        offsetNanosToServer = 0;
        offsetNanosToServerStreaming = 0;
        offsetNanosToServerHasBeenUpdatedOnce = false;
        errorNanosUpperBound = 99999999999L;
        timeOfLastDriftIncrement = 0L;
        errorNanosLowerBound = -99999999999L;
        broadcastAddress = "";
        previousExperimentName = "";
        previousRecordingNumber = -1;
        tag1 = "";
        tag2 = "";
        tag3 = "";
        myShortId = 0;
        errorDescription = "";
        deviceId = "-";
        longDeviceId = "";
        usesAppTags = false;
        hasReceivedTags = false;
        selectedPortOffset = 0;
        workingForOtherServer = false;
        isStreaming = false;
        knownSensorLag = 0;

        if (!appTag1.equals("") || !appTag2.equals("") || !appTag3.equals(""))
        {
            usesAppTags = true;
            tag1 = appTag1;
            tag2 = appTag2;
            tag3 = appTag3;
        }

        Locale.setDefault(Locale.ROOT);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
        {
            uniqueId = loadUniqueId();
            deviceId = loadDeviceId();
            longDeviceId = loadLongDeviceId();

            if (listenForServerMessages())
            {
                myNetworkTimer = new Timer(true);

                WifiManager wifiManager = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                if (wifiManager.isWifiEnabled() == false) {
                    wifiManager.setWifiEnabled(true);
                }

                if(wifiManager != null){
                    wifiMulticastLock = wifiManager.createMulticastLock("WifiLockSyncService");
                    wifiMulticastLock.acquire();
                    wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "WifiPerformanceLock");
                    wifiLock.acquire();
                }

                setAppState(STATE_WAITING_FOR_WIFI);
                myNetworkTimer.schedule(new FindOwnIpTask(myNetworkTimer), 100, 500);

                myNetworkTimer.schedule(new FindServerTask(), 100, 1000);
                if (usesAppTags) {
                    myNetworkTimer.schedule(new SendAppTagsTask(), 500, 1000);
                }
                else
                {
                    myNetworkTimer.schedule(new AskForTagsTask(), 1000, 1000);
                }
                myNetworkTimer.schedule(new SyncDriftTask(), 100, 1000);

                fileTransferThread = new Thread(fileTransferRunner());
                fileTransferThread.start();
            }

            timeOfLastDriftIncrement = SystemClock.elapsedRealtimeNanos();

            // Preventing cpu from going to sleep mode (the lock needs to be statically stored to hold)
            PowerManager powerManager = (PowerManager) activity.getSystemService(POWER_SERVICE);
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
            wakeLock.acquire();
        }
        else
        {
            errorDescription = "More permissions are needed.";
            setAppState(STATE_ERROR);
        }
    }

    public String getTag1() {
        return tag1;
    }

    public String getTag2() {
        return tag2;
    }

    public String getTag3() {
        return tag3;
    }

    public int getMyShortId() {
        return myShortId;
    }

    public long getOffsetNanosToServer() {
        return offsetNanosToServer;
    }

    public long getOffsetNanosToServerStreaming()
    {
        return offsetNanosToServerStreaming;
    }

    public long getErrorNanosUpperBound() {
        return errorNanosUpperBound;
    }

    public long getErrorNanosLowerBound() {
        return errorNanosLowerBound;
    }

    public String getBroadcastAddress() {
        return broadcastAddress;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public boolean getStreamState() {return isStreaming; }

    private String loadLongDeviceId()
    {
        String id = "";
        try {
            File path = getSettingsDirectory();
            File file = new File(path.getAbsolutePath() + File.separator + "sharedlongDeviceId.txt");
            if (file.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(file));
                id = br.readLine();
                br.close();
            }
            if (id.length() != 36) {
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
                id = UUID.randomUUID().toString();
                PrintWriter output = new PrintWriter(file, "UTF-8");
                output.println(id);
                output.close();
            }
        } catch (Exception ex) {
            System.out.println("Error in id file access.");
            id = UUID.randomUUID().toString();
        }
        return id;
    }

    private String loadUniqueId()
    {
        String id = "";
        try {
            File path = getSettingsDirectory();
            System.out.println(path.getAbsolutePath());
            File file = new File(path.getAbsolutePath() + File.separator + "uniqueId_" + clientType + ".txt");
            if (file.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(file));
                id = br.readLine();
                br.close();
            }
            if (id.length() != 36) {
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
                id = UUID.randomUUID().toString();
                PrintWriter output = new PrintWriter(file, "UTF-8");
                output.println(id);
                output.close();
            }
        } catch (Exception ex) {
            System.out.println("Error in id file access.");
            id = UUID.randomUUID().toString();
        }
        return id;
    }

    public static String loadDeviceId()
    {
        String s = "-";
        try
        {
            File path = getSettingsDirectory();
            File file = new File(path.getAbsolutePath() + File.separator + "devicedId.txt");
            if (file.exists())
            {
                BufferedReader br = new BufferedReader(new FileReader(file));
                s = br.readLine();
                br.close();
            }
        }
        catch (IOException ex)
        {
            System.out.println("Error in loading device physical ID.");
        }
        return s;
    }

    public static void saveDeviceId(String deviceId)
    {
        try
        {
            File path = getSettingsDirectory();
            File file = new File(path.getAbsolutePath() + File.separator + "devicedId.txt");
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            PrintWriter output = new PrintWriter(file, "UTF-8");
            output.println(deviceId);
            output.close();
        }
        catch (IOException ex)
        {
            System.out.println("Error in loading device ID.");
        }
    }

    public static File getSettingsDirectory()
    {
        File path = Environment.getExternalStorageDirectory();
        String settingDirectoryName = "FSenSyncSettings";
        path = new File(path.getAbsolutePath() + File.separator + settingDirectoryName);
        if (path.exists() == false)
        {
            path.mkdir();
        }
        return path;
    }

    public static File getDataDirectory()
    {
        File path = Environment.getExternalStorageDirectory();
        String settingDirectoryName = "FSenSyncData";
        path = new File(path.getAbsolutePath() + File.separator + settingDirectoryName);
        if (path.exists() == false)
        {
            path.mkdir();
        }
        return path;
    }

    public static File getStimulusDirectory()
    {
        File path = Environment.getExternalStorageDirectory();
        String settingDirectoryName = "FSenSyncStimFiles";
        path = new File(path.getAbsolutePath() + File.separator + settingDirectoryName);
        if (path.exists() == false)
        {
            path.mkdir();
        }
        return path;
    }

    private String sanitizeExperimentName(String name)
    {
        name = name.replaceAll(File.separator, "");
        name = name.replaceAll(File.pathSeparator, "");
        return name;
    }

    private void loadRecordingNumber(String experimentName)
    {
        previousRecordingNumber = -1;
        try
        {
            File path = getSettingsDirectory();
            File file = new File(path.getAbsolutePath() + File.separator + "recordingNumber_" + experimentName + "_" + clientType + ".txt");
            if (file.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(file));
                previousRecordingNumber = Integer.valueOf(br.readLine());
                br.close();
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error in loading recording number.");
        }
    }

    private void saveRecordingNumber(int number, String experimentName)
    {
        previousRecordingNumber = number;
        try
        {
            File path = getSettingsDirectory();
            File file = new File(path.getAbsolutePath() + File.separator + "recordingNumber_" + experimentName + "_" + clientType + ".txt");
            if (file.exists())
            {
                file.delete();
            }
            file.createNewFile();
            PrintWriter output = new PrintWriter(file, "UTF-8");
            output.println(number);
            output.close();
        }
        catch (IOException ex)
        {
            System.out.println("Error in saving recording number.");
        }
    }

    private Boolean listenForServerMessages()
    {
        Boolean listeningOk = true;

        // Broadcast receiver (ip addresses)
        Boolean broadcastPortReady = false;
        for (int portId = 0; portId < ADDRESS_ALTERNATIVES && !broadcastPortReady ; portId++) {
            int broadcastPort = 11300 + portId;
            try {
                portBroadcast = new OSCPortIn(broadcastPort, true); // Broadcast receive

                OSCListener listener = new OSCListener() {
                    public void acceptMessage(java.util.Date time, OSCMessage message) {
                        String foundIp = findMyIp();
                        if (foundIp.length() > 0) {
                            myIp = foundIp;
                        }
                        sendServerGiveIp();
                    }
                };
                portBroadcast.addListener("/updateip", listener);
                portBroadcast.startListening();
                broadcastPortReady = true;
            } catch (Exception ex) {
                System.out.println("Broadcast port " + broadcastPort + " is reserved.");
            }
        }

        if (!broadcastPortReady)
        {
            listeningOk = false;
            errorDescription = "Network all broadcast ports are reserved.";
            setAppState(STATE_ERROR);
        }

        // Unicast receiver
        portUnicast = null;
        for (int portId = 0; portId < ADDRESS_ALTERNATIVES && portUnicast == null ; portId++)
        {
            try {
                portUnicast = new OSCPortIn(DEFAULT_UNICAST_PORT + portId); // Regular receive
                selectedPortOffset = portId;
            }
            catch (Exception ex)
            {
                System.out.println("Network port " + (DEFAULT_UNICAST_PORT + portId) + " is reserved.");
            }
        }

        if (portUnicast == null)
        {
            System.out.println("Listening to unicast port failed.");
            errorDescription = "All unicast ports are reserved.";
            setAppState(STATE_ERROR);
            listeningOk = false;
        }
        else
        {
            System.out.println("Selected port offset:" +  selectedPortOffset);
        }

        try
        {

            OSCListener listenerSetTags = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 4)
                    {
                        System.out.println("Received malformed message. (001)");
                        return;
                    }

                    String uniqueIdInMessage = (String) a.get(3);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }

                    tag1 = (String) a.get(0);
                    tag2 = (String) a.get(1);
                    tag3 = (String) a.get(2);
                    if (usesAppTags
                            && appTag1.equals(tag1)
                            && appTag2.equals(tag2)
                            && appTag3.equals(tag3))
                    {
                        usesAppTags = false;
                        appTag1 = "";
                        appTag2 = "";
                        appTag3 = "";
                    }
                    hasReceivedTags = true;
                    client.setTagsToUi();

                }
            };
            OSCListener listenerReport = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 3)
                    {
                        System.out.println("Received malformed message. (002)");
                        return;
                    }

                    String uniqueIdInMessage = (String) a.get(2);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }

                    myShortId = (int)a.get(0);
                    String experimentName = sanitizeExperimentName((String)a.get(1));
                    client.updateExperimentInfoToUi();
                    if (!previousExperimentName.equals(experimentName) || previousRecordingNumber == -1)
                    {
                        if (appState > STATE_SYNCED_AND_READY)
                        {
                            workingForOtherServer = true;
                        }
                        previousExperimentName = experimentName;
                        loadRecordingNumber(experimentName);
                    }
                    if (appState > STATE_FINDING_SERVER)
                    {
                        reportToServer(serverIp);
                    }
                }
            };
            OSCListener listenerStartRecording = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    if (appState == STATE_SYNCED_AND_READY)
                    {
                        List<Object> a = message.getArguments();
                        if (a.size() != 5)
                        {
                            System.out.println("Received malformed message. (004)");
                            return;
                        }

                        String uniqueIdInMessage = (String) a.get(4);
                        if (uniqueIdInMessage.equals(uniqueId) == false)
                        {
                            System.out.println("Received a wrong app unique id.");
                            return;
                        }

                        String experimentName = sanitizeExperimentName((String) a.get(0));
                        previousExperimentName = experimentName;
                        int recNumber = (int)a.get(1);
                        String shortId = String.format(Locale.ROOT, "%03d", (int)a.get(2));
                        myShortId = (int)a.get(2);

                        recordingServerId = (int)a.get(3);

                        Boolean ok = openRecordingFiles(experimentName, shortId, recNumber);

                        reportToServer(serverIp);
                    }
                }
            };
            OSCListener listenerStopRecording = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 1)
                    {
                        System.out.println("Received malformed message. (014)");
                        return;
                    }
                    String uniqueIdInMessage = (String) a.get(0);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }
                    if (appState == STATE_RECORDING_DATA)
                    {
                        client.stopRecording();
                        workingForOtherServer = false;
                        if (client.doesPostSync())
                        {
                            setAppState(STATE_POST_SYNC);
                        }
                        else {
                            setAppState(STATE_SYNCED_AND_READY);
                        }
                    }
                    if (appState == STATE_PREPARING_PLAY || appState == STATE_PLAYING)
                    {
                        client.cancelStimuli();
                        client.stopRecording();
                        workingForOtherServer = false;
                        if (client.doesPostSync())
                        {
                            setAppState(STATE_POST_SYNC);
                        }
                        else {
                            setAppState(STATE_SYNCED_AND_READY);
                        }
                    };
                }
            };
            OSCListener listenerStartStreaming = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    if (appState >= STATE_SYNCED_AND_READY)
                    {
                        List<Object> a = message.getArguments();
                        if (!(a.size() == 6 || a.size() == 7))
                        {
                            System.out.println("Received malformed message. (021)");
                            return;
                        }

                        String uniqueIdInMessage = (String) a.get(0);
                        if (uniqueIdInMessage.equals(uniqueId) == false)
                        {
                            System.out.println("Received a wrong app unique id.");
                            return;
                        }

                        int streamServerId = (int)a.get(1); // Currently not used
                        multicastIp = (String)a.get(2);
                        multicastPort = (int)a.get(3);
                        maxMulticastFps = (int)a.get(4);
                        streamingZeroTime = (long)a.get(5);

                        if (a.size() == 7)
                        {
                            knownSensorLag = (int)a.get(6);
                        }
                        else
                        {
                            knownSensorLag = 0;
                        }

                        if (!isStreaming)
                        {
                            multicastPacketNumber = 0;
                            multicastPacketNumberDict.clear();
                        }

                        try {
                            InetAddress address = InetAddress.getByName(multicastIp);
                            multicastSender = new OSCPortOut(address, multicastPort);
                            isStreaming = true;
                        }
                        catch (SocketException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch (UnknownHostException ex)
                        {
                            ex.printStackTrace();
                        }

                        reportToServer(serverIp);
                    }
                }
            };
            OSCListener listenerStopStreaming = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    if (appState >= STATE_SYNCED_AND_READY)
                    {
                        List<Object> a = message.getArguments();
                        if (a.size() != 1)
                        {
                            System.out.println("Received malformed message. (022)");
                            return;
                        }

                        String uniqueIdInMessage = (String) a.get(0);
                        if (uniqueIdInMessage.equals(uniqueId) == false)
                        {
                            System.out.println("Received a wrong app unique id.");
                            return;
                        }

                        isStreaming = false;

                        reportToServer(serverIp);
                    }
                }
            };
            OSCListener listenerServerAddress = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 1)
                    {
                        System.out.println("Received malformed message. (005)");
                        return;
                    }
                    serverIp = (String) a.get(0);
                    if (appState == STATE_FINDING_SERVER)
                    {
                        // Initial contact with server
                        setAppState(STATE_FIRST_SYNC);
                        myNetworkTimer.schedule(new ScheduleClockSyncsTask(myNetworkTimer), 100, 15000);
                    }
                    else
                    {
                        // Silent update
                        System.out.println("Server ip updated");
                    }
                }
            };
            OSCListener listenerServerTimeQuery = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 3)
                    {
                        System.out.println("Received malformed message. (005)");
                        return;
                    }

                    String uniqueIdInMessage = (String) a.get(0);
                    int serverId = (int) a.get(1);
                    long serverTime = (long) a.get(2);

                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a clock query message with a wrong app unique id.");
                        return;
                    }

                    if (appState > STATE_FIRST_SYNC)
                    {
                        long appTime = SystemClock.elapsedRealtimeNanos() + offsetNanosToServer + getErrorNanosUpperBound() + getErrorNanosLowerBound();
                        long appUncertainty = (getErrorNanosUpperBound() - getErrorNanosLowerBound())/ 2;

                        try {
                            InetAddress address = InetAddress.getByName(serverIp);
                            OSCPortOut sender = new OSCPortOut(address, 11100);
                            ArrayList<Object> reply = new ArrayList<Object>(6);
                            reply.add(uniqueId);
                            reply.add(serverId);
                            reply.add(timeServerId);
                            reply.add(serverTime);
                            reply.add(appTime);
                            reply.add(appUncertainty);
                            OSCMessage msg = new OSCMessage("/clienttimereply", reply);
                            sender.send(msg);
                        } catch (Exception ex) {
                            System.out.println("Error when sending unicast message.");
                            ex.printStackTrace();
                        }
                    }
                }
            };
            OSCListener listenerClockReplies = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    long tabletTime2 = SystemClock.elapsedRealtimeNanos();
                    syncMessageCount = syncMessageCount + 1;
                    List<Object> a = message.getArguments();
                    if (a.size() != 4)
                    {
                        System.out.println("Received malformed message. (006)");
                        return;
                    }
                    long tabletTime1 = (long) a.get(0);
                    long serverTime = (long) a.get(1);
                    int thisServerId = (int) a.get(2);
                    String uniqueIdInMessage = (String) a.get(3);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a synchronization message with a wrong app unique id.");
                        return;
                    }

                    // One millisecond is added as some of the time keeping has only millisecond accuracy
                    long newMaxError = ((tabletTime2 - tabletTime1 + NANOS_IN_MILLI) / 2);

                    long newOffsetNanosToServer = (serverTime * NANOS_IN_MICRO) - ((tabletTime2 + tabletTime1) / 2);
                    if (offsetNanosToServerHasBeenUpdatedOnce == false)
                    {
                        offsetNanosToServerHasBeenUpdatedOnce = true;
                        offsetNanosToServer = newOffsetNanosToServer;
                        timeServerId = thisServerId;
                    }

                    if (appState > STATE_SYNCED_AND_READY && thisServerId != recordingServerId)
                    {
                        workingForOtherServer = true;
                    }

                    if (workingForOtherServer && appState > STATE_SYNCED_AND_READY && thisServerId == recordingServerId)
                    {
                        workingForOtherServer = false;
                    }

                    if (appState >= STATE_RECORDING_DATA)
                    {
                        long newErrorNanosUpperBound = newOffsetNanosToServer - offsetNanosToServer + newMaxError;
                        long newErrorNanosLowerBound = newOffsetNanosToServer - offsetNanosToServer - newMaxError;
                        if (newMaxError * 2 < errorNanosUpperBound - errorNanosLowerBound
                                && workingForOtherServer == false) {
                            errorNanosUpperBound = newErrorNanosUpperBound;
                            errorNanosLowerBound = newErrorNanosLowerBound;
                            offsetNanosToServerStreaming = newOffsetNanosToServer;
                        }
                    }

                    if (appState >= STATE_FIRST_SYNC && appState <= STATE_SYNCED_AND_READY )
                    {
                        if (timeServerId == thisServerId) {
                            if (newMaxError * 2 < errorNanosUpperBound - errorNanosLowerBound) {
                                offsetNanosToServer = newOffsetNanosToServer;
                                offsetNanosToServerStreaming = newOffsetNanosToServer;
                                errorNanosUpperBound = newMaxError;
                                errorNanosLowerBound = -newMaxError;
                            }

                            int minSyncMessageCount = 20;
                            if (appState < STATE_SYNCED_AND_READY && syncMessageCount > minSyncMessageCount) {
                                if (client.getRecordingServiceReady()) {
                                    setAppState(STATE_SYNCED_AND_READY);
                                } else {
                                    setAppState(STATE_SYNCED_AND_WAITING_FOR_SENSORS);
                                }
                            }
                        }
                        else
                        {
                            timeServerId = thisServerId;
                            offsetNanosToServer = newOffsetNanosToServer;
                            offsetNanosToServerStreaming = newOffsetNanosToServer;
                            errorNanosUpperBound = newMaxError;
                            errorNanosLowerBound = -newMaxError;
                        }
                    }

                    if (syncMessagesSent != null)
                    {
                        processSyncMessagesSent(null, tabletTime1, false);
                    }

                    if (pingTimeList != null)
                    {
                        pingTimeList.add((tabletTime2 - tabletTime1) / (double)NANOS_IN_MILLI);
                        while (pingTimeList.size() > 40)
                        {
                            pingTimeList.remove(0);
                        }
                    }
                }
            };
            OSCListener listenerVersionError = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    errorDescription = "This version of the app is incompatible with the server.";
                    setAppState(STATE_ERROR);
                }
            };
            OSCListener listenerStartStimuli = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    if (appState == STATE_RECORDING_DATA || appState == STATE_SYNCED_AND_READY)
                    {
                        List<Object> a = message.getArguments();
                        if (a.size() != 7)
                        {
                            System.out.println("Received malformed message. (007)");
                            return;
                        }

                        String uniqueIdInMessage = (String) a.get(5);
                        if (uniqueIdInMessage.equals(uniqueId) == false)
                        {
                            System.out.println("Received a wrong app unique id.");
                            return;
                        }

                        String experimentName = sanitizeExperimentName((String) a.get(0));
                        previousExperimentName = experimentName;
                        String shortId = String.format(Locale.ROOT, "%03d", (int)a.get(1));
                        myShortId = (int)a.get(1);
                        recordingServerId = (int)a.get(2);
                        long desiredStartTime = (long)a.get(3);
                        String stimuliName = (String)a.get(4);
                        int recNumber = (int)a.get(6);

                        if (appState == STATE_SYNCED_AND_READY)
                        {
                            Boolean ok = openRecordingFiles(experimentName, shortId, recNumber);
                            reportToServer(serverIp);
                        }


                        if (appState == STATE_RECORDING_DATA) {
                            File path = getDataDirectory();
                            String fileBaseName = path.getAbsolutePath() + File.separator + experimentName + "_" + shortId + "_";
                            setAppState(STATE_PREPARING_PLAY);
                            client.startPreparingPlay(fileBaseName, stimuliName, desiredStartTime);
                        }
                        
                        reportToServer(serverIp);
                    }
                }
            };
            OSCListener listenerCancelStimuli = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 1)
                    {
                        System.out.println("Received malformed message. (011)");
                        return;
                    }
                    String uniqueIdInMessage = (String) a.get(0);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }
                    if (appState == STATE_PREPARING_PLAY || appState == STATE_PLAYING)
                    {
                        setAppState(STATE_RECORDING_DATA);
                        client.cancelStimuli();
                    }
                }
            };
            OSCListener listenerEnableScreenTouchSound = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 1)
                    {
                        System.out.println("Received malformed message. (009)");
                        return;
                    }
                    String uniqueIdInMessage = (String) a.get(0);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }
                    client.enableScreenTouchSound();
                    if (appState > STATE_FINDING_SERVER) {
                        reportToServer(serverIp);
                    }
                }
            };
            OSCListener listenerDisableScreenTouchSound = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 1)
                    {
                        System.out.println("Received malformed message. (008)");
                        return;
                    }
                    String uniqueIdInMessage = (String) a.get(0);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }
                    client.disableScreenTouchSound();
                    if (appState > STATE_FINDING_SERVER) {
                        reportToServer(serverIp);
                    }
                }
            };
            OSCListener listenerSetAppSetting = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 3)
                    {
                        System.out.println("Received malformed message. (016)");
                        return;
                    }
                    String uniqueIdInMessage = (String) a.get(0);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }
                    client.setAppSetting((String) a.get(1), (String) a.get(2));
                    reportSettingToServer(serverIp, (String) a.get(1), client.getAppSetting((String) a.get(1)));
                }
            };
            OSCListener listenerGetAppSetting = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 2)
                    {
                        System.out.println("Received malformed message. (017)");
                        return;
                    }
                    String uniqueIdInMessage = (String) a.get(0);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }
                    client.getAppSetting((String) a.get(1));
                    reportSettingToServer(serverIp, (String) a.get(1), client.getAppSetting((String) a.get(1)));
                }
            };
            OSCListener listenerCloseApp = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 1)
                    {
                        System.out.println("Received malformed message. (018)");
                        return;
                    }
                    String uniqueIdInMessage = (String) a.get(0);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }
                    client.closeApp();
                }
            };
            OSCListener listenerClockReset = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 2)
                    {
                        System.out.println("Received malformed message. (019)");
                        return;
                    }
                    String uniqueIdInMessage = (String) a.get(0);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }

                    int serverId = (int)a.get(1);

                    if (appState <= STATE_SYNCED_AND_READY && serverId != timeServerId)
                    {
                        errorNanosUpperBound += 100000 * NANOS_IN_MILLI;
                        errorNanosLowerBound -= 100000 * NANOS_IN_MILLI;
                        reportToServer(serverIp);
                        timeServerId = serverId;
                        myNetworkTimer.schedule(new ScheduleClockSyncsTask(myNetworkTimer), 1000);
                    }
                }
            };
            OSCListener listenerDeleteFiles = new OSCListener() {
                public void acceptMessage(java.util.Date time, OSCMessage message) {
                    List<Object> a = message.getArguments();
                    if (a.size() != 4)
                    {
                        System.out.println("Received malformed message. (020)");
                        return;
                    }
                    String uniqueIdInMessage = (String) a.get(0);
                    if (uniqueIdInMessage.equals(uniqueId) == false)
                    {
                        System.out.println("Received a wrong app unique id.");
                        return;
                    }

                    final Boolean onlyThisExperiment = (Boolean) a.get(1);
                    final String experimentName = (String) a.get(2);
                    final Boolean onlyStimulusFiles = (Boolean) a.get(3);

                    if (!onlyStimulusFiles && appState == SyncService.STATE_SYNCED_AND_READY) {
                        File path = getDataDirectory();

                        File[] files = path.listFiles(new FilenameFilter() {
                            @Override
                            public boolean accept(File file, String name) {
                                if (onlyThisExperiment) {
                                    return testExperimentFileName(experimentName, name);
                                } else {
                                    return true;
                                }
                            }
                        });

                        for (int i = 0; i < files.length; i++) {
                            files[i].delete();
                        }


                        File settingsPath = getSettingsDirectory();
                        File[] settingFiles = settingsPath.listFiles(new FilenameFilter() {
                            @Override
                            public boolean accept(File file, String name) {
                                if (onlyThisExperiment) {
                                    return Pattern.matches("recordingNumber_" + experimentName + "_\\w{0,6}\\.txt", name);
                                } else {
                                    return Pattern.matches("recordingNumber_.*\\.txt", name);
                                }
                            }
                        });

                        for (int i = 0; i < settingFiles.length; i++)
                        {
                            settingFiles[i].delete();
                        }
                    }

                    if (onlyStimulusFiles || !onlyThisExperiment)
                    {
                        if (appState == SyncService.STATE_SYNCED_AND_READY) {
                            File path = getStimulusDirectory();

                            File[] files = path.listFiles();

                            for (int i = 0; i < files.length; i++) {
                                files[i].delete();
                            }
                        }
                    }

                    reportToServer(serverIp);

                }
            };
            portUnicast.addListener("/givetimeanduncertainty", listenerServerTimeQuery);
            portUnicast.addListener("/clockreset", listenerClockReset);
            portUnicast.addListener("/serveraddress", listenerServerAddress);
            portUnicast.addListener("/replycurrenttime", listenerClockReplies);
            portUnicast.addListener("/report", listenerReport);
            portUnicast.addListener("/settags", listenerSetTags);
            portUnicast.addListener("/startrecording", listenerStartRecording);
            portUnicast.addListener("/stoprecording", listenerStopRecording);
            portUnicast.addListener("/startstreaming", listenerStartStreaming);
            portUnicast.addListener("/stopstreaming", listenerStopStreaming);
            portUnicast.addListener("/startstimuli", listenerStartStimuli);
            portUnicast.addListener("/cancelstimuli", listenerCancelStimuli);
            portUnicast.addListener("/versionerror", listenerVersionError);
            portUnicast.addListener("/enabletouchsound", listenerEnableScreenTouchSound);
            portUnicast.addListener("/disabletouchsound", listenerDisableScreenTouchSound);
            portUnicast.addListener("/setappsetting", listenerSetAppSetting);
            portUnicast.addListener("/getappsetting", listenerGetAppSetting);
            portUnicast.addListener("/closeapp", listenerCloseApp);
            portUnicast.addListener("/deletefiles", listenerDeleteFiles);
            portUnicast.startListening();
        }
        catch (Exception ex)
        {
            errorDescription = "Could not set unicast listeners.";
            setAppState(STATE_ERROR);
            listeningOk = false;
        }

        return listeningOk;
    }

    private Boolean openRecordingFiles(String experimentName, String shortId, int recNumber)
    {
        String recordingNumber = String.format(Locale.ROOT, "%03d", recNumber);

        File path = getDataDirectory();
        String fileBaseName = path.getAbsolutePath() + File.separator + experimentName + "_" + recordingNumber + "_" + shortId + "_";

        Boolean ok = true;

        PrintWriter metaOutputStream = null;
        {
            try
            {
                File file = new File(fileBaseName + clientType + ".meta");
                if (file.exists()) {
                    sendErrorToServer("Refused to overwrite old metafile with number " + recordingNumber);
                }
                file.createNewFile();
                metaOutputStream = new PrintWriter(file, "UTF-8");
                try {
                    metaOutputStream.println("Server tag 1: " + tag1);
                    metaOutputStream.println("Server tag 2: " + tag2);
                    metaOutputStream.println("Server tag 3: " + tag3);
                    metaOutputStream.println("Device: " + Build.MANUFACTURER + "_" + Build.MODEL);
                    metaOutputStream.println("Android version: " + Build.VERSION.RELEASE);
                    metaOutputStream.println("Android SDK: " + android.os.Build.VERSION.SDK_INT);
                    metaOutputStream.println("Network code version: " + BuildConfig.VERSION_CODE);
                    metaOutputStream.println("Experiment name: " + experimentName);
                    metaOutputStream.println("App type: " + clientType);
                    metaOutputStream.println("App id: " + myShortId);
                    metaOutputStream.println("Device id: " + deviceId);
                    metaOutputStream.println("Long app id: " + uniqueId);
                    metaOutputStream.println("Long device id: " + longDeviceId);
                    metaOutputStream.flush();
                    reportFileToServer(serverIp, file.getName());
                } catch (Exception ex) {
                    System.out.println("Error while writing meta to file.");
                    ok = false;
                }
            }
            catch (Exception ex)
            {
                System.out.println("Error when opening meta output file.");
                ok = false;
            }
        }

        if (ok) {
            ok = client.startRecording(fileBaseName, recordingNumber, metaOutputStream);
        }

        previousExperimentName = experimentName;
        saveRecordingNumber(recNumber, experimentName);
        if (ok) {
            setAppState(STATE_RECORDING_DATA);
        }
        return ok;
    }

    public void clientWantsToStopRecording()
    {
        if (appState == STATE_RECORDING_DATA || appState == STATE_PREPARING_PLAY || appState == STATE_PLAYING)
        {
            client.stopRecording();
            workingForOtherServer = false;
            setAppState(STATE_SYNCED_AND_READY);
        }
    }

    private void setAppState(int state)
    {
        int oldState = appState;
        appState = state;
        if (appState >= STATE_FIRST_SYNC)
        {
            reportToServer(serverIp);
        }

        client.newSyncState(appState, oldState, getStateString());
    }

    public void sensorStateChanged()
    {
        if (appState >= STATE_SYNCED_AND_READY && appState != STATE_FILE_TRANSFER && appState != STATE_POST_SYNC)
        {
            if (client.getRecordingServiceReady() == false)
            {
                if (appState == STATE_RECORDING_DATA)
                {
                    client.stopRecording();
                    workingForOtherServer = false;
                }
                setAppState(STATE_SYNCED_AND_WAITING_FOR_SENSORS);
            }
        }
        else
        {
            if (appState == STATE_SYNCED_AND_WAITING_FOR_SENSORS && client.getRecordingServiceReady()) {
                setAppState(STATE_SYNCED_AND_READY);
            }
        }
    }

    public void postSyncReady()
    {
        if (appState == STATE_POST_SYNC)
        {
            setAppState(STATE_SYNCED_AND_WAITING_FOR_SENSORS);
        }
        sensorStateChanged();
    }

    public void setStatePlayingStimuli()
    {
        if (appState == STATE_PREPARING_PLAY)
        {
            setAppState(STATE_PLAYING);
            reportToServer(serverIp);
        }
        else
        {
            System.out.println("Invalid transition to 'STATE_PLAYING'");
        }
    }

    public void playingStimuliEnded()
    {
        if (appState == STATE_PREPARING_PLAY)
        {
            setAppState(STATE_RECORDING_DATA);
            reportToServer(serverIp);
        }
        else
        {
            if (appState == STATE_PLAYING)
            {
                setAppState(STATE_RECORDING_DATA);
            }
            else {
                System.out.println("Invalid transition to 'STATE_RECORDING_DATA'");
            }
        }
    }

    public int getAppState()
    {
        return appState;
    }

    private class FindServerTask extends TimerTask
    {
        public FindServerTask() {}

        @Override
        public void run() {

            if (appState == STATE_FINDING_SERVER)
            {
                sendServerGiveIp();
                System.out.println("Finding server.");
            }
            if (appState > STATE_FINDING_SERVER)
            {
                this.cancel();
            }
        }
    }

    private class SendAppTagsTask extends TimerTask
    {
        public SendAppTagsTask() {}

        @Override
        public void run()
        {
            if (!usesAppTags)
            {
                this.cancel();
            }

            if (appState > STATE_FINDING_SERVER && usesAppTags)
            {
                try {
                    InetAddress address = InetAddress.getByName(serverIp);
                    OSCPortOut sender = new OSCPortOut(address, 11100);
                    ArrayList<Object> a = new ArrayList<Object>(4);
                    a.add(uniqueId);
                    a.add(appTag1);
                    a.add(appTag2);
                    a.add(appTag3);

                    OSCMessage msg = new OSCMessage("/setapptags", a);
                    if (usesAppTags)
                    {
                        sender.send(msg);
                    }
                } catch (Exception ex) {
                    System.out.println("Error when sending unicast message.");
                    ex.printStackTrace();
                }
            }
        }
    }

    public void delayedCancelAndStop()
    {
        if (myNetworkTimer != null)
        {
            myNetworkTimer.schedule(new DelayedCancelAndStop(), 1300l);
        }
    }

    private class DelayedCancelAndStop extends TimerTask
    {
        public DelayedCancelAndStop() {}

        @Override
        public void run()
        {
            clientWantsToStopRecording();
        }
    }

    private class AskForTagsTask extends TimerTask
    {
        public AskForTagsTask() {}

        @Override
        public void run()
        {
            if (hasReceivedTags)
            {
                this.cancel();
            }

            if (appState > STATE_FINDING_SERVER && !hasReceivedTags)
            {
                try {
                    InetAddress address = InetAddress.getByName(serverIp);
                    OSCPortOut sender = new OSCPortOut(address, 11100);
                    ArrayList<Object> a = new ArrayList<Object>(1);
                    a.add(uniqueId);

                    OSCMessage msg = new OSCMessage("/givetags", a);
                    sender.send(msg);
                } catch (Exception ex) {
                    System.out.println("Error when sending unicast message.");
                    ex.printStackTrace();
                }
            }
        }
    }

    private void sendServerGiveIp()
    {
        try {
            InetAddress address = InetAddress.getByName(broadcastAddress);
            OSCPortOut sender = new OSCPortOut(address, 11200);
            ArrayList<Object> a = new ArrayList<Object>(9);
            a.add(myIp);
            a.add(uniqueId);
            a.add(clientType);
            a.add(versionNumber);
            a.add(Build.MANUFACTURER + "_" + Build.MODEL);
            a.add(selectedPortOffset);
            a.add(deviceId);
            a.add(longDeviceId);
            a.add(usesAppTags);
            OSCMessage msg = new OSCMessage("/giveserverip", a);
            sender.send(msg);
        } catch (Exception ex) {
            System.out.println("Error sending broadcast message.");
            ex.printStackTrace();
        }
    }

    private class SyncDriftTask extends TimerTask
    {
        public SyncDriftTask() {}

        @Override
        public void run()
        {
            long timeNow = SystemClock.elapsedRealtimeNanos();
            long elapsedTimeNanos = timeNow - timeOfLastDriftIncrement;
            timeOfLastDriftIncrement = timeNow;
            // Worst case drift of 100 ms in 1 hour
            long assumedDrift = elapsedTimeNanos * 2800L / (100L * NANOS_IN_MILLI);
            errorNanosUpperBound = errorNanosUpperBound + assumedDrift;
            errorNanosLowerBound = errorNanosLowerBound - assumedDrift;
        }
    }

    private class ScheduleClockSyncsTask extends TimerTask
    {
        Timer timer;

        public ScheduleClockSyncsTask(Timer timer)
        {
            this.timer = timer;
        }

        @Override
        public void run()
        {

            if (appState == STATE_FIRST_SYNC)
            {
                int messageCount = 40;
                for (long i = 0; i < messageCount; i++)
                {
                    timer.schedule(new AskClockSyncTask(), (100L*i)+100L);
                }
            }
            if (appState > STATE_FIRST_SYNC)
            {
                int messageCount = 10;
                long messageInterval = 100L;
                double syncUncertainty = (errorNanosUpperBound - errorNanosLowerBound) / (double)NANOS_IN_MILLI / 2.0;
                if (syncUncertainty > 3.0)
                {
                    messageCount = 20;
                }
                if (syncUncertainty > 5.0)
                {
                    messageCount = 30;
                }
                if (syncUncertainty > 8.0)
                {
                    messageCount = 40;
                }
                for (long i = 0; i < messageCount; i++) {
                    timer.schedule(new AskClockSyncTask(), (messageInterval * i) + messageInterval);
                }
            }
        }
    }

    private String findMyIp()
    {
        String foundIp = "";
        try {

            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while(en.hasMoreElements()){
                NetworkInterface ni= en.nextElement();
                Enumeration<InetAddress> ee = ni.getInetAddresses();
                while(ee.hasMoreElements()) {
                    InetAddress ia= ee.nextElement();
                    if (ia.isSiteLocalAddress() && ia instanceof Inet4Address)
                    {
                        foundIp = ia.getHostAddress();
                    }
                }
                if(!ni.isLoopback() && ni.isUp())
                {
                    Iterator<InterfaceAddress> eBr = ni.getInterfaceAddresses().iterator();
                    while(eBr.hasNext()) {
                        InterfaceAddress ia = eBr.next();
                        if (ia != null && ia.getBroadcast() != null)
                        {
                            broadcastAddress = ia.getBroadcast().toString();
                            broadcastAddress = broadcastAddress.replaceAll("/", "");
                        }
                    }
                }
            }
        }
        catch (SocketException e) {
            System.out.println("Error while finding my own ip.");
            e.printStackTrace();
        }
        return foundIp;
    }

    private class FindOwnIpTask extends TimerTask
    {
        Timer timer;

        public FindOwnIpTask(Timer timer)
        {
            this.timer = timer;
        }

        @Override
        public void run()
        {

            if (appState == STATE_WAITING_FOR_WIFI) {

                String foundIp = findMyIp();

                if (foundIp.length() > 0) {
                    System.out.println("Selected ip: " + foundIp);
                    System.out.println("Broadcast: " + broadcastAddress);
                    myIp = foundIp;
                    setAppState(STATE_FINDING_SERVER);
                    myNetworkTimer.schedule(new FindOwnIpAgainTask(myNetworkTimer), 10000, 10000);
                    this.cancel();
                }
            }
        }
    }

    private class FindOwnIpAgainTask extends TimerTask
    {
        Timer timer;

        public FindOwnIpAgainTask(Timer timer)
        {
            this.timer = timer;
        }

        @Override
        public void run()
        {
            String foundIp = findMyIp();

            if (foundIp.length() > 0 && !myIp.equals(foundIp))
            {
                System.out.println("Selected new ip: " + foundIp);
                myIp = foundIp;
                reportToServer(serverIp);
            }

        }
    }


    private class AskClockSyncTask extends TimerTask
    {

        public AskClockSyncTask() {}

        @Override
        public void run()
        {
            // Unicast send

            if (appState >= STATE_FIRST_SYNC)
            {
                long t = 0;
                try {
                    InetAddress address = InetAddress.getByName(serverIp);
                    OSCPortOut sender = new OSCPortOut(address, 11100);
                    ArrayList<Object> a = new ArrayList<Object>(3);
                    t = SystemClock.elapsedRealtimeNanos();
                    a.add(t);
                    a.add(myIp);
                    a.add(uniqueId);
                    a.add(selectedPortOffset);
                    OSCMessage msg = new OSCMessage("/givecurrenttime", a);
                    sender.send(msg);
                } catch (Exception ex) {
                    System.out.println("Error when sending unicast message.");
                    ex.printStackTrace();
                }

                if (syncMessagesSent != null)
                {
                    SyncMessageInfo i = new SyncMessageInfo(t);
                    processSyncMessagesSent(i, 0, false);
                }
            }
        }
    }

    public void reportSettingToServer(String host, String setting, String value)
    {
        try {
            InetAddress address = InetAddress.getByName(host);
            OSCPortOut sender = new OSCPortOut(address, 11100);
            ArrayList<Object> a = new ArrayList<Object>(3);
            a.add(uniqueId);
            a.add(setting);
            a.add(value);
            OSCMessage msg = new OSCMessage("/appsettingreport", a);
            sender.send(msg);
        } catch (Exception ex) {
            System.out.println("Error when sending unicast message.");
            ex.printStackTrace();
        }
    }

    public void reportFileToServer(String host, String fileName)
    {
        try {
            InetAddress address = InetAddress.getByName(host);
            OSCPortOut sender = new OSCPortOut(address, 11100);
            ArrayList<Object> a = new ArrayList<Object>(2);
            a.add(uniqueId);
            a.add(fileName);
            OSCMessage msg = new OSCMessage("/filereport", a);
            sender.send(msg);
        } catch (Exception ex) {
            System.out.println("Error when sending unicast message.");
            ex.printStackTrace();
        }
    }

    public void reportToServer(String host)
    {
        try {
            InetAddress address = InetAddress.getByName(host);
            OSCPortOut sender = new OSCPortOut(address, 11100);
            ArrayList<Object> a = new ArrayList<Object>(11);
            a.add(myIp);
            a.add((double)getBatteryLevel());
            a.add((errorNanosUpperBound - errorNanosLowerBound) / (double)NANOS_IN_MILLI / 2.0);
            a.add(appState);

            long freeSpace = 0L;
            File path = getDataDirectory();
            if (path.exists())
            {
                freeSpace = path.getFreeSpace();
            }
            a.add(freeSpace);

            a.add(previousRecordingNumber);
            a.add(client.getIsOnBackGround());
            a.add(workingForOtherServer);
            a.add(uniqueId);

            if (pingTimeList != null)
            {
                double mean = 0.0;
                for (int i = 0; i < pingTimeList.size() - 1; i++) {
                    mean += pingTimeList.get(i);
                }
                mean /= pingTimeList.size() - 1;
                a.add(mean);
            }
            else
            {
                a.add(-1.0d);
            }

            if (syncMessagesSent != null && syncMessagesSent.size() > 10)
            {
                a.add(processSyncMessagesSent(null, 0, true));
            }
            else
            {
                a.add(-1);
            }

            a.add(isStreaming);

            OSCMessage msg = new OSCMessage("/clientreport", a);
            sender.send(msg);
        } catch (Exception ex) {
            System.out.println("Error when sending unicast message.");
            ex.printStackTrace();
        }

        reportSensorHealthToServer(host, client.giveSensorHealth());
    }

    public void reportSensorHealthToServer(String host, int[] health)
    {
        try {
            InetAddress address = InetAddress.getByName(host);
            OSCPortOut sender = new OSCPortOut(address, 11100);
            ArrayList<Object> a = new ArrayList<Object>();
            a.add(uniqueId);
            a.add(health.length);
            for (int i = 0; i < health.length; i++)
            {
                a.add(health[i]);
            }
            OSCMessage msg = new OSCMessage("/sensorhealth", a);
            sender.send(msg);
        } catch (Exception ex) {
            System.out.println("Error when sending unicast message.");
            ex.printStackTrace();
        }
    }

    public void sendErrorToServer(String error)
    {
        sendErrorToServer(serverIp, error);
    }

    public void sendClosingAppToServer()
    {
        try {
            InetAddress address = InetAddress.getByName(serverIp);
            OSCPortOut sender = new OSCPortOut(address, 11100);
            ArrayList<Object> a = new ArrayList<Object>();
            a.add(myIp);
            a.add(uniqueId);
            OSCMessage msg = new OSCMessage("/closingapp", a);
            sender.send(msg);
        } catch (Exception ex) {
            System.out.println("Error when sending closing message to server.");
            ex.printStackTrace();
        }

        closeServices = true;
        portBroadcast.stopListening();
        portUnicast.stopListening();
        portBroadcast.close();
        portUnicast.close();
        try {
            fileTransferSocket.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        fileTransferThread.interrupt();
        myNetworkTimer.cancel();
        if (wakeLock.isHeld()) {
            System.out.println("wakelock released");
            wakeLock.release();
        }
        if (wifiMulticastLock.isHeld())
        {
            wifiMulticastLock.release();
        }
        if (wifiLock.isHeld())
        {
            wifiLock.release();
        }
        appState = STATE_CLOSING;
    }

    private void sendErrorToServer(String host, String error)
    {
        try {
            InetAddress address = InetAddress.getByName(host);
            OSCPortOut sender = new OSCPortOut(address, 11100);
            ArrayList<Object> a = new ArrayList<Object>(3);
            a.add(myIp);
            a.add(uniqueId);
            a.add(error);
            OSCMessage msg = new OSCMessage("/errormessage", a);
            sender.send(msg);
        } catch (Exception ex) {
            System.out.println("Error when sending error message to server.");
            ex.printStackTrace();
        }
    }

    private Boolean testExperimentFileName(String experimentName, String name)
    {
        return Pattern.matches(experimentName + "_\\d+_\\d+_\\w{0,15}_?\\w{0,6}\\.\\w{0,6}", name);
    }

    private Runnable fileTransferRunner()
    {
        Runnable runner = new Runnable() {
            public void run()
            {
                fileTransferSocket = null;

                while (fileTransferSocket == null) {
                    int number = DEFAULT_TCP_PORT + selectedPortOffset;
                    try {
                        fileTransferSocket = new ServerSocket(number);
                    } catch (IOException ex) {
                        String message = "Cannot listen to port " + number + ".";
                        System.out.println(message);
                        client.showExtraMessageOnScreen(message);
                    }
                    try {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException ex2) {}
                }

                while (!closeServices) {
                    try {
                        //serverSocket.setSoTimeout(60000);
                        Socket socket = fileTransferSocket.accept();
                        socket.setSoTimeout(7000);
                        fileTransferConnection(socket);
                    }
                    catch (Exception ex)
                    {
                        if (!closeServices) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ex2) {
                            }
                        }
                    }
                }

                if (fileTransferSocket != null)
                {
                    try {
                        fileTransferSocket.close();
                    }
                    catch (IOException ex)
                    {

                    }
                }
            }
        };
        return runner;
    }

    public void fileTransferConnection(Socket socket)
    {
        if (appState <= STATE_SYNCED_AND_READY)
        {
            setAppState(STATE_FILE_TRANSFER);

            Timer timer = new Timer(true);
            TimeoutTask timeout = new TimeoutTask(socket);
            timer.schedule(timeout, 1000, 1000);

            try {
                BufferedReader netReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter netWriter = new PrintWriter(socket.getOutputStream());

                String command = netReader.readLine();

                switch (command) {
                    case "Give list of files.": {
                        final String experimentName = netReader.readLine();
                        netWriter.println("FILES START");
                        File path = getDataDirectory();
                        File[] files = path.listFiles(new FilenameFilter() {
                            @Override
                            public boolean accept(File file, String name) {
                                return testExperimentFileName(experimentName, name);
                            }
                        });
                        for (int i = 0; i < files.length; i++) {
                            timeout.delaySocketClosingTimeout();
                            netWriter.println(files[i].getName());
                            netWriter.println(files[i].length());
                        }
                        netWriter.println("FILES END");
                        netWriter.flush();

                        break;
                    }
                    case "Give file.": {
                        String fileName = netReader.readLine();
                        File pathToFile = getDataDirectory();
                        File file = new File(pathToFile.getAbsolutePath() + File.separator + fileName);
                        if (file.exists()) {
                            byte[] bytes = new byte[16 * 1024];
                            InputStream in = new FileInputStream(file);
                            OutputStream out = socket.getOutputStream();
                            int count;
                            while ((count = in.read(bytes)) > 0) {
                                out.write(bytes, 0, count);
                                timeout.delaySocketClosingTimeout();
                            }
                            out.close();
                            in.close();
                        } else {
                            System.out.println("Request for non existing file: " + fileName);
                        }
                        break;
                    }
                    case "Receive stimulus file.": {
                        String touchStr = netReader.readLine();

                        Boolean isSoundTouchFile = false;

                        if (touchStr != null && touchStr.equals("Is touch sound file."))
                        {
                            isSoundTouchFile = true;
                        }

                        String fileName = netReader.readLine();
                        long fileLength = Long.valueOf(netReader.readLine());
                        String serverCheckSum = netReader.readLine();
                        File pathToFile = getStimulusDirectory();

                        File folder = new File(pathToFile.getAbsolutePath());
                        if (!folder.exists())
                        {
                            folder.mkdir();
                        }

                        File file;
                        if (isSoundTouchFile)
                        {
                            file = new File(pathToFile.getAbsolutePath() + File.separator + "screen_touch_sound_file");
                        }
                        else
                        {
                            file = new File(pathToFile.getAbsolutePath() + File.separator + fileName);
                        }

                        boolean alreadyHadSameFile = false;
                        if (file.exists())
                        {
                            if (file.length() == fileLength)
                            {
                                String clientCheckSum = CheckSum.getCheckSum(file.getAbsolutePath(), netWriter, timeout);
                                if (clientCheckSum.equals(serverCheckSum))
                                {
                                    alreadyHadSameFile = true;
                                }
                                else
                                {
                                    file.delete();
                                }
                            }
                            else
                            {
                                file.delete();
                            }
                        }

                        String message;

                        if (alreadyHadSameFile)
                        {
                            message = "Already had identical file '" + fileName + "'";
                            netWriter.println("ALREADY HAD FILE");
                            netWriter.flush();
                        }
                        else
                        {
                            netWriter.println("READY TO RECEIVE");
                            netWriter.flush();

                            CheckSum check = new CheckSum();

                            byte[] bytes = new byte[16 * 1024];
                            OutputStream out = new FileOutputStream(file);
                            InputStream in = socket.getInputStream();
                            int count = 0;
                            int accumCount = 0;
                            while (accumCount < fileLength && (count = in.read(bytes)) > 0) {
                                out.write(bytes, 0, count);
                                check.update(bytes, count);
                                timeout.delaySocketClosingTimeout();
                                accumCount += count;
                            }

                            out.close();

                            String myCheckSum = check.getCheckSum();


                            if (file.exists() && file.length() == fileLength && myCheckSum.equals(serverCheckSum)) {
                                netWriter.println("RECEIVED OK");
                                netWriter.flush();
                                message = "File '" + fileName + "' downloaded successfully.";
                                if (isSoundTouchFile) {
                                    client.enableScreenTouchSound();
                                }
                            } else {
                                if (file.exists() == false) {
                                    message = "File '" + fileName + "' downloaded failed.";
                                } else {
                                    message = "File '" + fileName + "' had a mismatch in size or MD5 checksum.";
                                    file.delete();
                                }
                            }

                            in.close();
                        }

                        System.out.println(message);
                        client.showExtraMessageOnScreen(message);

                        break;
                    }
                    default:
                        System.out.println("Invalid file command:" + command);
                        break;
                }

                netReader.close();
                netWriter.close();
                socket.close();
            } catch (SocketTimeoutException ex) {
                System.out.println("File transfer timeout.");
            } catch (IOException ex) {
                System.out.println("Error in file transfer connection.");
                ex.printStackTrace();
            }

            timer.cancel();

            setAppState(STATE_SYNCED_AND_READY);
            sensorStateChanged();
        }
    }

    public long getStreamingZeroTime()
    {
        return streamingZeroTime + knownSensorLag;
    }

    public float getBatteryLevel() {
        Intent batteryIntent = activity.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        return ((float)level / (float)scale) * 100.0f;
    }

    public class TimeoutTask extends TimerTask {
        private long lastActivity;
        private Socket timedSocket;

        public TimeoutTask(Socket s) {
            lastActivity = System.currentTimeMillis();
            timedSocket = s;

        }

        @Override
        public void run() {
            if (lastActivity < System.currentTimeMillis() - 8000) {
                if (timedSocket != null && !timedSocket.isClosed()) {
                    System.out.println("File transfer timeout reached.");
                    try {
                        timedSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                this.cancel();
            }
        }

        public void delaySocketClosingTimeout() {
            lastActivity = System.currentTimeMillis();
        }
    }

    public String currentServerIp()
    {
        return serverIp;
    }

    public String currentUniqueId()
    {
        return uniqueId;
    }
}
