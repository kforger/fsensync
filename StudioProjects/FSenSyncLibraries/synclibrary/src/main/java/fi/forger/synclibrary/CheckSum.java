/*
FSenSync Common Libraries
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package fi.forger.synclibrary;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.security.MessageDigest;

public class CheckSum
{
    private MessageDigest myMd5;

    public CheckSum()
    {
        try
        {
            myMd5 = MessageDigest.getInstance("MD5");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void update(byte[] bytes, int count)
    {
        try
        {
            myMd5.update(bytes, 0, count);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public String getCheckSum()
    {
        String result = "";

        byte[] csum = myMd5.digest();

        for (int i = 0; i < csum.length; i++)
        {
            Byte b = csum[i];
            result += Integer.toHexString(b.intValue());
        }

        return result;
    }

    public static String getCheckSum(String filename)
    {
        String result = "";

        try {
            InputStream in =  new FileInputStream(filename);

            byte[] bytes = new byte[1024];
            MessageDigest md5 = MessageDigest.getInstance("MD5");

            int count;

            while ((count = in.read(bytes)) > 0)
            {
                md5.update(bytes, 0, count);
            }

            in.close();

            byte[] csum = md5.digest();

            for (int i = 0; i < csum.length; i++)
            {
                Byte b = csum[i];
                result += Integer.toHexString(b.intValue());
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return result;
    }

    public static String getCheckSum(String filename, PrintWriter netWriter, SyncService.TimeoutTask timeout)
    {
        String result = "";

        long time = System.currentTimeMillis();

        try {
            InputStream in =  new FileInputStream(filename);

            byte[] bytes = new byte[1024];
            MessageDigest md5 = MessageDigest.getInstance("MD5");

            int count;
            int counter = 0;

            while ((count = in.read(bytes)) > 0)
            {
                md5.update(bytes, 0, count);
                counter += 1;
                if (counter % 100 == 0)
                {
                    if (time + 4000 < System.currentTimeMillis())
                    {
                        timeout.delaySocketClosingTimeout();
                        time = System.currentTimeMillis();
                        netWriter.println("WAIT FOR IT.");
                        netWriter.flush();
                    }
                }
            }

            in.close();

            byte[] csum = md5.digest();

            for (int i = 0; i < csum.length; i++)
            {
                Byte b = csum[i];
                result += Integer.toHexString(b.intValue());
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return result;
    }
}
