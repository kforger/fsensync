/*
FSenSync Common Libraries
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package fi.forger.remotecontrollibrary;

import android.app.ActionBar;
import android.app.ActivityManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import fi.forger.synclibrary.RecordingService;
import fi.forger.synclibrary.SyncedClient;

public class ControlledActivity extends AppCompatActivity implements SyncedClient
{
    private static int backButtonPresses = 0;
    private static Boolean isOnBackground = false;
    private Timer eventTimer;
    private static Boolean tryToHideActionBar = true;

    public static Boolean readyToExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Locale.setDefault(Locale.ROOT);

        // Keeps screen on and allows setting brightness
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (android.os.Build.VERSION.SDK_INT >= 23)
        {
            tryToHideActionBar = false;
        }

        if (tryToHideActionBar) {
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
            getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        }
        else
        {
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        }

        eventTimer = new Timer(true);
    }

    public void setTryToHideActionBar(Boolean b)
    {
        tryToHideActionBar = b;
    }

    @Override
    protected void onStop()
    {
        isOnBackground = true;
        super.onStop();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        RecordingService.userActivity = this;

        isOnBackground = false;
    }

    @Override
    protected void onUserLeaveHint()
    {
        super.onUserLeaveHint();
    }

    public Boolean getIsOnBackGround()
    {
        return isOnBackground;
    }

    private class BackPressTimeoutTask extends TimerTask {
        ControlledActivity main;

        public BackPressTimeoutTask(ControlledActivity main) {
            this.main = main;
        }

        @Override
        public void run()
        {
            main.backButtonPresses -= 1;
            if (main.backButtonPresses < 0)
            {
                main.backButtonPresses = 0;
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            backButtonPresses++;
            eventTimer.schedule(new BackPressTimeoutTask(this), 6000);
            if(backButtonPresses >= 5)
            {
                readyToExit = true;
                this.finish();
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    public void closeApp()
    {
        readyToExit = true;
        this.finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (tryToHideActionBar) {
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            getWindow().getDecorView().setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {

                actionBar.hide();
            }
        }
        else
        {
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        }
        return super.dispatchTouchEvent(event);
    }

    public Boolean getRecordingServiceReady() { return false; }
    public void setTagsToUi() {}
    public void updateExperimentInfoToUi() {}
    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream)
    {
        metaOutputStream.close();
        return false;
    }
    public Boolean stopRecording() {return false; }
    public void newSyncState(int syncState, int oldState, String syncStateString) {}
    public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {}
    public void cancelStimuli() {}
    public void showExtraMessageOnScreen(String message) {}
    public void enableScreenTouchSound() {}
    public void disableScreenTouchSound() {}

    public String getAppSetting(String setting)
    {
        return "INVALID_SETTING";
    }
    public void setAppSetting(String setting, String value) {}

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean doesPostSync()
    {
        return false;
    }
    public int[] giveSensorHealth() { return new int[0]; }
}
