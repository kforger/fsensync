/*
FSenSync EDA
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import fi.forger.remotecontrollibrary.ControlledActivity;
import fi.forger.synclibrary.*;

public class MainActivity extends ControlledActivity {

    private Timer myUiTimer;
    private TextView myTextView;
    private TextView textTag1;
    private TextView textTag2;
    private TextView textTag3;
    private TextView unitText;
    private Button buttonScan;
    private Button buttonSelect;
    private Button buttonTags;
    private TextView textExperiment;
    private static int STORAGE_PERMISSION_REQUEST = 1;
    private static final int ENABLE_BLUETOOTH = 1;

    private TextView scanText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);

        startServices();

        // UI stuff
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textBox);
        textTag1 = (TextView) findViewById(R.id.textTag1);
        textTag2 = (TextView) findViewById(R.id.textTag2);
        textTag3 = (TextView) findViewById(R.id.textTag3);
        textExperiment = (TextView) findViewById(R.id.textExperiment);
        unitText = (TextView) findViewById(R.id.unitText);
        setTagsToUi();
        updateExperimentInfoToUi();
        myUiTimer = new Timer(true);
        myUiTimer.schedule(new UiUpdateTask(this), 1000,10);
        myUiTimer.schedule(new UiUpdateTask2(this), 1000,1000);

        buttonScan = (Button) findViewById(R.id.buttonScan);
        buttonScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (EdaService.edaRecorder != null)
                {
                    EdaService.edaRecorder.toggleScan();
                }
            }
        });

        buttonTags = (Button) findViewById(R.id.buttonTags);
        buttonTags.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (EdaService.syncServ != null
                        && EdaService.syncServ.getAppState() != SyncService.STATE_RECORDING_DATA) {
                    Intent i = new Intent(getApplicationContext(), SetTagsActivity.class);
                    startActivity(i);
                }
            }
        });

        buttonSelect = (Button) findViewById(R.id.buttonSelect);
        buttonSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (EdaService.syncServ != null
                        && EdaService.syncServ.getAppState() != SyncService.STATE_RECORDING_DATA) {
                    Intent i = new Intent(getApplicationContext(), SelectRingActivity.class);
                    startActivity(i);
                }
            }
        });

        scanText = (TextView) findViewById(R.id.scanState);
    }

    public void changeInScanState(boolean scanning)
    {
        setScanningTexts(scanning);
    }

    private void setScanningTexts(final boolean scanning)
    {
        if (EdaService.edaRecorder != null) {
            if (EdaService.edaRecorder.bluetoothIsOn()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (scanning) {
                            buttonScan.setText("Stop scan");
                            scanText.setText("Scanning for rings.");
                        } else {
                            buttonScan.setText("Start scan");
                            scanText.setText("Scanning is off.");
                        }
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        buttonScan.setText("Start scan");
                        scanText.setText("Bluetooth is off.");
                    }
                });
            }
        }
        else
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    buttonScan.setText("Start scan");
                    scanText.setText("");
                }
            });
        }
    }

    public void onResume()
    {
        if (EdaService.edaRecorder != null)
        {
            EdaService.edaRecorder.resetAllowedRings();

            setScanningTexts(EdaService.edaRecorder.isScanning());
        }

        super.onResume();
    }

    private void startServices()
    {
        final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (!adapter.isEnabled())
        {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, ENABLE_BLUETOOTH);
        }

        Intent intent = new Intent(MainActivity.this, EdaService.class);
        intent.setAction(RecordingService.MSG_START);
        startService(intent);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_REQUEST);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        if (requestCode == STORAGE_PERMISSION_REQUEST && grantResults.length > 0)
        {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
            {
                System.out.println("Required permission was not granted by user.");
            }
            else
            {
                System.out.println("Permission granted");
                startServices();
            }
        }
    }

    public void setTagsToUi()
    {
        if (EdaService.syncServ != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textTag1.setText(EdaService.syncServ.getTag1());
                    textTag2.setText(EdaService.syncServ.getTag2());
                    textTag3.setText(EdaService.syncServ.getTag3());
                }
            });
        }
        else
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textTag1.setText("");
                    textTag2.setText("");
                    textTag3.setText("");
                }
            });
        }
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (readyToExit)
        {
            myUiTimer.cancel();
            if (EdaService.syncServ != null) {
                EdaService.syncServ.sendClosingAppToServer();
            }
            Intent intent = new Intent(MainActivity.this, EdaService.class);
            intent.setAction(RecordingService.MSG_STOP);
            startService(intent);
            //System.exit(0);
        }
    }


    public void updateExperimentInfoToUi()
    {
        if (EdaService.syncServ != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (EdaService.syncServ.getAppState() < SyncService.STATE_FIRST_SYNC) {
                        textExperiment.setText("Device ID: " + EdaService.syncServ.getDeviceId());
                    }
                    else
                    {
                        String devId = EdaService.syncServ.getDeviceId();
                        int myAppIdInt = EdaService.syncServ.getMyShortId();
                        String myAppId = "";
                        if (myAppIdInt > 0) {
                            myAppId = "" + myAppIdInt;
                        }
                        else
                        {
                            myAppId = "?";
                        }
                        if (devId.equals("-")) {
                            textExperiment.setText("App ID: " + myAppId);
                        }
                        else
                        {
                            textExperiment.setText("App ID: " + myAppId + " (" + devId + ")");
                        }
                    }
                }
            });
        }
    }

    private class UiUpdateTask2 extends TimerTask {
        MainActivity main;

        public UiUpdateTask2(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            if (EdaService.syncServ != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setTagsToUi();
                        updateExperimentInfoToUi();
                    }
                });
            }
        }
    }

    private class UiUpdateTask extends TimerTask {
        MainActivity main;

        public UiUpdateTask(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            if (EdaService.syncServ != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String s = EdaService.syncServ.getStateString();
                        if (EdaService.syncServ.getAppState() > SyncService.STATE_FIRST_SYNC) {
                            String time = "\n" + (SystemClock.elapsedRealtimeNanos() + EdaService.syncServ.getOffsetNanosToServer());
                            s = s + time.substring(0, Math.max(0, time.length() - 6));
                        }
                        myTextView.setText(s);
                    }
                });
            }
        }
    }

    public void setRingText(final String text)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                unitText.setText(text);
            }
        });
    }

}
