/*
FSenSync EDA
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import android.bluetooth.BluetoothGatt;

import java.io.PrintWriter;


public class EdaRing {

    public BluetoothGatt bleGatt;
    public int batteryLevel;
    public String statusText;
    public long lastReceiveTime;
    public float meanFps;
    public double lastAcc;
    public double lastSkinResistance;
    public long scanTime;
    public int lastStatus;
    public long anyContactTime;
    public String oldAddress;
    public int lastRssi;
    public int sampleRateQueryCount;
    public int batteryQueryCounter;

    public String ringTag;

    public boolean selectedByUser;

    public PrintWriter output;

    private int ringState;

    public static final int RING_STATE_NOT_SELECTED = -1;
    public static final int RING_STATE_INITIAL = 0;
    public static final int RING_STATE_SELECTED = 1;
    public static final int RING_STATE_CONNECTED = 2;
    public static final int RING_STATE_SERVICES_DISCOVERED = 3;
    public static final int RING_STATE_SAMPLE_RATE_OK = 4;
    public static final int RING_STATE_STREAM_REQUESTED = 5;
    public static final int RING_STATE_STREAM_RECEIVED = 6;


    public EdaRing()
    {
        batteryLevel = -1;
        statusText = "";
        lastReceiveTime = Long.MIN_VALUE;
        meanFps = 0.0f;
        lastAcc = 0;
        lastSkinResistance = -1;
        scanTime = Long.MIN_VALUE;
        lastStatus = -1;
        lastRssi = -1;
        oldAddress = "";
        sampleRateQueryCount = 0;
        batteryQueryCounter = 0;
        setState(RING_STATE_INITIAL);
        ringTag = "";
        selectedByUser = false;
    }

    public int getState()
    {
        return ringState;
    }

    public void setState(int state, boolean verbose)
    {
        ringState = state;
        if (verbose)
        {
            switch (state)
            {
                case RING_STATE_NOT_SELECTED:
                    System.out.println("STATE: Ring not selected");
                    break;
                case RING_STATE_INITIAL:
                    System.out.println("STATE: Initial state");
                    break;
                case RING_STATE_SELECTED:
                    System.out.println("STATE: Ring selected");
                    break;
                case RING_STATE_CONNECTED:
                    System.out.println("STATE: Ring connected");
                    break;
                case RING_STATE_SERVICES_DISCOVERED:
                    System.out.println("STATE: Services discovered");
                    break;
                case RING_STATE_SAMPLE_RATE_OK:
                    System.out.println("STATE: Sample rate correct");
                    break;
                case RING_STATE_STREAM_REQUESTED:
                    System.out.println("STATE: Stream requested");
                    break;
                case RING_STATE_STREAM_RECEIVED:
                    System.out.println("STATE: Stream received");
                    break;
                default:
                    System.out.println("STATE: Unknown ring state");
                    break;
            }
        }
    }

    public void setState(int state)
    {
        setState(state, true);
    }

}
