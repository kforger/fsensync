/*
FSenSync EDA
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fi.forger.remotecontrollibrary.ControlledActivity;

public class SelectRingActivity  extends AppCompatActivity {

    private Button buttonMakeSelection;
    private Button buttonCancel;
    private ArrayList<MyCheckBox> checkBoxes;
    private String[] rings; // TODO: poista merkkijonoilla kikkailu


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_ring);

        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ringBoxes);

        checkBoxes = new ArrayList<MyCheckBox>();

        if (EdaService.edaRecorder != null) {

            ArrayList<EdaRing> devices = EdaService.edaRecorder.getRings();

            if (devices.size() > 0) {
                for (EdaRing dev : devices)
                {
                    MyCheckBox ch = new MyCheckBox(this, dev.oldAddress);
                    ch.setText(dev.oldAddress);
                    if (dev.selectedByUser)
                    {
                        ch.setChecked(true);
                    }
                    linearLayout.addView(ch);
                    checkBoxes.add(ch);
                }
            }
            else
            {
                TextView t = new TextView(this);
                t.setText("No rings found in scan.");
                linearLayout.addView(t);
            }
        }

        buttonMakeSelection = (Button) findViewById(R.id.buttonMakeSelection);
        buttonMakeSelection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (EdaService.edaRecorder != null) {

                    ArrayList<EdaRing> devices = EdaService.edaRecorder.getRings();

                    if (devices != null) {
                        for (MyCheckBox box : checkBoxes)
                        {
                            for (EdaRing dev : devices)
                            {
                                if (dev.oldAddress.equals(box.id))
                                {
                                    dev.selectedByUser = box.isChecked();
                                }
                            }
                        }
                    }
                }

                finish();
            }
        });


        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }

    private class MyCheckBox extends AppCompatCheckBox
    {
        public String id;

        MyCheckBox(Context context, String id)
        {
            super(context);
            this.id = id;
        }
    }


}
