/*
FSenSync EDA
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputFilter;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class SetTagsActivity extends AppCompatActivity {

    private Button buttonMakeSelection;
    private Button buttonCancel;
    private ArrayList<MyEditText> textBoxes;

    public static EdaService.EdaRecorder edaRec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_tags);

        edaRec = EdaService.edaRecorder;

        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.childTag);

        textBoxes = new ArrayList<MyEditText>();

        if (edaRec != null && edaRec.getRings() != null) {

            ArrayList<EdaRing> devices = edaRec.getRings();

            if (devices.size() > 0) {
                for (EdaRing dev : devices)
                {
                    if (dev.selectedByUser) {
                        LinearLayout smallLayout = new LinearLayout(this);
                        TextView label = new TextView(this);
                        label.setText(dev.oldAddress);
                        smallLayout.setOrientation(LinearLayout.HORIZONTAL);
                        smallLayout.setGravity(Gravity.CENTER_HORIZONTAL);
                        MyEditText eText = new MyEditText(this, dev.oldAddress);
                        eText.setText(dev.ringTag);
                        textBoxes.add(eText);
                        eText.setMinEms(5);
                        eText.setMaxEms(5);
                        eText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});

                        smallLayout.addView(label);
                        smallLayout.addView(eText);
                        linearLayout.addView(smallLayout);
                    }
                }
            } else {
                TextView t = new TextView(this);
                t.setText("No devices selected.");
                linearLayout.addView(t);
            }
        }

        buttonMakeSelection = (Button) findViewById(R.id.buttonSetTags);
        buttonMakeSelection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (edaRec != null) {

                    ArrayList<EdaRing> devices = edaRec.getRings();

                    if (devices != null) {
                        for (MyEditText text : textBoxes)
                        {
                            for (EdaRing dev : devices)
                            {
                                if (dev.oldAddress.equals(text.id))
                                {
                                    dev.ringTag = text.getText().toString();
                                }
                            }
                        }
                    }
                }

                finish();
            }
        });


        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }

    private class MyEditText extends AppCompatEditText
    {
        String id;

        MyEditText(Context context, String id)
        {
            super(context);
            this.id = id;
            this.setSingleLine();
        }
    }
}
