/*
FSenSync EDA
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import fi.forger.synclibrary.RecordingService;
import fi.forger.synclibrary.SyncService;
import fi.forger.synclibrary.SyncedClient;

public class EdaService extends RecordingService implements SyncedClient {

    private static final String clientType = "EDA";
    private static final int majorVersion = BuildConfig.MAJOR_VERSION;

    public static EdaRecorder edaRecorder;

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId)
    {
        final EdaService parent = this;

        Runnable runner = new Runnable()
        {
            public void run()
            {
                if (intent.getAction().equals(RecordingService.MSG_START)) {

                    if (syncServ == null || syncServ.getAppState() <= syncServ.STATE_ERROR)
                    {
                        syncServ = new SyncService(parent, parent, clientType, majorVersion);
                    }

                    if (edaRecorder == null && syncServ.getAppState() > SyncService.STATE_ERROR)
                    {
                        edaRecorder = new EdaRecorder(parent);
                    }

                    Intent activityIntent = new Intent(parent, MainActivity.class);
                    PendingIntent resultPendingIntent =
                            PendingIntent.getActivity(parent, 0, activityIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT);

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(parent)
                                    .setSmallIcon(R.drawable.ic_stat_name)
                                    .setContentTitle("FSenSync EDA")
                                    .setContentText("Service is currently running.")
                                    .setContentIntent(resultPendingIntent);

                    startForeground(123456, mBuilder.build());
                }
                else if (intent.getAction().equals(RecordingService.MSG_STOP))
                {
                    closeApp();
                }
            }
        };

        Thread thread = new Thread(runner);
        thread.start();

        return Service.START_NOT_STICKY;
    }

    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream)
    {
        Boolean ok = true;

        try
        {
            metaOutputStream.println("App version: " + BuildConfig.VERSION_CODE);
            for (EdaRing dev : edaRecorder.getRings())
            {
                if (dev.selectedByUser) {
                    metaOutputStream.println("Tag for unit: \"" + dev.oldAddress.replace(":", "") + "\""
                            + " = \"" + dev.ringTag + "\"");
                }
            }
            metaOutputStream.close();
        }
        catch (Exception ex)
        {
            System.out.println("Error when writing meta output file.");
        }

        if (ok)
        {
            ok = ok && edaRecorder.startSavingData(fileBaseName);
        }

        return ok;
    }

    public Boolean stopRecording()
    {
        edaRecorder.stopSavingData();
        return true;
    }

    public int[] giveSensorHealth()
    {
        return edaRecorder.giveSensorHealth();
    }

    public Boolean getRecordingServiceReady()
    {
        return edaRecorder.readyToRecord();
    }

    public void sensorStateChanged()
    {
        if (syncServ != null) {
            syncServ.sensorStateChanged();
        }
    }

    public class EdaRecorder
    {
        private NumberFormat formatter;
        EdaService main;

        private BluetoothAdapter bluetoothAdapter;
        private ArrayList<EdaRing> rings;
        private boolean scanning;

        private static final int SAMPLE_RATE = 3;

        private final UUID mmServiceUUID =
                UUID.fromString("dd499b70-e4cd-4988-a923-a7aab7283f8e");
        private final UUID streamingCharacteristicUUID =
                UUID.fromString("a0956420-9bd2-11e4-bd06-0800200c9a66");
        // Defined by the BLE standard
        private final UUID clientCharacteristicConfigurationUUID =
                UUID.fromString("00002902-0000-1000-8000-00805F9B34FB");

        private final UUID Battery_Service_UUID = UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb");
        private UUID Battery_Level_UUID = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
        private UUID SampleRate_UUID = UUID.fromString("2c1531b0-4bd2-11e5-b970-0800200c9a66");

        private Timer timer;
        private Timer stopTimer;

        private Boolean scanIsNeeded;

        public EdaRecorder(EdaService main)
        {
            this.main = main;
            formatter = NumberFormat.getInstance(Locale.ROOT);
            formatter.setGroupingUsed(false);
            formatter.setMinimumIntegerDigits(1);
            formatter.setMinimumFractionDigits(1);
            formatter.setMaximumFractionDigits(4);

            scanIsNeeded = false;

            rings = new ArrayList<EdaRing>();
            timer = new Timer(true);
            stopTimer = new Timer(true);

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            timer.schedule(new StartScanTask(), 0, 2000);
            timer.schedule(new RingConnectionTask(this), 1000, 1000);
            timer.schedule(new UiTask(), 1000, 50);

            if (userActivity != null) {
                MainActivity user = (MainActivity) userActivity;
                user.changeInScanState(scanning);
            }
        }

        public ArrayList<EdaRing> getRings()
        {
            return rings;
        }

        public boolean readyToRecord()
        {
            boolean ready = false;

            if (rings != null && rings.size() > 0)
            {
                for (EdaRing dev : rings) {
                    if (dev.selectedByUser) {
                        ready = true;
                    }
                }
            }
            return ready;
        }

        public void resetAllowedRings()
        {
            int numNewSelections = 0;

            if (userActivity != null) {
                if (rings != null && rings.size() > 0) {
                    for (int i = 0; i < rings.size(); i++) {
                        if (!rings.get(i).selectedByUser)
                        {
                            if (rings.get(i).getState() >= EdaRing.RING_STATE_SELECTED)
                            {
                                rings.get(i).setState(EdaRing.RING_STATE_NOT_SELECTED);
                                if (rings.get(i).bleGatt != null) {
                                    enableStreaming(rings.get(i).bleGatt, false);
                                    rings.get(i).bleGatt.close();
                                }

                            }
                        }
                        else
                        {
                            if (rings.get(i).getState() < EdaRing.RING_STATE_SELECTED)
                            {
                                numNewSelections += 1;
                            }
                        }
                    }
                }
            }

            if (numNewSelections > 0)
            {
                startScanning();
            }

            sensorStateChanged();
        }

        public boolean isScanning()
        {
            return scanning;
        }

        public void toggleScan()
        {
            if (scanning)
            {
                stopScanning();
            }
            else
            {
                startScanning();
            }
        }

        public boolean bluetoothIsOn()
        {
            if (bluetoothAdapter != null && bluetoothAdapter.isEnabled())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public EdaRing findRing(BluetoothGatt gatt)
        {
            for (int i = 0; i < rings.size(); i++) {
                if (gatt.getDevice().getAddress().equals(rings.get(i).bleGatt.getDevice().getAddress())) {

                    if (gatt != rings.get(i).bleGatt)
                    {
                        rings.get(i).bleGatt = gatt;
                    }

                    return rings.get(i);
                }
            }

            return null;
        }

        public boolean startSavingData(String fileBaseName)
        {
            boolean ok = true;

            for (EdaRing dev : rings) {
                if (dev.selectedByUser) {

                    PrintWriter outputStream = null;

                    try {
                        File file = new File(fileBaseName + "" + dev.oldAddress.replace(":", "") + "_EDA.csv");
                        if (file.exists()) {
                            if (syncServ != null) {
                                syncServ.sendErrorToServer("Refused to overwrite old recording.");
                            }
                            return false;
                        }
                        file.createNewFile();
                        outputStream = new PrintWriter(file, "UTF-8");
                        try {
                            String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,Status,MoodNumber,SkinResistance,Xacceleration,Yacceleration,Zacceleration";
                            outputStream.println(header);
                            outputStream.flush();
                        } catch (Exception ex) {
                            System.out.println("Error while writing data to file.");
                        }
                    } catch (Exception ex) {
                        ok = false;
                        System.out.println("Error when opening output file.");
                    }

                    if (ok)
                    {
                        dev.output = outputStream;
                    }
                }
            }

            return ok;
        }

        public void stopSavingData()
        {
            for (EdaRing dev : rings) {
                if (dev.output != null)
                {
                    dev.output.close();
                }
            }
        }

        public boolean enableStreaming(BluetoothGatt gatt, boolean enable)
        {
            boolean success = false;
            BluetoothGattService mmService = gatt.getService(mmServiceUUID);
            if (mmService != null) {
                BluetoothGattCharacteristic streamingCharacteristic = mmService.getCharacteristic(streamingCharacteristicUUID);
                if (streamingCharacteristic != null) {
                    boolean success1 = gatt.setCharacteristicNotification(streamingCharacteristic, true);
                    BluetoothGattDescriptor cccDescriptor = streamingCharacteristic.getDescriptor(clientCharacteristicConfigurationUUID);
                    if (enable) {
                        cccDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    }
                    else
                    {
                        cccDescriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                    }
                    success = gatt.writeDescriptor(cccDescriptor);
                }
            }

            return success;
        }


        private final BluetoothGattCallback gattCallback = new BluetoothGattCallback()
        {
            @Override
            public void onConnectionStateChange(final BluetoothGatt gatt, int status, int newState) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    EdaRing r = findRing(gatt);
                    try {
                        r.setState(EdaRing.RING_STATE_CONNECTED);
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                    // Starting service discovery immediately
                    gatt.discoverServices();
                    //System.out.println("Connected:" + gatt.getDevice().getName());
                }

                if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    System.out.println("Disconnected:" + gatt.getDevice().getName());
                }

                if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                    System.out.println("Disconnecting:" + gatt.getDevice().getName());
                }
            }

            @Override
            public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
                //enableStreamingNotification(gatt);

                EdaRing ring = findRing(gatt);
                try {
                    ring.setState(EdaRing.RING_STATE_SERVICES_DISCOVERED);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }

                ring.anyContactTime = SystemClock.elapsedRealtimeNanos();

            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status)
            {
                EdaRing r = findRing(gatt);

                if (r != null) {

                    boolean found = false;
                    if (characteristic.getUuid().equals(Battery_Level_UUID)) {
                        found = true;
                        r.batteryLevel = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                        r.anyContactTime = SystemClock.elapsedRealtimeNanos();
                    }

                    if (characteristic.getUuid().equals(SampleRate_UUID)) {
                        found = true;
                        int sampleRate = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                        System.out.println("Sample rate for ring " + r.oldAddress + ": " + sampleRate);
                        if (sampleRate == SAMPLE_RATE) {
                            r.setState(EdaRing.RING_STATE_SAMPLE_RATE_OK);
                        }
                        else
                        {
                            boolean success = false;
                            BluetoothGattService mmService = r.bleGatt.getService(mmServiceUUID);
                            if (mmService != null) {
                                BluetoothGattCharacteristic sampleRateCharacteristic =
                                        mmService.getCharacteristic(SampleRate_UUID);
                                byte[] value = new byte[1];
                                value[0] = (byte) 4;
                                sampleRateCharacteristic.setValue(value);
                                success = r.bleGatt.writeCharacteristic(sampleRateCharacteristic);
                            }
                            if (success) {
                                System.out.println("Setting sample rate success " + r.oldAddress);
                            } else {
                                System.out.println("Setting sample rate failure " + r.oldAddress);
                            }
                        }
                    }

                    if (found == false) {
                        System.out.println(" Other onCharacteristicRead()" + characteristic.getUuid().toString());
                    }
                }
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
            {
                long nanoStamp = SystemClock.elapsedRealtimeNanos();
                if (characteristic.getUuid().equals(streamingCharacteristicUUID))
                {
                    EdaRing r = findRing(gatt);

                    if (r != null)
                    {
                        byte[] payload = characteristic.getValue();
                        // Decode payload
                        int status = payload[0] & 0xff;
                        int mm = payload[1] & 0xff;
                        // Instant EDA value is in payload bytes 2 and 3 in big-endian format
                        int instant = ((payload[2] & 0xff) << 8) | (payload[3] & 0xff);
                        // Acceleration in x, y and z directions
                        int ax = payload[4] & 0xff;
                        int ay = payload[5] & 0xff;
                        int az = payload[6] & 0xff;
                        // Acceleration magnitude
                        double aax = (ax - 127.0) * 2.0 * 9.81 / 127.0;
                        double aay = (ay - 127.0) * 2.0 * 9.81 / 127.0;
                        double aaz = (az - 127.0) * 2.0 * 9.81 / 127.0;
                        double a = Math.sqrt(aax * aax + aay * aay + aaz * aaz);

                        PrintWriter outputStream = null;

                        for (EdaRing dev : rings) {
                            if (gatt.getDevice().getAddress().equals(dev.oldAddress))
                            {
                                outputStream = dev.output;
                            }
                        }

                        if (outputStream != null && syncServ != null && syncServ.getAppState() == SyncService.STATE_RECORDING_DATA) {
                            long reportedTimeStamp = (nanoStamp + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
                            long reportedUpperBound = syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
                            long reportedLowerBound = syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;
                            String output = reportedTimeStamp + ",";
                            output = output + reportedUpperBound + "," + reportedLowerBound;
                            output = output + "," + status;
                            output = output + "," + mm;
                            output = output + "," + instant;
                            output = output + "," + formatter.format((ax - 127.0) * 2.0 * 9.81 / 127.0);
                            output = output + "," + formatter.format((ay - 127.0) * 2.0 * 9.81 / 127.0);
                            output = output + "," + formatter.format((az - 127.0) * 2.0 * 9.81 / 127.0);
                            try {
                                outputStream.println(output);
                                outputStream.flush(); // Needed as the Android applications are killed suddenly, and without flush last line would be partial
                            } catch (Exception ex) {
                                System.out.println("Error while writing data to file.");
                            }
                        }

                        if (syncServ != null && syncServ.getAppState() >= SyncService.STATE_SYNCED_AND_READY && syncServ.getStreamState())
                        {
                            int reportedTimeStamp = (int)(
                                    ((nanoStamp + syncServ.getOffsetNanosToServerStreaming()) / SyncService.NANOS_IN_MILLI)
                                            - syncServ.getStreamingZeroTime());
                            ArrayList<Object> data = new ArrayList<Object>();
                            data.add(reportedTimeStamp);
                            data.add(gatt.getDevice().getAddress());
                            data.add(status);
                            data.add(mm);
                            data.add(instant);
                            data.add((float)((ax - 127.0) * 2.0 * 9.81 / 127.0));
                            data.add((float)((ay - 127.0) * 2.0 * 9.81 / 127.0));
                            data.add((float)((az - 127.0) * 2.0 * 9.81 / 127.0));
                            Boolean ok = syncServ.streamData(data);
                        }

                        float elapsedTime= (float)(nanoStamp - r.lastReceiveTime);
                        if (r.lastReceiveTime > Long.MIN_VALUE && elapsedTime > 1f) {
                            r.meanFps = (r.meanFps * 0.8f)
                                    + (((float)SyncService.NANOS_IN_SECOND / elapsedTime) * 0.2f);
                        }
                        else
                        {
                            r.meanFps = 0.0f;
                        }
                        r.lastReceiveTime = nanoStamp;

                        r.anyContactTime = nanoStamp;
                        r.lastAcc = a;
                        r.lastSkinResistance = instant;
                        r.lastStatus = status;

                        if (r.getState() != EdaRing.RING_STATE_STREAM_RECEIVED)
                        {
                            r.setState(EdaRing.RING_STATE_STREAM_RECEIVED);
                        }
                    }
                }
                else
                {
                    System.out.println("Got characteristic: " + characteristic.getUuid().toString());
                }
            }
        };

        private final BluetoothAdapter.LeScanCallback scanCallback =
                new BluetoothAdapter.LeScanCallback() {
                    @Override
                    public synchronized void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

                        // Finding out if the ring has been previously connected
                        boolean isNewDevice = true;
                        long timeNow = SystemClock.elapsedRealtimeNanos();
                        for (int i = 0; i < rings.size(); i++) {
                            if (device.getAddress().equals(rings.get(i).oldAddress))
                            {
                                isNewDevice = false;
                                rings.get(i).lastRssi = rssi;

                                if (rings.get(i).selectedByUser && rings.get(i).getState() == EdaRing.RING_STATE_NOT_SELECTED)
                                {
                                    rings.get(i).bleGatt = device.connectGatt(EdaRecorder.this.main, true, gattCallback);
                                    rings.get(i).setState(EdaRing.RING_STATE_SELECTED);
                                    rings.get(i).scanTime = SystemClock.elapsedRealtimeNanos();
                                    rings.get(i).anyContactTime = SystemClock.elapsedRealtimeNanos();
                                    rings.get(i).oldAddress = device.getAddress();
                                    rings.get(i).lastRssi = rssi;
                                }

                                if (rings.get(i).selectedByUser) {
                                    System.out.println("Advertisement from a previously connected ring. " + rings.get(i).oldAddress);
                                    if (timeNow > rings.get(i).scanTime + (1 * SyncService.NANOS_IN_SECOND))
                                    {
                                        System.out.println("Trying to connect again. " + rings.get(i).oldAddress);
                                        rings.get(i).bleGatt = device.connectGatt(EdaRecorder.this.main, true, gattCallback);
                                    }
                                }
                                else
                                {
                                    //System.out.println("Advertisement from a not allowed ring.");
                                }
                            }
                        }

                        // Adding new devices to the ring list
                        if (isNewDevice)
                        {
                            EdaRing r = new EdaRing();
                            r.setState(EdaRing.RING_STATE_NOT_SELECTED);
                            r.scanTime = SystemClock.elapsedRealtimeNanos();
                            r.anyContactTime = SystemClock.elapsedRealtimeNanos();
                            r.oldAddress = device.getAddress();
                            r.lastRssi = rssi;
                            rings.add(r);
                        }
                    }

                };

        private void startScanning() {

            if (!bluetoothAdapter.isEnabled())
            {
                scanning = false;
                if (userActivity != null) {
                    MainActivity main = (MainActivity) userActivity;
                    main.changeInScanState(scanning);
                }
                return;
            }

            if (scanning)
            {
                stopTimer.cancel();
                stopTimer = new Timer(true);
                stopTimer.schedule(new StopScanTask(), 120000);
                return;
            }
            else
            {
                scanning = true;

                if (userActivity != null) {
                    MainActivity main = (MainActivity) userActivity;
                    main.changeInScanState(scanning);
                }

                scanIsNeeded = false;

                // Start scanning for devices that support the Moodmetric service
                bluetoothAdapter.startLeScan(new UUID[]{mmServiceUUID}, scanCallback);

                stopTimer.cancel();
                stopTimer = new Timer(true);
                stopTimer.schedule(new StopScanTask(), 120000);
            }
        }

        private void stopScanning() {
            if (!scanning) return;
            scanning = false;
            if (userActivity != null) {
                MainActivity main = (MainActivity) userActivity;
                main.changeInScanState(scanning);
            }
            bluetoothAdapter.stopLeScan(scanCallback);

            for (int i = rings.size() - 1; i >= 0; i--) {
                if (!rings.get(i).selectedByUser)
                {
                    if (rings.get(i).bleGatt != null)
                    {
                        rings.get(i).bleGatt.close();
                    }

                    rings.remove(i);
                }
            }

            stopTimer.cancel();
            stopTimer = new Timer(true);
        }

        public void closeAllGatts()
        {
            if (scanning) stopScanning();
            if (rings != null) {
                for (int i = 0; i < rings.size(); i++) {
                    if (rings.get(i).bleGatt != null) {
                        if (enableStreaming(rings.get(i).bleGatt, false))
                        {
                            System.out.println("Stopping stream success " + rings.get(i).oldAddress);
                        }
                        else
                        {
                            System.out.println("Stopping stream failure " + rings.get(i).oldAddress);
                        }
                        rings.get(i).bleGatt.close();
                    }
                }
            }
        }

        private class UiTask extends TimerTask {

            public UiTask()
            {

            }

            @Override
            public void run() {

                String s = "";

                if (!bluetoothAdapter.isEnabled())
                {
                    stopScanning();
                }

                if (userActivity != null) {
                    MainActivity main = (MainActivity) userActivity;
                    main.changeInScanState(scanning);
                }

                Locale l = Locale.ROOT;

                if (rings.size() > 0) {

                    s = "Addr  Acc SkinR Bat Fps Tag  \n";
                    long time = SystemClock.elapsedRealtimeNanos();
                    for (int i = 0; i < rings.size(); i++) {

                        EdaRing r = rings.get(i);

                        String tag = new String(r.ringTag);
                        if (tag.length() > 5)
                        {
                            tag = tag.substring(0, 5);
                        }
                        if (tag.length() < 5)
                        {
                            tag = tag + new String(new char[5-tag.length()]).replace("\0", " ");
                        }

                        if (r.oldAddress.length() > 0) {
                            s = s + r.oldAddress.substring(r.oldAddress.length() - 5, r.oldAddress.length()).replace(":", "")
                            + " ";
                        }
                        else
                        {
                            s = s + "???? ";
                        }

                        switch (r.getState())
                        {
                            case EdaRing.RING_STATE_NOT_SELECTED:
                                s = s + "Not selected, Rssi: " + r.lastRssi + "\n";
                                break;
                            case EdaRing.RING_STATE_INITIAL:
                                s = s + "Initial state\n";
                                break;
                            case EdaRing.RING_STATE_SELECTED:
                                s = s + "Ring selected, Rssi: " + r.lastRssi +  " " + tag + "\n";
                                break;
                            case EdaRing.RING_STATE_CONNECTED:
                                s = s + "Processing new connection " + tag + "\n";
                                break;
                            case EdaRing.RING_STATE_SERVICES_DISCOVERED:
                                s = s + "Services discovered " + tag + "\n";
                                break;
                            case EdaRing.RING_STATE_SAMPLE_RATE_OK:
                                s = s + "Sample rate correct " + tag + "\n";
                                break;
                            case EdaRing.RING_STATE_STREAM_REQUESTED:
                                s = s + "Stream requested " + tag + "\n";
                                break;
                            case EdaRing.RING_STATE_STREAM_RECEIVED:

                                float fps = 0.0f;
                                float elapsedTime = (float)(SystemClock.elapsedRealtimeNanos() - r.lastReceiveTime);
                                if (r.lastReceiveTime > Long.MIN_VALUE && elapsedTime > SyncService.NANOS_IN_SECOND) {
                                    fps = 0.0f;
                                }
                                else
                                {
                                    fps = r.meanFps;
                                }

                                s = s //+ String.format(l, "%2d", (Integer)(r.lastStatus))
                                     + String.format(l, "%4.1f", r.lastAcc)
                                    + " " + String.format(l, "%5.0f", r.lastSkinResistance)
                                    + " " + String.format(l, "%3d", (Integer)(r.batteryLevel))
                                    + " " + String.format(l, "%1.1f", fps)
                                    + " " + tag + "\n";
                                break;
                            default:
                                System.out.println("Unknown ring state!");
                                break;
                        }

                    }
                }
                else
                {
                    s = "No rings connected.";
                }


                if (userActivity != null)
                {
                    MainActivity main = (MainActivity) userActivity;
                    main.setRingText(s);
                }

            }
        }

        public int[] giveSensorHealth()
        {
            int selectedCount = 0;
            for (int i = 0; i < rings.size(); i++)
            {
                EdaRing dev = rings.get(i);
                if (dev.getState() >= EdaRing.RING_STATE_SELECTED)
                {
                    selectedCount += 1;
                }
            }

            int[] health = new int[selectedCount];

            int index = 0;
            for (int i = 0; i < rings.size(); i++)
            {
                EdaRing dev = rings.get(i);
                if (dev.getState() >= EdaRing.RING_STATE_SELECTED)
                {
                    health[index] = SyncService.SENSOR_HEALTH_INITIAL;
                    if (dev.getState() == EdaRing.RING_STATE_STREAM_RECEIVED)
                    {
                        float elapsedTime = (float)(SystemClock.elapsedRealtimeNanos() - dev.lastReceiveTime);
                        if (dev.lastReceiveTime > Long.MIN_VALUE && elapsedTime > SyncService.NANOS_IN_SECOND)
                        {
                            health[index] = SyncService.SENSOR_HEALTH_BAD;
                        }
                        else
                        {
                            if (dev.meanFps > 2.5)
                            {
                                health[index] = SyncService.SENSOR_HEALTH_GOOD;
                            }
                            else
                            {
                                health[index] = SyncService.SENSOR_HEALTH_BAD;
                            }
                        }
                    }
                    index += 1;
                }
            }

            return health;
        }

        private class RingConnectionTask extends TimerTask {

            private EdaRecorder parent;

            public RingConnectionTask(EdaRecorder parent) {
                this.parent = parent;
            }

            @Override
            public void run() {
                for (int i = 0; i < rings.size(); i++) {
                    EdaRing r = rings.get(i);
                    switch (r.getState())
                    {
                        case EdaRing.RING_STATE_NOT_SELECTED:
                            break;
                        case EdaRing.RING_STATE_INITIAL:
                            break;
                        case EdaRing.RING_STATE_SELECTED:
                            break;
                        case EdaRing.RING_STATE_CONNECTED:
                            break;
                        case EdaRing.RING_STATE_SERVICES_DISCOVERED:

                            boolean sampleRateReadSuccess = false;
                            BluetoothGattService bService2 = rings.get(i).bleGatt.getService(mmServiceUUID);
                            if (bService2 != null) {
                                BluetoothGattCharacteristic bCharacteristic = bService2.getCharacteristic(SampleRate_UUID);
                                sampleRateReadSuccess = rings.get(i).bleGatt.readCharacteristic(bCharacteristic);
                            }
                            r.sampleRateQueryCount += 1;
                            if (sampleRateReadSuccess) {
                                System.out.println("Sample rate query success " + r.oldAddress);
                            } else {
                                System.out.println("Sample rate query failure " + r.sampleRateQueryCount + " " + r.oldAddress);
                                if (r.sampleRateQueryCount >= 6)
                                {
                                    // The ring does not want to change sample rate, let go forward with current rate
                                    r.setState(EdaRing.RING_STATE_SAMPLE_RATE_OK);
                                }
                            }

                            break;
                        case EdaRing.RING_STATE_SAMPLE_RATE_OK:
                            BluetoothGatt gatt = r.bleGatt;
                            if (gatt != null) {
                                boolean successStream = enableStreaming(gatt, true);
                                if (successStream) {
                                    System.out.println("Stream request success " + r.oldAddress);
                                    r.setState(EdaRing.RING_STATE_STREAM_REQUESTED);
                                } else {
                                    System.out.println("Stream request failure " + r.oldAddress);
                                }
                            }
                            break;
                        case EdaRing.RING_STATE_STREAM_REQUESTED:
                            break;
                        case EdaRing.RING_STATE_STREAM_RECEIVED:
                            if (r.batteryQueryCounter == 0)
                            {
                                boolean success = false;
                                BluetoothGattService bService = rings.get(i).bleGatt.getService(Battery_Service_UUID);
                                if (bService != null) {
                                    BluetoothGattCharacteristic bCharacteristic = bService.getCharacteristic(Battery_Level_UUID);
                                    success = r.bleGatt.readCharacteristic(bCharacteristic);
                                }
                                if (success) {
                                    r.anyContactTime = SystemClock.elapsedRealtimeNanos();
                                    System.out.println("Battery query success " + r.oldAddress);
                                } else {
                                    System.out.println("Battery query failure " + r.oldAddress);
                                }
                            }
                            r.batteryQueryCounter = (1 + r.batteryQueryCounter) % 10;

                            // Scanning as last resort if ring has been gone a long time
                            if (r.anyContactTime < SystemClock.elapsedRealtimeNanos() - (SyncService.NANOS_IN_SECOND * 60)
                                    && r.anyContactTime > SystemClock.elapsedRealtimeNanos() - (SyncService.NANOS_IN_SECOND * 70))
                            {
                                if (!scanning && bluetoothAdapter.isEnabled())
                                {
                                    startScanning();
                                }
                            }

                            break;
                        default:
                            System.out.println("Unknown ring state!");
                            break;
                    }
                }
            }
        }

        private class StartScanTask extends TimerTask {

            public StartScanTask()
            {
            }

            @Override
            public void run() {
                if (!scanning && scanIsNeeded)
                {
                    startScanning();
                }
            }
        }

        private class StopScanTask extends TimerTask {

            public StopScanTask()
            {
            }

            @Override
            public void run() {
                if (scanning)
                {
                    stopScanning();
                }
                this.cancel();
            }
        }

    }

    public void closeApp()
    {
        System.out.println("Service closeApp()");
        if (edaRecorder != null) {
            edaRecorder.closeAllGatts();
        }
        super.closeApp();
    }

    public void onDestroy()
    {
        super.onDestroy();
    }
}
