/*
FSenSync Acceleration
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import java.io.File;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import fi.forger.synclibrary.RecordingService;
import fi.forger.synclibrary.SyncService;
import fi.forger.synclibrary.SyncedClient;

public class AccelerometerService extends RecordingService implements SyncedClient {

    private static final String clientType = "ACC";
    private static final int majorVersion = BuildConfig.MAJOR_VERSION;

    public static AccelerometerRecorder accService;

    private static PrintWriter outputStream;

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId)
    {
        final AccelerometerService parent = this;

        Runnable runner = new Runnable()
        {
            public void run()
            {
                if (intent.getAction().equals(RecordingService.MSG_START)) {

                    if (syncServ == null || syncServ.getAppState() <= syncServ.STATE_ERROR)
                    {
                        syncServ = new SyncService(parent, parent, clientType, majorVersion);
                    }

                    if (accService == null && syncServ.getAppState() > SyncService.STATE_ERROR)
                    {
                        accService = new AccelerometerRecorder(parent);
                    }

                    Intent activityIntent = new Intent(parent, MainActivity.class);
                    PendingIntent resultPendingIntent =
                            PendingIntent.getActivity(parent, 0, activityIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT);

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(parent)
                                    .setSmallIcon(R.drawable.ic_stat_name)
                                    .setContentTitle("FSenSync Acceleration")
                                    .setContentText("Service is currently running.")
                                    .setContentIntent(resultPendingIntent);

                    startForeground(123456, mBuilder.build());
                }
                else if (intent.getAction().equals(RecordingService.MSG_STOP))
                {
                    closeApp();
                }
            }
        };

        Thread thread = new Thread(runner);
        thread.start();

        return Service.START_NOT_STICKY;
    }

    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream)
    {
        Boolean ok = true;

        try
        {
            metaOutputStream.println("App version: " + BuildConfig.VERSION_CODE);
            metaOutputStream.close();
        }
        catch (Exception ex)
        {
            System.out.println("Error when writing meta output file.");
        }

        try
        {
            File file = new File(fileBaseName + "ACC.csv");
            if (file.exists())
            {
                if (syncServ != null) {
                    syncServ.sendErrorToServer("Refused to overwrite old recording with number " + recordingNumber);
                }
                return false;
            }
            file.createNewFile();
            outputStream = new PrintWriter(file, "UTF-8");
            try {
                String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,Xacceleration,Yacceleration,Zacceleration";
                outputStream.println(header);
                outputStream.flush();
            } catch (Exception ex) {
                System.out.println("Error while writing data to file.");
            }
        }
        catch (Exception ex)
        {
            ok = false;
            System.out.println("Error when opening output file.");
        }
        return ok;
    }

    public Boolean stopRecording()
    {
        outputStream.close();
        return true;
    }

    public Boolean getRecordingServiceReady()
    {
        return accService.accEventSyncReady;
    }

    public void sensorStateChanged()
    {
        if (syncServ != null) {
            syncServ.sensorStateChanged();
        }
    }

    public class AccelerometerRecorder implements SensorEventListener
    {
        private boolean firstEvent = true;
        private SensorManager sensorManager;
        private Sensor accelerometer;
        private long minDifferenceBetweenEventsAndRealNanos;
        private long startOfAccEventSync;
        public Boolean accEventSyncReady;
        private long previousEventTime;
        private long previousStreamedTime;
        private NumberFormat formatter;
        AccelerometerService main;

        public AccelerometerRecorder(AccelerometerService main)
        {
            this.main = main;
            sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, 10000);
            accEventSyncReady = false;
            formatter = NumberFormat.getInstance(Locale.ROOT);
            formatter.setGroupingUsed(false);
            formatter.setMinimumIntegerDigits(1);
            formatter.setMinimumFractionDigits(1);
            formatter.setMaximumFractionDigits(4);
        }

        public void onSensorChanged(SensorEvent event)
        {
            if (syncServ != null && syncServ.getAppState() < SyncService.STATE_RECORDING_DATA)
            {
                long diff = SystemClock.elapsedRealtimeNanos() - event.timestamp;
                if (firstEvent)
                {
                    startOfAccEventSync = SystemClock.elapsedRealtimeNanos();
                    minDifferenceBetweenEventsAndRealNanos = diff;
                    firstEvent = false;
                }
                else if (minDifferenceBetweenEventsAndRealNanos > diff)
                {
                    minDifferenceBetweenEventsAndRealNanos = diff;
                }
            }

            if (accEventSyncReady == false)
            {
                // Here we make sure that we have reserved enough time for the
                // minDifferenceBetweenEventsAndRealNanos to converge.
                if (SystemClock.elapsedRealtimeNanos() > startOfAccEventSync + (SyncService.NANOS_IN_MILLI * 3000))
                {
                    accEventSyncReady = true;
                    main.sensorStateChanged();
                }
            }

            if (syncServ != null && syncServ.getAppState() == SyncService.STATE_RECORDING_DATA)
            {
                if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER)
                {
                    return;
                }
                else
                {
                    if (event.timestamp > previousEventTime + (SyncService.NANOS_IN_MILLI * 2L))
                    {
                        long reportedTimeStamp = (event.timestamp + syncServ.getOffsetNanosToServer() + minDifferenceBetweenEventsAndRealNanos) / SyncService.NANOS_IN_MICRO;

                        long reportedUpperBound = syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
                        long reportedLowerBound = syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;
                        String output = reportedTimeStamp + ",";
                        output = output + reportedUpperBound + "," + reportedLowerBound + ",";
                        output = output + formatter.format(event.values[0]);
                        output = output + "," + formatter.format(event.values[1]);
                        output = output + "," + formatter.format(event.values[2]);
                        try {
                            outputStream.println(output);
                            outputStream.flush(); // Needed as the Android applications are killed suddenly, and without flush last line would be partial
                        } catch (Exception ex) {
                            System.out.println("Error while writing acceleration to file.");
                        }
                    }
                }
            }

            if (syncServ != null && syncServ.getAppState() >= SyncService.STATE_SYNCED_AND_READY && syncServ.getStreamState())
            {
                if (event.timestamp > previousStreamedTime + (SyncService.NANOS_IN_SECOND / syncServ.getMaxMulticastFps()))
                {
                    previousStreamedTime = event.timestamp;
                    int reportedTimeStamp = (int)(
                            ((event.timestamp + syncServ.getOffsetNanosToServerStreaming() + minDifferenceBetweenEventsAndRealNanos)
                                    / SyncService.NANOS_IN_MILLI)
                                    - syncServ.getStreamingZeroTime());
                    ArrayList<Object> data = new ArrayList<Object>();
                    data.add(reportedTimeStamp);
                    data.add(event.values[0]);
                    data.add(event.values[1]);
                    data.add(event.values[2]);
                    Boolean ok = syncServ.streamData(data);
                }
            }
            else
            {
                previousStreamedTime = event.timestamp;
            }

            previousEventTime = event.timestamp;
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    }

    public void onDestroy()
    {
        super.onDestroy();
    }
}
