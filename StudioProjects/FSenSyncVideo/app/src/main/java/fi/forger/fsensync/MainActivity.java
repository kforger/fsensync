/*
FSenSync Video
Copyright (C) 2017-2019  Klaus Förger, Förger Analytics, klaus@forger.fi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package fi.forger.fsensync;

import android.Manifest;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;


import java.io.File;
import java.io.PrintWriter;
import java.util.Timer;
import java.util.TimerTask;

import fi.forger.remotecontrollibrary.ControlledActivity;
import fi.forger.synclibrary.SyncService;
import fi.forger.synclibrary.SyncedClient;

public class MainActivity extends ControlledActivity implements SyncedClient
{

    private static final String clientType = "VID";
    private static final int majorVersion = BuildConfig.MAJOR_VERSION;
    private static final String TAG = "VID";

    private static PrintWriter outputStream;

    private Timer myUiTimer;
    private TextView textTag1;
    private TextView textTag2;
    private TextView textTag3;
    private TextView myTextView;
    private static Boolean slowUiUpdates = false;

    private static int PERMISSION_REQUEST = 1;

    public static SyncService syncServ;
    private static Camera2VideoFragment cam;

    private static Boolean recorderReady;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        recorderReady = false;

        startServices();

        // UI stuff
        setContentView(R.layout.activity_camera);
        if (null == savedInstanceState) {
            cam = Camera2VideoFragment.newInstance();
            cam.setParents(this, syncServ);
            getFragmentManager().beginTransaction().replace(R.id.container, cam).commit();
        }

        checkCameraSupport();

        myUiTimer = new Timer(true);
        myUiTimer.schedule(new UiUpdateTask(this), 1000, 10);
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (readyToExit)
        {
            syncServ.sendClosingAppToServer();
            syncServ = null;
            System.exit(0);
        }
    }

    public void setTagsToUi()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (textTag1 == null || textTag2 == null || textTag3 == null)
                {
                    textTag1 = (TextView) findViewById(R.id.textTag1);
                    textTag2 = (TextView) findViewById(R.id.textTag2);
                    textTag3 = (TextView) findViewById(R.id.textTag3);
                }

                if (!(textTag1 == null || textTag2 == null || textTag3 == null))
                {
                    textTag1.setText(syncServ.getTag1());
                    textTag2.setText(syncServ.getTag2());
                    textTag3.setText(syncServ.getTag3());
                }
            }
        });
    }

    private class UiUpdateTask extends TimerTask {
        MainActivity main;

        public UiUpdateTask(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    myTextView = (TextView) findViewById(R.id.textExperiment);
                    String s = syncServ.getStateString();
                    if (syncServ.getAppState() > SyncService.STATE_FIRST_SYNC)
                    {
                        String time = "\n" + (SystemClock.elapsedRealtimeNanos() + syncServ.getOffsetNanosToServer());
                        s = s + time.substring(0, Math.max(0, time.length()-6));
                    }
                    if (myTextView != null)
                    {
                        myTextView.setText(s);
                    }
                }
            });
        }
    }

    private class TagTask extends TimerTask {
        MainActivity main;

        public TagTask(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTagsToUi();
                }
            });
        }
    }

    private class StorageTask extends TimerTask {
        MainActivity main;

        public StorageTask(MainActivity main)
        {
            this.main = main;
        }

        @Override
        public void run()
        {
            long freeSpace = 0L;
            if (syncServ != null && syncServ.getAppState() == SyncService.STATE_RECORDING_DATA) {
                File path = syncServ.getDataDirectory();
                if (path.exists()) {
                    freeSpace = path.getFreeSpace();
                    if (freeSpace < 100L * 1024L * 1024L)
                    {
                        syncServ.clientWantsToStopRecording();
                        syncServ.sendErrorToServer("The device storage is almost full. Stopped recording the video.");
                    }
                }
            }
        }
    }

    private int checkCameraSupport()
    {
        int foundSupport = -1;
        CameraManager manager = (CameraManager)getSystemService(Context.CAMERA_SERVICE);
        try {
            for (int i = 0; i < 1; i++) { // manager.getCameraIdList().length

                String cameraIdS = manager.getCameraIdList()[i];
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraIdS);
                int support = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);

                foundSupport = support;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return foundSupport;
    }

    private void startServices()
    {
        if (syncServ == null || syncServ.getAppState() == SyncService.STATE_ERROR)
        {
            syncServ = new SyncService(this, this, clientType, majorVersion);
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                    PERMISSION_REQUEST);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        for (int i = 0; i < grantResults.length; i++) {
            if (requestCode == PERMISSION_REQUEST) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    System.out.println("Required permission was not granted by user.");
                } else {
                    System.out.println("Permission granted");
                    startServices();
                }
            }
        }
    }

    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream)
    {
        Boolean ok = true;

        try
        {
            metaOutputStream.println("App version: " + BuildConfig.VERSION_CODE);

            switch (checkCameraSupport())
            {
                case CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY:
                    metaOutputStream.println("Camera 2 support: Legacy");
                    break;
                case CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED:
                    metaOutputStream.println("Camera 2 support: Limited");
                    break;
                case CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_FULL:
                    metaOutputStream.println("Camera 2 support: Full");
                    break;
                case CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_3:
                    metaOutputStream.println("Camera 2 support: Full, Level 3");
                    break;
                default:
                    metaOutputStream.println("Camera 2 support: unknown");
                    break;
            }

            long freeSpace = 0L;
            if (syncServ != null) {
                File path = syncServ.getDataDirectory();
                if (path.exists()) {
                    freeSpace = path.getFreeSpace();
                    if (freeSpace < 109L * 1024L * 1024L)
                    {
                        syncServ.sendErrorToServer("The device storage is almost full. Did not start recording the video.");
                        metaOutputStream.println("Error: Device storage was almost full.");
                        metaOutputStream.close();
                        return false;
                    }
                }
            }
            metaOutputStream.close();
        }
        catch (Exception ex)
        {
            System.out.println("Error when writing meta output file.");
        }

        try
        {
            String mp4FileName = fileBaseName + "VID.mp4";
            File file = new File(fileBaseName + "VID.csv");
            if (file.exists())
            {
                ok = false;
                syncServ.sendErrorToServer("Refused to overwrite old recording with number " + recordingNumber);
            }
            else
            {
                file.createNewFile();
                outputStream = new PrintWriter(file, "UTF-8");
                try {
                    String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,FrameNumber,FrameStamp";
                    outputStream.println(header);
                    outputStream.flush();
                } catch (Exception ex) {
                    System.out.println("Error while writing eeg to file.");
                }
                cam.startRecordingVideo(mp4FileName, outputStream);
            }
        }
        catch (Exception ex)
        {
            ok = false;
            System.out.println("Error when opening output file.");
            ex.printStackTrace();
        }
        return ok;
    }

    public Boolean stopRecording()
    {
        if (cam != null) {
            cam.stopRecordingVideo();
        }
        if (outputStream != null) {
            outputStream.close();
        }
        return true;
    }

    public Boolean getRecordingServiceReady()
    {
        return recorderReady;
    }

    public void setRecorderReady()
    {
        recorderReady = true;
        syncServ.sensorStateChanged();
        syncServ.reportToServer(syncServ.currentServerIp());
    }

    public void setRecorderNotReady()
    {
        recorderReady = false;
        syncServ.sensorStateChanged();
        syncServ.reportToServer(syncServ.currentServerIp());
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if (cam.mTextureView != null && cam.mTextureView.getSurfaceTexture() == null)
        {
            cam = Camera2VideoFragment.newInstance();
            cam.setParents(this, syncServ);
            getFragmentManager().beginTransaction().replace(R.id.container, cam).commit();

            myUiTimer = new Timer(true);
            myUiTimer.schedule(new UiUpdateTask(this), 1000, 10);
            myUiTimer.schedule(new TagTask(this), 1000);
            myUiTimer.schedule(new StorageTask(this), 2000, 2000);
        }
    }

    public void onPause() {

        if (syncServ.getAppState() == SyncService.STATE_RECORDING_DATA) {
            outputStream.close();
        }
        recorderReady = false;
        syncServ.sensorStateChanged();

        if (readyToExit)
        {
            syncServ.sendClosingAppToServer();
            syncServ = null;
            System.exit(0);
        }

        super.onPause();
    }
}
