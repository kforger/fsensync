FSenSync is a software package that enables synchronized recording and streaming of
sensor data from affordable Android devices (smart watches, phones and tablets).
The software aims to support designing and running of installations and experiments
that have both artistic and scientific requirements.

Note that the FSenSync distribution includes files under several different licenses.
Check the license files and folders of each project before distributing them.

Contents of the repository:
- EclipseProjects
  * Source codes of the FSenSync desktop programs
- setting_and_app_file_examples
  * Example files for tags and stuff that can be uploaded to the apps
- sketchbook
  * Processing examples
- StudioProjects
  * Source codes of the FSenSync Android apps 
- test_visualization_scripts
  * Python visualization scripts that can be used in testing the apps
- documentation
  * User Manual, readme, etc.

The DesktopPlayer app requires native GStreamer libraries that work on Linux simply
by installing GStreamer 1.x for your distribution. For Windows and Mac, the libraries
should be bundled (in the same way as in the Processing Video library), but they are
currently not included due to licensing issues (the source code is not easily available).

To build the Android apps, you may need to edit the gradle files to fit a new
version of Android Studio (those seem to change in each version).
