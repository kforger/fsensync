class MyRecorder implements SyncedClient {

  SyncService syncServ;
  boolean canRecord = true;
  int lastAppId = -1;
  PrintWriter outputStream;

  MyRecorder() {
    File file = new File(sketchPath());
    SyncService.appTag1 = "RecorderExample";
    SyncService.appTag2 = "my tag 2";
    syncServ = new SyncService(this, "PROC", 12, file);
  }

  public Boolean getRecordingServiceReady() {
    return canRecord;
  };

  public void setTagsToUi() {
    println("Tags from server: " 
      + syncServ.getTag1() + ", "
      + syncServ.getTag2() + ", "
      + syncServ.getTag3());
  };

  public void updateExperimentInfoToUi() {
    if (lastAppId != syncServ.getMyShortId())
    {
      lastAppId = syncServ.getMyShortId();
      println("App id: " + syncServ.getMyShortId());
    }
  };

  public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream) { 
    Boolean ok = true;

    try
    {
      metaOutputStream.println("Extra information: This will appear in the meta file");
      metaOutputStream.close();
    }
    catch (Exception ex)
    {
      println("Error when writing meta output file.");
    }

    try {
      File file = new File(fileBaseName + "PROC.csv");
      if (file.exists()) {
        syncServ.sendErrorToServer("Refused to overwrite old recording with number " + recordingNumber);
        return false;
      }
      file.createNewFile();
      outputStream = new PrintWriter(file, "UTF-8");
      try {
        String header = "Timestamp(microseconds),DriftUpperBound,DriftLowerBound,MouseX,MouseY";
        outputStream.println(header);
        outputStream.flush();
      } 
      catch (Exception ex) {
        System.out.println("Error while writing to file.");
      }
    } 
    catch (Exception ex) {
      ok = false;
      System.out.println("Error when opening output file.");
    }
    return ok;
  };

  public Boolean stopRecording() { 
    outputStream.close();
    return true;
  };

  public void newSyncState(int syncState, int oldState, String stateString) {
    println(stateString);
  };

  public void showExtraMessageOnScreen(String message) {
    println(message);
  };

  public void saveMouseXY(int x, int y)
  {
    long timeNow = System.nanoTime();
    String output = getTimeAndBounds(timeNow) + "," + x + "," + y;
    try {
      outputStream.println(output);
      outputStream.flush();
    } 
    catch (Exception ex) {
      System.out.println("Error while writing to data file.");
    }
  }

  public String getTimeAndBounds(long actualTime)
  {
    long reportedTimeStamp = (actualTime + syncServ.getOffsetNanosToServer()) / SyncService.NANOS_IN_MICRO;
    long reportedUpperBound = syncServ.getErrorNanosUpperBound() / SyncService.NANOS_IN_MICRO;
    long reportedLowerBound = syncServ.getErrorNanosLowerBound() / SyncService.NANOS_IN_MICRO;
    return reportedTimeStamp + "," + reportedUpperBound + "," + reportedLowerBound;
  }

  public void closeApp() { 
    exit();
  };

  // These are needed if you want to do the same things as the Stimulusa app.
  public String getAppSetting(String setting) { 
    return "";
  };
  public void setAppSetting(String setting, String value) {
  };
  public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime) {
  };
  public void cancelStimuli() {
  };
}