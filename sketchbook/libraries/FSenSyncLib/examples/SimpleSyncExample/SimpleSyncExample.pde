/**
 * This is a minimal example of how to get timestamps synced with the FSenSync Server,
 * and through it with other synced apps.
 *
 * Klaus Förger, klaus@forger.fi
 * Get the FSenSync from: http://www.forger.fi/fsensync/
 */

import fi.forger.desktopsynclibrary.*;

SimpleSync sync;

void setup() {
  size(500, 200, P3D);
  frameRate(60);

  // The SimpleSync tries to find an FSenSync Server from the
  // same LAN or WLAN. Some setting files are saved to the sketch folder.
  sync = new SimpleSync(sketchPath(), "SyncExample");
}

void draw() {
  background(0);
  colorMode(HSB, 100);
  strokeWeight(2);
  textSize(32);

  if (sync.isSynced())
  {
    // If the server was found successfully we usually get 
    // less than 10 milliseconds of error even in a WLAN. See the
    // current accuracy from the FSenSync Server.
    long t = sync.serverTimeInMillis();
    text("Time: " + t, 80, 100);
  }
  else
  {
    text("Not yet synced", 80, 100);
  }
}