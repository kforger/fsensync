/*
FSenSync Desktop Libraries
Copyright (C) 2019  Klaus Förger, Förger Analytics, klaus@forger.fi

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package fi.forger.desktopsynclibrary;

import java.io.PrintWriter;

/**
 * 
 * Extend this class to create a recorder class that can be managed by the {@link fi.forger.desktopsynclibrary.SyncService}
 * 
 * @author Klaus Förger, klaus@forger.fi
 *
 */
public interface SyncedClient
{
    /**
     * @return true if all preparations are done and the recording can be started at command from the FSenSync Server
     */
    public Boolean getRecordingServiceReady();
    
    /**
     * This is called when tags have been assigned to the app at the FSenSync Server.
     */
    public void setTagsToUi();
    
    /**
     * Called when the {@link fi.forger.desktopsynclibrary.SyncService} has received a short id from the server.
     * The same id can be sent multiple times.
     */
    public void updateExperimentInfoToUi();
    
    /**
     * 
     * Called when server wants to start recordings.
     * 
     * @param fileBaseName the start of the file name that should be used so that the meta file matched with your data files
     * @param recordingNumber number of the recording that is same with all other apps started at the same time by the server
     * @param metaOutputStream Stream where extra meta information can be printed. Close this when you are done with it!
     * @return true if the start of logging was successful
     */
    public Boolean startRecording(String fileBaseName, String recordingNumber, PrintWriter metaOutputStream);
    
    /**
     * 
     * Called when server wants to stop recordings.
     * Remember to close your files, so that they can be downloaded by the server.
     * 
     * @return true if the stopping of logging was successful
     */
    public Boolean stopRecording();
    
    /**
     * 
     * Called when the {@link fi.forger.desktopsynclibrary.SyncService} enters a new state.
     * 
     * @param syncState matches a state defined in {@link fi.forger.desktopsynclibrary.SyncService}
     * @param oldState matches a state defined in {@link fi.forger.desktopsynclibrary.SyncService}
     * @param stateString a string the describes the current state e.g. "Recording"
     */
    public void newSyncState(int syncState, int oldState, String stateString);
    
    /**
     * 
     * Used by apps displaying stimuli to the users.
     * 
     * @param fileBaseName
     * @param stimuliName
     * @param desiredStartTime
     */
    public void startPreparingPlay(String fileBaseName, String stimuliName, long desiredStartTime);
    
    /**
     * Used by apps displaying stimuli to the users.
     */
    public void cancelStimuli();
    
    /**
     * @param message error message that should seen by an user e.g. a missing stimulus file
     */
    public void showExtraMessageOnScreen(String message);
    
    /**
     * 
     * Used by apps displaying stimuli to the users. 
     * 
     * @param setting the setting id string
     * @return the setting value to be sent to the server
     */
    public String getAppSetting(String setting);
    
    /**
     * 
     * Used by apps displaying stimuli to the users.
     * 
     * @param setting the setting id string
     * @param value the setting value to be used by the client
     */
    public void setAppSetting(String setting, String value);
    
    /**
     * This is called when the user has clicked "Close apps" button at the FSenSync Server.
     */
    public void closeApp();
    
}
