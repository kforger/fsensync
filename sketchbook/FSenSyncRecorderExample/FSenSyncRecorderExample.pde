/**
 * This is an example of a full integration with the FSenSync Server.
 *
 * In this example, the mouse moves are timestamped and logged to a file.
 *
 * In addition to clock sync, this example lets the server start/stop
 * recordings, and allows the server to download the files. The files are
 * saved in a format that allows the server to correct the clock drift
 * between the server and the sketch. This usually results in a timing error
 * less than 2 milliseconds. The accuracy can be limited by the rate your
 * operating system gives the mouse data.
 *
 * Klaus Förger, klaus@forger.fi
 * Get the FSenSync from: http://www.forger.fi/fsensync/
 */
 
 import fi.forger.desktopsynclibrary.*;


MyRecorder rec;
String mouseText = "";

void setup() {
  size(400, 200, P3D);
  frameRate(60);

  rec = new MyRecorder();
}

void draw() {
  background(0);
  colorMode(HSB, 100);
  strokeWeight(2);
  textSize(32);

  if (rec.syncServ.getAppState() >= SyncService.STATE_SYNCED_AND_READY)
  {
    long t = (System.nanoTime() + rec.syncServ.getOffsetNanosToServerStreaming()) / SyncService.NANOS_IN_MILLI;
    textSize(32);
    text("" + t, 80, 100);
  }
  else
  {
    text("Not yet ready", 80, 100);
  }

  textSize(12);
  text(mouseText, mouseX, mouseY);
}

void mouseMoved()
{
  if (rec.syncServ.getAppState() == SyncService.STATE_RECORDING_DATA)
  {
    mouseText = "Recording X and Y";
    rec.saveMouseXY(mouseX, mouseY);
  }
  else
  {
    mouseText = "Not recording";
  }
}