
class EdaSample {
  
  float x;
  float y;
  float z;
  int time;
  int id;
  int skin;
  int mm;
  
  EdaSample(float x , float y, float z, int id, int time, int skin, int mm)
  {
    this.x = x;
    this.y = y;
    this.z = z;
    this.id = id;
    this.time = time;
    this.skin = skin;
    this.mm = mm;
  }
  
}